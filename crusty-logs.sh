#!/bin/bash

#USAGE:
# crusty-logs [OPTIONS] [FILES] ...
# Runs beagle on each of the files as given
# Options:
# -d[NAME] Destination for summary and output files is NAME-summary, NAME-output
#   defaults to a numeric representation of the date.
# -m[STRING] An optional message describing the run, currently unused.
# -o[OPTION_STRING] Passed as options to beagle. Enclose in quotes.
# -t[NUM] Timeout in seconds. Enforced by CPULimitedRun.

#Prover path
PROVER='./beagle'
CPU_LR_SCRIPT='./tools/CPULimitedRun'

#TPTP path: already accepts env var TPTP

#Declare strings
#These are regexs used by sed to extract the rating and status of problem files
SMT_LIB_STATUS='(set-info :status \([a-z]*\))'
TPTP_STATUS='% Status[ ]*: \(.*\)'
TPTP_RATING='% Rating[ ]*: \([0-9.]*\) v.*'

#Map the library vocabularies to those used by RunSystem
#result is theorem | non_theorem | no_soln | error
#I use error instead of memory
declare -A STATUS_MAP
#SMT-lib
STATUS_MAP[unsat]='theorem'
STATUS_MAP[sat]='non_theorem'

#TPTP
STATUS_MAP[Theorem]='theorem'
STATUS_MAP[Unsatisfiable]='theorem'
STATUS_MAP[CounterSatisfiable]='non_theorem'
STATUS_MAP[Satisfiable]='non_theorem'
#STATUS_MAP[Nontheorem]='non_theorem'

#Beagle
#Theorem, Unsatisfiable already mapped above
STATUS_MAP[Unknown]='no_soln'
STATUS_MAP[Timeout]='timeout'
STATUS_MAP[error]='error'


#this folder used for output of trace files
#note that it will be deleted after the run
OUT_DIR="./tmp_crusty"
if [ ! -d "$OUT_DIR" ]
then
    mkdir $OUT_DIR
fi

if [ ! $TPTP ]
then
    TPTP="$HOME/TPTP-v6.1.0"
fi

#################################################
# Parsed options and defaults
#################################################
#Default prefix for Summary output (shell & file)
FILE_PREFIX="$(date +%H%M_%d%m%y)"

#Default limit in seconds- used by CPULimitedRun
TIME_LIM=120

PROVER_OPTS=''

#parse options
while getopts "o:t:m:d:" optname
  do
    case "$optname" in
      "o")
        PROVER_OPTS=$OPTARG
        ;;
      "t")
        TIME_LIM=$OPTARG
        ;;
      "m")
	MSG=$OPTARG
	MSG_SET=1
	;;
      "d")
        FILE_PREFIX=$OPTARG
        ;;
      "?")
        echo "Unknown option $OPTARG"
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        ;;
      *)
      # Should not occur
        echo "Unknown error while processing options"
        exit
        ;;
    esac
    
  done

shift $((OPTIND - 1))

SUMMARY_FILE="$FILE_PREFIX-summary"
OUTPUT_FILE="$FILE_PREFIX-output"
rm -f $SUMMARY_FILE
rm -f $OUTPUT_FILE

PROVER_CMD="$PROVER $PROVER_OPTS -q"

#Print to file and to std_out
function echoTee {
  echo -e $1
  echo -e $1 >> $SUMMARY_FILE
}

#################################################
# Body
#################################################

echoTee "------------------------------------------------"
echoTee "It's CRUSTY-LOGS v0.002!"
echoTee "$(date)"
echoTee "Git HEAD\t\t: "`git log -1 --format="%h"`
echoTee "Timeout (s)\t\t: $TIME_LIM"
echoTee "Execute format string\t: $PROVER_CMD"
echoTee "\nName\t\t\tRating\tExpect\tResult\tTime"
echoTee "------------------------------------------------"

echo -e \
"------------------------------------------------
$(date)
Git HEAD\t\t: `git log -1 --format="%h"`
Timeout (s)\t\t: $TIME_LIM" >> $OUTPUT_FILE

#statistics
solved=0
error=0
no_soln=0
timeout=0
unsound=0

#array counter
i=0
for prob in $*
do
    TMP_OUT="$OUT_DIR/`basename $prob`.tmp"

    #configure the regex based on which type of file: .smt2 or .p
    if [ `expr match "$prob" .*\.smt2` -ne 0 ]; then
      #smt
      status_reg=$SMT_LIB_STATUS
      rating=0.0
    else
      #tptp
      status_reg=$TPTP_STATUS
      rating=`cat $prob | sed "/$TPTP_RATING/ ! d;s/$TPTP_RATING/\1/"`
    fi

    expected=`cat $prob | sed "/$status_reg/ ! d;s/$status_reg/\1/"`
    #expected=${STATUS_MAP[$expected]}
    
    #default value if not found:
    [ -z "$expected" ] && expected="?" || expected=${STATUS_MAP[$expected]}
    [ -z "$rating" ] && rating="?"

    #two ways of timing: /usr/bin/time and CPULimitedRun suffix
    #err=`/usr/bin/time -f$TIME_F -a -o$TMP_OUT $CPU_LR_SCRIPT $TIME_LIM $PROVER_CMD $prob 2>&1 >$TMP_OUT`
    #For /usr/bin/time:
    #time=`grep "TIME" $TMP_OUT | cut -d'=' -f2`
	    
    `env TPTP=$TPTP $CPU_LR_SCRIPT $TIME_LIM $PROVER_CMD $prob &> $TMP_OUT`
    pid=$!

    if wait $pid 2>/dev/null; then
	# expect format 'SZS status X for FILE
	status=`grep "SZS" $TMP_OUT | cut -d' ' -f4`

    	if [ -n "$status" ]; then
	    status="${STATUS_MAP[$status]}"
	    #either both theorem, both non_theorem
	    if [ "$status" == "$expected" ]; then
		solved=$(($solved + 1))
	    elif [ "$status" == "no_soln" ]; then
		no_soln=$(($no_soln + 1))
	    else
		unsound=$(($unsound + 1))
	    fi
	elif [ -n "`grep 'time limit exceeded' $TMP_OUT`" ]; then
    	    status='timeout'
	    timeout=$(($timeout + 1))
    	else
	    # Errors may not be reported in trace
	    # status=`grep "SZS" $TMP_OUT | cut -d' ' -f3`
	    status="error"
	    error=$(($error + 1))
    	fi
    fi
    time=`cat $TMP_OUT | sed "/The child used.*/ ! d;s/The child used \([0-9]*\)s \([0-9]\{2\}\)[0-9]*ms/\1.\2/"`

    echoTee "$prob\t$rating\t$expected\t$status\t$time"
    #for the output listing
    echo -e \
"------------------------------------------------
Problem: $prob
Expected: $expected
Command: $PROVER_CMD
------------------------------------------------\n" >> $OUTPUT_FILE

    cat $TMP_OUT >> $OUTPUT_FILE
    rm $TMP_OUT

    i=$(($i + 1))
	
done
rmdir $OUT_DIR

echoTee "================================================"
echoTee "Solved\t: $solved"
echoTee "Error\t: $error"
echoTee "No Soln\t: $no_soln"
echoTee "Timeout\t: $timeout"
echoTee "Unsound\t: $unsound"
echoTee "------------------------------------------------"
echo "Note that Beagle reports UNKNOWN for timeouts, so No Solution \
also counts timeouts when the -t flag is passed to beagle and less than the global -t flag"

echo "Summary written to $SUMMARY_FILE"
echo "Traces written to $OUTPUT_FILE"
echo ""
echo -e "Enjoy the debugging :)"

