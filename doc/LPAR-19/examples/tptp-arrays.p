%Just the definitions and conjectures necessary for tests in LPAR paper
%$ read > write > init
tff(array_type,type,(
    array: $tType )).

tff(read_type,type,(
    read: ( array * $int ) > $int )).

tff(write_type,type,(
    write: ( array * $int * $int ) > array )).

tff(ax1,axiom,(
     ! [A: array,I: $int, V: $int] : read(write(A,I,V),I) = V )).

tff(ax2,axiom,(
    ! [A: array,I: $int,J: $int,V: $int] :
      ( I = J
      | read(write(A,I,V),J) = read(A,J) ) )).

tff(ext, axiom, (
	 ![A: array, B: array]: 
	      ( (![I: $int]: read(A,I)=read(B,I) ) 
	      	=> A=B )
)).

tff(init_type, type, init: $int > array).

tff(ax3, axiom, 
   ! [V: $int] : 
     ( ! [I: $int] : read(init(V), I) = V )).

%%%%% Max

tff(max, type, max: (array * $int) > $int).
tff(a, axiom, (
       ! [A: array, N: $int, W: $int] : 
         ( max(A,N) = W 
       	 <= (( ! [I: $int] : 
	       	 (( $greater(N,I) &
		    $greatereq(I,0) )
                 => $lesseq(read(A, I), W) )) &
             ( ? [I: $int] : 
	       	 ( $greater(N,I) &
		   $greatereq(I,0) &
                   read(A, I) = W ))
	     )))).

% %%Proof of functionality of prefix max function
% % BEAGLE - Theorem
% % SPASS+T - Theorem
% tff(c, conjecture, 
%    ! [A: array, N: $int, W1: $int, W2: $int] : 
%      ( ( (   ( ! [I: $int] : 
% 	       	 (( $greater(N,I) &
% 		    $greatereq(I,0) )
%                  => $lesseq(read(A, I), W1) )) &
%              ( ? [I: $int] : 
% 	       	 ( $greater(N,I) &
% 		   $greatereq(I,0) &
%                    read(A, I) = W1 ))
% 	     )
%        & ( ( ! [I: $int] : 
% 	       	 (( $greater(N,I) &
% 		    $greatereq(I,0) )
%                  => $lesseq(read(A, I), W2) )) &
%              ( ? [I: $int] : 
% 	       	 ( $greater(N,I) &
% 		   $greatereq(I,0) &
%                    read(A, I) = W2 ))
% 	     ) )
%     => W1 = W2 )).

% %%%%% Sorted
% %Expresses that the first N elements are sorted
% %$ sorted > rev

tff(sorted_type,type,( sorted: ( array * $int ) > $o )).

tff(sorted1, axiom, (
    ! [A: array, N: $int] :
      ( sorted(A, N)
    <=> ( ! [I: $int, J: $int] :
          ( ( $lesseq(0, I) 
            & $less(I, N) 
	    & $less(I, J)
	    & $less(J, N) )
        => $lesseq(read(A, I), read(A, J)) ))))).

% %%%%% inRange

tff(inRange, type, inRange: (array * $int * $int) > $o).
tff(inRange, axiom, (
   ! [ A: array, R: $int, N: $int ] :
     ( inRange(A,R,N) 
   <=> (
       ![I: $int]: (
       	    ($greater(N,I) & 
	    $greatereq(I,0)) => 
       	    	   ($greatereq(R,read(A,I)) & 
		    $greatereq(read(A,I),0)) 
	)
     )
))).

% %%%%% Distinct
% %$ distinct > write
% %$ distinct > read

tff(distinct, type, distinct: (array * $int) > $o).
tff(distinct, axiom, (
   ! [ A: array, N: $int ] :
     ( distinct(A,N) 
   <=> (![I: $int, J: $int]: (
         ($greater(N,I) &
	    $greater(N,J) &
	    $greatereq(J,0) & 
	    $greatereq(I,0)) 
	    => (read(A,I)=read(A,J) => I=J) 
	)
     )
))).

% %%%%% Reverse
%$rev > write
%$rev > read

tff(rev_n, type, rev: (array * $int) > array ).

tff(rev_n1_proper, axiom, ![A: array, B: array, N: $int]: (
	    rev(A,N)=B <= (
	    	       	  ( ![I: $int]: ((
			    ( $greatereq(I,0) & 
			      $greater(N,I)) & 
			      read(B,I)=read(A,$difference(N,$sum(I,1)))) |
			    ( ($greater(0,I) | 
			       $greatereq(I,N) )
			       & read(B,I)=read(A,I) ))
	    )))).

%Functionality proof
% Beagle - ?
% SPASS+T - Theorem (doesn't finish when sorted definition is included, all others fine)
% tff(c, conjecture, ![A: array, B1: array, B2: array, N: $int]: (
%        		 (( ![I: $int]: ((
% 		    ( $greatereq(I,0) & 
% 		      $greater(N,I)) & 
% 		      read(B1,I)=read(A,$difference(N,$sum(I,1)))) |
% 		    ( ($greater(0,I) | 
% 		       $greatereq(I,N) )
% 		       & read(B1,I)=read(A,I) ))
% 	    	    ) &
% 		    ( ![I: $int]: ((
% 			    ( $greatereq(I,0) & 
% 			      $greater(N,I)) & 
% 			      read(B2,I)=read(A,$difference(N,$sum(I,1)))) |
% 			    ( ($greater(0,I) | 
% 			       $greatereq(I,N) )
% 			       & read(B2,I)=read(A,I) ))
% 	    	   ) )
% 		   => B1 = B2 )
% ).

%%EXAMPLES:
%1
%tff(c1, conjecture, ~![A: array, N: $int]: ( $greatereq(N,0) => inRange(A,max(A,N),N) )).

%2
%tff(c2, conjecture, ~( ![N: $int, I: $int]: distinct(init(N),I) )).

%3- (false for N<0 where read(A,N)!=read(A,0)...)
%NOTE: SPASS only terminates if definition for inRange is excluded!
%tff(c3, conjecture, ~(![A: array, N: $int]: read(rev(A,$sum(N,1)),0)=read(A,N)) ).

%4
%tff(c4, conjecture, ~![A: array, N: $int]: (sorted(A,N) => ~sorted(rev(A,N),N))).

%5
%tff(c5, conjecture, ~ ![M: $int]: (?[N: $int]: ~sorted(rev(init(N),M),M))).

%6
% tff(c6, conjecture, ~![A: array, N: $int]: ( (sorted(A,N) & $greater(N,0)) => distinct(A,N)) ).
