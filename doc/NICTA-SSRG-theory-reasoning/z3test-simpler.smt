(set-option :pull-nested-quantifiers true)
(set-option :mbqi true)
(set-option :macro-finder true)
(set-option :produce-models true) ; enable model generation
(set-option :produce-proofs true) ; enable proof generation

(declare-sort I 0)
(declare-fun p (Int I) Bool)
(declare-fun q (Int I) Bool)

(assert (forall ((x I)) (p 0 x) ))
;; (assert (forall ((i Int) (x I)) 
;; 	(implies (p i x) (p (+ i 1) x))))
(assert (forall ((i Int) (j Int) (x I)) 
	(implies (and (p i x) (not (= i j))) (p (+ j 1) x))))
(assert (forall ((x I)) (not (p 3 x))))
(check-sat)

;; (declare-fun p (Int) Bool)

;; (assert (p 0))
;; (assert (forall ((i Int)) 
;; 		(implies (p i) (p (+ i 1)))))
;; (assert (not (p 3)))
;; (check-sat)

