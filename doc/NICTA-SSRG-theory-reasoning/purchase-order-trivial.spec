// modelcheck-sym purchase-order-trivial.spec "X inRange(db.order, db.nrStockItems)"   (*)

TYPES

DB = { 
  order: List[Integer],
  nrStockItems: Integer
}

SIGNATURE

inRange: [List[Integer], Integer] -> Bool

DEFINITIONS

inRange: ∀ l: List[Integer] . ∀ n: Integer . 
    ( inRange(l, n) 
    ⇔ ( l= [| |] 
       ∨ (0 <= head(l) ∧ head(l) < n ∧ inRange(tail(l), n))))

CONSTRAINTS

initDB: db.nrStockItems >= 0 ∧ 
        ¬(db.order = [| |]) ∧
        inRange(db.order, db.nrStockItems)

DIGRAPH

"start" [ init = "true" ]
"stop" 

"start" -> "stop" [ script = "db.order = tail(db.order)" ]
