% Declare some precedences:
%$ max > in > swap > inRange > tail > head > cons > nil

tff(list_type, type, ( list: $tType )).
tff(nil_type,  type, ( nil: list )).
tff(cons_type, type, ( cons: ($int * list) > list )).
tff(head_type, type, ( head: list > $int )).
tff(tail_type, type, ( tail: list > list )).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% List axioms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Selectors:
tff(l1, axiom, ( ! [ K: $int, L: list ] : head(cons(K, L)) = K )).
tff(l2, axiom, ( ! [ K: $int, L: list ] : tail(cons(K, L)) = L )).
%% %% %% Constructors:
tff(l3, axiom, ( ! [ L: list ] : 
   ( L = nil
   | L = cons(head(L), tail(L)) ))).
tff(l4, axiom, ( ! [ K: $int, L: list ] : cons(K, L) != nil)).

%% %% Sufficient completeness:
%% tff(l1, axiom, ? [E: $int] : head(nil) = E).
%% tff(l1, axiom, tail(nil) = nil).


%%%%%%%%%%%%%%%%%%%%%%%
%% inRange
%%%%%%%%%%%%%%%%%%%%%%%
tff(inRange, type, inRange: ($int * list) > $o).
tff(inRange, axiom, (
   ! [ N: $int, L: list ] :
     ( inRange(N, L) 
  <=>  ( L = nil 
       | ( $lesseq(0, head(L)) 
	 & $less(head(L), N) 
	 & inRange(N, tail(L)) ))))).

%% Provable
tff(c, conjecture, inRange(4, cons(1, cons(3, cons(2, nil))))).
%% tff(c, conjecture, 
%%     ! [N: $int] : 
%%       ( $greatereq(N, 4) => inRange(N, cons(1, cons(3, cons(2, nil)))))).

%% tff(c, conjecture, (
%%    ! [L: list, N: $int] :
%%      ( L != nil 
%%     => ( inRange(N, L)
%%       => inRange(N, cons(head(L), L)))))).


