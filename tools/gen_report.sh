#!/bin/bash

problems=$1
summary=$2

SMT_DIFF="../smtlib-difficulties-2014.txt"

echo "SMT Logic `cat $problems | sed 's/.*non-incremental\/\([^\/]*\).*/\1/' | sort -u | tr '\n' ', '`
===================================================
Solver arguments:"`grep 'Execute format string' $summary | sed 's/[^:]*: .\/beagle \(.*\)/\1/'`
echo ""

#produces thm, error, no_soln, timeout
grep 'theorem\stheorem' $summary | cut -f1 > thm
grep 'error' $summary | cut -f1 > error
grep 'no_soln' $summary | cut -f1 > none
grep 'timeout' $summary | cut -f1 > timeout

echo -e \
"Solved		: `wc -l thm | cut -d' ' -f1`
Error		: `wc -l error | cut -d' ' -f1`
No Solution	: `wc -l none | cut -d' ' -f1`
Timeout		: `wc -l timeout | cut -d' ' -f1`
Total		: `grep -c 'theorem' $summary`"
echo ""

echo \
"By Category
============="

cat $1 | sed 's/.*non-incremental\/\(.*\)\/[^\/]*\/[^\/]*.smt2/\1/' > sub_cats
sort -u sub_cats > tmp
mv tmp sub_cats

rm -f results_by_sub.out
echo -e "Category\tSolved\tUnsolved\tError\tTimeout"
for c in `cat sub_cats`; do
echo -e \
"$c\t`grep -c /$c/ thm`\
\t`grep -c /$c/ none`\
\t`grep -c /$c/ error`\
\t`grep -c /$c/ timeout`"
done
#rm sub_cats

echo ""

#Theorems: ID Name in thm_id_name
cat thm | cut -f1 -d' ' | sed 's/.*non-incremental.*\/\([^\/]*\)\/\([^\/]*\).smt2/\1\t\2/' | sort > thm_id_name

#thm_id_name joined with difficulties table
sort $SMT_DIFF | join thm_id_name - | tr ' ' '\t' > thm_diff_table

echo \
"Origin		Solved
----------------------"
cut -f6 thm_diff_table > tmp_find_file

for c in `sort -u tmp_find_file`
do
  echo -e "$c\t`grep -c $c tmp_find_file`"
done
rm tmp_find_file

echo "
By Difficulty
=============

Solved by Beagle
----------------"
#make difficulty list by join...

#join - master
cut -f1,2,3,8 thm_diff_table | sed 's/[^\/]*.smt2\s//' > thm-diffs1
grep 'theorem\stheorem' $2 | cut -f1,5 | sed 's/.*non-incremental.*\/\([^\/]*\)\/[^\/]*.smt2\s\(.*\)/\1\t\2/' | sort > thm_id_time

#cut and paste
cut -f2 thm_id_time | paste thm-diffs1 - | sed 's/trivial/0.0/'> thm-diffs
rm thm-diffs1 thm_id_time thm_id_name

unsolved=0
better=0
worse=0

rm -f hardest
while read l; do
  #ID Name Diff Cat Time
  diff=`echo $l | cut -f3 -d' '`
  beagle=`echo $l | cut -f5 -d' '`
  td=`perl -E "say $diff-$beagle"`

  if [ `perl -E "say $diff >= 1600"` ]; then
      ((unsolved++))
      echo $l >> hardest
  fi

  if [ `perl -E "say $td > 10"` ]; then
      ((better++))
  elif [ `perl -E "say $td < -10"` ]; then
      ((worse++))
  fi
done <thm-diffs

echo "Unsolved by SMT	: $unsolved
Performed better: $better
Performed worse	: $worse

*Better/Worse if difference > 10s.
"

#grep 1600 thm-diffs | cut -f4 > hardest

echo -e "Hardest solved by Cat:
Cat\t\tSolved
------------------------"
#breakdown by category
for c in `cat sub_cats`; do
if [ `grep -c $c hardest` -ne 0 ]; then
    echo -e "$c\t`grep -c $c hardest`"
fi
done
rm hardest

echo "
Unsolved by Beagle
------------------"
#make difficulty list by join...
#ID Name
cat none | cut -f1 | sed 's/.*non-incremental.*\/\([^\/]*\)\/\([^\/]*\).smt2/\1\t\2/' | sort > none_id_name
#join - master
sort $SMT_DIFF | join none_id_name - | tr ' ' '\t' | cut -f1,2,3,8 | sed 's/[^\/]*.smt2\s//' > none-diffs

rm none_id_name

unsolved=0
fast=0
triv=`grep -c trivial none-diffs`
cat none-diffs | sed 's/trivial/0.0/' > none-diffs1

while read l; do
  #ID Name Diff Cat Time
  diff=`echo $l | cut -f3 -d' '`

  if [ `perl -E "say $diff >= 1600"` ]; then
      ((unsolved++))
  fi

  if [ `perl -E "say $diff < 10"` ]; then
      ((fast++))
  fi
done <none-diffs1

total=`wc -l none-diffs1 | cut -f1 -d' '`

echo "Solved by SMT	: $(($total - $unsolved))
Trivial	  	: $triv
Faster than 10s : $fast
"
rm none-diffs1

grep trivial none-diffs | cut -f4 > easiest

echo -e "Easiest unsolved by Cat:
Cat\t\tTrivial unsolved
------------------------"
#breakdown by category
for c in `cat sub_cats`; do
if [ `grep -c $c easiest` -ne 0 ]; then
    echo -e "$c\t`grep -c $c easiest`"
fi
done
rm easiest
