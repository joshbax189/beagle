import beagle._
import fol._
import term._
import bgtheory._
import cnfconversion._
import datastructures._
import parser._

/** A script to check for (possible) cardinality constraint clauses in TPTP files */
object main {

  /**
   * Return true if there is an FCC for at least one sort.
   */
  def hasFCC(sig: Signature, cs: List[Clause]): Boolean = {
    //generate an exemplar cardinality constraint for the domain
    //this is just X=c1 | X=c2 | X = ... this is matched to clauses to find the FCC
    def exemplarFCC(consts: List[Term]) = {
      val x = GenVar("X",consts.head.sort)
      Clause(consts.map(Eqn(x,_).toLit)).fresh()
    }
    
    //cs foreach { println _ }

    if(!cs.isEmpty) {
    for(sort <- sig.sorts;
        if(sort!=Signature.OSort && sort!=Signature.TSort && sort.kind!=BG)) {
      val constants = sig.operators.filter(op => op.sort()==sort && op.arity.nrArgs < 1)
      
      //println("Testing sort "+sort+" with domain "+constants)
      
      if(constants.isEmpty) ()
        //println("Not applicable")
      else {
	      val fcc = exemplarFCC(constants.map(SymConst(_)).toList)
	      
	      //if(cs.exists(c => c.subsumes(fcc))) return true
	      for(c <- cs.find(c => c.subsumes(fcc) || fcc.subsumes(c));
	      		if( c.lits.forall( _.eqn.rhs.isSymConst ) )) {
	      	println("\tDomain: "+constants)
	      	println("\tFCC: "+c)
	      	return true
	      }
      }
    }
    }
    return false
  }

	def main(args: Array[String]) {
		//check for FCC, if one exists print name and exit
		
		val name = args(0)
		//println(name)
		util.flags.tptpHome.setValue("/home/jbax/TPTP-v6.1.0")
		
		//if parser error or anything else, just exit
		try {
			val inSig = LFA.addStandardOperators(LRA.addStandardOperators(LIA.addStandardOperators(Signature.signatureEmpty)))
			//println("l1")
			//parse- this also affects the global signature?
			val (formulas, sig, _, _) = TPTPParser.parseTPTPFile(name, inSig, genvars=false)
			//println("l2")
			fol.Sigma = sig
			bgtheory.setSolver("cooper-clauses")
			
			//cnf transform
			val input: List[Clause] = 
				formulas.flatMap( toCNF(_,true))
					.flatMap(literalListToClauses(_,true))
					.flatMap(_.simplifyCheap)
			//println("l3")
			//need to use Sigma here
			if(hasFCC(fol.Sigma, input)) println(name+"\n--------------------")
			//else println("No FCC found")
			
			System.exit(0)
		} catch {
			case e: util.SyntaxError => System.exit(0)
			case e: bgtheory.NonLinearTermFail => System.exit(0)
			case e: Throwable => { println(s"$name: $e"); System.exit(0) }
		}
	}
	
}
