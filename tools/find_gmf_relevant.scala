import beagle.API._, beagle._, fol._

util.flags.arraysFlag.setValue("true")

//def main(args: Array[String]): Unit = {
for(file <- args) {

  val (fs, fc) = parseSMT(file)
  //val (fs, fc) = parseTPTP(args(0))
  //are there BSFG terms?
  //how many
  val numInSig = fc.sig.fgOperators.count(op => op.isBSFGOp && !op.isInstanceOf[DefinedOp] && !op.isInstanceOf[NLPPOp])

  var numOccur = 0
  var allGMF = true
  for(f <- fs) {
    //for now only apply to formulas, skip CNF
    //number of occurrences
    numOccur += f.atoms.foldLeft(0)((acc,atom) => acc + atom.args.flatMap(_.subterms).count(_.isBSFG))

    //every occurrence is ground or has only Int sorted vars free
    allGMF &= f.atoms.flatMap(_.args.flatMap(_.subterms.filter(_.isBSFG))).forall(_.vars.forall(_.isBG))
  }
  
  println(s"$file\t$numInSig\t$numOccur\t$allGMF")

}
