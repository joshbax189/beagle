import beagle.API._
import beagle._
import fol._
import term._
import util._
import datastructures._
import calculus.ByInput
import bgtheory.NLPPOp

import scala.concurrent._
import scala.concurrent.duration._
import ExecutionContext.Implicits.global

/** Combines all 'find_X' scripts which test for syntactic structures in input files
  * args: timeout filenames */
object main {

  def readArgs(args: Array[String]) = {
    /*(args(0) match {
      case "ffs" => ffSorts
      //case "gmf" => gmfRel
      case "fcc" => hasFCC
      //case "epr" => epr
      //case "ncs" => ncs
      case _ => {
        println("Unrecognised check")
        System.exit(0)
        throw new Exception("never thrown")
      }
    },*/
    (args.head.toInt,
    args.tail)
  }

  //timeout value
  val to = 20.seconds

  var errors = 0
  var timeouts = 0
  var parsed = 0

  /** Also filter DefinedOp and NLPPOp from BSFG operators. */
  def getBSFGOps(sig: Signature) =
    sig.fgOperators filter { op =>
      op.isBSFGOp && !op.isInstanceOf[DefinedOp] && !op.isInstanceOf[NLPPOp]
    }

  def gmfIdxInteresting(sig: Signature, cls: List[ConsClause], filename: String): Unit = {

    if (getBSFGOps(sig).isEmpty) return ()

    val interestingSorts: Set[Type] = Set(bgtheory.LIA.IntSort) ++ sig.fgSorts.filter( s => 
      isFCC(s, sig, cls) /*||
        isFFS(s, _) ||
        isNCS(s,_)*/
    )
    
    //only S or Int vars appear under BSFG operators
    if (cls.maxBSFGTerms.forall(_.vars.flatMap(_.sorts) subsetOf interestingSorts))
      println("GMF-IDX: "+filename)
  }

  /* Find GMF */
  //could do before cnf conversion
  def gmf(sig: Signature, fs: Traversable[Formula], filename: String): Unit = {
    //how many BSFG operators are in the signature?
    if (getBSFGOps(sig).isEmpty) return ()

      /*
       var numOccur = 0
       var allGMF = true
       for(f <- fs) {
       //for now only apply to formulas, skip CNF
       //number of occurrences
       numOccur += f.atoms.foldLeft(0)((acc,atom) => acc + atom.args.flatMap(_.subterms).count(_.isBSFG))

       //every occurrence is ground or has only Int sorted vars free
       allGMF &= f.atoms.flatMap(_.args.flatMap(_.subterms.filter(_.isBSFG))).forall(_.vars.forall(_.isBG))
       }*/

      //find a BSFG term instance that is not usable
      val res =
        fs find { f =>
          f.atoms flatMap {
            _.args.flatMap(_.subterms.filter(_.isBSFG))
          } exists {
            _.vars exists { !_.isBG }
          }
        }

      if (res.isEmpty) {
        //gmfRel += 1
        println("GMF: "+filename)
      }

  }

  /* Find FCC */
  def isFCC(sort: Type, sig: Signature, cs: List[ConsClause]): Boolean = {
    //generate an exemplar cardinality constraint for the domain
    //this is just X=c1 | X=c2 | X = ... this is matched to clauses to find the FCC
    def exemplarFCC(consts: List[Term]) = {
      val x = GenVar("X", consts.head.sort)
      Clause(consts.map(Eqn(x,_).toLit), ClsInfo(Set(), 0, ByInput, 0)).fresh()
    }
    
    if (sort != Signature.OSort &&
      sort != Signature.TSort &&
      sort.thyKind != BG) {

      val constants = sig.operators.filter(op => op.sort() == sort && op.arity.nrArgs < 1)
      //println(s"Tested $sort with $constants")

      if (!constants.isEmpty) {
	val fcc = exemplarFCC(constants.map(SymConst(_)).toList)

	cs exists { c =>
          c.subsumes(fcc) &&
          c.lits.forall( l => l.eqn.symConsts.size == 1 && l.eqn.vars.size == 1)
        }
      } else 
        false
    } else
      false
  }

  /** If there is an FCC for at least one sort */
  def hasFCC(sig: Signature, cs: List[ConsClause], filename: String): Unit = 
    for (sort <- sig.sorts) {
      if (isFCC(sort, sig, cs)) {
        println("FCC: "+filename)
        return ()
      }
    }

  /* Find EPR */

  /** An EPR problem has no non-defined operators */
  def isEPR(sig: Signature) =
    sig.operators.forall(op => op.defined ||
      op.kind==BG ||
      op.sort()==Signature.OSort ||
      op.arity.nrArgs==0 ||
      op.name=="#nlpp")

  def epr(sig: Signature, cls: List[ConsClause], filename: String): Unit = {
    if (isEPR(sig)) println(s"EPR: $filename")
  }

  /* Find Function free sorts */
  def ffSorts(sig: Signature, cls: List[ConsClause], filename: String): Unit = {
    //op count counts the # of operators into a sort.
    val emptyOpCount: Map[Type,Int] = sig.sorts.map((_,0)).toMap
    val opCount: Map[Type,Int] =
      sig.operators.foldLeft(emptyOpCount)({
	case (acc, Operator(_, _, arity)) if (arity.nrArgs > 0) =>
	  acc + (arity.resSort -> (acc(arity.resSort) + 1))
	case (acc,_) => acc
      })
    
    //print info
    //defined sorts don't count
    if (opCount.exists({case (s,c) => s.name(0)!='$' && c==0 })) {
      println(s"FFS: $filename")
    }
  }

  /* Find non-cyclic sorts */
  def nonCyclicSorts(sig: Signature, cls: List[ConsClause], filename: String): Unit = {
    //in the dependency graph there are no cycles
    //ignore BG sorts and OSort
    //keep a list of visited sorts
    //there is a cycle if there is a path from A to B and from B to A
    // A --> B 
  }

  def basicDefinitions(sig: Signature, cls: Traversable[ConsClause], filename: String): Unit = {
    //clause has the form f(x) = y v D[x,y]
    //basic if D doesn't contain f.
    //collect all candidates of that form and print

    //c has a positive literal f(x,y) = z
    //sth all variables below f are distinct
    //all variables appear in D
    //D does not contain f
    var found = Map[Operator, List[(Lit, List[Lit])]]()

    for { 
      c <- cls
      i <- c.iPosLits
      l = c(i)
    } {
      l.eqn match {
        case Eqn(PFunTerm(f, args), x : Var) 
            if (f.kind != BG && args.forall(_.isVar) && args.vars.size == args.length) => {
          val d = c.lits.patch(i, Nil, 1)
          val fVars: Set[Term] = (x :: args).toSet
          if (fVars.subsetOf(d.vars.asInstanceOf[Set[Term]]) && !d.operators(f)) {
            val existing = found.getOrElse(f, Nil)
            found += (f -> ((l,c.lits) :: existing))
          }
        }
        case Eqn(x : Var, PFunTerm(f, args))
            if (f.kind != BG && args.forall(_.isVar) && args.vars.size == args.length) => {
          val d = c.lits.patch(i, Nil, 1)
          val fVars: Set[Term] = (x :: args).toSet
          if (fVars.subsetOf(d.vars.asInstanceOf[Set[Term]]) && !d.operators(f)) {
            val existing = found.getOrElse(f, Nil)
            found += (f -> ((l,c.lits) :: existing))
          }
        }
        case PredEqn(PFunTerm(f, args)) if (f.kind != BG) => {
            val d = c.lits.patch(i, Nil, 1)
            if (args.vars.subsetOf(d.vars) && !d.operators(f)) {
              val existing = found.getOrElse(f, Nil)
              found += (f -> ((l, c.lits) :: existing))
            }
        }
        case Eqn(PFunTerm(f, args), t) //allow unit clause defs of function symbols
            if (f.kind != BG && c.isUnitClause && args.forall(_.isVar) && args.vars.size == args.length) => {
          if (t.vars.subsetOf(args.vars) && !t.operators(f)) {
            val existing = found.getOrElse(f, Nil)
            found += (f -> ((l, Nil) :: existing))
          }
        }
        case _ => () //skipped
      }

    }

    if (found.nonEmpty) {
      println(filename)
      println("-----------------")

      for { (sym, decls) <- found } {
        println(s"$sym -defined by:")
        for { (d, dCls) <- decls } println(s"$d in $dCls")
        println()
        cls.filter(_.operators(sym)).foreach(println(_))
        println()
      }
    }
  }

  def basicDefFormula(sig: Signature, fs: Traversable[Formula], filename: String): Unit = {
    // basic if it has the forms:
    // forall x. P(x) <=> D[x]
    // forall x. f(x) = t <= D[x]
    // s.t. P or f not in D, more generally no circular dependencies.
    // consider both orientations of <=

    object TermDef {
      def unapply(a: Formula) = a match {
        case Equation(PFunTerm(f, args), t) 
            if (!t.operators(f) && 
              args.forall(_.isVar) && 
              args.vars.size == args.length) =>
          Some((f, args, t))
        case Equation(t, PFunTerm(f, args))
            if (!t.operators(f) && 
              args.forall(_.isVar) && 
              args.vars.size == args.length) =>
          Some((f, args, t))
        case _ => None
      }
    }

    object PredDef {
      def unapply(a: Formula) = a match {
        case Atom(p, args) if (p != "=" && args.forall(_.isVar) && args.vars.size == args.length) =>
          Some((sig.findOperator(p).get, args))
        case _ => None
      }
    }

    // collect quantifier prefix- must be only univ.
    def stripUniQ(f: Formula): (List[Var], Formula) = {
      var working = f
      var prefix = List[Var]()
      while (Forall.unapply(working).nonEmpty) {
        val Forall(xs, g) = working
        prefix ++= xs
        working = g
      }
      (prefix, working)
    }

    var found = Map[Operator, List[Formula]]()

    for { f <- fs } {

      val (xs, g) = stripUniQ(f)

      g match {
        case Iff(PredDef(p, args), d) if (!d.operators(p) && p.kind != BG) => found += (p -> (f :: (found.getOrElse(p, Nil))))
        case Iff(d, PredDef(p, args)) if (!d.operators(p) && p.kind != BG) => found += (p -> (f :: (found.getOrElse(p, Nil))))
        case Implies(d, TermDef(h, args, t)) if (!d.operators(h) && h.kind != BG) => found += (h -> (f :: (found.getOrElse(h, Nil))))
        case Implied(TermDef(h, args, t), d) if (!d.operators(h) && h.kind != BG) => found += (h -> (f :: (found.getOrElse(h, Nil))))
        case TermDef(h, args, t) if (h.kind != BG) => found += (h -> (f :: (found.getOrElse(h, Nil))))
        case _ => ()
      }
    }
    //condition: every formula is either a definition or conjecture

    //definitions are correctly stratified:
    // 1. build dependency graph f: {g1, g2, g3}
    def buildDeps(ops: Map[Operator, List[Formula]]): Map[Operator, Set[Operator]] =
      ops.map({ case (op, fs) =>
        (op, 
          fs.flatMap(_.operators).toSet.filterNot(op2 => op2.defined || op2.kind == BG || op2 == op || op2.arity.nrArgs == 0))
      }).toMap

    // 2. build stratification- return list of operators sth each set is defined by the union of those before
    //assume that BG operators do not appear in dependencies
    def stratify(deps: Map[Operator, Set[Operator]]): List[Set[Operator]] = {
      println("Dependencies:\n"+deps)
      //init with any operators defined by nothing/BG only
      var defined: Set[Operator] = deps collect { case (op, dep) if (dep.isEmpty) => op } toSet
      //invariant: defined = union of strata
      var strata = defined :: Nil
      var remaining = deps.filterKeys(!defined(_))

      while (remaining.nonEmpty) {
        //push any completely defined operators onto strata and add to defined
        val (def2, rem2) = remaining partition { case (op, dep) => dep.subsetOf(defined) }

        if (rem2.nonEmpty && def2.isEmpty) //couldn't find any new definitions-- exit
          return Nil

        remaining = rem2
        defined ++= def2.keys
        strata ::= def2.keys.toSet
      }

      return strata
    }

    if (found.nonEmpty) {
      println(filename)
      println("-----------------")

      for { (sym, decls) <- found } {
        println(s"$sym -defined by:")
        decls.foreach(println(_))
        println()
      }

      //condition 1: every formula either a definition or a conjecture
      val nonBasic = fs.filterNot(f => f.role == "negated_conjecture" || found.values.exists(_.contains(f)))
      if (nonBasic.isEmpty)
        println("*** Every formula either definitional or conjecture")
      else
        println(s"*** ${nonBasic.size} formulas are neither definitional or conjecture")

      println()

      val strata = stratify(buildDeps(found))
      if (strata.nonEmpty)
        println("*** Definitions stratify:\n"+strata)
      else
        println("*** Definitions DO NOT stratify")

      println()
    }

  }

  /* Entry point */
  def main(args: Array[String]) {
    //init beagle flags and settings:
    flags.tptpHome.setValue("/home/jbax/TPTP-v6.1.0")
    flags.quietFlag.setValue("true")
    beagle.util.reporter = QuietReporter
    bgtheory.setSolver("cooper")

    val (to, files) = readArgs(args)

    println(s"** Timeout=${to}s")

    val timeout = to.seconds

    for (filename <- files) {

      try {
        /* Parse a file, dynamically testing if SMT or TPTP */
        val (fs, fc) = filename.split('.').last match {
          case "smt2" => {
            //smt specific settings
            flags.arraysFlag.setValue("true")
            flags.cnfConversion.setValue("optimized")

            parseSMT(filename)
          }
          case "p" => {
            //tptp specific settings
            parseTPTP(filename)
          }
        }

        parsed += 1

        Sigma = fc.sig
        
        try {
          //test that doesn't need CNF conversion
          // Await.result(
          //   Future { gmf(fc.sig, fs, filename) },
          //   timeout)
          Await.result(
            Future { basicDefFormula(fc.sig, fs, filename) },
            timeout)

          //begin CNF conversion -- how to time this?
          //maybe launch in a separate thread and kill if it takes too long?
          // val cls = Await.result(
          //             Future { cnfconversion.formulasToClauses(fs, false, haveLIA=true) },
          //             timeout)

          //test fs and signature, repeat for each input
          //normal behaviour
          //check(fol.Sigma, cls, filename)
          // Await.result(
          //   Future { hasFCC(Sigma, cls, filename) },
          //   timeout)
          // Await.result(
          //   Future { epr(Sigma, cls, filename) },
          //   timeout)
          // Await.result(
          //   Future { ffSorts(Sigma, cls, filename) },
          //   timeout)
          // Await.result(
          //   Future { gmfIdxInteresting(Sigma, cls, filename) },
          //   timeout)
          // Await.result(
          //   Future { basicDefinitions(Sigma, cls, filename) },
          //   timeout)

        } catch {
          case e: TimeoutException => timeouts += 1
        }
      } catch {
        case e: SyntaxError => () //skip
        case e: Throwable => {
          println(e)
          errors += 1
        }
      }
    }

    println(
      "-------------------------------\n"+
        s"Parsed files        : $parsed\n"+
        s"Errors              : $errors\n"+
        s"Timeouts            : $timeouts\n"
    )
  }
}
