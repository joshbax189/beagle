
(set-info :source |

C. L. Liu, Elements of Discrete Mathematics, 1985, describes the following problem:

   Suppose we have stamps of two different denominations, 3 cents and 5 cents. We want to show
   that it is possible to make up exactly any postage of 8 cents or more using stamps of these two
   denominations.

The formula below asserts this, however "8" replaced by "some lower bound".
|)

(set-info :status unsat)

(set-logic LIA)

(assert
 (not
  (exists ((l Int))
	  (forall ((k Int))
		  (=> (> k l)
		      (exists ((s1 Int) (s2 Int))
			      (and (>= s1 0)
				   (>= s2 0)
				   (= k (+ (* s1 3) (* s2 5))))))))))
		      

(check-sat)
(exit)
