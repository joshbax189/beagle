;; The SMT-LIB parser is taken from 
;; http://users.cecs.anu.edu.au/~baumgart/systems/smttotptp/
;; See the README file there for more information on supported options.
(set-option :declare-list-datatype true)

;; definition of append
(declare-fun append ((List Int) (List Int)) (List Int))
(assert (forall ((l (List Int))) 
		(= (append (as nil (List Int)) l) l)))
(assert (forall ((i Int) (k (List Int)) (l (List Int))) 
		(= (append (insert i k) l) (insert i (append k l)))))

;; definition of length
(declare-fun length ((List Int)) Int)
(assert (= (length (as nil (List Int))) 0))
(assert (forall ((h Int) (t (List Int))) (= (length (insert h t)) (+ 1 (length t)))))


;; this formula is unsatisfiable with the above:
(assert (forall ((k (List Int)) (l (List Int)))
		(= (length (append k l)) (length k))))

(check-sat)
