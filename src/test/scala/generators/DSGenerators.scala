package generators

import org.scalacheck._
import beagle._
import fol._
import term._
import datastructures._
import bgtheory.LIA._
import Signature.{ISort,OSort}
import finite._
import calculus.{ByInput, Inference}

/** Generators for datastructures Eqn, Lit and Clause */
object DSGenerators {
  import Gen._
  import Arbitrary.arbitrary

  import TermGenerators._

  /* ----Eqn---------------------------- */
  /** 25% of the time return a trivial Eqn l=l */
  def eqnGen(s: Sort): Gen[Eqn] = 
    for {
      l <- termGen(s)
      r <- Gen.frequency( (3, termGen(s)), (1, l) )
    } yield try {
      Eqn(l,r)
    } catch {
      case e:NoSuchElementException => Eqn(l,l) //does this avoid the precedence lookup?
    }

  def eqnGen(sig: Signature): Gen[Eqn] = 
    for {
      s <- sortGen
      l <- termGen(s,sig)
      r <- Gen.frequency( (3, termGen(s,sig)), (1, l) )
    } yield try {
      Eqn(l,r)
    } catch {
      case e:NoSuchElementException => Eqn(l,l) //does this avoid the precedence lookup?
    }

  val eqnGen: Gen[Eqn] = 
    for { s <- sortGen; e <- eqnGen(s) } yield e

  //TODO- add other bg predicates: Divides, IsInt ...
  val bgPredGen: Gen[Eqn] = 
    for {
      p <- oneOf(LessEqn.op,LessEqEqn.op,GreaterEqn.op,GreaterEqEqn.op)
      l <- termGen(IntSort)
      r <- Gen.frequency( (3, termGen(IntSort)), (1, l) )
    } yield PredEqn( PFunTerm(p, List(l,r)) )

  def bgPredGen(sig: Signature): Gen[Eqn] = 
    for {
      p <- oneOf(LessEqn.op,LessEqEqn.op,GreaterEqn.op,GreaterEqEqn.op)
      l <- termGen(IntSort,sig)
      r <- Gen.frequency( (3, termGen(IntSort,sig)), (1, l) )
    } yield PredEqn( PFunTerm(p, List(l,r)) )

  /** Makes an PFunTerm with sort OSort and wrap it in a PredEqn */
  val fgPredGen: Gen[Eqn] = 
    for { t <- funTermGen(OSort) } yield PredEqn(t)

  def fgPredGen(sig: Signature): Gen[Eqn] = 
    for { t <- funTermGen(OSort,sig) } yield PredEqn(t)

  implicit def arbEqn: Arbitrary[Eqn] = 
    Arbitrary( oneOf(eqnGen, 
		     bgPredGen, 
		     fgPredGen ))

  /* ----Lit------------------------------ */

  /** Generate a Lit from an Eqn between non-logical terms.
   * @note Includes PredEqns too, is this ok?
   */
  val litEqnGen: Gen[Lit] = 
    for {
      eqn <- arbitrary[Eqn]
      sign <- arbitrary[Boolean]
    } yield Lit(sign,eqn)

  def litEqnGen(sig: Signature): Gen[Lit] = 
    for {
      eqn <- oneOf(eqnGen(sig),fgPredGen(sig),bgPredGen(sig))
      sign <- arbitrary[Boolean]
    } yield Lit(sign,eqn)

  /** Generates either a constant true, false, or a list with no logical constants.
   * Better for clauses, because it generates fewer trivial clauses.
   */
  val litsForClauses: Gen[List[Lit]] = 
    Gen.frequency((1, const(List(Lit.TrueLit))),
		  (1, const(List(Lit.FalseLit))),
		  (8, Gen.containerOf[List,Lit](litEqnGen)))

  def litsForClauses(sig: Signature): Gen[List[Lit]] = 
    Gen.frequency((1, const(List(Lit.TrueLit))),
		  (1, const(List(Lit.FalseLit))),
		  (8, Gen.containerOf[List,Lit](litEqnGen(sig))))
  
  /** Includes constants, BG predicates.
   * Should it include hard ones like is_int too?
   * @return a proper equation literal, or a boolean constant with equal frequency.
   */
  implicit def arbLit: Arbitrary[Lit] = 
    Arbitrary( oneOf(litEqnGen,
		     const(Lit.TrueLit),
		     const(Lit.FalseLit) ))
    
  /* ----Clause----------------------------- */

  //TODO- anything to test here?
  //val clauseWParentsGen = ???

  def clauseGen(sig: Signature): Gen[ConsClause] = 
    for {
      lits <- litsForClauses(sig)
      idxRel <- Gen.containerOf[List,Int](choose(0,lits.length))
      age <- choose(0,1000)
      deltaConjecture <- choose(0,1000)
    } yield 
      Clause(lits, ClsInfo(idxRel.toSet, age, ByInput, deltaConjecture))

  /* Other clause types from beagle.finite */
  lazy val fixedSigClGen: Gen[ConsClause] = {
    //todo this could apply for any datastruct not just clauses
    var i = 0
    var sig: Option[Signature] = None
    while(sig==None && i < 1000) {
      sig=sigGen.sample
      i+=1
    }
    if(sig==None) throw new Error("Could not generate a signature for testing")
    else {
      val s = sig.get
      Sigma=s
      println("Global signature is now:")
      Sigma.show
      clauseGen(s)
    }
  }

  /** Let Clause object manage clause IDs */
  implicit def arbClause: Arbitrary[ConsClause] = 
    Arbitrary(for {cl <- fixedSigClGen} yield cl)
    /*Arbitrary(for {
      lits <- litsForClauses
      idxRel <- Gen.containerOf[Set,Int](choose(0,lits.length))
      age <- choose(0,1000)
    } yield Clause(lits,idxRel,age))*/
/*
  //LabelClause
  implicit def arbLClause: Arbitrary[LClause] = 
    Arbitrary(for {
      cl <- fixedSigClGen //arbitrary[ConsClause]
      t <- funTermGen(IntSort,Sigma) //this can lead to really stupid looking definitions...
    } yield new LClause(cl, List(SimpAssump(makeParam(t),t,Map()))) )
 */
  //FQClause

}
