package bench

import beagle._
import fol._
import term._
import bgtheory.LIA._
import util._
import datastructures._

/**
* Generate large simultaneous equations to solve via cooper
*/
object SimulGen {

  def generate(n: Int) = {
    val vars = for(i <- 0 until n) yield { AbstVar("X"+i,IntSort) }

    //val coeffs = (0 until n).toArray.map((0 until n).toArray.map{ (Math.random()*n*4 - (n*2)).toInt })

    //how to guarantee a solution? use Linear algebra :D

    //a better way for now
    (for(j <- 0 until n) yield {
      var last = Product(DomElemInt((Math.random*n*4 - n*2).toInt),vars.head)
      for(i <- 1 until n) {
	val c = (Math.random*n*4 - n*2).toInt
	last = Sum(last,Product(DomElemInt(c),vars(i)))
      }
      last
    }).toList
    .map(Equation(_,DomElemInt(0)))
    .toAnd
		  
  }

  def genHard(n: Int, hardness: Int = 5000): List[Formula] = {
    //threshold 5s?
    var limit = 100

    var probs = List[Formula]()
    while(probs.length < n && limit >= 0) {
      val t = generate(3).closure(Exists)
      val start = System.currentTimeMillis
      try {
	cooper.QE(t,true,List())
	val time = (System.currentTimeMillis-start)
	if(time >= hardness) { 
	  println("Problem found with t="+time)
	  probs::=t
	}
      }catch {
	case e: InternalError => println("Internal error "+e.s+"\nProblem: "+t)
	case e: OutOfMemoryError => println("Out of Memory!\nProblem: "+t)
	case e: StackOverflowError => println("Stack overflow!\nProblem: "+t)
      }
      limit-=1
    }

    if(probs.isEmpty) println("Sorry, no problems found")
    probs
  }

  //use for calling genHard from the console
  def main(args: Array[String]) {
    genHard(args(0).toInt,args(1).toInt) foreach { println _ }
  }

}
