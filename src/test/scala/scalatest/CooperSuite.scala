package scalatest

import org.scalatest._

import beagle._
import util._
import fol._
import term._
import bgtheory.LIA._
import cooper._

/**
 * Tests for changes made to cooper
 */
class CooperSuite extends FunSuite with GivenWhenThen {
  
  test("LCM of empty list") {
    assert(lcm(List()) == 1,"Lcm of empty list failed")
  }

  //technically this wont happen when used with cooper, but
  //better safe than sorry
  test("LCM of a list with 0") {
    assert(lcm(List(0)) == 0, "LCM of List(0)!=0")
    assert(lcm(List(0,2,0)) === 0)
  }

  test("LCM of unit length lists") {
    assert(lcm(List(1)) === 1)    
    assert(lcm(List(5)) === 5)
  }

  test("LCM for lists std. behaviour"){    
    //list(3,6,9) lcm = 18
    assert(lcm(List(3,6,9)) === 18)

    //list(3,6,10) lcm = 30
    assert(lcm(List(3,6,10)) === 30)

    //list of coprimes
    assert(lcm(List(5,17,31)) === (5*17*31))

  }
/*
  test("LCM does not loop") {
    intercept[IntOverflowException] {
      assert(lcm(List(Int.MaxValue,2,3)) > 0)
    }
  }
*/
  info("Testing elimEquation(f: Formula, x: VarOrSymConst): Option[Formula]")
  //gives None iff x cannot be eliminated via equations

  //how can you test if it is sound?
  //could do elimEquation((0=x+t)&f,x) === T & replace(f,x,t) for lots of f?

  val x = AbstVar("X",bgtheory.LIA.IntSort)
  val y = AbstVar("Y",bgtheory.LIA.IntSort)
  val z = AbstVar("Z",bgtheory.LIA.IntSort)
  val c = BGConst("c",bgtheory.LIA.IntSort)

  //basic atoms
  //0 = x+c -ok
  val a1 = ZeroEQPoly(Polynomial(Monomial(1,x) :: Monomial(1,c) :: Nil,0,Nil))
  //0 < x+c+1 -none
  val a2 = ZeroLTPoly(Polynomial(Monomial(1,x) :: Monomial(1,c) :: Nil,1,Nil))
  //0 = y+c+1 -none
  val a3 = ZeroEQPoly(Polynomial(Monomial(1,y) :: Monomial(1,c) :: Nil, 1, Nil))
  
  //a1 & a2 -ok
  val f1 = fol.And(a1,a2)
  //a1 | a2 -none
  val f2 = fol.Or(a1,a2)

  test("basic operation") {
    Given(a1.toString)
    Then("can eliminate x")
    assert(bgtheory.LIA.cooper.cooper.elimEquation(a1,x)===Some(TrueAtom))
    assert(bgtheory.LIA.cooper.cooper.elimEquation(a1,y)===None)
    
    Given(a2.toString)
    Then("cannot eliminate")
    assert(bgtheory.LIA.cooper.cooper.elimEquation(a2,x)===None)
    assert(bgtheory.LIA.cooper.cooper.elimEquation(a2,y)===None)

    Given(a3.toString)
    Then("can eliminate y")
    assert(bgtheory.LIA.cooper.cooper.elimEquation(a3,x)===None)
    assert(bgtheory.LIA.cooper.cooper.elimEquation(a3,y)===Some(TrueAtom))

    Given(f1.toString)
    Then("can eliminate x (assume simplification is done after)")
    assert(bgtheory.LIA.cooper.cooper.elimEquation(f1,x)===Some(fol.And(TrueAtom,TrueAtom)))
    assert(bgtheory.LIA.cooper.cooper.elimEquation(f1,y)===None)

    Given(f2.toString)
    Then("cannot eliminate from Or")
    assert(bgtheory.LIA.cooper.cooper.elimEquation(f2,x)===None)
    assert(bgtheory.LIA.cooper.cooper.elimEquation(f2,y)===None)

    Given(TrueAtom.toString)
    Then("cannot eliminate anything")
    assert(bgtheory.LIA.cooper.cooper.elimEquation(TrueAtom,x)===None)
    assert(bgtheory.LIA.cooper.cooper.elimEquation(TrueAtom,y)===None)
    assert(bgtheory.LIA.cooper.cooper.elimEquation(TrueAtom,c)===None)

    Given(FalseAtom.toString)
    Then("cannot eliminate anything")
    assert(bgtheory.LIA.cooper.cooper.elimEquation(FalseAtom,x)===None)
    assert(bgtheory.LIA.cooper.cooper.elimEquation(FalseAtom,y)===None)
    assert(bgtheory.LIA.cooper.cooper.elimEquation(FalseAtom,c)===None)

  }

  info("Testing elimShadows")

  test("elimShadows properly accounts for constants in polys") {
    //(0 < e2 + 37) (0 < -e2 -2)
    val test = List(ZeroLTPoly(Polynomial(Monomial(1,c)::Nil,37,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-1,c)::Nil,-2,Nil)))
    Given(s"Input formula: ${test.toAnd}")
    When(s"calling elimShadows for $c")
    Then("The result is not false")
    assert(bgtheory.LIA.cooper.cooper.elimShadows(test,c) != List(FalseAtom))
  }

  test("elimShadows missing inequalities possible bug") {
    //c < x, c < y, 3 < c
    val test = List(ZeroLTPoly(Polynomial(Monomial(-1,c)::Monomial(1,x)::Nil,0,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-1,c)::Monomial(1,y)::Nil,0,Nil)),
		    ZeroLTPoly(Polynomial(Monomial(1,c)::Nil,-3,Nil)))
    Given(s"Input formula: ${test.toAnd}")
    When(s"calling elimShadows for $x")
    //x is not eliminated and the formula should be unchanged
    Then("The result is unchanged")
    assert(bgtheory.LIA.cooper.cooper.elimShadows(test,x) == test)
  }

  test("Simple test cases for elimShadows") {
    //1 < 3x, 5x < 2 is false
    //5 < 15x < 6
    val t1 = List(ZeroLTPoly(Polynomial(Monomial(3,x)::Nil,-1,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-5,x)::Nil,2,Nil)))

    assert(bgtheory.LIA.cooper.cooper.elimShadows(t1,x) == List(FalseAtom))

    //1 < x, x < 5 is simplified to T since it is equivalent to 1 < 4
    val t2 = List(ZeroLTPoly(Polynomial(Monomial(1,x)::Nil,-1,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-1,x)::Nil,5,Nil)))

    assert(bgtheory.LIA.cooper.cooper.elimShadows(t2,x).distinct == List(TrueAtom))

    //1 < x, x < 5, 5x < 3 is false
    val t3 = List(ZeroLTPoly(Polynomial(Monomial(1,x)::Nil,-1,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-1,x)::Nil,5,Nil)),
		    ZeroLTPoly(Polynomial(Monomial(-5,x)::Nil,3,Nil)))

    assert(bgtheory.LIA.cooper.cooper.elimShadows(t3,x) == List(FalseAtom))

    // y < 2x, x < -3y ==> y < -6y - 1 (satisfiable) NOT simplified
    val t4 = List(ZeroLTPoly(Polynomial(Monomial(2,x)::Monomial(-1,y)::Nil,0,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-1,x)::Monomial(-3,y)::Nil,0,Nil)))
    
    assert(bgtheory.LIA.cooper.cooper.elimShadows(t4,x) == t4)
    //3y < 2x, 2x < 3y ==> 6y < 6y -1 is false
    val t5 = List(ZeroLTPoly(Polynomial(Monomial(2,x)::Monomial(-3,y)::Nil,0,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-2,x)::Monomial(3,y)::Nil,0,Nil)))

    assert(bgtheory.LIA.cooper.cooper.elimShadows(t5,x) == List(FalseAtom))
    
    //3y+2 < x, x < 5y-1 ==> 3y+2 < 5y-2 = 0 < 2y-4
    val t6 = List(ZeroLTPoly(Polynomial(Monomial(1,x)::Monomial(-3,y)::Nil,-2,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-1,x)::Monomial(5,y)::Nil,-1,Nil)))

    val t6_ans =List(ZeroLTPoly(Polynomial(Monomial(2,y)::Nil,-4,Nil)))

    assert(bgtheory.LIA.cooper.cooper.elimShadows(t6,x) == t6_ans)

    //this should not simplify, because of the != sign
    //(((0 < (x + 37)) ∧ (0 ≠ -x)) ∧ (0 < (-x + -2)))
    //-37 < x, x < -2 ==> T 
    val t7 = List(ZeroLTPoly(Polynomial(Monomial(1,x)::Nil,37,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-1,x)::Nil,-2,Nil)),
		    ZeroNEPoly(Polynomial(Monomial(1,x)::Nil,0,Nil)))

    assert(bgtheory.LIA.cooper.cooper.elimShadows(t7,x) == t7)

    //eliminate x from ((0 < ((-y + x) + 1)) ∧ (0 < (-x + z)))
    //yields 0 < z -y

    val t8 = List(ZeroLTPoly(Polynomial(Monomial(1,x)::Monomial(-1,y)::Nil,1,Nil)), 
		    ZeroLTPoly(Polynomial(Monomial(-1,x)::Monomial(1,z)::Nil,0,Nil)))
    val t8_ans = List(ZeroLTPoly(Polynomial(Monomial(-1,y)::Monomial(1,z)::Nil,0,Nil)))

    assert(bgtheory.LIA.cooper.cooper.elimShadows(t8,x) == t8_ans)
  }

}
