package specification

import org.scalacheck._
//import beagle._
import beagle.fol._
import beagle.datastructures._
import beagle.calculus.derivationrules._
//import beagle.finite.FiniteSearchContext.LClause

/* TODO- update to descript ConClause */

/*
object LabelSpecification extends Properties("LClause") {
  import org.scalacheck.Prop.{forAll, BooleanOperators}
  import DSGenerators._

  /* Test label permanence wrt derivation rules and simp rules for clauses */

  //derivation rules:
  //single premise: TODO- set lower give up ratio
  //or generate clauses over the same signature
  property("single premise rules") = forAll { (cl: LClause) => {
    cl.operators.foreach(Sigma+=_)
    ( "infRef should preserve labels"    |: Prop.all( infRef(cl).map({ //map each result to a Prop, all of which should be true
      case lc: LClause => ( s"Conclusion $lc should have label ${cl.label}" |: lc.label == cl.label )
      case c =>           ( s"Conclusion $c should be an LClause"           |: Prop.falsified )
    }):_* ) ) &&
    ( "infFact should preserve labels"   |: Prop.all( infFact(cl).map({ 
      case lc: LClause => ( s"Conclusion $lc should have label ${cl.label}" |: lc.label == cl.label )
      case c =>           ( s"Conclusion $c should be an LClause"           |: Prop.falsified )
    }):_*) ) &&
    ( "infDefine should preserve labels" |: ( infDefine(cl) match { 
      case Some((newCl: LClause, defs)) => { 
	( s"Rewritten clause $newCl should have label" |: newCl.label==cl.label ) && 
	Prop.all( defs.map({ 
	  case lc: LClause => ( s"Definition $lc should have label ${cl.label}" |: lc.label==cl.label )
	  case c =>           ( s"Definition $c was not an LClause"             |: Prop.falsified )  
	}).toSeq:_*) 
      }
      case Some((c,_)) => ( s"Rewritten clause $c was not an LClause" |: Prop.falsified )
      case None =>        Prop.passed
      case _ =>           Prop.falsified //?
    })) &&
    ( "infSplit should preserve labels"  |: ( infSplit(cl,0) match {
      case Some((leftCls: List[LClause], rightClFn)) => {
	val leftCl = leftCls.head
	val rightCls = rightClFn(Set())
	( s"Left clause $leftCl should have label ${cl.label}" |: leftCl.label == cl.label ) &&
	( Prop.all( rightCls.map({
	  case lc: LClause => ( s"Right clause $lc should have label ${cl.label}" |: lc.label==cl.label )
	  case c =>           ( s"Right clause $c was not an LClause"             |: Prop.falsified )
	}).toSeq:_*) )
      }
      case Some((c,_)) => ( s"Left clause $c was not an LClause" |: Prop.falsified )
      case None =>        Prop.passed
      case _ =>           Prop.falsified
    } ))
  }}

  //multi premise
  property("multipremise rules") = forAll { (cl1: LClause, cl2: LClause) => {
    val expectedLabel = (cl1.label ++ cl2.label).toSet
    val cs = new ListClauseSet
    cs.add(cl2)

    //TODO- perhaps add guards so that only successful inferences count
    // def infChaining(cl: Clause, cs: ListClauseSet) -- needs special generators?
    ( "infChaining should preserve labels" |: Prop.all( infChaining(cl1,cs).map({
      case lc: LClause => ( s"Conclusion with label ${lc.label} should have label $expectedLabel" |: lc.label.toSet == expectedLabel )
      case c =>           ( s"Conclusion should be an LClause"                                    |: Prop.falsified )
    }).toSeq:_*)) &&
    ( "infSup should preserve labels from both premises" |: 
     (( s"Superposition of $cl1 into $cl2" |: Prop.all( infSupIntoClauses(cl1,cs).map({
	case lc: LClause => ( s"Conclusion with label ${lc.label} should have label $expectedLabel" |: lc.label.toSet == expectedLabel )
	case c =>           ( s"Conclusion should be an LClause"           |: Prop.falsified )
      }).toSeq:_*)) &&
      ( s"Superposition from $cl2 into $cl1" |: Prop.all( infSupFromClauses(cs,cl1).map({
	case lc: LClause => ( s"Conclusion with label ${lc.label} should have label $expectedLabel" |: lc.label.toSet == expectedLabel )
	case c =>           ( s"Conclusion should be an LClause"           |: Prop.falsified )
      }).toSeq:_*))
    ))
   }}

  //simp rules:
//   def
// reduce(cls: Iterable[Clause]): List[Clause]
// lazy val
// simplifyBG: Clause
// def
// simplifyCheap: Iterable[Clause]
// lazy val
// unabstr: Clause

}
 */
