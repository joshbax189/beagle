package specifications

import org.scalacheck._
import beagle._
import fol._
import term._
import bgtheory.LIA._
import generators._

object SubstSpecification extends Properties("Substitutions") {
  import org.scalacheck.Prop.{forAll, BooleanOperators}
  import TermGenerators._

  //Not tested:
  //apply[T](e: Expression[T]): T
  //apply[T](es: List[Expression[T]]): List[T]
  //val env: Map[Var, Term] -?
  //isIdempotent: Boolean
  //join(sigma: Subst)
  //lookup(v: Var): Option[Term]

  //+(Var,Term) Extend with one binding
  property("+(Var,Term)") = forAll { (x: Var, t: Term, s: Subst) => {
    ("Adding the binding (x->t) makes s(x)=t" |: (s+(x->t))(x) == t ) &&
    ("Adding a binding twice has no effect"   |: ((s+(x->t))+(x->t)) == s+(x->t) ) /*&&
    ("Adding a binding should preserve idempotence" |: {
      val y = x.fresh()
      val s2 = (s+(x->y))+(y->t)
      (s"After adding ($x->$y) and ($y->$t)") |:
      (  s"Expect that s($x)=$t"              |: s2(x) == t)
    })*/
  }}

/* //only for disjoint domains
  property("join") = forAll { (s1: Subst, s2: Subst, s3: Subst) => {
    ("join is associative" |: {
  //++(sigma: Subst): Subst - Composition of substitutions
  property("++(sigma: Subst)") = forAll { (s1: Subst, s2: Subst) => {
    val comp = s1 ++ s2
    ( "++ is composition" |: Prop.all( (s1.dom ++ s2.dom).map(x => {
      (s"$comp($x)=s2(s1($x))" |: comp(x)==s2(s1(x)))
    }).toSeq:_* ))
  }}
*/
  //actsOn[T](e: Expression[T]): Boolean
  property("actsOn(e: Expression)-Term case") = forAll { (t: Term, s: Subst) =>
    (("If Subst acts on a term" |: (s actsOn t) ==> 
      (" then the application of s changes the term" |: s(t)!=t )) ||
    ("If Subst does not act on a term" |: !(s actsOn t) ==> 
      ("then the term is the same" |: s(t)==t)))
  }

  //actsOn(v: Var): Boolean
  property("actsOn(v: Var)") = forAll {
    (x: Var, t: Var) => {
      val s1 = Subst(x->t)
      val s2 = Subst(x->x)
      (s"A Subst acts on $x if it contains a non-identity binding ($x->$t) for it" |: (s1.actsOn(x) || x==t) ) && 
      (s"A Subst does not act on $x if the only binding is ($x -> $x)" |: !s2.actsOn(x))
    }
  }

  //derived from the above:
  //actsOn(vs: Set[Var]): Boolean
  property("actsOn(vs: Set[Var])- empty edge case") = forAll { (s: Subst) =>
    !s.actsOn(Set.empty[Var])
  }

  //actsOn(vs: List[Var]): Boolean
  property("actsOn(vs: List[Var])- empty edge case") = forAll { (s: Subst) =>
    !s.actsOn(List())
  }

  //isEmpty: Boolean
  property("Subst.empty is empty") = Subst.empty.isEmpty

  property("Subst.isEmpty") = forAll { (x: Var, t: Term) =>
    ( s"Adding ($x->$x) to Subst.empty should be empty"                   |: (Subst.empty + (x -> x)).isEmpty ) &&
    ( s"An identity substitution Subst(Map($x->$x)) should be empty"      |: Subst(x -> x).isEmpty ) &&
    ( s"Adding and removing a binding from Subst.empty satisfies isEmpty" |: ((Subst.empty+(x -> t)) -- (List(x))).isEmpty )
  }

  //isRenaming: Boolean - Whether this substitution is an injective map from variables to variables.
  property("isRenaming: Subst.empty is a renaming") = Subst.empty.isRenaming

  //range only vars && s is injective
  property("isRenaming") = forAll { (s: Subst) => 
    (s"Non variable terms in range of $s cannot be renaming" |: (s.env.values.forall(_.isVar) || !s.isRenaming)) &&
    //non-injective => ! a renaming
    (s"Non-injective $s cannot be a renaming" |:
      (s.env.values.toSet.size == s.env.values.size ||
        s.env.keys.forall(x => s(x)!=s(s(x))) || 
        !s.isRenaming))
  }
  
  //TODO-
  //--(xs: List[Var]): Subst - Remove bindings for a given list of variables
  property("--(xs: List[Var])") = forAll { s: Subst => 
    (s"Removing all variables from $s should be empty" |: (s -- (s.dom)).isEmpty) &&
    (s"Removing a disjoint list of vars does not change $s" |: (s -- (s.dom.map(_.fresh))) == s)
  }

  //combine(that: Subst): Option[Subst] - Combine two unifiers (typically mgus) into one, simultaneous unifier, if possible

  //equals(any: Any): Boolean - Two substitutions are equal if they have the same env.
  //a substitution is equal after adding an identity x->x

  //values: Set[Term]
  //vars contains only the variables of a substitutions which are modified

  //TODO gives up!!
  property("Term matchers") = forAll { (s: Term, t: Term) => 
    ( "Matchings extending an empty subst" |: {
      val m1 = unification.matchTo(s,t,Subst.empty)
      val m2 = unification.matchTo(t,s,Subst.empty)
      ( s"For matching terms $s and $t" |: ((m1!=None) ==> 
	( s"Applying $m1 should make the first term equal the second" |: m1.get(s)==t ))) &&
      ( s"For matching terms $t and $s" |: ((m2!=None) ==> 
	( s"Applying $m2 should make the first term equal the second" |: m2.get(t)==s ))) &&
      ( s"Terms match themselves" |: unification.matchTo(s, s, Subst.empty).nonEmpty ) &&
      ( s"Terms match a fresh version of themselves" |: unification.matchTo(t, t.fresh, Subst.empty).nonEmpty )
    })
  }

  property("Term unifiers") = forAll { (s: Term, t: Term) =>
    val mgu = s.mgu(t)
    ( s"For unifiable terms $s and $t" |: ((!mgu.isEmpty) ==> 
	( s"The unifier should make the terms equal" |: mgu.get(s)==mgu.get(t) )))
    }

  //TODO not valid!
  property("MSG") = forAll { (t: Term) =>
    val (r, s1, s2) = SubstAux.msg(t, t, Subst.empty, Subst.empty).get

    ( s"Any term $t is its own MSG $r" |: 
       r == t &&
       s1.isEmpty &&
       s2.isEmpty)
  }

  property("MSG 2") = forAll { (s: Term, t: Term) =>
    val res = SubstAux.msg(s,t,Subst.empty,Subst.empty)
    ( "For a non-empty generalisation" |: !res.isEmpty ==> (
      s"the result ${res.get} must produce the original terms" |: {
	val (u,s1,s2) = res.get
	(s1(u)==s) && (s2(u)==t)
      }))
  }

}
