package specification

import org.scalacheck._

// import beagle._
// import beagle.fol._
import beagle.datastructures._
// import beagle.calculus.derivationrules._

object ClauseSpecification extends Properties("Clause") {
  import org.scalacheck.Prop.{forAll, BooleanOperators}
  import generators.DSGenerators._

  /* every clause should be subsumed by any proper non-empty subset of itself or a renaming of itself */
  beagle.bgtheory.setSolver("cooper")

  property("subsumption") = forAll { c: ConsClause =>
    ("A clause should be subsumed by itself" |: c.subsumes(c))
    ("A clause should be subsumed by a variant of itself" |: {
      val cVar = c.fresh
      ( s"$cVar subsumes input clause" |: cVar.subsumes(c)) &&
      ( s"$cVar is subsumed by input clause" |: c.subsumes(cVar))
    })
  }

  property("subsumption 2") = forAll { c: ConsClause =>
    ("A clause should be subsumed by a proper subset of itself" |: {
      val properSubsets = c.lits.tails.toSeq.tail
      Prop.all(properSubsets map { sub =>
        val cSub = c.modified(lits = sub)
        (s"Subset $cSub must subsume input clause" |: cSub.subsumes(c))
      } :_*)
    })
  }

  property("subsumption + unabstraction") = forAll { c: ConsClause =>
    ("A clause should be subsumed by an unabstracted version of itself" |: {
      val c1 = c.abstr
      ( s"Given the weak abstracted form $c1" |:
        ( "The input clause should subsume its abstraction" |: c.subsumes(c1) ) &&
        ( "The abstracted form should subsume the input clause" |: c1.subsumes(c)))

      val c2 = c.unabstrCautious
      ( s"Given the unabstracted form $c2" |:
        ( "The input clause should subsume its unabstraction" |: c.subsumes(c2) ) &&
        ( "The unabstracted form should subsume the input clause" |: c2.subsumes(c)))

      val c3 = c.unabstrAggressive
      ( s"Given the aggressive unabstracted form $c3" |:
        ( "The input clause should subsume its unabstraction" |: c.subsumes(c3) ) &&
        ( "The unabstracted form should subsume the input clause" |: c3.subsumes(c)))
    })
  }

 /* clauses are subsumed by the empty clause */
/* a clause should be subsumed by a permutation of itself */

}
