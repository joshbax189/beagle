package beagle.bgtheory

/*
 *  Generic interface to SMT-solvers
 */

import beagle._
import fol._
import term._
import fol.Signature._
import datastructures._
import util._
import bgtheory._
import LIA._

import scala.util.control._

abstract class SMTSolver {

  // Abstract members:
  val name: String // The name of the SMT solver, e.g. "Z3"
  val executable: String // The string to use as executable for "name", e.g. "z3 -smt2"
  val logic: String // The SMT logic to use, set via set-logic command, e.g. "QF_LIA". Set to empty if not required
  var canUNSATCore = false // The the solver can compute unsatisfiable cores, set by init()

  var extraArgs = ""

  /**
    * init(): checks if the executable runs ok and sets canUNSATCore
    */
  def init() {
    import scala.sys.process._
    // Create a temporary file
    val outfile = java.io.File.createTempFile("for-SMT-solver", ".smt2")
    outfile.deleteOnExit()
    val outname = outfile.getAbsolutePath()
    val out = new java.io.PrintStream(new java.io.FileOutputStream(outname))
    // Write the test contents
    out.println("""(set-logic QF_LIA)
(set-option :produce-unsat-cores true)
(assert (! (= 1 2) :named c1))
(check-sat)
(get-unsat-core)""")
    out.close()

    val resStream = (executable + " " + outname).lineStream_!

    var haveResult = false
    for (line ← resStream)
      if (line contains "(c1)")
        canUNSATCore = true
      else if (line == "unsat")
        haveResult = true
    if (!haveResult)
      throw GeneralError("SMT-Solver: test call does not yield 'unsat'")

    // See if the bgsolver is CVC4
    try {
      val resStream1 = (executable + " --version").lineStream_!
      val breakLoop = new Breaks
      import breakLoop.{break, breakable}
      breakable {
        for (line ← resStream1)
          if (line contains "CVC4") {
            reporter.debug("Background SMT solver seems to be CVC4\n")
            extraArgs = "--rewrite-divk"
            break()
          }
      }
    } catch {
      case _:Throwable => () // ignore
    }
  }


  // TPTP operators that are built-in SMT LIB
  val builtinOps = Map(
    "$sum" -> "+",
    "$uminus" -> "-",
    "$difference" -> "-",
    "$quotient_e" -> "???_$quotient_e", // see TFF paper
    "$quotient_t" -> "???_$quotient_t", // see TFF paper
    "$quotient_f" -> "???_$quotient_f", // see TFF paper
    "$quotient" -> "div", // see TFF paper
    "$remainder_e" -> "???_$remainder_e", // see TFF paper
    "$remainder_t" -> "???_$remainder_t", // see TFF paper
    "$remainder_f" -> "???_$remainder_f", // see TFF paper
    "$product" -> "*",
    "$divides" -> "thisIsADummy",
    "$less" -> "<",
    "$lesseq" -> "<=",
    "$greater" -> ">",
    "$greatereq" -> ">="
  )

  def safeIdent(s: String) = "|"+s+"|"

  // Could possibly also use util.Printers, but prefer simpler direct approach here
  def clauseToSMT(cl: ConsClause) = {
    def litToSMT(l: Lit): String = {
      def eqnToSMT(e: Eqn) = {
        def termListToSMT(ts: List[Term]): String =
          (ts map { termToSMT(_) }).mkString(" ")
        def termToSMT(t: Term): String = {
          def intToSMT(i: Int) = if (i < 0) "(- %s)".format(i * -1) else i.toString
          t match {
            case Var(name, index, _) => safeIdent(name) + (if (index == 0) "" else "_" + index)
              // $divides is a special case:
            case PFunTerm(LIA.DividesEqn.op, List(number, term)) => "(= 0 (mod %s %s))".format(termToSMT(term), termToSMT(number))
            case PFunTerm(op, args) => "(%s %s)".format(builtinOps.getOrElse(op.name, safeIdent(op.name)), termListToSMT(args))
            case SymConst(op) => safeIdent(op.name)
            case LRA.DomElemRat(Rat(numer, denom)) => {
              if (denom == 1) intToSMT(numer) else "(/ %s %s)".format(intToSMT(numer), intToSMT(denom))
            }
            case LFA.DomElemReal(Rat(numer, denom)) => {
              if (denom == 1) intToSMT(numer) else "(/ %s %s)".format(intToSMT(numer), intToSMT(denom))
            }
            case DomElemInt(i) => intToSMT(i)
            // many cases missing
          }
        }
        // Body of eqnToSMT
        e match {
          case PredEqn(a) ⇒ termToSMT(a)
          case Eqn(l ,r) ⇒ "(= %s %s)".format(termToSMT(l), termToSMT(r))
        }
      }
      // Body of litToSMT
      if (l.isPositive) eqnToSMT(l.eqn) else "(not %s)".format(eqnToSMT(l.eqn))
    }
    cl.lits match {
      case Nil => "false"
      case l :: Nil => litToSMT(l)
      case many => "(or %s)".format((many map { litToSMT(_) }).mkString(" "))
    }
  }

  def sortToSMT(s: Type) =
    s.name match {
      case "$int" => "Int"
      case "$rat" => "Real"
      case "$real" => "Real"
      case name => safeIdent(name)
    }

  /*
   * Call the SMT solver on a given clause set.
   */
  def check(clauses: Iterable[ConsClause]): SolverResult = {

    import scala.sys.process._

    val outfile = java.io.File.createTempFile("for-SMT-solver", ".smt2")
    // On my Mac TMP files are in /var/folders/l4/xh_s2hzx2v36khg8bxn62v480000gq/T/ or vicinity
    outfile.deleteOnExit()
    val outname = outfile.getAbsolutePath()
    // val outname = "/tmp/for-smt.smt2"
    // println(s"outname = $outname")

    val out = new java.io.PrintStream(new java.io.FileOutputStream(outname))
    if (logic.nonEmpty) out.println("(set-logic %s)".format(logic))

    if (canUNSATCore && !flags.noMuc.value) out.println("(set-option :produce-unsat-cores true)")

    // Inform the SMT solver about the relevant BG sorts.
    // These are all sorts except the builtin-ones $int, $rat, $real.
    // All these (TPTP) sorts are 0-ary
    Sigma.bgSorts foreach { sort =>
      sort.name match {
        case "$int" | "$rat" | "$real" => ()
        case _ => out.println("(declare-sort %s 0)".format(sortToSMT(sort)))
      }
    }

    // out.println("(declare-fun |$$true| () Bool)")
    out.println("(define-fun |$$true| () Bool true)")
    // declare all the BG operators, except the builtin-ones
    Sigma.bgOperators foreach { op =>
      if (! (builtinOps contains op.name)) {
        out.println("(declare-fun %s (%s) %s)".format(
          safeIdent(op.name),
          (op.arity.argsSorts map { sort => sortToSMT(sort)} ).mkString(" "),
          sortToSMT(op.arity.resSort))
        )
      }
    }

    clauses foreach { cl =>
      if (canUNSATCore && !flags.noMuc.value)
        out.println("(assert (! %s :named %s))".format(clauseToSMT(cl), "c"+cl.id.toString))
      else
        out.println("(assert %s)".format(clauseToSMT(cl)))
    }
    out.println("(check-sat)")
    if (canUNSATCore && !flags.noMuc.value) out.println("(get-unsat-core)")
    out.close()

    if (debugBG) {
      println("Calling SMT solver on the following input:")
      ("/bin/cat " + outname).!
    }

    // val resStream = ("z3 -T:150 -memory:1000 -smt2 " + outname).lines_!
    // util.Timer.z3.start()
    val resStream = (executable +
    (if (extraArgs == "") "" else " " + extraArgs) +
    " " + outname).lineStream_!

    var res:SolverResult = UNKNOWN

    val breakLoop = new Breaks
    import breakLoop.{break, breakable}
    breakable {
      for (line ← resStream) {
        // println(line) // debug
        if (res == UNKNOWN) {
          // see if we get our result now
          if (line contains "error") {
            throw InternalError("SMT solver: file %s: %s".format(outname,line))
          } else if (line == "sat") {
            res = SAT
            break()
          } else if (line == "unknown") {
            res = UNKNOWN
            break()
          } else if (line == "unsat") {
            res = UNSAT(None)
            // get the next line, with the unsat core,
            // if the flag sys so, hence no break then
            if (! (canUNSATCore && !flags.noMuc.value)) break()
          }
        } else if (res == UNSAT(None)) {
          // line contains the unsat core
          // Dummy implementation
          val s = line.drop(1).dropRight(1) // get rid of "(" and ")"
          // split s, turn "c123" into "123" then into 123
          val coreIdx = ((s split(" ")) map {s => s.drop(1).toInt }).toList
          // println(line)
          // println(coreIdx)
          res = UNSAT(Some(coreIdx))
          break()
        }
      }
    }
    if (debugBG) {
      println("SMT solver says: %s".format(res))
      println()
    }
    // println("done")
    // println()
    // util.Timer.test.stop()
      res
    }


}

// class Z3(val logic: String) extends SMTSolver {
//   // Implement abstract members
//   val name = "Z3"
//   val execString = "/usr/local/bin/z3 -smt2 %s"
//   init()
// }

// class CVC4(val logic: String) extends SMTSolver {
//   // Implement abstract members
//   val name = "CVC4"
//   val execString = "/usr/local/bin/cvc4 --lang smt2 %s"
//   init()
// }




