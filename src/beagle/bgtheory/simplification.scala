package beagle.bgtheory

import beagle._
import fol._
import term._
import datastructures._
import fol.Signature._
import util._
import bgtheory._
import LIA._

/**
 * Simplification is expressed by `SimpRule`s.
 * @tparam T is a `Term` or `Lit`.
 */
case class SimpRule[T](name: String, rule: T ⇒ T) {
  def apply(t: T) = rule(t)
}

/**
 * When a rule is not applicable to a given sequent.
 */
case object NotApplicable extends Exception

/** Describe the possible BG simp levels */
sealed trait BGSimpLevel
case object OFF_BG_SIMP extends BGSimpLevel
case object CAUT_BG_SIMP extends BGSimpLevel
case object AGGR_BG_SIMP extends BGSimpLevel

/** recovers the old functionality of simplification */
// object simplification {
//   def simplify(t: Term) = bgtheory.solver.simplify(t)
//   def simplify(l: Lit) = bgtheory.solver.simplify(l)
//   def simplify(cl: Clause) = bgtheory.solver.simplify(cl)
// }

/**
 * As a trait, Simplification exposes simplify(term/lit/clause).
 * It has the rules safe/unsafe as abstract value members and a method setState
 * which dynamically sets them.
 * This way you can't set bgsimp level without first setting a prover.
 * But now the provers have a mutable state variable: the particular level
 * of BG simplification.
 */
trait Simplification {
  //bgsolvers have their own version of these:

  /**
   * Theory simplification rules for terms.
   * "Safe" rules always preserve sufficient completeness.
   */
  val simpRulesTermSafe: List[SimpRule[Term]]
  /**
   * "Unsafe" rules can possibly destroy sufficient completeness.
   *  The unsafe ones do not need to include the safe ones; in aggressive simplification
   *  the two lists are concatenated.
   */
  val simpRulesTermUnsafe: List[SimpRule[Term]]

  val simpRulesLitSafe: List[SimpRule[Lit]]
  val simpRulesLitUnsafe: List[SimpRule[Lit]]

  def subsumes(cl1: ConsClause, cl2: ConsClause): Boolean = false
  // whether cl1 subsumes cl2, theory-specific
  // Should be overridden

  //these are set dynamically
  private var simpRulesTerm = List.empty[SimpRule[Term]]
  private var simpRulesLit = List.empty[SimpRule[Lit]]

  /** Set the simplification level when the solver is first called */
  def init() {
    flags.bgsimp.value match {
      case "off" => setBGSimpLevel(OFF_BG_SIMP)
      case "cautious" => setBGSimpLevel(CAUT_BG_SIMP)
      case "aggressive" => setBGSimpLevel(AGGR_BG_SIMP)
    }
  }

  /**
   * Note that this is called from flags.bgsimp.setValue.
   * @param setFlags whether to set the bgsimp flag to match the new
   * value. Default is true, so if called anywhere other than flag.setValue
   * it also updates the flag.
   */
  def setBGSimpLevel(level: BGSimpLevel, setFlags: Boolean=true) {
    level match {
      case OFF_BG_SIMP => {
	if (setFlags) flags.bgsimp.setValue("off")
	simpRulesTerm = List.empty
	simpRulesLit = List.empty
      } case CAUT_BG_SIMP => {
	if (setFlags) flags.bgsimp.setValue("cautious")
	simpRulesTerm = simpRulesTermSafe
	simpRulesLit =  simpRulesLitSafe
      } case AGGR_BG_SIMP => {
	if (setFlags) flags.bgsimp.setValue("aggressive")
	simpRulesTerm = simpRulesTermSafe ::: simpRulesTermUnsafe
	simpRulesLit = simpRulesLitSafe ::: simpRulesLitUnsafe
      }
    }
  }

  /**
   * Simplify a clause by evaluation of ground BG literals
   * Returns true in its second component iff simplification occurred
   * @return The simplified clause and a boolean flag indicating whether
   * simplification has occurred.
   */
  def simplify(t: Term): (Term, Boolean) = simplify(t,false)

  private def simplify(t: Term, haveSimplified: Boolean): (Term, Boolean) = {

    stats.theorySimp.tried

    var touched = ( false | haveSimplified )
    var ts = t

    //code from simplifyBG for terms
    if (t.sort == IntSort && flags.bgsimp.value == "aggressive") {
      if (main.inPreprocessing) {
        // we do a little more, to get rid off all these is_int, to_int etc
        ts = t.canonical
	//update if canonical is a simplification
	touched |= (ts != t) 
      }else 
	return (t.canonical, (touched || t.canonical != t))
    } //else - continue as normal
    
    ts match {
      case NonDomElemFunTerm(op, args) ⇒ {
        // First simplify the arguments
        val h = 
	  if (args.isEmpty) t 
	  else NonDomElemFunTerm(op, args.map( arg => { val (s, haveTouched) = simplify(arg); touched |= haveTouched; s }))

        // See if h can be simplified at the top level
        for (rule ← simpRulesTerm) try {
          val hSimp = rule(h)

	  stats.theorySimp.succeed
	  // Can possibly simplify further- but how to set touched?
          return simplify(hSimp, true)
        } catch {
          case NotApplicable ⇒ () // try next rule
        }
        // Nothing found
        return (h, touched)
      }
      case _ ⇒ { 
        return (t, touched) // All other cases, i.e. Variables, domain elements
      }
    }
  }

  def simplify(l: Lit): (Lit, Boolean) = simplify(l, false)

  private def simplify(l: Lit, haveSimplified: Boolean): (Lit, Boolean) = {
    stats.theorySimp.tried

// This is not always better, but rather time-consuming
/*    if (util.flags.experimental.value && lhs.sort == LIA.IntSort) {
      // A proper equation, not an inequality
      // Use the rules in PolyAtom for equations and disequations
      // todo: this is a rather obfuscated back and forth between datatypes, fix it
      isPositive match {
        case true => 
          ZeroEQPoly(rhs.asPolynomial.get - lhs.asPolynomial.get).toLit match {
            case Lit(true, Eqn(d1: DomElemInt, d2: DomElemInt)) ⇒
              if (d1.value == d2.value) Lit.TrueLit else Lit.FalseLit
            case l => l
          }
        case false => 
          ZeroNEPoly(rhs.asPolynomial.get - lhs.asPolynomial.get).toLit match {
            case Lit(false, Eqn(d1: DomElemInt, d2: DomElemInt)) ⇒
              if (d1.value != d2.value) Lit.TrueLit else Lit.FalseLit
            case l => l
          }
      }
 } else  {*/
    
    //apply simplifyBG to the arguments of l
    var touched = false | haveSimplified

    var l2 = l

    if (util.flags.bgsimp.value == "aggressive") {
      if (main.inPreprocessing) {
        // we do a little more, to get rid off all these is_int, to_int etc
	touched |= (l.canonical != l)
        l2 = l.canonical
      }else 
        return (l.canonical, touched || (l != l.canonical) )
    }

    val Lit(isPositive, Eqn(lhs, rhs)) = l2

    val lSimpArgs = {
      if (lhs==rhs)
	if (isPositive) Lit.TrueLit else Lit.FalseLit
      else if (l.isPredLit) {
	val (simpLHS,touch) = simplify(lhs)
	if(touch){ 
	  touched = true
	  Lit(isPositive, Eqn(simpLHS, TT))
	}else l
	//Lit(isPositive, Eqn(lhs.simplifyBG, TT))
      }else {//Lit(isPositive, Eqn(lhs.simplifyBG, rhs.simplifyBG))
	val (simpRHS,touch1) = simplify(rhs)
	val (simpLHS,touch2) = simplify(lhs)
	if(touch1 || touch2) {
	  touched = true
	  Lit(isPositive, Eqn(lhs.simplifyBG, rhs.simplifyBG))
	}else l
      }
    }

    //then try each literal simplification rule in turn
    for (rule ← simpRulesLit) try {
      val lSimp = rule(lSimpArgs)
      
      stats.theorySimp.succeed
      
      return simplify(lSimp, true) // Can possibly simplify further
    } catch {
      case NotApplicable ⇒ () // try next rule
    }
    // Nothing found
    return (lSimpArgs, touched)
  }
//moved back to clause as it simply lifts Lit.simplifyBG...
/*
  def simplify(cl: ConsClause): ConsClause = {

    Timer.simplificationBG.start()
    //must filter false lits since the continue condition in simplifyBG requires that
    //the clause length decreases on simplification!
    val res = Clause(cl.lits map { _.simplifyBG } filterNot { _ == Lit.FalseLit}, 
      cl.idxRelevant, cl.age, ByBGSimp(cl, solver.theoryName), cl.deltaConjecture+1)
    Timer.simplificationBG.stop()
    // println(s"simplify($cl:%d) = $res:%d".format(cl.id, res.id))
    if (res.lits == cl.lits)
      cl // avoid mentioning redundant simplification in proof
    else
      res

    // if (touched)
    //    (Clause(h, cl.idxRelevant, cl.age), true)
    // else
    //    (cl, false)

    // println("xx simplification of %s:\n  %s".format(cl, Clause(h, cl.idxRelevant, cl.age)))

  }*/

}
