package beagle.bgtheory

import beagle._
import fol._
import fol.Signature._
import datastructures.Lit
import util._

package object LIA {

  // var signatureLIA = signatureEmpty

  object IntSort extends BGSort("$int") {
    override def toString = "ℤ"
  }

  // signatureLIA += IntSort
  val Arity0Int = Arity0(IntSort)

  val IsIntOpInt = new IsIntOp(IntSort)
  val IsRatOpInt = new IsRatOp(IntSort)
  val IsRealOpInt = new IsRealOp(IntSort)
    
  val ToIntOpInt = new ToIntOp(IntSort)
  val ToRatOpInt = new ToRatOp(IntSort)
  val ToRealOpInt = new ToRealOp(IntSort)

  // Notice only QuotientEOpInt and RemainderEOpInt have been added to LFAOperators
  val QuotientEOpInt = new QuotientEOp(IntSort)
  val QuotientTOpInt = new QuotientTOp(IntSort)
  val QuotientFOpInt = new QuotientFOp(IntSort)

  val RemainderEOpInt = new RemainderEOp(IntSort)
  val RemainderTOpInt = new RemainderTOp(IntSort)
  val RemainderFOpInt = new RemainderFOp(IntSort)


  val NLPPOpInt = new NLPPOp(IntSort)

  val LIAOperators = Set(Sum.op, UMinus.op, GreaterEqn.op, DividesEqn.op, LessEqn.op, 
			 Difference.op, Product.op, LessEqEqn.op, GreaterEqEqn.op, 
			 IsIntOpInt, IsRatOpInt, IsRealOpInt,
			 ToIntOpInt, ToRatOpInt, ToRealOpInt, NLPPOpInt, QuotientEOpInt, RemainderEOpInt,
			 QuotientTOpInt, RemainderTOpInt)
  
  def addStandardOperators(s: Signature) = {
    var res = s
    res += IntSort
    LIAOperators foreach { res += _ } 
    res
  }

  // More equality operators will be added to the current signature during parsing

  val ZeroInt = DomElemInt(0)
  val OneInt = DomElemInt(1)
  val MinusOneInt = DomElemInt(-1)

  IntSort.someElement = DomElemInt(0)

  def isLIA(e: Expression[_]) = e.isBG && (e.sorts subsetOf Set(IntSort)) // && (e.operators subsetOf LIAOperators)

  /* currently unused
  def normalize(l: Lit): Lit = l match {
    case Lit(true, GreaterEqn(t1, t2))    ⇒ Lit(true, LessEqn(t2, t1))
    case Lit(true, GreaterEqEqn(t1, t2))  ⇒ Lit(true, LessEqEqn(t2, t1))
    case Lit(false, GreaterEqn(t1, t2))   ⇒ Lit(true, LessEqEqn(t1, t2))
    case Lit(false, GreaterEqEqn(t1, t2)) ⇒ Lit(true, LessEqn(t1, t2))
    case _                                ⇒ l
  }
*/

}
