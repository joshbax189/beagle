package beagle.bgtheory.LIA

import beagle._
import fol._
import term._
import util._

/**
 * Monomials and Polynomials as a canonical representation of terms
 */

/**
 * Monomials represent c*x.
 */
final case class Monomial(c: Int, x: VarOrSymConst) {

//  def applySubst(sigma: Subst) =
    //if (x.isSymConst) this else Monomial(c, sigma(x.asVar).asInstanceOf[Var])

  def hasUnitCoeff = c == -1 || c == 1

  /** This is only well-defined if n is non-zero */
  def *(n: Int) = {
    assume(n != 0, { println("Multiplying a monomial by 0 should never occur") })
    Monomial(c * n, x)
  }

  /** Assume c is divisible by n */
  def /(n: Int) = {
    assume(c % n == 0, { println("Dividing a monomial by a non-factor: " + this + " / " + n) })
    Monomial(c / n, x)
  }

  lazy val toTerm = c match {
    case 1  ⇒ x.toTerm
    case -1 ⇒ UMinus(x.toTerm)
//    case 2 ⇒ Sum(x.toTerm, x.toTerm)
    case _  ⇒ Product(DomElemInt(c), x.toTerm)
  }

  /** Assume n is not zero */
  override def toString = toTerm.toString
    // c match {
    //   case 1  ⇒ x.toString
    //   case -1 ⇒ "-" + x.toString
    //   case c  ⇒ c + "·" + x.toString
    // }
}

final case class ImproperMonomial(c: Int, t: Term) {
  // assume(t.isInstanceOf[PFunTerm] || t.isInstanceOf[Let] || t.isInstanceOf[ITE], "ImproperMonomial: neither a PFunTerm nor a Let nor an ITE: " + t)

  def hasUnitCoeff = c == -1 || c == 1

  def *(n: Int) = {
    assume(n != 0, { println("Multiplying an (improper) monomial by 0 should never occur") })
    ImproperMonomial(c * n, t)
  }

  /** Assume c is divisible by n */
  def /(n: Int) = {
    assume(c % n == 0, { println("Dividing a monomial by a non-factor: " + this + " / " + n) })
    ImproperMonomial(c / n, t)
  }

  lazy val toTerm = { 
    val h = c match {
      // todo: is this the right thing?
      case 1  ⇒ t
      case -1 ⇒ UMinus(t)
      //    case 2 ⇒ Sum(t, t)
      case _  ⇒ Product(DomElemInt(c), t)
    }
    /*
    // This is too time consuming, cf DAT040=1.p without adding much benefit
    // We need to do it, though to eliminate remainder and quotient and such things. Hack!
    if ((t.operators contains QuotientEOpInt) || (t.operators contains RemainderEOpInt))
      // could carry on with a longer list, is_int etc 
      bgtheory.solver.simplify(h)._1
    else h*/
    //they are eliminated elsewhere
    h
  }

  /** Assume n is not zero */
  override def toString = toTerm.toString
}


/**
 * Invariant: each monomial is over a different variable,
 * which can be achieved by normalizing.
 * @param ms is its (linear) monomials.
 * @param k is the constant in the polynomial.
 */
final case class Polynomial(ms: List[Monomial], k: Int, ims: List[ImproperMonomial]) {

//  lazy val mssorted = ms.sortBy(m ⇒ m.x.toTerm)
//  lazy val imssorted = ims.sortBy(m ⇒ m.t)

  /** The variables or symbolic constants in this polynomial */
  lazy val varOrSymConsts: Set[VarOrSymConst] =
    ms.foldLeft(Set.empty[VarOrSymConst])(_ + _.x) 

  /** The set of variables or symbolic constants in this with a unit coefficient */
  lazy val unitCoeffVarOrSymConsts = ms.filter(_.hasUnitCoeff)
  
  /**
   * A map from a variable or constant to its coefficient, assumes that each
   * variable occurs just once and ignores improper monomials
   */
  lazy val coeffMap: Map[VarOrSymConst, Int] = ms map { m => (m.x->m.c) } toMap

  /** @return this polynomial with monomials sorted according to mssorted. */
  // It seems we do not need this
  // def sorted = Polynomial(mssorted, k)

  lazy val separate = {
    var resLhs = Polynomial(List.empty, 0, List.empty)
    var resRhs = Polynomial(List.empty, 0, List.empty)
    for (m <- ms) if (m.c < 0) resLhs += m * -1 else resRhs += m
    for (im <- ims) if (im.c < 0) resLhs += im * -1 else resRhs += im
    // Remove complexity by replacing a 0 on either side with (-)k:
    if (resLhs == Polynomial.Zero)
      resLhs += -k
    else if (resRhs == Polynomial.Zero)
      resRhs += k
    // Make positive k's
    else if (k < 0) 
      resLhs += -k 
    else 
      resRhs += k
    (resLhs, resRhs)
  }

  lazy val separatePivot = {
    assume(ms.nonEmpty)
    var pivot = ms.head
    var open = ms.tail
    var other = List.empty[Monomial]
    while (open.nonEmpty) {
      val next = open.head
      open = open.tail
      // Use string comparison as total order
      // Check if next is our new pivot candidate
      if (next.x > pivot.x) {
        other ::= pivot // current pivot becomes an other
        pivot = next
      } else
        other ::= next // no change pivot
    }
    (pivot, Polynomial(other.reverse, k, ims))
  }


  /**
   * Make the coefficient of the variable arranged for positive.
   * Makes sense only within EQZERO and NEZERO atoms.
   */
  lazy val withPositiveCoeff = this match {
    case Polynomial(Monomial(c, _) :: _, _, _) if (c < 0) ⇒ this * -1
    case _ ⇒ this
  }

  /**
   * Does not check in ims.
   * @return the coefficient of the variable given which is 0 if the
   * variable does not occur in this poly.
   */
  def getCoeff(x: VarOrSymConst): Int =
    ms find { _.x == x } map { _.c } getOrElse {0}

  /** @return the head monomial and the tail as a new poly. */
  def uncons: Option[(Monomial, Polynomial)] = 
    if(ms.isEmpty) None
    else Some( (ms.head, Polynomial(ms.tail,k,ims)) )

  /**
   * Replace the variable `x` in this by `p`.
   * Assume that `p` does not contain `x`.
   * Assume that this has been rearranged for `x`.
   */
  def replace(x: VarOrSymConst, p: Polynomial) = {
    if(ms.isEmpty) this
    else {
      assume(!p.varOrSymConsts(x), "Must not replace with a polynomial containing x")
      assume(ms.head.x == x || !this.varOrSymConsts(x), s"$this has not been rearranged for $x")
      ms match {
	case Monomial(c, y) :: hms if (y == x) ⇒ Polynomial(hms, k, ims) + (p * c)
	case _                                 ⇒ this
      }
    }
  }

  /** @return The gcd of all coefficients. */
  lazy val gcdCoeffs = {
    val h = ms.map(m => math.abs(m.c)) ::: ims.map(im => math.abs(im.c))
    (if (k==0) { if (h.isEmpty) List(1) else h } else (math.abs(k) :: h)) reduce { gcd(_,_) }
  }

  /** Make the monomial with x the leftmost monomial, if x occurs in this */
  def rearrange(x: VarOrSymConst): Polynomial = {
    if (!(varOrSymConsts(x)) ) return this // shortcut
    val (xms, other) = ms.partition( _.x == x )
    if (!xms.tail.isEmpty) {
      //this was not normalised to begin with
      val c = xms.foldLeft(0)( _ + _.c )
      return Polynomial(Monomial(c,x) :: other, k, ims)
    } else {
      return Polynomial(xms.head :: other, k, ims)
    }
  }

  /** Add a monomial, while preserving the invariant */
  def +(m: Monomial): Polynomial = {
    // Iterate over ms and update or extend
    var skippedMs = List.empty[Monomial]
    var oldMs = ms // Iteration variable
    while (!oldMs.isEmpty) {
      val hm = oldMs.head
      oldMs = oldMs.tail
      if (hm.x == m.x)
        // found it, update the coefficient
        return Polynomial(skippedMs.reverse :::
          (if (hm.c + m.c == 0)
            oldMs
          else
            Monomial(hm.c + m.c, m.x) :: oldMs), k, ims)
      else
        skippedMs ::= hm
    }
    // Coming here means that m was not found - add it
    return Polynomial(m :: ms, k, ims)
  }

  /** Add an improper monomial, while preserving the invariant */
  def +(im: ImproperMonomial): Polynomial = {
    // Iterate over ms and update or extend
    var skippedIms = List.empty[ImproperMonomial]
    var oldIms = ims // Iteration variable
    while (!oldIms.isEmpty) {
      val him = oldIms.head
      oldIms = oldIms.tail
      if (him.t == im.t)
        // found it, update the coefficient
        return Polynomial(ms, k, skippedIms.reverse :::
          (if (him.c + im.c == 0)
            oldIms
          else
            ImproperMonomial(him.c + im.c, im.t) :: oldIms))
      else
        skippedIms ::= him
    }
    // Coming here means that m was not found - add it
    return Polynomial(ms, k, im :: ims)
  }



  /**
   * Push a monomial as the first monomial into this.
   * Applied when the monomial is over the variable the current arrangement is for
   * Assume m's variable is different from the vars in all other ms
   */
  def +:(m: Monomial) = {
    // assume(!(vars contains m.x), "internal error: incorrect usage of +:")
    Polynomial(m :: ms, k, ims)
  }

  /** Add an integer constant to a polynomial */
  def +(n: Int): Polynomial = Polynomial(ms, k + n, ims)
  def -(n: Int): Polynomial = Polynomial(ms, k - n, ims)

  // Subtraction of monomials
  def -(m: Monomial): Polynomial = this + Monomial(-1*m.c,m.x)
  def -(im: ImproperMonomial): Polynomial = this + ImproperMonomial(-1*im.c,im.t)

  // Addition of a polynomial
  def +(p: Polynomial): Polynomial = {
    var res = this + p.k // the constant in p
    // add one monomial in p after the other
    p.ms foreach { m ⇒ res += m }
    p.ims foreach { im ⇒ res += im }
    res
  }

  /** Multiplication by a constant */
  def *(n: Int): Polynomial =
    n match {
      case 0 ⇒ Polynomial(List.empty, 0, List.empty)
      case 1 ⇒ this
      case n ⇒ Polynomial(ms map { _ * n }, k * n, ims map { _ * n })
    }

  /**
   * Division by a constant.
   * Assume every coefficient and the constant are divisble by it.
   */
  def /(n: Int): Polynomial = {
    n match {
      case 1 ⇒ this
      case n ⇒ Polynomial(ms map { _ / n }, k / n, ims map { _ / n })
    }
  }

  /** Divide by the gcd of the coefficients */
  lazy val factorize = this / gcdCoeffs

  /** Subtraction of a polynomial */
  def -(p: Polynomial): Polynomial = this + (p * -1)

  lazy val toTerm = {
    if (ms.isEmpty && ims.isEmpty) 
      DomElemInt(k)
    else {
      val h = ms.map(_.toTerm) ::: ims.map(_.toTerm)
      val allTerms = if (k == 0) h else DomElemInt(k) :: h
      allTerms reduceRight { (m: Term, s: Term) ⇒ Sum(m, s) }
      // val allTerms = ims.map(_.toTerm) ::: ms.map(_.toTerm)
      // if (k == 0)
      //   allTerms.tail.foldRight(allTerms.head)((m: Term, s: Term) ⇒ Sum(m, s))
      // else
      //   allTerms.foldRight(DomElemInt(k).asInstanceOf[Term])((m: Term, s: Term) ⇒ Sum(m, s))
    }
  }

  // Want to reflect term structure:
  override def toString = toTerm.toString

/*
  override def toString = {
    val all = if(k==0) ims ++ ms else ims ++ ms ++ List(k)
    
    if(all.isEmpty) k.toString
    else if(all.tail.isEmpty) all.head.toString
    else
      all.tail.foldLeft(all.head.toString)((a,t) => util.printer.sumFF(a :: t :: Nil))
  }
 */
  
  /** @return This polynomial as an unevaluated scala expression.
   * Only works for single variable polynomials.
   */
  def expr: Option[Int => Int] = this.ms match {
    case Nil => Option( x => k )
    case Monomial(c,x) :: Nil => Option ( x => c * x + k)
    case _ => None
  }

  /** Partial ordering */
  def gtr(that: Polynomial) =
    (this, that) match {
      // Rather limited for now - could normalize msThis and msThat by sorting first
      case (Polynomial(msThis, kThis, Nil), Polynomial(msThat, kThat, Nil)) if msThis == msThat =>
        kThis > kThat
      case _ => false
    }

}

object Polynomial {

  /** Polynomial representing 0 */
  val Zero = Polynomial(List.empty, 0, List.empty)

  def Const(k: Int) = Polynomial(List.empty, k, List.empty)
  
  /**
   * Convert a term to a polynomial.
   * @param allowImproper if set can use `ImproperMonomial` to represent
   * integer sorted FG terms.
   * @throws IllSortedTermFail if `t` is not an integer sorted term.
   * @throws InternalError if the term cannot be converted.
   */
  def toPolynomial(t: Term, allowImproper: Boolean): Polynomial = {
    import Signature._

    if (t.sort != IntSort) throw bgtheory.IllSortedTermFail(t)

    t match {
      case x @ AbstVar(_, _, IntSort) ⇒ Polynomial(List(Monomial(1, x)), 0, List.empty)
      case x @ GenVar(_, _, IntSort) ⇒ Polynomial(List(Monomial(1, x)), 0, List.empty)
      // Treat parameters as variables
      // case BGConst(op) => Polynomial(List(Monomial(1, Var(op.name + "_parameter", 0, IntSort))), 0)
      // case BGConst(op) => Polynomial(List(Monomial(1, Var(op.name, 0, IntSort))), 0)
      case x @ SymConst(Operator(BG, _, Arity0(IntSort))) ⇒ Polynomial(List(Monomial(1, x)), 0, List.empty)
      case DomElemInt(k) ⇒ Polynomial(List.empty, k, List.empty)
      case UMinus(arg) ⇒ toPolynomial(arg, allowImproper) * -1
      case Sum(arg1, arg2) ⇒ toPolynomial(arg1, allowImproper) + toPolynomial(arg2, allowImproper)
      case Difference(arg1, arg2) ⇒ toPolynomial(arg1, allowImproper) - toPolynomial(arg2, allowImproper)
      case p @ Product(arg1, arg2) =>
        (toPolynomial(arg1, allowImproper), toPolynomial(arg2, allowImproper)) match {
          case (Polynomial(Nil, k, Nil), arg2p) ⇒ arg2p * k
          case (arg1p, Polynomial(Nil, k, Nil)) ⇒ arg1p * k
          // Allow e.g. nonlinear multiplication terms
          case (arg1p, arg2p) =>
            if (allowImproper) {
              Polynomial(Nil, 0, List(ImproperMonomial(1, Product(arg1p.toTerm, arg2p.toTerm))))
            }
            else
              throw bgtheory.NonLinearTermFail(t)

        }
      case PFunTerm(op, args) => 
        // Can at least canonicalize the arguments
        if (allowImproper) {
	  Polynomial(Nil, 0, List(ImproperMonomial(1, PFunTerm(op, args map { toCanonical(_) }))))
        }else 
	  //IllSortedTerm fail is caught and can be recovered from in main.
	  //sometimes skolem functions get added and cause this error, but it can be avoided by setting allowImproper
	  //or by skipping the LIA preprocessing.
          throw bgtheory.ConversionError("toPolynomial: cannot normalize this term: " + t)
      case t => 
        // SymConsts, ITE, Let (and possibly others)
        if (allowImproper)
          // let-terms
          Polynomial(Nil, 0, List(ImproperMonomial(1, t)))
        else
          throw bgtheory.ConversionError("toPolynomial: cannot normalize this term: " + t)
    }
  }

  /** Convert to the (improper) polynomial representation if possible
   * otherwise recurse into subterms and make those polynomial.
   * @note It is always possible to convert an integer sorted term simply by
   * using toPolynomial, but canonicalisation can also apply to non-integer
   * terms.
   */
  def toCanonical(t: Term): Term = {
    if(t.sort == IntSort) {
      toPolynomial(t, true).toTerm
    }else
      t match {
	case PFunTerm(op, args) => // Can canonicalize the arguments
          PFunTerm(op, args map { toCanonical(_) })
	case _ => t
      }
  }

}

/**
 * An alternative to Polynomial, using maps to represent monomials.
 * Notice that rearranged no longer makes sense but the functionality of that can
 * be recovered since the internal `ms` map effectively rearranges in constant time
 * so getCoeff == rearrange + ms.head of the normal Polynomial.
 */
case class MapPoly(ms: Map[VarOrSymConst,Int],k: Int) {

  /** The variables or symbolic constants in this polynomial */
  lazy val varOrSymConsts = ms.keys

  /** The set of variables or symbolic constants in this with a unit coefficient */
  lazy val unitCoeffVarOrSymConsts = ms filter { case (x,c) => c == -1 || c == 1 } toSet
  
  /**
   * A map from a variable or constant to its coefficient, assumes that each
   * variable occurs just once and ignores improper monomials
   */
  lazy val coeffMap: Map[VarOrSymConst, Int] = ms 

/* //TODO
  lazy val separate = {
    var resLhs = Polynomial(List.empty, 0, List.empty)
    var resRhs = Polynomial(List.empty, 0, List.empty)
    for (m <- ms) if (m.c < 0) resLhs += m * -1 else resRhs += m
    for (im <- ims) if (im.c < 0) resLhs += im * -1 else resRhs += im
    // Remove complexity by replacing a 0 on either side with (-)k:
    if (resLhs == Polynomial.Zero)
      resLhs += -k
    else if (resRhs == Polynomial.Zero)
      resRhs += k
    // Make positive k's
    else if (k < 0) 
      resLhs += -k 
    else 
      resRhs += k
    (resLhs, resRhs)
  }
*/
  def separate = {
    // for (m <- ms) 
    //   if (m.c < 0) resLhs += m * -1 else resRhs += m

    var (msLhs, msRhs) = ms partition { case (x,c) => c < 0 }

    // Remove complexity by replacing a 0 on either side with (-)k:
    var (resLhs,resRhs) = (MapPoly(msLhs,0) * -1,MapPoly(msRhs,0))
    if (msLhs.isEmpty)
      resLhs += -k
    else if (msRhs.isEmpty)
      resRhs += k
    // Make positive k's
    else if (k < 0) 
      resLhs += -k
    else 
      resRhs += k

    (resLhs,resRhs)
  }

  /**
   * Make the coefficient of the variable arranged for positive.
   * Makes sense only within EQZERO and NEZERO atoms.
   */
/*  lazy val withPositiveCoeff = this match {
    case Polynomial(Monomial(c, _) :: _, _, _) if (c < 0) ⇒ this * -1
    case _ ⇒ this
  }*/
  def normalizeFor(x: VarOrSymConst) = 
    ms.get(x) map { c =>
      if (c < 0) this * -1
      else this
    } getOrElse {
      this
    }

  /**
   * Does not check in ims.
   * @return the coefficient of the variable given or None if the
   * variable does not occur in this poly.
   */
  def getCoeff(x: VarOrSymConst): Option[Int] = ms.get(x)

  def getMon(x: VarOrSymConst): Option[Monomial] = ms.get(x) map { Monomial(_,x) }

  /** @return the head monomial and the tail as a new poly. */
  /*def uncons: Option[(Monomial, Polynomial)] = 
    if(ms.isEmpty) None
    else Some( (ms.head, Polynomial(ms.tail,k,ims)) )*/
  def pop(x: VarOrSymConst): Option[(Monomial,MapPoly)] = 
    getMon(x) map { (_,MapPoly((ms-x),k)) }

  /**
   * Replace the variable x in this by p.
   * Assume that p does not contain x.
   * Assume that this has been rearranged for x.
   */
  def replace(x: VarOrSymConst, p: Polynomial) = {
    assume(!p.varOrSymConsts(x), "Must not replace with a polynomial containing x")
    ms.get(x) map { c =>
      MapPoly((ms-x), k) + (p * c)
    } getOrElse {
      this
    }
  }

  /** @return The gcd of all coefficients. */
  lazy val gcdCoeffs = {
    val h = ms.values.map(math.abs(_))
    (if (k==0) { if (h.isEmpty) List(1) else h } else (math.abs(k) :: h.toList)) reduce { gcd(_,_) }
  }

  def +(m: Monomial): MapPoly = 
    MapPoly(
      (ms.get(m.x) map { c => 
	if(c+m.c == 0) ms
	else ms+(m.x->(c+m.c))
      } getOrElse { //don't have x 
	ms + (m.x->m.c)
      }), k)

  /**
   * Push a monomial as the first monomial into this.
   * Applied when the monomial is over the variable the current arrangement is for
   * Assume m's variable is different from the vars in all other ms
   */
  def +:(m: Monomial) = 
    // assume(!(vars contains m.x), "internal error: incorrect usage of +:")
    MapPoly(ms+(m.x->m.c), k)

  /** Add an integer constant to a polynomial */
  def +(n: Int) = MapPoly(ms, k + n)
  def -(n: Int) = MapPoly(ms, k - n)

  // Subtraction of monomials
  def -(m: Monomial): MapPoly = this + Monomial(-1*m.c,m.x)

  // Addition of a polynomial
  def +(p: Polynomial): MapPoly = 
    p.ms.foldLeft(this + p.k)(_+_)

  /** Multiplication by a constant */
  def *(n: Int): MapPoly =
    n match {
      case 0 ⇒ MapPoly(Map.empty, 0)
      case 1 ⇒ this
      case n ⇒ MapPoly(ms mapValues { _ * n }, k * n)
    }

  /**
   * Division by a constant.
   * Assume every coefficient and the constant are divisble by it.
   */
  def /(n: Int): MapPoly = {
    n match {
      case 1 ⇒ this
      case n ⇒ MapPoly(ms mapValues { _ / n }, k / n)
    }
  }

  /** Divide by the gcd of the coefficients */
  lazy val factorize = this / gcdCoeffs

  /** Subtraction of a polynomial */
  def -(p: Polynomial) = this + (p * -1)

  lazy val toTerm = {
    if (ms.isEmpty) 
      DomElemInt(k)
    else {
      val h = ms map {case (x,c) => Monomial(c,x).toTerm }
      val allTerms = if (k == 0) h else DomElemInt(k) :: h.toList
      allTerms reduceRight { (m: Term, s: Term) ⇒ Sum(m, s) }
    }
  }

  override def toString = toTerm.toString

}


