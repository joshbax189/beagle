package beagle.bgtheory.LIA

import beagle._
import fol._
import term._
import fol.Signature._
import datastructures._
import calculus._
import util._
import bgtheory._

/**
 * Linear Integer Arithmetic
 * Provides methods for quantifier elimination, consistency checking and simplification.
 */

/** Things that are common to each LIA solver */
abstract class LIASolver extends Solver {

  val theoryName = "LIA"

  override def subsumes(cl1: ConsClause, cl2: ConsClause) =
    // rather special case
    (cl1.lits, cl2.lits) match {
      case (Lit(true, LessEqn(d1: DomElemInt, t1)) :: Nil, Lit(true, LessEqn(d2: DomElemInt, t2)) :: _) if (d2.value <= d1.value && t1 ~ t2) => true
      case (Lit(true, LessEqn(d1: DomElemInt, t1)) :: Nil, Lit(false, LessEqn(t2, d2: DomElemInt)) :: _) if (d2.value < d1.value && t1 ~ t2) => true
      case _ => false
    }


  /** The default implementation for LIA QE is cooper */
  def QE(cl: ConsClause) = {
    assume(cl.isBG)
    if (cl.vars.isEmpty)
      List(cl)
    else {
      val f = cooper.QE(cl.toNegFormula, forced = false)
      for (conj ← cnfconversion.toDNF(f))
        yield //Clause(conj map { _.toLit.compl }, cl.idxRelevant, cl.age, ByQE(cl, theoryName), cl.deltaConjecture+1)
          cl.modified(lits = conj map { _.toLit.compl }, 
                      inference = ByQE(cl, theoryName), 
                      deltaConjecture = cl.info.deltaConjecture+1)
    }
  }

  // def QE(f: Formula, params: Iterable[SymConst] = Iterable.empty) = cooper.QE(f, forced = false, params)

  val simpRulesTermSafe = simpRules.simpRulesTermSafe
  val simpRulesTermUnsafe = simpRules.simpRulesTermUnsafe
  val simpRulesLitSafe = simpRules.simpRulesLitSafe
  val simpRulesLitUnsafe = simpRules.simpRulesLitUnsafe
}



