package beagle.bgtheory.LIA.iQE

import beagle._
import fol._
import term._
import util._
import bgtheory._
import LIA._

case class CLOSED(n: Node, falseClause: False) extends Exception // thrown when n.cls contains the empty clause

abstract class Node(val variable: VarOrSymConst) {

  // Abstract members
  def backtrack(relNodes: Set[Int], relClauses: Set[Int]): Node

  // Whether this subsumes cl specific to the subtype of Node
  def specificallySubsumes(cl: QEClause): Boolean

  def show(): Unit

  def haveFiniteDomains(variables: Set[VarOrSymConst]): Boolean

  // Unique identification of this
  val id = iQE.nodeCtr.next()

  // The clauses of this node, which still need to be processed
  var cls = List.empty[QEClause]

  // QEClauses taken from cls and pushed down into the successor.
  // I.e. QEClauses that are conjoined with cls but are no longer there.
  // Used for set up cls on backtracking
  var pushed = List.empty[QEClause]

  var succ: Node = null // The successor of this node, if any
  var pred: Node = null // The predecessor of this node, if any

  var relAnc = Set.empty[Int]// The relevant ancestor nodes, for backtracking
  var relAncClauses = Set.empty[Int]

  override def equals(that: Any) = {
    that match {
      case that: Node ⇒ this.id == that.id
      case _ ⇒ false
    }
  }

  override def hashCode: Int = 41 * id

  // Whether some clause in a collection of clauses subsumes a given clause
  implicit class ClsWithSubsumption(cls: Iterable[QEClause]) {
    def subsumes(cl: QEClause) = cls exists { _ subsumes cl }
  }

  // Whether some QEAtom, treated as a unit clause, in a collection of QEAtoms subsumes a given clause
  implicit class QEAtomsWithSubsumption(ls: Iterable[QEAtom]) {
    def subsumes(cl: QEClause) = ls exists { QEClause(_) subsumes cl }
  }

  // Go through cls and remove subsumed or tautological clauses
  def simplify() {
    val oldCls = cls
    cls = List.empty

    oldCls foreach { cl =>
      if (cl.lits exists { _.isInstanceOf[True] }) {
        if (iQE.debug) println(s"\n--> delete tautology $cl")
      } else if ((cls subsumes cl) ||
        (pushed subsumes cl) ||
        specificallySubsumes(cl)) {
        if (iQE.debug) println(s"\n--> delete subsumed clause $cl")
      } else {
        // println(s"node $id does not subsume $cl")
        cls ::= cl
      }
    }
  }

  // Insert a new Elim node below this 
  def insertElim(variable: VarOrSymConst) {
    if (iQE.debug) println(s"\n--> insert($variable)")
    val next = Elim(variable)
    next.pushed = this.pushed
    next.succ = this.succ
    if (next.succ != null) next.succ.pred = next
    next.pred = this
    succ = next
  }

  // Insert a new Split node below this
  // returns that node
  def insertSplit(variable: VarOrSymConst) = {
    if (iQE.debug) println(s"\n--> insert($variable)")
    val splitNode = Split(variable)
    splitNode.pushed = this.pushed
    splitNode.succ = this.succ
    if (splitNode.succ != null) splitNode.succ.pred = splitNode
    splitNode.pred = this
    succ = splitNode
    splitNode
  }

  // Push cl down into the successor node, which must exist
  def sink(cl: QEClause) {
    if (iQE.debug) println(s"\n--> sink($cl)")
    assume(!cl.isEmpty)
    assume(succ != null)
    pushed ::= cl
    succ.cls ::= cl
    succ.simplify()
  }

  // Select the best clause in cls for processing
  // betterThan is such that no clause is better than a False clause
  def best = {
    assume(!cls.isEmpty)
    var (best, rest) = (cls.head, cls.tail)
    while (rest.nonEmpty) {
      val next = rest.head
      if (next betterThan best) best = next
      rest = rest.tail
    }
    best
  }

  def process() {
    process(best)
  }

  // Process a single clause cl in cls
  def process(cl: QEClause) {
    assume(cls contains cl)

    // Check if cl is empty clause
    cl match {
      case QEClause(List(f: False)) => {
        succ = null // Successors not needed any more
        // println("XXX CLOSED" + f)
        throw CLOSED(this, f)
      }
      case _ => ()
    }

    // remove cl from cls
    cls = cls filterNot { _ == cl }

    cl.lits match {
      // Unit clause:
      // case List(NE(p, o, relNodes)) => {
      //   // Expand into disjunction. todo: something better, a la cooper
      //   // p != o iff o < p or p < o
      //   cls ::= QEClause(LB(o, p, relNodes), UB(p, o, relNodes))
      //   simplify()
      // }
      case List(DIVNot(k, pivot, rest, relNodes, relClauses)) => {
        // Poor man's implementation of non-divisibility. Todo: treat DIVNot natively
        val expansion = (1 to k - 1) map { i => QEAtom.nDivPolyToQEAtom(k, rest + pivot + i, relNodes, relClauses) }
        cls ::= QEClause(expansion.toList)
        simplify()
      }
      // Other unit clause
      case List(l) => {
        // Look at the variable
        // as long as possible sink
        if (variable == l.variable && isInstanceOf[Elim]) {
          // found destination
          asInstanceOf[Elim].put(l)
        } else {
          // variable > l.variable or this is a Split node
          // Sink cl into succ. Create succ if needed
          if (succ == null || l.variable > succ.variable)
            // No, cannot sink
            insertElim(l.variable)
          // Now we can sink as succ.variable >= l.variable
          sink(cl)
        }
      }

      // Non-Unit clause:
      case _ :: _ :: _ => {
        // Must have (variable >= cl.maxVar)
        if ((succ == null) || !(succ.variable > cl.maxVar)) {
          // The second condition is equivalent to cl.maxVar >= succ.variable
          // OR-nodes need to be above elimination nodes, hence the second condition
          // Create the node
          if (iQE.debug) println(s"\n--> split $cl")
          val splitNode = insertSplit(cl.maxVar)
          // push cl into splitNode and split there
          pushed ::= cl
          splitNode.cls = List(QEClause(cl.lits.head added Set(splitNode.id)))
          splitNode.open = cl.lits.tail
        } else {
          // succ != null and succ.variable > cl.maxVar
          sink(cl)
        }
      }
    }
  }

}

case class Split(override val variable: VarOrSymConst) extends Node(variable) {

  // QEAtoms or-ed with this node
  var open = List.empty[QEAtom]

  def specificallySubsumes(cl: QEClause) = false

  def haveFiniteDomains(variables: Set[VarOrSymConst]): Boolean =
    if (succ == null) false else succ.haveFiniteDomains(variables)

  def backtrack(relNodes: Set[Int], relClauses: Set[Int]): Node = {
    succ = null
    if (!(relNodes contains id)) {
      // Irrelevant node
      // Backtrack to predeccessor, if any
      if (iQE.debug) println(s"\n--> skip irrelevant node $id")
      // The root node is never relevant, hence need the check for root node only in the current case
      if (pred == null) {
        // if (iQE.debug) {
        //   println("\n--> INCONSISTENT")
        //   println("Relevant clauses: " + relClauses.mkString("{", ", ", "}"))
        // }
        throw INCONSISTENT(relClauses)
      } else
        pred.backtrack(relNodes ++ relAnc, relClauses ++ relAncClauses)
    } else if (open.nonEmpty) {
        // remove subtree
        succ = null
        cls = pred.pushed
        pushed = List.empty
        // Switch to next case
        cls ::= QEClause(open.head added Set(id))
        open = open.tail
        relAnc ++= relNodes
        relAncClauses ++= relClauses
        simplify()
        this
    } else {
      // Node is relevant but running out of alternatives
      if (iQE.debug) println(s"\n--> no alternatives in node $id")
      pred.backtrack(relNodes ++ relAnc, relClauses ++ relAncClauses)
    }
  }

  def show() {
    println(s"\nSplit($variable) id = $id ---")
    iQE.showListCls("pushed", pushed)
    iQE.showListCls("cls", cls)
    iQE.showListLits("open", open)
  }

}

case class Elim(override val variable: VarOrSymConst) extends Node(variable) {
  // Specific constraints, each empty initialized

  // The equational constraints to be eliminated
  var eqs = List.empty[EQ]
  // The selected equational constraint to eliminate variable
  def selEq = eqs.head

  // The disequational constraints to be eliminated
  var disEqs = List.empty[NE]

  var lbs = List.empty[LB]
  var glbCandidates = List.empty[LB]
  def selGlb = glbCandidates.head

  var elimSymbolic = false

  var elimBySaturation = false
  var elimByEquation = false
  var elimByFiniteDomain = false

  var ubs = List.empty[UB]
  var divs = List(TrivialDIV(variable))

  // The lcm of all coefficents of the pivotal variable x;
  // delta == 0 means "elimination constraints not computed yet"
  // Once delta is > 0, the elimination constraints are kept up to date as an invariant
  var delta = 0

  var nrCases: Int = _ // how many cases there are
  var i: Int = _ // i = 1 .. nrCases

  var closedI = Set.empty[Int] // those values of i that lead to unsatisfiability
  var openI = Set.empty[Int] // those values of i that still need to be checked
  // Invariant: closed union { i } union open = { 1 ..nrCases }

  def isBang = iQE.isBang(variable)

  def hasFiniteDomain = domainLB != None && domainUB != None

  // Whether all given variables have finite domains
  // Looks from this node downwards
  // Todo: imperative version for better efficiency (?)
  def haveFiniteDomains(variables: Set[VarOrSymConst]): Boolean = {
    if (variables exists { _ > variable })
      false // wont find it in successors anyway
    else if (variables contains variable)
      hasFiniteDomain
    else {
      // all variables in variables are smaller than variable
      if (succ == null) false else succ.haveFiniteDomains(variables)
    }
  }

  // Whether elimination is by a symbolic loop increment i
  // Only non-loop increment variables are eliminated symbolically
  // def elimSymbolic = !hasFiniteDomain // !iQE.isBang(variable)

  def domainLB = lbs collectFirst { case LB(Polynomial(Nil, k, Nil), Monomial(1, _), _, _) => k }
  def domainUB = ubs collectFirst { case UB(Monomial(1, _), Polynomial(Nil, k, Nil), _, _) => k }


  // Make the constraint saying that glb is a lower bound for lb
  def makeGLBConstraint(glb: LB, lb: LB, strict: Boolean) = {
    val inc = if (strict) 0 else 1
    val (LB(s, Monomial(a, _), relNodes, relClauses), LB(s1, Monomial(a1, _), relNodes1, relClauses1)) = (glb, lb)
    QEClause(QEAtom.zeroLTPolyToQEAtom(s * a1 - s1 * a + inc, relNodes ++ relNodes1 + id, relClauses ++ relClauses1))
  }

  // All units not in cls that belong to this node (conjoined with cls)
  def allLits: List[QEAtom] = eqs ::: disEqs ::: lbs ::: ubs ::: divs

  def allVariables = allLits.foldLeft(Set.empty[VarOrSymConst])(_ + _.variable)

  def specificallySubsumes(cl: QEClause) = allLits subsumes cl

  // Removes all clauses and literals related to elimination
  // and restores cls as they were initially
  def uneliminate() {
    cls = pred.pushed
    pushed = List.empty
    delta = 0
    succ = null
  }

  // Make the constraint entailed by an equation ax = s and another atom, for eliminating x
  // Eg for bx < t we get abx < at and abx = bs, hence bs<at
  def elimByEqConstraint(eq: EQ, l: QEAtom): QEClause = {
    assume(eq.variable == l.variable)
    val EQ(Monomial(a, _), s, eqRelNodes, eqRelClauses) = eq
    // ax = s
    val lElim = l match {
      // bx = t => bs - at = 0
      case EQ(Monomial(b, _), t, relNodes, relClauses) =>
        QEAtom.zeroEQPolyToQEAtom(s * b - t * a, relNodes ++ eqRelNodes, relClauses ++ eqRelClauses)
      // bx /= t => bs - at /= 0
      case NE(Monomial(b, _), t, relNodes, relClauses) =>
        QEAtom.zeroNEPolyToQEAtom(s * b - t * a, relNodes ++ eqRelNodes, relClauses ++ eqRelClauses)
      // t < bx => 0 < bs - at
      case LB(t, Monomial(b, _), relNodes, relClauses) =>
        QEAtom.zeroLTPolyToQEAtom(s * b - t * a, relNodes ++ eqRelNodes, relClauses ++ eqRelClauses)
      // bx < t => 0 < at - bs
      case UB(Monomial(b, _), t, relNodes, relClauses) =>
        QEAtom.zeroLTPolyToQEAtom(t * a - s * b, relNodes ++ eqRelNodes, relClauses ++ eqRelClauses)
      // k | bx + t => ak | bs + at
      case DIV(k, Monomial(b, _), t, relNodes, relClauses) =>
        QEAtom.nDivPolyToQEAtom(k * a, s * b + t * a, relNodes ++ eqRelNodes, relClauses ++ eqRelClauses)
      case DIVNot(k, Monomial(b, _), t, relNodes, relClauses) =>
        QEAtom.nNotDivPolyToQEAtom(k * a, s * b + t * a, relNodes ++ eqRelNodes, relClauses ++ eqRelClauses)
    }
    // println(s"elimByEqConstraint($eq, $l) = $lElim")
    QEClause(lElim)
  }

  def elimByEqConstraint(l: QEAtom): QEClause = elimByEqConstraint(selEq, l)

  def elimByFdConstraint(l: QEAtom) = {
    assume(elimByFiniteDomain)
    assume(!elimSymbolic)
    assume(hasFiniteDomain)
    val lb = domainLB.get
    // hOfI is the polynomial replacing x
    val hOfI = lb + i
    val xEqHofI = EQ(Monomial(1, variable), Polynomial.Zero + hOfI, Set(id), Set.empty)
    elimByEqConstraint(xEqHofI, l)
  }

  // put l in the proper slot
  def put(l: QEAtom) {
    assume(l.variable == variable)
    if (iQE.debug) println(s"\n--> put($l)")

    // If delta == 0 don't need to do anything, except putting l into its slot
    // Same for bang variables which are treated externally
    if (delta > 0) {
      if (elimByFiniteDomain)
        cls ::= elimByFdConstraint(l)
      else if (elimByEquation)
        cls ::= elimByEqConstraint(l)
      else {
        // Here we know eqs.isEmpty
        // Here we know we are not eliminating by FD constraints
        // Elimination by lb or by saturation
        l match {
          case eq: EQ => uneliminate()
            // Introduction of eq invalidates current elimination and switches mode
            // to elimination by equation
          case ne: NE => {
            if (elimBySaturation)
              // Unfortuntaely, disequations do not seem to be compatible with elimination by saturation
              // (unless expanding disequations into disjunctions, which is prohibitively expensive)
              // Hence make mode switch
              uneliminate()
            else {
              // Elimination by bounds. A disequation counts as a lower bound
              if (lbs.nonEmpty && ubs.nonEmpty)
                // If lbs or ubs is empty work with large/small solutions, hence nothing to do
                // Maintain elimination status
                cls ::= elimNEConstraint(ne)
            }
          }
          case (lb: LB) => {
            if (elimBySaturation) {
              if (lb.coeff == 1) 
                // Can continue elimination by saturation
                // Maintain elimination status
                cls :::= (for (ub <- ubs) yield QEClause(lb resolve ub))
              else
                // lb.coeff != 1
                // Cannot continue elimination by saturation
                // todo: improve
                uneliminate()
            } else {
              if (ubs.nonEmpty) {
                // If ubs is empty work with large solutions, hence nothing to do
                if (lbs.nonEmpty)
                  // Maintain elimination status
                  cls ::= makeGLBConstraint(selGlb, lb, strict = false)
                else
                  // Adding the first lb invalidates current elimination by small solutions
                  // Also will possibly switch to elimination by finite domain
                  uneliminate()
              }
            }
          }
          case (ub: UB) => {
            if (elimBySaturation) {
              // Can continue elimination by saturation
              // Maintain elimination status
              cls :::= (for (lb <- lbs) yield QEClause(lb resolve ub))
            } else {
              if (lbs.nonEmpty) {
                // If lbs is empty work with small solutions, hence nothing to do
                if (ubs.nonEmpty) {
                  // Maintain elimination status
                  cls ::= elimUBConstraint(ub)
                } else
                  // Adding the first ub invalidates current elimination by big solutions
                  // Also will possibly switch to elimination by finite domain
                  uneliminate()
              }
            }
          }
          case (div: DIV) => {
            if (elimBySaturation) {
              if (div.k == 1) {
                // Can continue elimination by saturation
                // No need to do anything as divisibility constraint is trivial
                ()
              } else {
                // div.k != 1
                // Cannot continue elimination by saturation
                // todo: improve
                uneliminate()
              }
            } else {
              // update elimination constraints
              val DIV(k, Monomial(c, _), u, _, _) = div
              val delta1 = lcm(delta, c)
              if (delta1 == delta) {
                // No change in delta
                val nrCases1 = nrCases
                nrCases = lcm(nrCases, (delta / c) * k)
                if (elimSymbolic && nrCases1 != nrCases)
                  // need to update upper bound for i
                  uneliminate()
                else 
                  cls :::= elimDivsConstraints(List(div))
              } else
                uneliminate()
            }
          }
        }
      }
    }

    // Put l into its slot
    l match {
      case eq: EQ => eqs :+= eq
      case ne: NE => {
        disEqs :+= ne
        glbCandidates :+= ne.toLB
      }
      case lb: LB => {
        lbs :+= lb
        glbCandidates :+= lb
      }
      case ub: UB => {
        ubs :+= ub
      }
      case div: DIV => divs :+= div
    }
    simplify()
  }

  lazy val iAsPolynomial = Polynomial.toPolynomial(iQE.variable("!i_" + id.toString), false)

  // h(i) is the polynomial replacing delta*x
  // The selected equation ax = s, if present, is taken as the lower bound (delta/a)*s - 1
  def hOfI = {
    assume(!elimBySaturation)
    assume(!elimByFiniteDomain)
    if (elimByEquation) {
      val EQ(Monomial(a, _), s, relNodes, relClauses) = selEq
      // Hence delta * x = (delta / a) * s
      // Set h(i) = ((delta / a) * s - 1) + i
      // Can have h(i) = delta * x iff i=1
      // require(i == 1)
      (s * (delta / a), relNodes + id, relClauses) // +id not needed, but doesn' make a difference as nrCases = 1
    } else {
      if (lbs.nonEmpty && ubs.nonEmpty) {
        val LB(s, Monomial(a, _), relNodes, relClauses) = selGlb
        if (elimSymbolic)
          (s * (delta / a) + iAsPolynomial, relNodes + id, relClauses)
        else
          (s * (delta / a) + i, relNodes + id, relClauses)
      } else {
        // work with big/small solutions to satisfy inequations and disequations
        if (elimSymbolic)
          (Polynomial.Zero + iAsPolynomial, Set(id), Set.empty)
        else
          (Polynomial.Zero + i, Set(id), Set.empty)
      }
    }
  }

  def elimUBConstraint(ub: UB) = {
    assume(!elimBySaturation)
    val hi = hOfI
    val UB(Monomial(b, _), t, relNodesUB, relClausesUB) = ub
    QEClause(QEAtom.zeroLTPolyToQEAtom(t * delta - hi._1 * b, hi._2 ++ relNodesUB, hi._3 ++ relClausesUB))
  }

  // Create for case i the elimination constraints for the lub in effect,
  // or all ubs if not eliminating ubs by cases
  // If bx < t is a (l)ub, its
  // elimination constraint is h(i) < (delta/b)*t
  def elimUBsConstraints = {
    assume(!elimByEquation)
    assume(!elimBySaturation)
    assume(lbs.nonEmpty) // otherwise work with big/small solutions
    assume(ubs.nonEmpty)
    ubs map { elimUBConstraint(_) }
  }

  // Create the maximality constraints for the GLB and LUB in effect
  def elimMaxConstraints = {
    assume(!elimByEquation)
    assume(!elimBySaturation)
    assume(lbs.nonEmpty) // otherwise work with big/small solutions
    assume(ubs.nonEmpty)
    lbs map { makeGLBConstraint(selGlb, _, strict = false) }
  }

  def elimNEConstraint(ne: NE) = {
    assume(!elimBySaturation)
    val hi = hOfI
    val NE(Monomial(b, _), t, relNodesUB, relClausesUB) = ne
    QEClause(QEAtom.zeroNEPolyToQEAtom(t * delta - hi._1 * b, hi._2 ++ relNodesUB, hi._3 ++ relClausesUB))
  }

  def elimNEsConstraints = {
    assume(!elimByEquation)
    assume(!elimBySaturation)
    assume(lbs.nonEmpty) // otherwise work with big/small solutions
    assume(ubs.nonEmpty)
    disEqs map { elimNEConstraint(_) }
  }

  // Let k|cx+u be a divisibility constraint.
  // Its elimination constraint is (delta/c)*k | (h(i) + (delta/c)*u)
  // Notice the divisibility constraints include 1 | x, which becomes delta | delta*x,
  // and so its elimination constraint is delta | h , as required
  // Works for both elimination by Lb and elimination by Eq
  def elimDivsConstraints(divs: List[DIV]) = {
    val hi = hOfI
    divs map {
      case DIV(k, Monomial(c, _), u, relNodesDiv, relClausesDiv) =>
        QEClause(QEAtom.nDivPolyToQEAtom((delta / c) * k, hi._1 + u * (delta / c), hi._2 ++ relNodesDiv, hi._3 ++ relClausesDiv))
    }
  }

  def elimByLbConstraints = {
    assume(!elimBySaturation)
    if (elimByFiniteDomain)
      elimByFdConstraints
    else if (lbs.nonEmpty && ubs.nonEmpty)
      elimMaxConstraints ::: elimUBsConstraints ::: elimNEsConstraints ::: elimDivsConstraints(divs)
    else
      elimDivsConstraints(divs)
  }

  def elimByEqConstraints = allLits map { elimByEqConstraint(_) }
  def elimByFdConstraints = allLits map { elimByFdConstraint(_) }

  def finalizeEliminate() {
    // The number of cases is given as the lcm of all coefficients of
    // the normalized divisibility constraints
    def numberOfCases = lcm(divs map { case DIV(k, Monomial(c, _), _, _, _) => (delta / c) * k })
    def divsCoeffs = (divs map { _.coeff })

    if (elimByFiniteDomain)
      delta = 1 // irrelevant but set to indicate elimination
    else if (lbs.nonEmpty && ubs.nonEmpty)
      delta = lcm(selGlb.coeff :: divsCoeffs)
    else
      // Use big/small solutions
      // Need to look at the divisibility constraints only
      delta = lcm(divsCoeffs)

    if (elimSymbolic) {
      nrCases = 1
      i = 0 // irrelevant
      closedI = Set.empty
      openI = Set.empty
      // add 0 < i
      cls ::= QEClause(QEAtom.zeroLTPolyToQEAtom(iAsPolynomial, Set(id), Set.empty))
      // add i =< nrCases equiv 0 =< nrCases - i equiv 0 < nrCases - i + 1
      cls ::= QEClause(QEAtom.zeroLTPolyToQEAtom(iAsPolynomial * -1 + numberOfCases + 1, Set(id), Set.empty))
    } else if (elimByFiniteDomain) {
      nrCases = domainUB.get - domainLB.get - 1 // could be negative, but elimByLbConstraints will generate false then
      i = 1 
      closedI = Set.empty
      openI = (2 to nrCases).toSet
    } else {
      // Compute the number of cases to count up to as the lcm
      // of the k's of the normalized divisibility constraints
      nrCases = numberOfCases
      // if (nrCases > 100) sys.exit()
      i = 1 // The first case
      closedI = Set.empty
      openI = (2 to nrCases).toSet
    }

    // The elimination constraints in case of GLB/LUB/DIVs constraints
    cls :::= elimByLbConstraints
    simplify()
  }


  // Eliminate the variable of this node
  // Recomputes everything related to elimination from scratch:
  // in particular kind of elimination and Lub/Glb candidates
  // This means eliminate() cannot be used for backtracking
  def eliminate() {

    // Whether can saturate. Saturation is ggod, no branching
    def canSaturate = (disEqs.isEmpty && ((lbs ::: divs) forall { _.coeff == 1 }) && (divs forall { _.k == 1 }))

    if (delta > 0) return // already eliminated

    // If all variables in this node have finite domains
    // we eliminate concretely. Alternatively,
    // the sub-branch from this node onwards could be given to a FD solver
    // Bug/problem: elimSymbolic once true should become false later after
    // enough finite domain constraints have been added.
    if (
      true // todo: elim symbolic once choco is coupled
      // haveFiniteDomains(allVariables)
      // isBang
    ) {
      // Need to concretely solve it
      elimSymbolic = false
    }
    else {
      elimSymbolic = true
    }

    // Heuristics - todo: take nrCases into account
    if (false && // todo: for now no enumeration
      !elimSymbolic &&
        hasFiniteDomain && // neccessary condition
        eqs.nonEmpty && // Equations are always better
      !canSaturate) { // Saturation is always better
      if (iQE.debug) println(s"\n--> eliminate variable $variable by finite domain enumeration")
      elimByEquation = false
      elimBySaturation = false
      elimByFiniteDomain = true
      delta = 1 // irrelevant, but set to 1 to indicate elimination
      nrCases = domainUB.get - domainLB.get - 1
      i = 1 
      closedI = Set.empty
      openI = Set.empty
      finalizeEliminate()
      return
    }

    if (eqs.nonEmpty) {
      if (iQE.debug) println(s"\n--> eliminate variable $variable by equation")
      elimByEquation = true
      elimBySaturation = false
      elimByFiniteDomain = false
      delta = 1
      nrCases = 1
      i = 1 // The only case
      closedI = Set.empty
      openI = Set.empty
      cls :::= elimByEqConstraints
      simplify()
      return
    }

    if (canSaturate) {
      // todo: how to do resolution in presence of disequations?
      if (iQE.debug) println(s"\n--> eliminate variable $variable by saturation")
      elimByEquation = false
      elimBySaturation = true
      elimByFiniteDomain = false
      delta = 1
      nrCases = 1
      i = 1 // The only case
      closedI = Set.empty
      openI = Set.empty
      // exhaustive resolution of all lb-ub pairs
      cls :::= (for (lb <- lbs; ub <- ubs) yield QEClause(lb resolve ub))
      simplify()
      return
    }

    // Eliminate by lower bounds
    if (iQE.debug) println(s"\n--> eliminate variable $variable by lower bounds")
    elimByEquation = false
    elimBySaturation = false
    elimByFiniteDomain = false
    // disEquations also provide lower bounds
    glbCandidates = lbs ::: (disEqs map { _.toLB })
    finalizeEliminate()
  }

  def backtrack(relNodes: Set[Int], relClauses: Set[Int]): Node = {
    succ = null
    if (!(relNodes contains id)) {
      // Irrelevant node
      // Backtrack to predeccessor
      if (iQE.debug) println(s"\n--> skip irrelevant node $id")
      return pred.backtrack(relNodes ++ relAnc, relClauses ++ relAncClauses)
    }

    // The node is relevant and so we need to explore the alternatives generated by elimination, if any

    // If delta == 0 then elimination has not taken place, and so there are trivially no alternaves to explore
    if (delta == 0)
      return pred.backtrack(relNodes ++ relAnc, relClauses ++ relAncClauses)

    // In case of an equation openI is always empty
    // In case of symbolic openI is also alway empty
    if (openI.nonEmpty) {
      // assume(!elimByEquation)
      assume(!elimBySaturation)
      assume(!elimSymbolic)
      // valid elimination case
      closedI += i // current case done
      i = openI.head // select some next case randomly
      openI = openI.tail
      // Invalidate clauses from current elimination case
      cls = pred.pushed
      succ = null
      pushed = List.empty
      // Generate elimination constraints for current case
      cls :::= elimByLbConstraints
      relAnc ++= relNodes
      relAncClauses ++= relClauses
      simplify()
      return this
    }

    // if glbCandidates is empty there is no glb in consideration
    // if glbCandidates.tail is empty we have just considered the last case for a glb
    // if ubs is empty work with large solutions and so there is nothing to di
    if (!elimByEquation && !elimByFiniteDomain && !elimBySaturation && glbCandidates.nonEmpty && glbCandidates.tail.nonEmpty && ubs.nonEmpty) {
      uneliminate()
      glbCandidates = glbCandidates.tail
      relAnc ++= relNodes
      relAncClauses ++= relClauses
      // Maintain elimination invariant (as delta > 0)
      // Recompute delta as per current case
      finalizeEliminate()
      return this
    }

    // All alternatives exhausted
    if (iQE.debug) println(s"\n--> no alternatives in node $id")
    return pred.backtrack(relNodes ++ relAnc, relClauses ++ relAncClauses)
  }

  def show() {
    println(s"\nElim($variable) id = $id ---")
    iQE.showListCls("pushed", pushed)
    iQE.showListCls("cls", cls)
    println("  elimSymbolic = " + elimSymbolic)
    println("  elimBySaturation = " + elimBySaturation)
    println("  elimByEquation = " + elimByEquation)
    println("  elimByFiniteDomain = " + elimByFiniteDomain)
    iQE.showListLits("eqs", eqs)
    println("  selEq = " + (if (eqs.nonEmpty) selEq else "N/A"))
    iQE.showListLits("disEqs", disEqs)
    iQE.showListLits("lbs", lbs)
    iQE.showListLits("glbCandidates", glbCandidates)
    println("  selGlb = " + (if (glbCandidates.nonEmpty) selGlb else "N/A"))
    iQE.showListLits("ubs", ubs)
    iQE.showListLits("divs", divs)
    println("  delta = " + delta)
    println("  nrCases = " + nrCases)
    println("  i = " + i)
    println("  closedI = " + closedI)
    println("  openI = " + openI)
  }

}

case class INCONSISTENT(relClauses: Set[Int]) extends Exception // Thrown when branch.check() determines unsatisfiability

class Branch(initNode: Node) {

  def this() {
    this(Split(iQE.variable("~")))
  }

  // Current is some node in the branch
  // It is the only access point
  // Invariant: n.cls is empty for all predeccessor nodes of current
  // This is used to quickly find the next node to process
  var current = initNode

  // All variables of all constraints ever put into this branch
  // Might be a superset of the actually occurring ones
  // var vars = Set.empty[VarOrSymConst]

  // The first node in the branch
  def first = {
    var hn = current
    while (hn.pred != null) hn = hn.pred
    hn
  }

  // The last node in the branch
  def last = {
    var hn = current
    while (hn.succ != null) hn = hn.succ
    hn
  }

  def toList = {
    var hn = last
    var res = List(last)
    while (hn.pred != null) {
      hn = hn.pred
      res ::= hn
    }
    res
  }

  // add a clause to first, i.e., "globally"
  def add(cl: QEClause) {
    current = first // maintains invariant
    current.cls ::= cl
    current.simplify()
    // vars ++= cl.vars
  }

  def show() {
    println("\n=== Branch ===")
    // println("vars: " + vars.mkString("{", ", ", "}"))
    var hn = first
    do {
      hn.show()
      hn = hn.succ
    } while (hn != null)
    //println("------ End Branch ------")
  }

  def checkClosed() {
    var hn = first
    while (hn != null) {
      hn.cls foreach {
        _ match {
          case QEClause(List(cl: False)) => {
            hn.succ = null // Successors not needed any more
            throw CLOSED(hn, cl)
          }
          case _ => ()
        }
      }
      hn = hn.succ
    }
  }

  def processTopDown() {

    current = first
    // Remove all clauses in cls from the current node
    def processCurrent() {
      while (current.cls.nonEmpty) {
        current.process()
        if (iQE.debug) show()
        checkClosed() // Additional check, empty clause could result by eager variable elimination
      }
    }

    processCurrent()
    // process successor nodes while there are any
    while (current.succ != null) {
      current = current.succ
      processCurrent()
    }
  }

  def processBottomUp() {

    // Return true if something changed
    def processLast() = {
      current = last
      while (current.cls.isEmpty && current.pred != null)
        current = current.pred
      if (current.cls.nonEmpty) {
        current.process()
        if (iQE.debug) show()
        checkClosed() // Additional check, empty clause could result by eager variable elimination
        true
      } else
        false
    }

    while (processLast()) {
      // nothing
    }
  }

  def processBest() {

    var nodes = toList filterNot { _.cls.isEmpty }

    while (nodes.nonEmpty) {

      // find the best clause in cls among all nodes
      var (bestNode, bestClause, rest) = (nodes.head, nodes.head.best, nodes.tail)

      while (rest.nonEmpty) {
        val (nextNode, nextClause) = (rest.head, rest.head.best)
        if (nextClause betterThan bestClause) {
          bestNode = nextNode
          bestClause = nextClause
        }
        rest = rest.tail
      }
      bestNode.process(bestClause)
      if (iQE.debug) show()
      // process removes cl from bestNode.cls
      nodes = toList filterNot { _.cls.isEmpty }
    }
  }

  // Eliminate some variable by generating elimination constraints; return true on success
  def eliminateTopDown(): Boolean = {

    var hn = first
    // Need to find an Elim node with a variable to be eliminated, if it exists
    do {
      hn match {
        case elim: Elim => {
          if (elim.delta == 0) {
            // need to eliminate
            elim.eliminate()
            if (iQE.debug) show()
            return true
          } else
            hn = elim.succ // already eliminated
        }
        case _ => hn = hn.succ
      }
    } while (hn != null)
    // couldn't find a node that eliminated a variable
    return false
  }

  def eliminateBottomUp(): Boolean = {

    var hn = last
    // Need to find a DIVs node with a variable to be eliminated, if it exists
    // Need to find an Elim node with a variable to be eliminated, if it exists
    do {
      hn match {
        case elim: Elim => {
          if (elim.delta == 0) {
            // need to eliminate
            elim.eliminate()
            if (iQE.debug) show()
            return true
          } else
            hn = elim.pred // already eliminated
        }
        case _ => hn = hn.pred
      }
    } while (hn != null)
    // couldn't find a node that eliminated a variable
    return false
  }

  // Prefer equational node for elimination, otherwise bottom-up
  def eliminateBest(): Boolean = {
    var hn = last
    do {
      hn match {
        case elim: Elim => {
          if (elim.delta == 0 && elim.elimByEquation) {
            // need to eliminate
            elim.eliminate()
            if (iQE.debug) show()
            return true
          } else
            hn = elim.pred // already eliminated
        }
        case _ => hn = hn.pred
      }
    } while (hn != null)
    return eliminateBottomUp()
  }

  def process() = processTopDown()
  // def process() = processBottomUp() // seems to be better, cf. GEG024=1.p
  // def process() = processBest() // seems to be better, cf. GEG024=1.p

  // def eliminate() = eliminateTopDown()
  // def eliminate() = eliminateBottomUp()
  def eliminate() = eliminateBest()

  // todo: eliminate variables with unit coefficients first

  // Main routine: check() manipulates this until an arguably satisfiable state has been reached
  // or throws CLOSED if arguably unsatisfiable.
  def check() {
    if (iQE.debug) show()
    var continue = true
    while (continue) {
      try {
        process()
        // coming here means that all nodes have been processed, leading to a satisfiable state
        // unless some variable can be eliminated
        continue = eliminate()
        // continue = false // do eager elimination now
      } catch {
        case CLOSED(n, False(relNodes, relClauses)) => {
          // println("XXX CHECK " + relNodes + relClauses)
          // n.show()
          // sys.exit()
          if (iQE.debug) println(s"\n--> backtrack")
          current = n.backtrack(relNodes, relClauses)
          current = first // don't rely on backtrack maintaining the invariant
          if (iQE.debug) show()
          // continue is true
        }
      }
    }
  }
}

object iQE {

  val debug = false

  def variable(name: String) = SymConst(Operator(BG, name, Arity0(IntSort)))
  def isBang(x: VarOrSymConst)=
    x match {
      case SymConst(Operator(BG, name, Arity0(IntSort))) if name.nonEmpty => name.head == '!'
      case _ => false
    }

  def isConsistent(cls: Iterable[QEClause]): SolverResult = {
    val b = new Branch()
    cls foreach { b.add(_) }
    if (debug) {
      println("==== BG Clauses sent to iQE ====")
      cls foreach { println(_) }
      println("================================")
    }
    try {
      b.check()
      // if (!debug) b.show()
      if (debug) println("CONSISTENT")
      return SAT
    } catch {
      case INCONSISTENT(core) => {
        if (debug) {
          println("INCONSISTENT")
          println("Relevant clauses:")
          for (cl <- cls) {
            // For each literal in an input QEClause its relClauses are a singleton
            // set consisting of the clause id of the clause it stems from
            // Furthermore these singleton sets are the same in each literal
            if (core contains cl(0).relClauses.head)
              println(cl)
          }
        }
        return UNSAT(Some(core.toList))
        // b.show()
      }
    }
    // b.show()
  }

  def showListCls(header: String = "", cls: List[QEClause]) {
    if (header != "") println("  " + header)
    cls foreach { cl => println("    " + cl) }
  }

  def showListLits(header: String = "", lits: List[QEAtom]) {
    if (header != "") println("  " + header)
    lits foreach { l => println("    " + l) }
  }

  def showOpt(header: String = "", litOpt: Option[QEAtom]) {
    if (header != "") println("  " + header + " = " + litOpt.getOrElse("None"))
  }

  val nodeCtr = new util.Counter()

}
