package beagle.bgtheory.LIA

import beagle._
import fol.term._
import fol.Operator
import fol.term.DomElem
import fol.BG
import fol.Arity0
import fol.Arity1
import fol.Arity2
import datastructures.PredEqn
import datastructures.Eqn

import fol.Signature.OSort

case class DomElemInt(i: Int) extends DomElem[Int] {
  val h = this
  lazy val op = new Operator(BG, i.toString, Arity0Int) {
    override def toTerm = Some(h)
  }
  override lazy val sort = IntSort
  val value = i
  // doesn't work, cf ARI621=1.p
  // override val weightForKBO = value.toString.length
}

/* Predicates */

object LessEqEqn {
  val op = new Operator(BG, "$lesseq", Arity2((IntSort, IntSort) -> OSort)) { 
      override def formatFn = util.printer.lessEqFF 
    }

  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == IntSort && rhs.sort == IntSort)
    PredEqn(PFunTerm(op, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(op1, List(lhs, rhs))) 
       if(op1==op) => Some((lhs, rhs))
    case _ => None
  }
}

object LessEqn {
  val op = new Operator(BG, "$less", Arity2((IntSort, IntSort) -> OSort)) { 
    override def formatFn = util.printer.lessFF 
  }

  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == IntSort && rhs.sort == IntSort)
    PredEqn(PFunTerm(op, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(op1, List(lhs, rhs))) 
      if(op1==op) => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqEqn {
  val op = new Operator(BG, "$greatereq", Arity2((IntSort, IntSort) -> OSort)) { 
    override def formatFn = util.printer.greaterEqFF 
  }

  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == IntSort && rhs.sort == IntSort)
    PredEqn(PFunTerm(op, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(op1, List(lhs, rhs))) 
      if(op1==op) => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqn {
  val op = new Operator(BG, "$greater", Arity2((IntSort, IntSort) -> OSort)) { 
    override def formatFn = util.printer.greaterFF 
  }
  
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == IntSort && rhs.sort == IntSort)
    PredEqn(PFunTerm(op, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(op1, List(lhs, rhs))) 
      if(op1==op) => Some((lhs, rhs))
    case _ => None
  }
}

object DividesEqn {
  val op = new Operator(BG, "$divides", Arity2((IntSort, IntSort) -> OSort)) { 
    override def formatFn = util.printer.dividesFF 
  }

  def apply(s: Term, t: Term) = PredEqn(PFunTerm(op, List(s, t)))      
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(op1, List(s, t))) 
      if(op1==op) => Some((s, t))
    case _ => None
  }
}

/* Operators */

object UMinus {
  val op = new Operator(BG, "$uminus", Arity1(IntSort -> IntSort)) { 
    override def formatFn = util.printer.uminusFF 
  }

  def apply(s: Term) = PFunTerm(op, List(s))

  def unapply(s: Term) = s match {
    case PFunTerm(op1, List(t)) if(op1==op) ⇒ Some(t)
    case _                                        ⇒ None
  }
}

object Sum {
  val op = new Operator(BG, "$sum", Arity2((IntSort, IntSort) -> IntSort)) { 
    override def formatFn = util.printer.sumFF 
  }

  def apply(s1: Term, s2: Term) = PFunTerm(op, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(op1, List(t1, t2)) if(op1==op) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Difference {
  val op = new Operator(BG, "$difference", Arity2((IntSort, IntSort) -> IntSort)) { 
    override def formatFn = util.printer.differenceFF 
  }

  def apply(s1: Term, s2: Term) = PFunTerm(op, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(op1, List(t1, t2)) if(op1==op) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Product {
  val op = new Operator(BG, "$product", Arity2((IntSort, IntSort) -> IntSort)) { 
    override def formatFn = util.printer.productFF 
  }

  def apply(s1: Term, s2: Term) = PFunTerm(op, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(op1, List(t1, t2)) if(op1==op) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}
