package beagle.bgtheory.LIA

import beagle._
import fol._
import term._
import util._
import datastructures.ConsClause

package object cooper {


  var isConsistentCnt = 0

  /**
   * The usual isConsistent specialised for lists of *ground* clauses.
   */
  def isConsistent(cls: Iterable[ConsClause]): Boolean = {
    //import cooper._

    reporter.debugBG(s"cooper.isConsistent (ground clauses) called on $cls")
    isConsistentCnt += 1
    util.Timer.cooperCalls.start()
    val f1 = cls.map(cl => cooper.toPolyAtomFormula(cl.toFormula)).toAnd
    //roughly cooper is:
    val f2 = cooper.eliminateMany(f1, f1.symConsts)
    util.Timer.cooperCalls.stop()
    val res = f2.simplifyCheap
    // We need to evaluate f2 to get the conclusive result
    res != FalseAtom
  }

  /**
   * This is the entry point to the Cooper algorithm.
   * Removes from f all *explicit* quantifiers and the also the given parameters.
   * Parameter forced, if true, enforces always quantifier elimination,
   * even if f is quantifier-free and contains no parameters.
   * The result in theis case will always be TrueAtom or FalseAtom,
   * so that QE acts as a decision procedure.
   */
  def QE(f: Formula, forced: Boolean, params: Iterable[SymConst] = List.empty) = {

    /** Cooper's algorithm requires a certain normal, achieved this way: */
    def normalize(f: Formula) = {
      import cnfconversion.rules._

      val elimUniv: FormulaRewriteRules = {
        case Forall(x, g) ⇒ Neg(Exists(x, Neg(g)))
      }
      f.reduceInnermost(elimUniv).
        reduceInnermost(elimIffNot).
        reduceInnermost(elimIff).
        reduceInnermost(elimImpl).
        reduceInnermost(pushdownNegNoQuant). // May not introduce universal quantifiers
        simplifyCheap
    }

    reporter.debugBG(s"cooper.QE called on $f")
    if (!forced && f.isQuantifierFree && params.isEmpty) {
      // println(" QE: leave untouched: %s, normalized %s".format(f, normalize(f)))
      f
    }
    else
    {
      util.Timer.cooperCalls.start()
      // println("QE " + f)
      util.Timer.normForCooper.start()
      val h = normalize(f)
      util.Timer.normForCooper.stop()
      // println("normalized: " + h)
      val res = cooper.cooper(h, params)
      reporter.debugBG("cooper: final QE result "+res)
      util.Timer.cooperCalls.stop()
      // println("QE of formula " + f + " and params " + params + " is " + res)
      res
    }
  }

}
