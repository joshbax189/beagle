package beagle.bgtheory.LIA.cooper

import beagle._
import fol.FalseAtom
import util.{IntOverflowException, InternalError}
import datastructures.ConsClause
import bgtheory._
import LIA._

object Solver extends LIASolver {
  val name = "cooper"
  val canUNSATCore = false
  def hasSolverLiterals(cl: ConsClause) = true
  def toSolverLiterals(cl: ConsClause) = cl
  def check(cls: Iterable[ConsClause]) =
    try {
      if (cls forall  { _.isGround }) 
	toSolverResult(isConsistent(cls))
      else 
	toSolverResult(LIA.cooper.QE((cls map { _.toFormula }).toAnd, false, cls flatMap { _.symConsts }) != FalseAtom)
    } catch {
      // todo: is this fallback really a good idea? Why are we doing this?
      // JOSH: this is in case the coefficients of some variable are too large to
      // form the LCM. Since BNB doesn't need this it can usually work when
      // cooper doesn't.
      // Ideally we would just be able to cleverly cast to a Long or BigInt to avoid
      // this issue while still using cooper.
      case IntOverflowException => throw InternalError("Integer overflow in Cooper")
    }

  val needsUnitClauses = false
}



