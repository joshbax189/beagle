package beagle.bgtheory.LFA

import beagle._
import fol._
import term._
import util._
import util.RatInt
import util.Rat

/**
 * Monomials and Polynomials as a canonical representation of terms
 */

case class Monomial(c: Rat, x: VarOrSymConst) {

  // * is only well-defined if n is non-zero
  def *(r: Rat) = {
    assume(r != RatInt(0), { println("Multiplying a monomial by 0 should never occur") })
    Monomial(c * r, x)
  }

  def /(r: Rat) = {
    assume(r != RatInt(0), { println("Dividing a monomial by 0: " + this + " / " + r) })
    Monomial(c / r, x)
  }

  def toTerm = c match {
    case RatInt(1)  ⇒ x.toTerm
    case RatInt(-1) ⇒ UMinus(x.toTerm)
    case _  ⇒ Product(DomElemReal(c), x.toTerm)
  }

  // assume n is not zero
  override def toString =
    c match {
      case RatInt(1)  ⇒ x.toString
      case RatInt(-1) ⇒ "-" + x.toString
      case c  ⇒ c + x.toString
    }
}

// n is the constant in the polynome, ms is its (linear) monomials.
// Invariant: each monomial is over a different variable,
// which can be achieved by nomalizing it
final case class Polynomial(ms: List[Monomial], k: Rat) {

  // Warning: normalization may destroy arrangements
  def normalize = {
    var res = Polynomial(List.empty, k)
    ms foreach { res += _ }
    res
  }

  // To test equality of two polynomials we compare their sorted version
/*  lazy val mssorted = ms.sortBy(m ⇒ m.x)

  def sorted = Polynomial(mssorted, k)
*/
  
  // The variables or symbolic constants in this polynomial
  lazy val varOrSymConsts = ms.foldLeft(Set.empty[VarOrSymConst])(_ + _.x) 
  

/*  override def equals(any: Any) = any match {
    case that: Polynomial ⇒ k == that.k && ms == that.ms
    case _: Any           ⇒ false
  }
 */
  // Make the coefficient of the variable arranged for positive.
  // Makes sense only within EQZERO and NEZERO atoms
/*  def withPositiveCoeff = this match {
    case Polynomial(Monomial(c, _) :: _, _) if (c < 0) ⇒ this * -1
    case _ ⇒ this
  }
*/
  // Replace the variable x in this by p. Assume that p does not contain x.
  // Assume that this has been rearranged for x
/*  def replace(x: VarOrSymConst, p: Polynomial) =
    ms match {
      case Monomial(c, y) :: hms if (y == x) ⇒ Polynomial(hms, k) + (p * c)
      case _                                 ⇒ this
    }
*/
  /*
   * The gcd of all coefficients in this
   */

  // def gcdCoeffs = ms.map(m ⇒ math.abs(m.c)).foldLeft(if (k == 0) 1 else math.abs(k))(gcd(_, _))

  // Make the monomial with x the leftmost monomial, if x occurs in this
  def rearrange(x: VarOrSymConst): Polynomial = {
    if (! ((ms map { _.x }) contains x)) return this // shortcut
    var done = List.empty[Monomial]
    var open = ms
    while (!open.isEmpty) {
      val m = open.head
      open = open.tail
      if (m.x == x)
        // Got the one we're looking for
        return Polynomial(m :: (done.reverse ::: open), k)
      else
        // Continue search
        done ::= m
    }
    return this // Nothing found
  }

  // add a monomial, while preserving the invariant
  def +(m: Monomial): Polynomial = {
    // Iterate over ms and update or extend
    var skippedMs = List[Monomial]()
    var oldMs = ms // Iteration variable
    while (!oldMs.isEmpty) {
      val hm = oldMs.head
      oldMs = oldMs.tail
      if (hm.x == m.x)
        // found it, update the coefficient
        return Polynomial(skippedMs.reverse :::
          (if (hm.c + m.c == 0)
            oldMs
          else
            Monomial(hm.c + m.c, m.x) :: oldMs), k)
      else
        skippedMs ::= hm
    }
    // Coming here means that m was not found - add it
    return Polynomial(m :: ms, k)
  }

  // Push a monomial as the first monomial into this.
  // Applied when the monomial is over the variable the current arrangement is for
  // Assume m's variable is different from the vars in all other ms
/*  def +:(m: Monomial) = {
    // assume(!(vars contains m.x), "internal error: incorrect usage of +:")
    Polynomial(m :: ms, k)
  }
*/
  // add a constant to a polynomial
  def +(n: Rat): Polynomial = Polynomial(ms, k + n)

  // Subtraction of monomials
  def -(n: Rat): Polynomial = Polynomial(ms, k - n)
  def -(m: Monomial): Polynomial = this + (m * RatInt(-1))

  // Addition of a polynomial
  def +(p: Polynomial): Polynomial = {
    var res = this + p.k // the constant in p
    // add one monomial in p after the other
    p.ms foreach { m ⇒ res += m }
    res
  }

  // Multiplication by a constant 
  def *(n: Rat): Polynomial =
    n match {
      case RatInt(0) ⇒ Polynomial(List.empty, RatInt(0))
      case RatInt(1) ⇒ this
      case n ⇒ Polynomial(ms map { _ * n }, k * n)
    }

  // Division by a constant 
  def /(n: Rat): Polynomial =
    n match {
      case RatInt(1) ⇒ this
      case n ⇒ Polynomial(ms map { _ / n }, k / n)
    }

  // def factorize = this / gcdCoeffs

  // Subtraction of a polynomial
  def -(p: Polynomial): Polynomial = this + (p * RatInt(-1))

  lazy val toTerm =
    if (ms.isEmpty) DomElemReal(k)
    else if (k == RatInt(0))
      ms.tail.foldRight(ms.head.toTerm)((m: Monomial, s: Term) ⇒ Sum(m.toTerm, s))
    else
      ms.foldRight(DomElemReal(k).asInstanceOf[Term])((m: Monomial, s: Term) ⇒ Sum(m.toTerm, s))

  override def toString =
    if (ms.isEmpty)
      k.toString
    else
      (ms map { _.toString } reduceLeft { _ + " + " + _ }) + (if (k == RatInt(0)) "" else " + " + k)

}

object Polynomial {

  def toPolynomial(t: Term): Polynomial = {
    import Signature._

    t match {
      // case p @ Polynomial(_, _) ⇒ p // nothing to do
      case x @ AbstVar(_, _, RealSort) ⇒ Polynomial(List(Monomial(RatInt(1), x)), RatInt(0))
      case x @ GenVar(_, _, RealSort) ⇒ Polynomial(List(Monomial(RatInt(1), x)), RatInt(0))
      // Treat parameters as variables
      // case BGConst(op) => Polynomial(List(Monomial(1, Var(op.name + "_parameter", 0, IntSort))), 0)
      // case BGConst(op) => Polynomial(List(Monomial(1, Var(op.name, 0, IntSort))), 0)
      case x @ SymConst(Operator(BG, _, Arity0(RealSort))) ⇒ Polynomial(List(Monomial(RatInt(1), x)), RatInt(0))
      case DomElemReal(k: Rat) ⇒ Polynomial(List.empty, k)
      case UMinus(arg) ⇒ toPolynomial(arg) * RatInt(-1)
      case Sum(arg1, arg2) ⇒ toPolynomial(arg1) + toPolynomial(arg2)
      case Difference(arg1, arg2) ⇒ toPolynomial(arg1) - toPolynomial(arg2)
      case Product(arg1, arg2) ⇒
        (toPolynomial(arg1), toPolynomial(arg2)) match {
          case (Polynomial(Nil, k), arg2p) ⇒ arg2p * k
          case (arg1p, Polynomial(Nil, k)) ⇒ arg1p * k
          case (_, _) ⇒
            throw bgtheory.NonLinearTermFail(t)
        }
      case Quotient(arg1, arg2) ⇒
        (toPolynomial(arg1), toPolynomial(arg2)) match {
          case (arg1p, Polynomial(Nil, k)) ⇒ arg1p / k
          case (_, _) ⇒
            throw bgtheory.NonLinearTermFail(t)
        }
      // The above should cover all cases
      // case t ⇒ throw new InternalError("toPolynomial: cannot normalize this term: " + t)
    }
  }
}

