package beagle.bgtheory.LFA

import beagle._
import fol._
import term._
import fol.Signature._
import datastructures._
import calculus._
import util._
import bgtheory._

/**
 * Linear Real Arithmetic
 * Provides methods for quantifier elimination, consistency checking and simplification.
 */

abstract class LFASolver extends Solver {
  val theoryName = "LFA"
  val canUNSATCore = false // can be overridden

  // This is a huge duplication of essentially the same code as in LRASolver
  def QE(cl: ConsClause) = fourierMotzkin.QE(cl)

  val simpRulesTermSafe = simpRules.simpRulesTermSafe
  val simpRulesTermUnsafe = simpRules.simpRulesTermUnsafe
  val simpRulesLitSafe = simpRules.simpRulesLitSafe
  val simpRulesLitUnsafe = simpRules.simpRulesLitUnsafe

}

