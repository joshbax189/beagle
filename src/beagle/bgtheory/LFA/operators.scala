package beagle.bgtheory.LFA

import beagle._
import fol._
import term._
import Signature.OSort

import datastructures.PredEqn
import datastructures.Eqn
import util.Rat

case class DomElemReal(r: Rat) extends DomElem[Rat] {

  val h = this
  lazy val op = new Operator(BG, r.toString, Arity0(RealSort)) {
    override def toTerm = Some(h)
  }
  override lazy val sort = RealSort
  val value = r
  // override def toString = r.toDouble.toString
}

/* Predicates */

object LessEqEqn {
  val op = new Operator(BG, "$lesseq", Arity2((RealSort, RealSort) -> OSort)) { 
    override def formatFn = util.printer.lessEqFF 
  }

  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RealSort && rhs.sort == RealSort)
    PredEqn(PFunTerm(op, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(op1, List(lhs, rhs))) if(op1==op) => Some((lhs, rhs))
    case _ => None
  }
}

object LessEqn {
  val op = new Operator(BG, "$less", Arity2((RealSort, RealSort) -> OSort)) { 
    override def formatFn = util.printer.lessFF 
  }
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RealSort && rhs.sort == RealSort)
    PredEqn(PFunTerm(op, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(op1, List(lhs, rhs))) if(op1==op) => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqEqn {
  val op = new Operator(BG, "$greatereq", Arity2((RealSort, RealSort) -> OSort)) { 
    override def formatFn = util.printer.greaterEqFF 
  }

  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RealSort && rhs.sort == RealSort)
    PredEqn(PFunTerm(op, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(op1, List(lhs, rhs))) if(op1==op)  => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqn {
  val op = new Operator(BG, "$greater", Arity2((RealSort, RealSort) -> OSort)) { 
    override def formatFn = util.printer.greaterFF 
  }
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RealSort && rhs.sort == RealSort)
    PredEqn(PFunTerm(op, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(op1, List(lhs, rhs))) if(op1==op) => Some((lhs, rhs))
    case _ => None
  }
}

/* Operators */

object UMinus {
  val op = new Operator(BG, "$uminus", Arity1(RealSort -> RealSort)) { 
    override def formatFn = util.printer.uminusFF 
  }

  def apply(s: Term) = PFunTerm(op, List(s))

  def unapply(s: Term) = s match {
    case PFunTerm(op1, List(t)) if(op1==op) ⇒ Some(t)
    case _                                        ⇒ None
  }
}

object Sum {
  val op = new Operator(BG, "$sum", Arity2((RealSort, RealSort) -> RealSort)) { 
    override def formatFn = util.printer.sumFF 
  }

  def apply(s1: Term, s2: Term) = PFunTerm(op, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(op1, List(t1, t2))  if(op1==op) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Difference {
  val op = new Operator(BG, "$difference", Arity2((RealSort, RealSort) -> RealSort)) { 
    override def formatFn = util.printer.differenceFF 
  }

  def apply(s1: Term, s2: Term) = PFunTerm(op, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(op1, List(t1, t2)) if(op1==op) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Product {
  val op = new Operator(BG, "$product", Arity2((RealSort, RealSort) -> RealSort)) { 
    override def formatFn = util.printer.productFF 
  }

  def apply(s1: Term, s2: Term) = PFunTerm(op, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(op1, List(t1, t2)) if(op1==op)  ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Quotient {
  //DefinedOp because it is FG
  val op = new DefinedOp(FG, "$quotient", Arity2((RealSort, RealSort) -> RealSort)) { 
    override def formatFn = util.printer.quotientFF 
  }

  def apply(s1: Term, s2: Term) = PFunTerm(op, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(op1, List(t1, t2)) if(op1==op) ⇒ Some((t1, t2))
    case _                                      ⇒ None
  }
}
