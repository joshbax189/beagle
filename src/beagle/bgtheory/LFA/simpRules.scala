package beagle.bgtheory.LFA

import beagle._
import fol._
import term._
import datastructures._
import util._
import bgtheory._

object simpRules extends Simplification { 

  val simpRulesTermSafe: List[SimpRule[Term]] = List(
    new SimpRule[Term]("evalSum1", {
      case LFA.Sum(i1: LFA.DomElemReal, i2: LFA.DomElemReal) ⇒ LFA.DomElemReal(i1.value + i2.value)
      case _                                   ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalProduct", {
      case LFA.Product(i1: LFA.DomElemReal, i2: LFA.DomElemReal) ⇒ LFA.DomElemReal(i1.value * i2.value)
      case _                                       ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalDifference1", {
      case LFA.Difference(i1: LFA.DomElemReal, i2: LFA.DomElemReal) ⇒ LFA.DomElemReal(i1.value - i2.value)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalQuotient", {
      case LFA.Quotient(i1: LFA.DomElemReal, i2: LFA.DomElemReal) ⇒ LFA.DomElemReal(i1.value / i2.value)
      case _                                        ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalUMinus", {
      case LFA.UMinus(i1: LFA.DomElemReal) ⇒ LFA.DomElemReal(i1.value * -1)
      case _                      ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalToX", {
      case PFunTerm(LFA.ToIntOpReal, List(i: LFA.DomElemReal)) ⇒ 
	LIA.DomElemInt( math.floor(i.value.toDouble).toInt )
      case PFunTerm(LFA.ToRatOpReal, List(i: LFA.DomElemReal)) ⇒ LRA.DomElemRat(i.value) // i.asInstanceOf[DomElemRat]
      case PFunTerm(LFA.ToRealOpReal, List(s)) if s.sort == LFA.RealSort ⇒ s
      case _                      ⇒ throw NotApplicable
    }),

    //used to recover from moving NL product to the foreground, if possible.
    //perhaps not possible here, but included for completeness.
    new SimpRule[Term]("replaceNLPP", {
      case PFunTerm(NLPPOpReal, List(n: DomElemReal, x)) ⇒ LFA.Product(n,x)
      case PFunTerm(NLPPOpReal, List(x, n: DomElemReal)) ⇒ LFA.Product(x,n)
      case _                      ⇒ throw NotApplicable
    })
  )

  val simpRulesTermUnsafe = List.empty

  val simpRulesLitSafe = List(
    // This rule gets rid of all >-literals,  positive <-literals, and positive ≤-literals,
    // in terms of negative <-literals and negative ≤-literals.
    new SimpRule[Lit]("NormalizeInequality", {
      case Lit(isPos, GreaterEqn(d1, d2)) ⇒ Lit(isPos, LessEqn(d2, d1))
      case Lit(isPos, GreaterEqEqn(d1, d2)) ⇒ Lit(isPos, LessEqEqn(d2, d1))
      case Lit(true, LessEqn(d1, d2)) ⇒ Lit(false, LessEqEqn(d2, d1))
      case Lit(true, LessEqEqn(d1, d2)) ⇒ Lit(false, LessEqn(d2, d1))
      case _ ⇒ throw NotApplicable
    }),
    new SimpRule[Lit]("evalLess", {
      case Lit(true, LessEqn(d1: DomElemReal, d2: DomElemReal)) ⇒
        if (d1.value < d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, LessEqn(d1: DomElemReal, d2: DomElemReal)) ⇒
        if (!(d1.value < d2.value)) Lit.TrueLit else Lit.FalseLit
      case Lit(sign, LessEqn(t1, t2)) if (t1 == t2) =>
	if (sign) Lit.FalseLit else Lit.TrueLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalLessEq", {
      case Lit(true, LessEqEqn(d1: DomElemReal, d2: DomElemReal)) ⇒
        if (d1.value <= d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, LessEqEqn(d1: DomElemReal, d2: DomElemReal)) ⇒
        if (!(d1.value <= d2.value)) Lit.TrueLit else Lit.FalseLit
      case Lit(sign, LessEqEqn(t1, t2)) if (t1==t2) =>
	if (sign) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalEqual", {
      case Lit(true, Eqn(d1: DomElemReal, d2: DomElemReal)) ⇒
        if (d1.value == d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, Eqn(d1: DomElemReal, d2: DomElemReal)) ⇒
        if (d1.value != d2.value) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalIsX", {
      case Lit(t, PredEqn(PFunTerm(IsIntOpReal, List(d: DomElemReal)))) ⇒
	if (d.value.isRatInt == t) Lit.TrueLit else Lit.FalseLit
      case Lit(t, PredEqn(PFunTerm(IsRatOpReal, List(d: DomElemReal)))) ⇒
        // All Real domain elements are rationals
	if (t) Lit.TrueLit else Lit.FalseLit
      case Lit(t, PredEqn(PFunTerm(IsRealOpReal, List(s)))) if s.sort == LFA.RealSort ⇒
	if (t) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    })
  )

  val simpRulesLitUnsafe: List[SimpRule[Lit]] = List(

    // Incomplete
    new SimpRule[Lit]("sumDiffEqual1", {
      case Lit(isPos, Eqn(Sum(d1: DomElemReal, t), d2: DomElemReal)) ⇒ Lit(isPos, Eqn(t, DomElemReal(d2.value - d1.value)))
      case Lit(isPos, Eqn(Difference(d1: DomElemReal, t), d2: DomElemReal)) ⇒ Lit(isPos, Eqn(t, DomElemReal(d1.value - d2.value)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumDiffEqual2", {
      case Lit(isPos, Eqn(Sum(d1: DomElemReal, t1), Sum(d2: DomElemReal, t2))) ⇒ Lit(isPos, Eqn(t1, Sum(DomElemReal(d2.value - d1.value), t2)))
      case Lit(isPos, Eqn(Difference(d1: DomElemReal, t1), Difference(d2: DomElemReal, t2))) ⇒ Lit(isPos, Eqn(t1, Sum(DomElemReal(d1.value - d2.value), t2)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorEqual1", {
      case Lit(isPos, Eqn(Product(d1: DomElemReal, t), d2: DomElemReal)) if (d1.value != 0) ⇒ Lit(isPos, Eqn(t, DomElemReal(d2.value / d1.value)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorEqual2", {
      case Lit(isPos, Eqn(Product(d1: DomElemReal, t1), Product(d2: DomElemReal, t2))) if (d1.value != 0) ⇒ Lit(isPos, Eqn(t1, Product(DomElemReal(d2.value / d1.value), t2)))
      case Lit(isPos, Eqn(Product(d1: DomElemReal, t1), Product(d2: DomElemReal, t2))) if (d2.value != 0) ⇒ Lit(isPos, Eqn(Product(DomElemReal(d1.value / d2.value), t1), t2))
      case _ ⇒ throw NotApplicable
    }),

    // Incomplete
    new SimpRule[Lit]("factorLessEq", {
      case Lit(isPos, LessEqEqn(Product(d1: DomElemReal, t), d2: DomElemReal)) if (d1.value != 0) ⇒ Lit(isPos, LessEqEqn(t, DomElemReal(d2.value / d1.value)).swapIfNeg(d1.value.signum))
      case Lit(isPos, LessEqEqn(d2: DomElemReal, Product(d1: DomElemReal, t))) if (d1.value != 0) ⇒ Lit(isPos, LessEqEqn(DomElemReal(d2.value / d1.value), t).swapIfNeg(d1.value.signum))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorLess", {
      case Lit(isPos, LessEqn(Product(d1: DomElemReal, t), d2: DomElemReal)) if (d1.value != 0) ⇒ Lit(isPos, LessEqn(t, DomElemReal(d2.value / d1.value)).swapIfNeg(d1.value.signum))
      case Lit(isPos, LessEqn(d2: DomElemReal, Product(d1: DomElemReal, t))) if (d1.value != 0) ⇒ Lit(isPos, LessEqn(DomElemReal(d2.value / d1.value), t).swapIfNeg(d1.value.signum))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumLessEq", {
      case Lit(isPos, LessEqEqn(Sum(d1: DomElemReal, t), d2: DomElemReal)) ⇒ Lit(isPos, LessEqEqn(t, DomElemReal(d2.value - d1.value)))
      case Lit(isPos, LessEqEqn(d2: DomElemReal, Sum(d1: DomElemReal, t))) ⇒ Lit(isPos, LessEqEqn(DomElemReal(d2.value - d1.value), t))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumLess", {
      case Lit(isPos, LessEqn(Sum(d1: DomElemReal, t), d2: DomElemReal)) ⇒ Lit(isPos, LessEqn(t, DomElemReal(d2.value - d1.value)))
      case Lit(isPos, LessEqn(d2: DomElemReal, Sum(d1: DomElemReal, t))) ⇒ Lit(isPos, LessEqn(DomElemReal(d2.value - d1.value), t))
      case _ ⇒ throw NotApplicable
    }) // todo: similar rules for difference
    )

}
