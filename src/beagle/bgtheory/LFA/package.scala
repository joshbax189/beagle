package beagle.bgtheory

import beagle._
import fol._
import fol.Signature._
//import util.RatInt
import util._
import bgtheory.LRA._

package object LFA {

  object RealSort extends BGSort("$real") { override def toString = "ℝ" }

  val IsIntOpReal = new IsIntOp(RealSort)
  val IsRatOpReal = new IsRatOp(RealSort)
  val IsRealOpReal = new IsRealOp(RealSort)
  
  val ToIntOpReal = new ToIntOp(RealSort)
  val ToRatOpReal = new ToRatOp(RealSort)
  val ToRealOpReal = new ToRealOp(RealSort)

  // Notoce these operators are not yet added to LFAOperators
  val QuotientEOpReal = new QuotientEOp(RealSort)
  val QuotientTOpReal = new QuotientTOp(RealSort)
  val QuotientFOpReal = new QuotientFOp(RealSort)
  val RemainderEOpReal = new RemainderEOp(RealSort)
  val RemainderTOpReal = new RemainderTOp(RealSort)
  val RemainderFOpReal = new RemainderFOp(RealSort)

  val NLPPOpReal = new NLPPOp(RealSort)

  val LFAOperators = Set(Sum.op, UMinus.op, GreaterEqn.op, LessEqn.op, Difference.op, 
			 Product.op, Quotient.op, LessEqEqn.op, GreaterEqEqn.op,
			 IsIntOpReal, IsRatOpReal, IsRealOpReal,
			 ToIntOpReal, ToRatOpReal, ToRealOpReal,
			 NLPPOpReal)

  def addStandardOperators(s: Signature) = {
    var res = s
    res += RealSort
    LFAOperators foreach { res += _ }
    res
  }

  val ZeroReal = DomElemReal(RatInt(0))
  val OneReal = DomElemReal(RatInt(1))
  val MinusOneReal = DomElemReal(RatInt(-1))

  RealSort.someElement = ZeroReal

  def isLFA(e: Expression[_]) = e.isBG && (e.sorts subsetOf Set(RealSort)) // && (e.operators subsetOf LFAOperators)

}
