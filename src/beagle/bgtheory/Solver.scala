package beagle.bgtheory

import beagle._
import fol._
import fol.Signature._
import datastructures._
import util._

/**
 * UnhandledBGClause exception is thrown when QE is attempted on a background
 * clause for which QE is not defined.
 */
case class UnhandledBGClause(cl: ConsClause, msg: String) extends Exception
case class UnhandledBGFormula(f: Formula, msg: String) extends Exception

sealed abstract class SolverResult
case object SAT extends SolverResult
case class UNSAT(core: Option[List[Int]]) extends SolverResult
case object UNKNOWN extends SolverResult


/**
 * Solver is a collection of methods that
 * a solver for a given background theory must/should have.
 * Now that solver extends Simplification it has state, namely
 * which bgsimp level is currently set.
 */
abstract class Solver extends Simplification {
  /** Used to refer to the solver from command line.*/
  val name: String

  /** The name of the theory handled by this solver, e.g. LIA */
  val theoryName: String 

  /** Whether the solver can compute unsatisfiable cores */
  val canUNSATCore: Boolean 

  /**
   *  Quantifier elimination
   *  @param cl A clause for which hasSolverSignature holds true.
   *  We also assume unabst has been applied to cl.
   */
  def QE(cl: ConsClause): List[ConsClause]

  /**
   *  Quantifier elimination
   *  @param cl A clause (for which hasSolverSignature does not neccessarily hold true).
   *  Eliminates all extraneous BG variables.
   *  We also assume unabst has been applied to cl.
   *  Used for optimizations only, hence is allowed to be implemented as the identity
   */

  /*
  def QEGeneral(cl: ConsClause): List[ConsClause]
   // Not used any more. If there is a pressing need could define it in terms of QE on formulas like this:
  def QEGeneral(cl: ConsClause): List[ConsClause] = {
    val extraVars = cl.BGLits.bgVars -- cl.nonBGLits.bgVars
    if (extraVars.isEmpty)
      List(cl)
    else {
      val f = QE(Exists(extraVars.toList, (cl.BGLits map { _.compl.toLiteral }).toAnd))
      val res = 
        for (conj ← cnfconversion.toDNF(f))
           yield Clause(cl.nonBGLits ::: (conj map { _.toLit.compl }), cl.idxRelevant, cl.age)
      // println("QEGeneral: given: " + cl)
      // res foreach { cl => println("QEGeneral: result: " + cl) }
      res
    }
  }
   */

  /**
   * Some solvers accept only literals from a sub-signature.
   * This tests whether the given clause is a member of that
   * signature.
   * @param cl Must be a ground clause.
   */
  def hasSolverLiterals(cl: ConsClause): Boolean

  /**
   * Converts into clauses of the sub-signature which the solver accepts.
   * @param cl Must be a ground clause.
   */
  def toSolverLiterals(cl: ConsClause): ConsClause

  /**
   * True if check only accepts unit clauses as input.
   */
  val needsUnitClauses: Boolean

  /**
   * check if a given set of clauses is consistent.
   * It is applied only to clauses for which isSolverClause holds true. 
   * In particular these clauses are ground, they are obtained by asSolverClauses
   * @return (true, None) if the given list of clauses is consistent.
   * @return (false, unsatCore) if inconsistent and unsatCore is an unsatisfiable core.
   */
  def check(cls: Iterable[ConsClause]): SolverResult

  /**
   * Naive approach to finding a minimal unsatisfiable subset of clauses,
   * used if isConsistent does not return one.
   * So List() => isConsistent==true, otherwise isConsistent!=true
   * @return a subset of clause ids such that this set of clauses is unsat
   * but every proper subset is sat.
   */
  def minUnsatCore(cls: Iterable[ConsClause]): List[Int] = {
    //TODO- could have timed cooper calls to avoid the situation where a few
    //clauses cause unsatisfiability, but the remainder takes a long time to
    //prove sat.
    if (check(cls) == SAT)
      List()
    else {
      util.Timer.muc.start()
      var i = 0
      var cs = cls.toList //the working clause set
      while(i < cs.size) {
	if (check(cs.removeNth(i)) == UNSAT(None)) {
	  cs = cs.removeNth(i)
	  i = 0
	} else i+=1
      }
      //if you get to here, no more removals possible

      util.Timer.muc.stop()
      //return the ids
      val res = cs.map(_.id).toList
      reporter.debug("*** min unsat. core of:")
      reporter.debug(cls map { _.id })
      reporter.debug("is "+res)
      // val uc = cls.filter(c => res.indexOf(c.id) >= 0)
      // println(s"***SANITY CHECK: ${!isConsistent(uc)}")

      res
    }
  }

  /**
   * Theory simplification rules for terms.
   * Two kinds: those that are "safe", i.e. always preserve sufficient completeness
   *  and those that are "unsafe" which can possibly destroy sufficient completeness
   *  The unsafe ones do not need to include the safe ones, for aggressive simplification
   *  the two lists are concatenated.
   */
  /*val simpRulesTermSafe: List[SimpRule[Term]]
  val simpRulesTermUnsafe: List[SimpRule[Term]]*/
  /** Theory simplification rules for literals.*/
/*  val simpRulesLitSafe: List[SimpRule[Lit]]
  val simpRulesLitUnsafe: List[SimpRule[Lit]]*/

  // Use cases:
  /**
   * @param cl Must be a background clause.
   * @return True if cl can be sent to the BG reasoner.
   */
  def isSolverClause(cl: ConsClause) = {
    assume(cl.isBG)
    // assume(bgtheory.solver.hasSolverSignature(cl)) 
    cl.isGround &&
      (!needsUnitClauses || cl.isUnitClause) &&
      hasSolverLiterals(cl)
  }

  /**
   * Given a background clause cl, compute a set of ground clauses
   * that is equivalent to cl over the background domain.
   * Moreover all literals are solver literals.
   * Return the set of ground clauses partitioned into a set that can directly be passed to the
   * bg solver and a set that cannot (because the solver needs unit clauses)
   */
  def asSolverClauses(cl: ConsClause): (List[ConsClause], List[ConsClause]) = {
    assume(cl.isBG, "asSolverClauses applied to non-background clause")
    // println(" xx asSolverClauses " + cl)
    if (isSolverClause(cl))
      (List(cl), List.empty)
    else {
      // assume(hasSolverSignature(cl), "QE applied to clause over wrong signature")
      val hcl = cl.unabstrAggressive 
      // Eliminate the variables, if there are any
      // simplifyCheap is needed to eliminate FalseLit
      val groundCls = (if (hcl.isGround) List(hcl) else QE(hcl)) flatMap { _.simplifyCheap }
      // println(" xx " + solver.name + " QE on " + hcl)
      // println(" xx QE result " + groundCls)

      // Convert the ground clauses into clauses over the proper sub-signature
      // The above QE step, as well as toSolverLiterals may result in a tautology,
      // in particular a clause containing the *FG* literal Lit.TrueLit,
      // thus turning a BG clause into a FG! We must avoid that.
      val withSolverLiterals = groundCls map {
        cl ⇒ (if (hasSolverLiterals(cl)) cl else toSolverLiterals(cl))
      } filterNot { _.isTautology }

      if (needsUnitClauses)
        (withSolverLiterals partition { _.length <= 1 })
      else
        (withSolverLiterals, List.empty)
    }
  }

}

/** A solver that can be used during preprocessing
  * It can only do evaluation of ground terms, including conversions like to_int, to_rat etc.
  * This way, clauses over multiple theories may collapse into clauses over one theory.
 */
object PreprocessingSolver extends Solver {
  val name = "PreProcSolver"
  val theoryName = "LRFIA"
  val canUNSATCore = false
  // def hasSolverSignature(cl: ConsClause) = false
  def QE(cl: ConsClause) = List(cl)
  // def QE(f: Formula, params: Iterable[SymConst]) = f
  def hasSolverLiterals(cl: ConsClause) = true
  def toSolverLiterals(cl: ConsClause) = cl
  def check(cls: Iterable[ConsClause]) = UNKNOWN

  val needsUnitClauses = false

  val simpRulesTermSafe = 
    LRA.simpRules.simpRulesTermSafe :::
    LFA.simpRules.simpRulesTermSafe :::
    LIA.simpRules.simpRulesTermSafe 
  val simpRulesTermUnsafe = List.empty
  val simpRulesLitSafe = 
    LRA.simpRules.simpRulesLitSafe :::
    LFA.simpRules.simpRulesLitSafe :::
    LIA.simpRules.simpRulesLitSafe 
  val simpRulesLitUnsafe = List.empty
}

object EmptySolver extends Solver {
  val name = "empty"
  val theoryName = "empty"
  val canUNSATCore = false
  // def hasSolverSignature(cl: ConsClause) = false
  def QE(cl: ConsClause) = List(cl)
  // def QE(f: Formula, params: Iterable[SymConst]) = f
  def hasSolverLiterals(cl: ConsClause) = true
  def toSolverLiterals(cl: ConsClause) = cl
  def check(cls: Iterable[ConsClause]) = UNKNOWN
  val needsUnitClauses = false

  val simpRulesTermSafe = List.empty
  val simpRulesTermUnsafe = List.empty
  val simpRulesLitSafe = List.empty
  val simpRulesLitUnsafe = List.empty
}

