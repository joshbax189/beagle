package beagle.bgtheory.LRA

/**
 * Linear Rational Arithmetic
 * Provides methods for quantifier elimination, consistency checking and simplification.
 */

import beagle._
import fol._
import term._
import datastructures._
import util._
import bgtheory._

abstract class LRASolver extends Solver {
  val theoryName = "LRA"
  val canUNSATCore = false // can be overridden
  // def hasSolverSignature(cl: Clause) = cl.isLRA
  def QE(cl: ConsClause) = fourierMotzkin.QE(cl)

  val simpRulesTermSafe = simpRules.simpRulesTermSafe
  val simpRulesTermUnsafe = simpRules.simpRulesTermUnsafe
  val simpRulesLitSafe = simpRules.simpRulesLitSafe
  val simpRulesLitUnsafe = simpRules.simpRulesLitUnsafe

}

