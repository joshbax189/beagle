package beagle.util


import beagle._
import datastructures._
import fol._
import term._

/**
 * This defines the messages which are printed out on various beagle actions.
 * It allows swapping between debug/regular/database levels of reporting.
 * Removes clutter from main code- instead of having logv(...); logd(...); logdb(...)
 * can just have a single statement.
 * 
 * Reporter should have the output expected when the -v flag is passed.
 * TODO- should be objects...
 *  Peter 2/4/2014: Reporter now is quiet, use VerboseReporter instead
 */

class Reporter {

  val isDebugReporter = false
  
  /**
   * debug is meant is meant, well, for debugging beagle,
   * log is meant for messages to the user,
   * debugBG is meant for debugging Bg reasoners.
   */
  def debug[T](cl: T): T = cl
  def debugBG[T](cl: T): T = cl
  def log[T](cl: T) = cl

  /**
   * Print the given string if in debug mode, i.e. only works if given class is
   * a subclass of debug.
   */
  def debug(msg: => String) {}
  def debugBG(msg: => String) {}
  def log(msg: String) {}
  
  def onPara(from: ConsClause, into: ConsClause, res: ConsClause, sigma: Subst) {}
  
  def onInference(selected: ConsClause, result: ConsClause, infType: String) {}
  
  /**
   * Called when a new split is made.
   * @param result the left clause of the split
   * @param decisionLevel the current decision level, this will be
   * backtracked to if the LH clause is refuted.
   */
  def onSplit(selected: ConsClause, result: List[ConsClause], decisionLevel: Int) = {}
  
  def onInst(selected: ConsClause, result: List[ConsClause]) = {}
  
  /**
   * Selected clause has been deleted by simplification.
   */
  def onClauseDeleted(deleted: ConsClause) = {}
  
  /**
   * Clause has been selected for the next inference rule.
   */
  def selectedClause(cl: ConsClause, decisionLevel: Int) = {}
  
  /**
   * An added BG clause is inconsistent wrt other BG clauses
   */
  def onBGInconsistent(cl: ConsClause, old: ListClauseSet) {}
  
  /**
   * @param level is the target decision level
   * @param old is the set of retained clauses at the new decision level
   * @param neu is the set of new clauses at the new decision level
   */
  def onBacktrack(level: Int, rightClauses: List[ConsClause], old: ClauseSet, neu: ClauseSet) {}
  
  def onSimplified(selected: ConsClause, simplified: ConsClause) {}

  /** Only used for proof reporter */
  def detailedSimp(reason: String, selected: ConsClause, simplified: ConsClause, clUsed: List[ConsClause]) {}
  
  /** Called if the define rule has been successfully applied.
   * @param selected The clause selected for a define inference,
   * it provides the ground BSFG terms for the LHS of the define equations.
   * @param defClauses The definitions found in `selected`
   * @param newClause `selected` demodulated with `defClauses`
   * @see [[beagle.calculus.derivationrules.infDefine]]
   */
  def onDefine(selected: ConsClause, defClauses: Iterable[ConsClause], newClause: ConsClause) = {}
  
  /** A clause has been added to old- the set of retained clauses */
  def onClauseAdded(cl: ConsClause) = {}
  
  def onNewState(old: ListClauseSet, neu: PQClauseSet) = {}
  
  def onProofStart(initClauses: List[ConsClause]) = {}
  
  def onClose(decisionLevel: Int, emptyCl: ConsClause) = {}

  def onClauseSubsumed(cl: ConsClause, subsumed: ConsClause) = {}
  
  /**
   * Called when a proof is successfully finished.
   */
  def onProofEnd() {}
  
  /**
   * Always called when program exits- either normally or terminated.
   * Use for finishing writes etc.
   */
  def onExit() {}
  
  def showClauseSets(old: ClauseSet,neu: ClauseSet) {
    println("======== Clause sets ==================")
    println("                                   new:")
    neu.show()
    println("                                   old:")
    old.show()
    println()
  }
}

class VerboseReporter extends Reporter {
  
  override def selectedClause(cl: ConsClause, decisionLevel: Int) { 
    println((if (decisionLevel==0) "" else "[" + decisionLevel + "] ") + cl)
  }
  
  override def onBacktrack(level: Int, rightClauses: List[ConsClause], old: ClauseSet, neu: ClauseSet) = {
    println("=== Backtrack to level " + level + " ===")
  }
  
  // override def onProofStart(initClauses: List[Clause]) = {
  //   println("\nProving...")
  // }
  
  override  def onClose(decisionLevel: Int, emptyCl: ConsClause) {
    println((if (decisionLevel==0) "" else "[" + decisionLevel + "] ") + emptyCl)
  }

  override def log[T](cl: T) = { println(cl); cl }
  override def log(msg: String) { println(msg) }

}

object DefaultReporter extends VerboseReporter

object QuietReporter extends Reporter 

/*
/**
 * Output statistics to file. Not set by commmand line, but set in code.
 * This version will output the number of maximal literals in a clause and
 * the total literals in the clause (unabstracted?).
 */
class CustomReporter extends Reporter {
  //create file for printing stats
  //val file = new File("clause_weights.out")
  val file = new File("split_times.out")
  val pw = new PrintWriter(file)
  
  override def onPara(from: ConsClause, into: ConsClause, res: ConsClause, sigma: Subst) {
    //output the # of maximal literals in the clause vs the total number
    /*pw.println(
		sigma(from).iMaximal.length+
		" "+
		from.length
		)
    pw.println(
		sigma(into).iMaximal.length+
		" "+
		into.length
		)*/
  }
  
  override def onInference(selected: ConsClause, result: ConsClause, infType: String) {
    /*pw.println(
    		sigma(selected).iMaximal.length+
    		" "+
    		selected.length
    		)*/
  }
  
  override def onSplit(selected: ConsClause, result: ConsClause, decisionLevel: Int) {
  }
  
  override def onExit() {
    pw.close()
  }
}
*/

/**
 * Gives the level of output expected when the -d flag is given.
 */
object DebugReporter extends VerboseReporter {

  override val isDebugReporter = true

  override def debug[T](cl: T): T = { println(cl); cl }
  
  override def debug(msg: => String) {
    println(msg)
  }
  
  override def onPara(from: ConsClause, into: ConsClause, res: ConsClause, sigma: Subst) {
    println("\n" + res + " by Para from " + from + " into " + into)
  }
  
  override def onInference(selected: ConsClause, result: ConsClause, infType: String) {
    println("\n" + result + " by "+infType+" from " + selected)
  }
  
  override def onSplit(selected: ConsClause, result: List[ConsClause], decisionLevel: Int) {
    println("Split with clause(s) " + result.mkString(", "))
  }
  
  override def onClauseDeleted(deleted: ConsClause) {
    println("Delete clause " + deleted)
  }
  
  override def onClauseSubsumed(cl: ConsClause, subsumed: ConsClause) {
    println(cl + " subsumes " + subsumed)
  }
  
  override def selectedClause(cl: ConsClause, decisionLevel: Int) {
    println("Selected clause: ")
    super.selectedClause(cl, decisionLevel)
  }
  
  override def onBGInconsistent(cl: ConsClause, old: ListClauseSet) {
    println("BG Clause " + cl + " is inconsistent with BG clauses ")
    old.show()
  }
  
  override def onDefine(selected: ConsClause, defClauses: Iterable[ConsClause], newClause: ConsClause) {
    println("Define on " + selected + ", giving " + defClauses + " and " + newClause)
  }
  
  override def onInst(selected: ConsClause, result: List[ConsClause]) {
    println("Inst on " + selected + " yields " + result)
  }
  
  override def onSimplified(selected: ConsClause, simplified: ConsClause) {
    println("   " + simplified + " by simplification of " + selected)
  }
  
  override def onNewState(old: ListClauseSet, neu: PQClauseSet) {
    showClauseSets(old,neu)
  }
  
  override def onBacktrack(level: Int, rightClauses: List[ConsClause], old: ClauseSet, neu: ClauseSet) {
    super.onBacktrack(level, rightClauses,old,neu)
    println("Right clauses are:")
    rightClauses foreach {println _}
    println("\nState is now:")
    showClauseSets(old,neu)
  }
  
}

/**
 * Gives the level of output expected when the -d flag is given.
 */

object DebugBGReporter extends VerboseReporter {

  override val isDebugReporter = true

  override def debugBG[T](cl: T): T = { println(cl); cl }
  
  override def debugBG(msg: => String) {
    println(msg)
  }
}

/* //For writing proof output to a DB- not used in trunk version
class DBReporter(file: String, flagStr: String) extends Reporter{
  val db = new DB(util.flags.dbName.value, file, flagStr)
  
  override def onProofStart(initClauses: List[Clause]) {
    initClauses foreach { db.write(_) }
  }
  
  override def onNewState(old: ListClauseSet, neu: PQClauseSet) {
    db.newState(List(), List())
  }
  
  override def onPara(from: ConsClause, into: ConsClause, res: ConsClause, l:Term, r: Term, s: Term, t: Term, sigma: Subst) {
    db.writeParaInf(from,into,res)
  }
  
  //usually ref or fact
  override def onInference(selected: ConsClause, result: ConsClause, sigma: Subst, infType: String) {
    db.writeInference(selected,result,infType)
  }
  
  override def onBacktrack(level: Int) {
    super.onBacktrack(level)
    db.newBranch(level)
  }
  
  override def onSplit(selected: ConsClause, result: ConsClause, decisionLevel: Int) {
    db.writeSplitInf(selected,result,decisionLevel)
  }
  
  override def onClauseDeleted(deleted: ConsClause) {
    db.setClauseDeleted(deleted)
  }
  
  override def onDefine(selected: ConsClause, defClause: ConsClause, result: ConsClause) {
    db.writeInference(selected,defClause,"def")
  }
}
*/
