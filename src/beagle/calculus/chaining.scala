package beagle.calculus

import beagle._
import datastructures._
import fol._
import fol.term._
import util._
import PMI._
import bgtheory._

/**
 * Implements the chaining rule for inequalities.
 * @example
 * s < t ∨ C<sub>from</sub>  u < v ∨ C<sub>into</sub> </br>
 * —————————————————————————————– </br>
 *   σ(s < v ∨ C<sub>from</sub> ∨ C<sub>into</sub>)   if σ=mgu(t,u) </br>
 */
object chaining {

  /* Note the explicit combination of constraints and application of subst in chain() */

  /**
   * @param from The given clause.
   * @param fromPos The index of the selected literal in the `from` clause.
   * @param fromLit The literal contributed by the `from` clause- this is assumed to be positive.
   * @param into The clause selected to do chaining with.
   * @param intoPos The index of the selected literal in the `into` clause.
   * @param intoLit The literal selected to chain on in the `into` clause.
   */
  def chain(from: ConsClause, fromPos: Int, fromLit: Lit, into: ConsClause, intoPos: Int, intoLit: Lit) = {
    var res = List.empty[ConsClause]

    /**
     * @param l The entailed literal
     * @param sigma The mgu used to produce it
     */
    def addToRes(l: Lit, sigma: Subst) {
      util.stats.nrInfChain += 1
      /*res = Clause(sigma(l :: from.lits.removeNth(fromPos) ::: into.lits.removeNth(intoPos)),
        into.idxRelevant ++ from.idxRelevant,
        math.max(into.age, from.age) + 1, ByChaining(from, into), math.min(from.deltaConjecture, into.deltaConjecture)+1).abstr :: res*/
      val newInfo = 
        ClsInfo(into.idxRelevant ++ from.idxRelevant,
          math.max(into.age, from.age) + 1, 
          ByChaining(from, into), 
          math.min(from.info.deltaConjecture, into.info.deltaConjecture) + 1)

      res ::= new ConsClause(l :: from.lits.removeNth(fromPos) ::: into.lits.removeNth(intoPos), 
        from.constraint ++ into.constraint, newInfo).applySubst(sigma).abstr
    }

    //TODO- from here it only works for LIA 
    // assume fromLit is positive
    val Lit(true, LIA.LessEqn(s, t)) = fromLit

    intoLit match {
      // (s < t) ∨ From  ¬(u < v) ∨ Into
      case Lit(false, LIA.LessEqn(u, v)) => {
        (s mgu u) foreach { //σ=mgu(s,u) then add ¬(t < v)
          sigma => addToRes(Lit(false, LIA.LessEqn(t, v)), sigma)
        }

        (t mgu v) foreach { //σ=mgu(t,v) then add ¬(u < s)
          sigma => addToRes(Lit(false, LIA.LessEqn(u, s)), sigma)
        }
      }
      // (s < t) ∨ From  (u < v) ∨ Into
      case Lit(true, LIA.LessEqn(u, v)) => {
        (t mgu u) foreach { //σ=mgu(t,u) then add (s < v)
          sigma => addToRes(Lit(true, LIA.LessEqn(s, v)), sigma)
        }
        (s mgu v) foreach { //σ=mgu(s,v) then add (u < t)
          sigma => addToRes(Lit(true, LIA.LessEqn(u, t)), sigma)
        }
      }
    }
    res
  }

  /**
   * Assumes clauses are already variable disjoint.
   * Compute all chaining inferences using clause `from`.
   * Currently only works for LIA inequalities.
   */
  def chainingIntoClauses(from: ConsClause, into: ListClauseSet) = {
    // Assume clauses are variable disjoint
    var res = List.empty[ConsClause]

    for {
      // eligible literals are never pure BG, hence no need to test that
      fromPos ← from.iEligible intersect from.iPosLits
      fromLit = from(fromPos)
    } fromLit match {
        case Lit(true, PredEqn(PFunTerm(Operator(_,"$less",_),_))) =>
	  //find an eligible '<'-literal in the into clauses
          for {
	    intoCl <- into.clauses
	    //intoPos ← intoCl.iEligible
	    intoPos <- intoCl.chainableLits(fromLit) intersect intoCl.iEligible //uses index
	    intoLit = intoCl(intoPos)
	  } res :::= chain(from, fromPos, fromLit, intoCl, intoPos, intoLit)
      /*intoLit match {
       case Lit(_, LIA.LessEqn(_, _)) =>
       res :::= chain(from, fromPos, fromLit, intoCl, intoPos, intoLit)
       case _ => ()
       }*/
        case _ => ()
      }

    res
  }

}
