package beagle.calculus

import beagle._
import datastructures.ConsClause
import fol.Formula

/** Inferences are carried by Clauses to describe their origin in a derivation. */
abstract class Inference {
  /** The name of the used inference rule */
  val name: String 

  /** The name of the involved BG theory */
  val bgTheory: String = "empty"

  def getString(cl: ConsClause, bgTheoryMentioned: String): String
  def getTFFString(cl: ConsClause, bgTheoryMentioned: String): String
}

abstract class OnePremiseInference extends Inference {
  val premise: ConsClause

  def getString(cl: ConsClause, bgTheoryMentioned: String) =
    "%5d: %s\n       by %s%s(%d)".format(cl.id, cl.toString, bgTheoryMentioned, name, premise.id)
  def getTFFString(cl: ConsClause, bgTheoryMentioned: String): String = 
    "tff(%s, plain, %s, inference(%s, [status(thm)%s], [%s])).".format("c_" + cl.id, cl, name, 
      bgTheoryMentioned, "c_" + premise.id)
}

abstract class TwoPremisesInference extends Inference {
  val leftPremise: ConsClause
  val rightPremise: ConsClause

  def getString(cl: ConsClause, bgTheoryMentioned: String) =
    "%5d: %s\n       by %s%s(%d, %d)".format(cl.id, cl.toString, bgTheoryMentioned, name, 
      leftPremise.id, rightPremise.id)
  def getTFFString(cl: ConsClause, bgTheoryMentioned: String): String = 
    "tff(%s, plain, %s, inference(%s, [status(thm)%s], [%s, %s])).".format("c_" + cl.id, cl, name, 
      bgTheoryMentioned, "c_" + leftPremise.id, "c_" + rightPremise.id)
}

abstract class MultiPremisesInference extends Inference {
  val premises: List[ConsClause]

  def getString(cl: ConsClause, bgTheoryMentioned: String) =
    "%5d: %s\n       by %s%s%s".format(cl.id, cl.toString, bgTheoryMentioned, name, 
      if (premises.nonEmpty) (premises map { _.id }).mkString("(", ", ", ")") else "")

  def getTFFString(cl: ConsClause, bgTheoryMentioned: String): String = 
    "tff(%s, plain, %s, inference(%s, [status(thm)%s], %s)).".format("c_" + cl.id, cl, name, bgTheoryMentioned,
                (premises map { "c_" + _.id }).mkString("[", ", ", "]"))
}

/* Inferences with special print functions */

case object ByInput extends Inference { 
  val name = "input"
  override def getString(cl: ConsClause, bgTheoryMentioned: String): String =
    "%5d: %s".format(cl.id, cl.toString, ByInput.name)
  override def getTFFString(cl: ConsClause, bgTheoryMentioned: String): String =
    "tff(%s, axiom, %s).".format("c_" + cl.id, cl)
}

case class ByCNF(premise: Formula) extends Inference { 
  val name = "cnfTransformation"
  override def getString(cl: ConsClause, bgTheoryMentioned: String): String =
    "%5d: %s\n       by %s(%s)".format(cl.id, cl.toString, name, "f_" + premise.id)
  override def getTFFString(cl: ConsClause, bgTheoryMentioned: String): String =
    "tff(%s, plain, %s, inference(%s, [status(thm)], [%s])).".format("c_" + cl.id, cl, name, "f_" + premise.id)
}

case class ByRef(premise: ConsClause) extends OnePremiseInference { val name = "reflexivity"; override val bgTheory = "equality" }
case class ByFact(premise: ConsClause) extends OnePremiseInference { val name = "factorization"; override val bgTheory = "equality" }
case class BySup(leftPremise: ConsClause, rightPremise: ConsClause) extends TwoPremisesInference { val name = "superposition"; override val bgTheory = "equality" }
case class ByRes(leftPremise: ConsClause, rightPremise: ConsClause) extends TwoPremisesInference { val name = "resolution" }
case class ByChaining(leftPremise: ConsClause, rightPremise: ConsClause) extends TwoPremisesInference { val name = "chaining"; override val bgTheory = "equality" }
case class ByDefine(premise: ConsClause) extends OnePremiseInference { val name = "define"; override val bgTheory = "equality" }
case class BySplitLeft(premise: ConsClause) extends OnePremiseInference { val name = "splitLeft" }
case class BySplitRight(leftEmptyClause: ConsClause, premise: ConsClause) extends OnePremiseInference { val name = "splitRight" }
case class ByClose(premises: List[ConsClause], override val bgTheory: String) extends MultiPremisesInference { val name = "close" }
case class ByDemod(premises: List[ConsClause]) extends MultiPremisesInference { val name = "demodulation"; override val bgTheory = "equality" }
case class ByNegUnitSimp(premises: List[ConsClause]) extends MultiPremisesInference { val name = "negUnitSimplification" }
case class ByBGSimp(premise: ConsClause, override val bgTheory: String) extends OnePremiseInference { val name = "backgroundSimplification" }
case class BySimple(premise: ConsClause) extends OnePremiseInference { val name = "simple" }
case class ByQE(premise: ConsClause, override val bgTheory: String) extends OnePremiseInference { val name = "quantifierElimination" }

trait ClauseOrFormula {
  val id: Int
}

object Proof {

  /** Generate a proof, i.e. a sequence of inferences from input formulas and derived clauses
    *  The given clause cl contains in its inference part a tree-shaped refutation
    */
  def mkRefutation(cl: ConsClause): List[ClauseOrFormula] = {

    import scala.collection.mutable.ListBuffer
    var linRef = ListBuffer.empty[ConsClause]

    /** Linearize the refutation behind the given clause */
    def linearRefutation(cl: ConsClause) {
      // println(s"%d linearRefutation($cl)".format(cl.id))
      if (!(linRef exists (_.id == cl.id))) {
        // Loop check
        cl.info.inference match {
          case ByInput => linRef += cl
          case inf: ByCNF => linRef += cl
          case inf: BySplitRight => {
            linearRefutation(inf.leftEmptyClause)
            linRef += cl
          }
          case inf: OnePremiseInference => {
            linearRefutation(inf.premise)
            linRef += cl
          }
          case inf: TwoPremisesInference => {
            linearRefutation(inf.leftPremise)
            linearRefutation(inf.rightPremise)
            linRef += cl
          }
          case inf: MultiPremisesInference => {
            (inf.premises foreach { linearRefutation(_) })
            linRef += cl
          }
        }
      }
    }

    linearRefutation(cl)

    // Put "ByCNF first. Todo: Same for ByInput once used
    val (byCnfs, others) = linRef.toList partition { _.info.inference.isInstanceOf[ByCNF] }

    // Extract input formulas and put them before the ByCnfs
    val fs = byCnfs map { byCnf =>
      val cl = byCnf.asInstanceOf[ConsClause]
      cl.info.inference.asInstanceOf[ByCNF].premise
    }

    val all = fs ::: byCnfs ::: others

    // Remove duplicates
    var noDupls = List.empty[ClauseOrFormula]
    for (h <- all) {
      if (!(noDupls exists { f => f.isInstanceOf[Formula] == h.isInstanceOf[Formula] && f.id == h.id }))
        noDupls ::= h
    }
    noDupls.reverse // preserve order
  }

  def showRefutationTFF(ref: List[ClauseOrFormula]) {

    ref foreach {
      _ match {
        case f: Formula =>
          println("tff(%s, %s, %s, file('%s', %s)).".format(
            "f_" + f.id,
            f.role.getOrElse("axiom"),
            f, f.source.getOrElse("unknown"), f.name.getOrElse("unknown")
          ))
        case cl: ConsClause => {
          val bgTheoryMentioned =
            cl.info.inference.bgTheory match {
              case "empty" => ""
              case s => ", theory('%s')".format(s)
            }
          println(cl.info.inference.getTFFString(cl, bgTheoryMentioned))
        }
      }
    }
  }

  def showRefutationDefault(ref: List[ClauseOrFormula]) {
    ref foreach {
      _ match {
        case f: Formula => println("%5s: %s".format("f_" + f.id, f))
        case cl: ConsClause => {
          val bgTheoryMentioned =
            cl.info.inference.bgTheory match {
              case "empty" | "equality" => ""
              case s => s + "-"
            }
          println(cl.info.inference.getString(cl, bgTheoryMentioned))
        }
      }
    }
  }
}

