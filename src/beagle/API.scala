package beagle

import parser._
import fol._
import term._
import bgtheory._

/**
 * Exposes basic beagle functions in a clean way in order to
 * simplify the reuse of beagle in other projects.
 *
 * There are two main ways to construct a formula to run beagle on:
 * 1) From a parser; either `TPTPParser` or `SMTLibParser` using the
 * `parseSMT` and `parseTPTP` entry points here.
 * 2) Manually using a `FormulaContext` object here.
 * `FormulaContext` objects carry signature information and construct new
 * operators where needed.
 */
object API {
  //formula construction
  //for simplicity these will build a signature as we go

  /* Some default signatures to use to initialise FormulaContext */

  def LIASig = { 
    println("Note: you can refer to the Int Sort with '$int'")
    LIA.addStandardOperators(Signature.signatureEmpty)
  }
  def LRASig = {
    println("Note: you can refer to Rat Sort with '$rat'")
    LRA.addStandardOperators(Signature.signatureEmpty)
  }
  def LFASig = {
    println("Note: you can refer to Real Sort with '$real'")
    LFA.addStandardOperators(Signature.signatureEmpty)
  }

  /**
   * A `FormulaContext` allows you to create terms and formulas with respect to
   * an implicit signature.</br>
   * The signature is constructed on the fly, only ensuring that no ambiguous operators
   * are created.
   * For example, starting with an empty signature if we call {{{ context.term('f', X) }}}
   * then `context.sig` after will contain the operator f with arity 1.
   * If we then try {{{ context.term('f',List(X,X)) }}} we should expect an exception.
   * @param initSig The initial signature to use when constructing terms is the union of signatures
   * given here. Note that you don't need to include the empty signature on the list.
   * @param prec A precedence with which to sort terms with.
   * We expect that any future signatures should respect the initial precedence.
   */
  class FormulaContext(val initSig: List[Signature] = Nil, val prec: List[String] = List()) {
    //sig must be a var, otherwise you could have the case that subterms of a formula refer
    //to different signatures in a formula context.
    var sig: Signature = 
      if(initSig.isEmpty) Signature.signatureEmpty
      else if(initSig.tail.isEmpty) //signatureEmpty is already a subset of this.
	initSig.head
      else
	initSig.foldLeft(Option(Signature.signatureEmpty))( 
	  (acc, next) => acc.flatMap( _.merge(next) )
	) getOrElse {
	  throw new util.SyntaxError("Initial signatures could not be merged")
	}

    //union of sorts and operators unless there is a conflict
    def merge(that: FormulaContext): FormulaContext = 
      (this.sig merge that.sig) map { s => 
	new FormulaContext(s :: Nil)
      } getOrElse {
	throw new util.SyntaxError("Signatures could not be merged")
      }

    /** A pair of FormulaContexts are equal iff they wrap the same signature */
    override def equals(other: Any) = other match {
      case fc: FormulaContext if(fc.sig == this.sig) => true
      case _ => false
    }

    //for now all given sorts are assumed to be FG
    private def getOrUpdateSort(sName: String): Type =
      if(sName.isEmpty) Signature.ISort
      else {
	sig.findSort(sName) getOrElse {
	  val res = new FGSort(sName)
	  sig += res
	  res
	}
      }

    /** The main way to add a new operator to an implicit signature */
    private def getOrUpdateOp(op: String, a: Arity): Operator =
      sig.findOperator(op, a.argsSorts) filterNot { 
	//if you have an existing op with same arg sorts but different result sort, do not return it
	//TODO- shouldn't this be impossible?
	_.sort() != a.resSort
      } getOrElse {
	//how do you construct BG constants?
	//only a BG operator if it already exists, so FG here
	val res = Operator(FG, op, a)
	sig += res
	res
      }

    /* term construction- possibly extending sig */

    /**
     * Produce a new variable relative to the given signature.
     * Notice that for now all constructed variables are GenVars.
     * @param name The name for the new variable
     * @param sort The sort of the new variable.
     * If the given sort does not already exist in the signature,
     * add a new sort with that name.
     */
    def mkVar(name: String, sort: String = "") = {
      val s = getOrUpdateSort(sort)
      GenVar(name, s)
    }

    def mkVar(name: Traversable[String], sort: String) = {
      val s = getOrUpdateSort(sort)
      name.map(x => GenVar(x,s))
    }

    // def mkTerm(op: String, arg: Term) = mkTerm(op, argFC :: Nil)
    // def mkTerm(op: String, arg: Term, sort: String) = mkTerm(op, argFC :: Nil, sort)

    //how will you handle built in ops here?
    //todo, use a more general collection to allow java arrays for args
    def mkTerm(op: String, args: List[Term], sort: String = ""): Term = {
      val s = getOrUpdateSort(sort)
      val opArity = Arity(args map { _.sort }, s)

      //do not create operators that begin with an int
      if (47 < op(0).toInt && op(0).toInt < 58)
        throw new InternalError("Invalid name for new operator: "+op)
      val f = getOrUpdateOp(op, opArity)

      opArity match {
	case Arity(Nil, s) if(s.thyKind == BG) => BGConst(f)
	case Arity(Nil, s) => FGConst(f)
	case Arity(_, s) => PFunTerm(f, args)
      }
    }

    def mkConst(name: String, sort: String = ""): Const = mkTerm(name, Nil, sort).asInstanceOf[Const]

    /* Domain elements */

    def mkConst(i: Int) = {
      if (!sig.bgSorts.contains(LIA.IntSort))
        sig = LIA.addStandardOperators(sig)
      LIA.DomElemInt(i)
    }

    def mkConst(r: util.Rat) = {
      if (!sig.bgSorts.contains(LRA.RatSort))
        sig = LRA.addStandardOperators(sig)
      LRA.DomElemRat(r)
    }
    
    /* Need predicates */
    val True = TrueAtom
    val False = FalseAtom

    def mkAtom(name: String, args: Traversable[Term]) = {
      val term = mkTerm(name, args.toList, "$o") //ensures that operator is properly added
      datastructures.PredEqn(term).toAtom
    }

    /* formula construction- never changes sig */

    def mkEqn(t1: Term, t2: Term) = Equation(t1, t2)
    //The following can just use regular Formula combinations for now
    // def And(f1: Formula, f2: Formula) = fol.And(f1, f2)
    // def Or(f1: Formula, f2: FormFC) = fol.Or(f1, f2)
    // def Neg(g: FormFC) = fol.Neg(g)

    //TODO- should we check that vars have valid sorts wrt fc?
    // def ForAll(xs: Var*)(f: FormFC) = FormFC(fol.Forall(xs.toList,f.f),f.fc) 
    // def Exists(xs: Var*)(f: FormFC) = FormFC(fol.Exists(xs.toList,f.f),f.fc)

    def show = sig.show

    def toCNF(f: Formula) = {
      //import cnfconversion.standard
      import cnfconversion._

      Sigma = this.sig

      // val (res, newSig) = standard.toCNF(f, false) //dont do full conversion so doesn't need a BG solver set
      // newSig foreach { sig += _ }

      val haveLIA = (f.sorts exists { _ == LIA.IntSort })
      bgtheory.setSolver("PreProcSolver") // Needed for simplification in CNF transformation
      val res = 
        try {
          formulasToClauses(f :: Nil, forceGenvars = false, haveLIA)
        } catch {
          case _: RuntimeException | _: IllSortedTermFail | _: bgtheory.ConversionError => {
            if (haveLIA)
              // try without LIA optimization, e.g. ARI519=1.p is solved this way
              formulasToClauses(f :: Nil, forceGenvars = false, false)
            else
              throw new Exception("CNF conversion failed")
	  }
        }
      sig = Sigma
      res
    }

    //TODO toDNF skolemises but updates the global signature.
    def toDNF(f: Formula) = ???

    //TODO test that this actually sets the precedence
    def prec(precs: List[(String, String)]): Unit = {
      sig = new Signature(sig.sorts, sig.operators, precs)
    }

  }

  /* Formula entry via parsing of TFF or SMTLib files */

  /** The default signature for parsing */
  val fullSignature = main.fullSignature

  private var axiomPath = ""

  def setAxiomPath(path: String) = {
    axiomPath = path
    util.flags.tptpHome.setValue(path)
  }

  def parseTPTP(file: String) = {
    val (tffs, outputSignature, haveConjecture,_) = 
      TPTPParser.parseTPTPFile(file, fullSignature, true)
    //TODO filter result signature so it only has those operators/sorts found in input!
    (tffs, new FormulaContext(outputSignature :: Nil))
  }

  def parseSMT(file: String) = {
    val (tffs, outputSignature, haveConjecture,_) = 
      SMTLibParser.parseSMTLibFile(file,true)
    //TODO filter result signature so it only has those operators/sorts found in input!
    (tffs, new FormulaContext(outputSignature :: Nil))
  }

  /* beagle init. e.g. flags etc. */

  /**
   * Produce a Flag object which contains all default settings for
   * a new beagle instance.
   */
  val defaultSettings = new util.Flags
  
  //could define commonly used variations of this?
  val debugSettings = new util.Flags
  val withProofSettings = new util.Flags

  /* run */
  //todo- where do we enter the main method?
  import util._, datastructures.ConsClause
  def saturate(formulas: List[Formula], fc: FormulaContext, lemmaFs: List[Formula] = Nil, settings: Flags = defaultSettings) = {
    //rough version- ignores flags for now
    fol.Sigma = fc.sig

    //apply flag settings
    util.flags = settings

    //set flags which depend on input or other flags...
    if (flags.hsp.value) main.setHSPFlags()
    
    //TODO- default/don't print for now
    //setPrinter()
    //setReporter(inputFileName)

    if (flags.nodemod.value) {
      //reporter.log("Setting -nodefine because -nodemod was given")
      flags.nodefine.setValue("true")
    }

    main.setOrdering(formulas.sorts)

    /* Conversion of formulas to clauses */
    //val (fs, sigOut, conjectures, isComplicated) = parseInputFile(inputFileName)
    val haveLIA = formulas.sorts.contains(LIA.IntSort) // hope to get rid of other BG sorts by preproc

    bgtheory.setSolver("PreProcSolver") // Needed for simplification in CNF transformation

    var clauses: List[ConsClause] = {
      try {
        cnfconversion.formulasToClauses(formulas, forceGenvars = false, haveLIA = haveLIA)
      } catch {
        case _: RuntimeException | _: IllSortedTermFail => {
          if (haveLIA)
            // try without LIA optimization, e.g. ARI519=1.p is solved this way
            cnfconversion.formulasToClauses(formulas, forceGenvars = false, haveLIA = false)
          else
            throw InternalError("preprocessing failed")
        }
      }
    }

    /* Conversion of lemmas */
    // requires the original signature
    var lemmas = cnfconversion.formulasToClauses(lemmaFs, forceGenvars = true, haveLIA = false)

//TODO
/*
    if (!flags.nonlpp.value) {
      val lemmasAndClauses = lemmas ::: clauses

      // Add axioms for multiplication if required
      if (lemmasAndClauses exists { cl => 
        ( cl.operators contains LIA.NLPPOpInt ) }) {
        //haveReplacedNLTerms = true
          val multAxiomsLemmas = List(
            "! [M: $int, N: $int] : '#nlpp'($sum(M, 1), N) = $sum(N, '#nlpp'(M, N))"
          )
          val multAxiomsClauses = List(
            "! [N: $int] : '#nlpp'(0, N) = 0"
          )
          clauses = clauses ::: parseFormulas(multAxiomsClauses, genvars = true, haveLIA = true)
          lemmas = lemmas ::: parseFormulas(multAxiomsLemmas, genvars = true, haveLIA = true)
      }
      else if (lemmasAndClauses exists { cl => 
        ((cl.operators contains LRA.NLPPOpRat) ||
          (cl.operators contains LRA.Quotient.op)) } ) {
        // to prevent reporting "satisfiable"
        //haveReplacedNLTerms = true
        val multAxiomsLemmas = List(
          "~ (? [X: $rat] : (2/1 = '#nlpp'(X, X)))",
          "! [X: $rat, Y: $rat] : ('#nlpp'(X, Y) = '#nlpp'(Y, X))",
          "! [X: $rat, Y: $rat] : (X = '#nlpp'(Y, $quotient(X,Y)))",
          "! [X: $rat, Y: $rat, Z: $rat] : ( Z = $quotient(X,Y) <=> X = '#nlpp'(Y, Z))"
        )
        lemmas = lemmas ::: parseFormulas(multAxiomsLemmas, genvars = true, haveLIA = false)
      }
      else if (lemmasAndClauses exists { cl => ((cl.operators contains LFA.NLPPOpReal) ||
          (cl.operators contains LFA.Quotient.op)) } ) {
        haveReplacedNLTerms = true
        val multAxiomsLemmas = List(
          "~ (? [X: $real] : (2.0 = '#nlpp'(X, X) & $is_rat(X)))",
          "! [X: $real] : ( $greatereq(X, 0.0) => ( ? [Y: $real] : X = '#nlpp'(Y, Y) ))",
          "! [X: $real, Y: $real] : ('#nlpp'(X, Y) = '#nlpp'(Y, X))",
          "! [X: $real, Y: $real] : (X = '#nlpp'(Y, $quotient(X,Y)))",
          "! [X: $real, Y: $real, Z: $real] : ( Z = $quotient(X,Y) <=> X = '#nlpp'(Y, Z))"
        )
        lemmas = lemmas ::: parseFormulas(multAxiomsLemmas, genvars = true, haveLIA = false)
      }
    }
    */
  // check for equality
    if (clauses forall (_.lits.forall(_.isPredLit))) util.noEquality = true

      if (util.flags.inferSorts.value) {
        //note that inferSorts updates Sigma too
        clauses = Sigma.inferSorts(clauses)
      }

      // toCNF may change the signature, by skolemization, hence can analyse only now
      // also want to pick up any inferred types which might be finite.
      Sigma.analyse()

      // Set the background solver
      main.setSolver(Sigma.bgSorts)

      // set the noAbstraction flag if there are no BG operators requiring abstraction
      // We can do this late only, because Skolemization may introduce parameters
      if (!(lemmas ::: clauses).operators.exists(op => op.kind==BG && !op.isDomElemOp)) {
        util.noAbstraction = true
        // We mus also disable Define, so that no parameters creep in dynamically
        util.flags.nodefine.setValue("true")
      }

      // Now, that the BG solver is set and the noAbstraction flag is (possibly) set, 
      // abstract as needed and simplifyCheap
      clauses = clauses map { _.abstr } flatMap { _.simplifyCheap }
      lemmas = lemmas map { _.abstr } flatMap { _.simplifyCheap }

      //var haveGBTClauseSet = clauses forall { _.unabstrAggressive.isGBTClause }

      //var useAllClauses = true
      // whether clauses consists of all input clauses.
      // Needed for determining if "saturation" means "satisfiable"

      //TODO conjectures?
/*
      if (flags.relevance.value >= 0 && conjectures.nonEmpty) {
        // Do relvancy filtering
        val conjectureFGOps = conjectures.foldLeft(Set.empty[Operator]) { (acc, f) => acc ++ f.FGOps }
        var relevantFGOps = conjectureFGOps 
        // partial transitive closure
        var (i, progress) = (0, true)
        while (i < flags.relevance.value && progress) {
          progress = false // pretend the following foreach-loop doesn't add anything new
          clauses foreach { cl =>
            if ((cl.FGOps intersects relevantFGOps) && // cl is relevant ...
                (cl.FGOps exists { op => ! ( relevantFGOps contains op ) } )) { // ... by providing at least one new FG op
              progress = true
              relevantFGOps = relevantFGOps union cl.FGOps
            }
          }
          i += 1
        }
        // Now get the relevant clauses; always include BG clauses
        val h = clauses.length
        clauses = clauses filter { cl =>  (cl.FGOps subsetOf relevantFGOps) || cl.isBG }
        useAllClauses = clauses.length == h
      }
 */
      //Timer.preProc.stop()

      /** Determine the time left for inferencing, i.e. remove the preprocessing time. */
      // def realTimeout() =
      //   if (flags.timeout.value == 0) 7.0 * 24.0 * 60.0 * 60.0 // say, 7 days
      //   else (flags.timeout.value.toDouble - Timer.total.sinceStarted())

      // needStats = true

// e of imported
    //TODO what goes into the first argument here?
    //TODO timeouts
    main.run({}, clauses, lemmas, 100000)
  }

  //todo- how to use this for incremental/resettable proving?
  import calculus.State
  def continue(s: State, fc: FormulaContext, settings: util.Flags) = {
    //reset signature-- what about var/const counters?
    fol.Sigma = fc.sig
    //re-enable given flags, if necessary?
    //any other state information?
    s.derive(100000)
    //todo this throws an exception to exit
  }

  //todo- bgsolvers etc
  def cooperQE() = ???
  def simplexQE() = ???
  def GMF() = ???

  //todo- can we inspect/communicate with a running instance?

}
