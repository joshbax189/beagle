package beagle.datastructures

import beagle._
import fol._
import term._
import util._

/**
 * Methods which implement abstraction and purification for terms, clauses and
 * equations.
 */
object purification {

  /**
   * Weak abstraction on a clause.
   * @return The new, weakly abstracted clause.
   */
  /*
 * Unfortunately, weak abstraction as defined earlier is buggy, we loose completeness.
 * As simple counterexample is the clause set { x + (1+1) != c + 2, 1+1 = 2 } which is unsatisfiable, sufficiently complete,
 * and weakly abstracted according to the buggy definition,
 * but the calculus does not find a refutation. Essentially it misses the case that x is instantiated to c, in which case the subterm
 * 1+1 needs to be abstracted out. See the latest version of the paper for a corrected definition.   
 * 
  def weakAbstractClause(cl: Clause) = {
    // all the assignments found during purification of cl
    var assignments = List.empty[Lit]
    def weakAbstractTerm(t: Term): Term = {

      // Abstract out the maximal (wrt subterm ordering) BG subterms of t other than variables or domain elements 
      def h(t: Term): Term = t match {
        case v: Var        ⇒ v
        case d: DomElem[_] ⇒ d
        case s: FunTerm ⇒
          if (s.isBG) {
            // found a BG other than a variable or a domain element -- need to abstract out
            val x = (if (s.isPureBG // && (s.sort.kind == BG && util.flags.nogenvars.value)
                )
              AbstVar("X", s.sort) else GenVar("X", s.sort)).fresh()
            assignments ::= Lit(false, Eqn(x, s))
            x
          } else {
            val NonDomElemFunTerm(op, args) = s
            NonDomElemFunTerm(op, args map { h(_) })
          }
      }
      // Need to abstract out BG terms from non-BG terms  
      if (t.isBG) t else h(t)
    }

    // Body of WeakAbstractClause
    val abstLits = cl.lits map {
      l ⇒ Lit(l.isPositive, Eqn(weakAbstractTerm(l.eqn.lhs), weakAbstractTerm(l.eqn.rhs)))
    }
    Clause(abstLits ::: assignments, cl.idxRelevant, cl.age)
  }
*/

  /**
   * Weak abstraction on a clause.
   * Unfortunately, weak abstraction as defined earlier is buggy, we lose completeness.
   * As simple counterexample is the clause set { x + (1+1) != c + 2, 1+1 = 2 } which is unsatisfiable, sufficiently complete,
   * and weakly abstracted according to the buggy definition,
   * but the calculus does not find a refutation. Essentially it misses the case that x is instantiated to c, in which case the subterm
   * 1+1 needs to be abstracted out. See the latest version of the paper for a corrected definition.
   * @return The new, weakly abstracted clause, it has the same id as the given clause
   */
  def weakAbstractClause(cl: ConsClause) = {
    //util.Timer.abs.start()
    // The unprocessed literals, those from cl and introduced during abstraction
    var open = List.empty[Lit]
    var closed = List.empty[Lit]

    def weakAbstractTerm(s: Term): Term = {

      def abstracted(t: Term) = {
        assume(t.isBG)
        val x = (if (t.isPureBG) AbstVar("X", t.sort) else GenVar("X", t.sort)).fresh()
        open ::= Lit(false, Eqn(x, t))
        x
      }

      // Possible target terms for abstraction are non-domain, non-variable BG terms
      def isPossibleTargetTerm(t: Term) = t.isBG && !t.isVar && !t.isDomElem
      // whether a term t is one of the "side terms" s_i in the definition of weak abstraction
      def isSideTerm(t: Term) = t.isImpureBG || t.isFG

      // body of weakAbstractTerm 
      if (s.isPureBG)
        s // cheap pre-test
      else s match {
        case PFunTerm(op, args) ⇒
          (op.kind, args) match {
            // Check the cases as per definition of weak abstraction
            case (FG, args) ⇒ PFunTerm(op, args map { t ⇒ if (isPossibleTargetTerm(t)) abstracted(t) else weakAbstractTerm(t) })
            // The remaining cases deal with BG operators
            // We check for arities 1 and 2 separately first because these are by far the most common ones 
            // case (BG, List()) => ... not needed - s is a pure BG term, which is caught above
            case (BG, List(arg)) ⇒
              // side terms do not exist, hence can straight recurse into argument
              PFunTerm(op, List(weakAbstractTerm(arg)))
            case (BG, List(arg1, arg2)) ⇒ {
              // check both arguments separately. This is possible because abstraction does not change the
              // side term property
              val arg1abst = if (isPossibleTargetTerm(arg1) && isSideTerm(arg2)) abstracted(arg1) else weakAbstractTerm(arg1)
              val arg2abst = if (isPossibleTargetTerm(arg2) && isSideTerm(arg1)) abstracted(arg2) else weakAbstractTerm(arg2)
              PFunTerm(op, List(arg1abst, arg2abst))
            }
            // Finally the general case
            case (BG, args) ⇒ {

              var soFarAbstracted = List.empty[Term]
              var rest = args
              while (!rest.isEmpty) {
                val next = rest.head
                rest = rest.tail
                if (isPossibleTargetTerm(next) && ((soFarAbstracted exists { isSideTerm(_) }) || (rest exists { isSideTerm(_) })))
                  soFarAbstracted ::= abstracted(next)
                else
                  soFarAbstracted ::= weakAbstractTerm(next)
              }
              PFunTerm(op, soFarAbstracted.reverse)
            }
          }
        case _ ⇒
          // Variables, domain elements, parameter, constants - they do not contain subterms, hence cannot contain target terms
          s
      }
    }

    if (cl.isPureBG){
      //util.Timer.abs.stop()
      cl // trivial case - pure BG clauses never need abstraction
    } else {
      open = cl.lits
      while (!open.isEmpty) {
        val next = open.head
        open = open.tail
        if (next.isPureBG) //|| (next.symConsts.isEmpty && !next.operators.exists(_.kind==BG))) //is this also cheap?
          // cheap pre-test
          closed = next :: closed
        else {
          //val res = Lit(next.isPositive, Eqn(weakAbstractTerm(next.eqn.lhs), weakAbstractTerm(next.eqn.rhs))) 
	  val res = next.map(weakAbstractTerm(_))
          // println("*** weak abstraction of " + next + " is\n" + res)
          closed = res :: closed
        }
      }
      val clr = closed.reverse
      if (clr == cl.lits) {
        //util.Timer.abs.stop()
	cl // Avoid building the clause
      } else {
	//util.Timer.abs.stop()
        //Clause(cl.id, clr, cl.idxRelevant, cl.age)
	cl.modified(lits=clr)
      }
    }

    // Body of WeakAbstractClause
    //    val abstLits = cl.lits map {
    //      l ⇒ Lit(l.isPositive, Eqn(weakAbstractTerm(l.eqn.lhs), weakAbstractTerm(l.eqn.rhs)))
    //    }
  }

  /**
   * Purifying a term.
   * A term is pure if it contains only operators of one kind (either FG or BG).
   * Then constants and variables are pure by definition.
   */
  def purify(s: Term): (Term, List[Eqn]) = {

    // Purifying a term at top-level position
    def hpurify(t: Term, keepKind: OpKind): (Term, List[(Var, FunTerm)]) = {

      // println("==> hpurify: " + t + " " + flip)
      t match {
        case v: Var ⇒ (v, List.empty)
        // Must explicitly distinguish between Consts and PFunTerms because
        // there is no constructor for FunTerm
        case c: Const ⇒
          if (c.op.kind == keepKind)
            (c, List.empty)
          else {
            // c itself is offending
            val name =
              if (c.isPureBG || (c.sort.thyKind == BG && !util.flags.genvars.value))
                AbstVar("X", c.sort).fresh()
              else
                GenVar("X", c.sort).fresh()
            (name, List((name, c)))
          }
        case t: PFunTerm ⇒ {
          val PFunTerm(op, args) = t
          if (op.kind == keepKind) {
            // t is allowed to stay, but purify args
            var (argsPurified, argsDefs) = (List.empty[Term], List.empty[(Var, FunTerm)])
            for (arg ← args) {
              val (a, ds) = hpurify(arg, keepKind)
              argsPurified ::= a
              argsDefs :::= ds
            }
            (PFunTerm(op, argsPurified.reverse), argsDefs)
          } else {
            // t itself is offending
            val name =
              if (t.isPureBG || (t.sort.thyKind == BG && !util.flags.genvars.value))
                AbstVar("X", t.sort).fresh()
              else
                GenVar("X", t.sort).fresh()
            (name, List((name, t)))
          }
        }
      }
    }

    s match {
      case v: VarOrSymConst ⇒ (v, List.empty) // Variables are always pure by definition
      case s: FunTerm ⇒ {
        val (sPure, sDefs) = hpurify(s, s.op.kind)
        var open = sDefs
        var done = List.empty[Eqn]
        while (!open.isEmpty) {
          // Select the first open element 
          val (x, t) = open.head
          //println("==> selected = " + selected)
          open = open.tail
          // We call hpurify so that it leaves the toplevel operator in place.
          // This guarantees termination.
          // We can be sure the Defs are FunTerms, by construction
          val (tPure, tDefs) = hpurify(t, t.asInstanceOf[FunTerm].op.kind)
          done ::= Eqn(x, tPure)
          open :::= tDefs
        }
        (sPure, done)
      }
    }
  }
/*
  //experimental- weakAbstraction- expects outer to abstract terms in equations
  def twoStepWA(t: Term) = {
    // Possible target terms for abstraction are non-domain, non-variable BG terms
    def isPossibleTargetTerm(t: Term) = t.isBG && !t.isVar && !t.isDomElem
    // whether a term t is one of the "side terms" s_i in the definition of weak abstraction
    def isSideTerm(t: Term) = t.isImpureBG || t.isFG

    val subterms = t.subTermsWithPos()
    val targetTerms = subterms.filter(isPossibleTargetTerm(_))
    val actualTTerms = targetTerms.filter({case (s,p) => {
      val pref = p.take(p.length-1)
      val parentT = if(pref.isEmpty) t else subterms.find(_._2==pref).get._1
      //if parent is FG or has a side term
      (parentT.isFG || parentT.asInstanceOf[FunTerm].args.exists(isSideTerm(_)))
    }})

    //actualTTerms contains all actual target terms of t and their positions
    //so replace these at their positions then add as abstractions
    var remaining = actualTTerms
    while (!remaining.isEmpty) {
      val (s,p) = remaining.head
      val pLength = p.length
      //replace terms in other abstracted terms
      for((s2,p2) <- remaining.tail) {
	if(p2.take(pLength) == p) //must replace this inside s
      }
    }

    
  }
*/
  /**
   * Purifying an equation e.
   * Essentially lifts purify for terms to equations.
   * @return A pair (e', es) where e' is the purified version of e
   * and es is a list of definitions given as Eqns for the extracted subterms from e.
   * es will consist of pure equations only.
   */
  def purifyEqn(e: Eqn): (Eqn, List[Eqn]) = {
    e match {
      case PredEqn(a) ⇒ {
        val (aPure, aDefs) = purify(a)
        (PredEqn(aPure), aDefs)
      }
      case Eqn(_: Var, _: Var) ⇒ (e, List.empty)
      case Eqn(lhs: Var, rhs: FunTerm) ⇒ {
        val (rhsPure, rhsDefs) = purify(rhs)
        (Eqn(lhs, rhsPure), rhsDefs)
      }
      case Eqn(lhs: FunTerm, rhs: Var) ⇒ {
        val (lhsPure, lhsDefs) = purify(lhs)
        (Eqn(lhsPure, rhs), lhsDefs)
      }
      case Eqn(lhs: FunTerm, rhs: FunTerm) ⇒ {
        val (FunTerm(Operator(lhsKind, _, _), _), FunTerm(Operator(rhsKind, _, _), _)) = (lhs, rhs)
        val (lhsPure, lhsDefs) = purify(lhs)
        val (rhsPure, rhsDefs) = purify(rhs)
        if (lhsKind == rhsKind)
          // no need to further purify
          (Eqn(lhsPure, rhsPure), lhsDefs ::: rhsDefs)
        else {
          // A bit of heuristics: when e is of the form t = f(...) where f is FG turn it around
          // so that "f(...) = x if x = t" results, this way providing a positive equation for f
          // This is "more complete"
          val (finalLhsPure, finalRhsPure) =
            if (rhsKind == FG) (rhsPure, lhsPure) else (lhsPure, rhsPure)

          val x = if ((finalLhsPure.isPureBG && finalRhsPure.isPureBG) ||
            (e.sort.thyKind == BG && !util.flags.genvars.value))
            AbstVar("X", e.sort).fresh()
          else
            GenVar("X", e.sort).fresh()
          (Eqn(finalLhsPure, x), Eqn(finalRhsPure, x) :: (lhsDefs ::: rhsDefs))
        }
      }
    }
  }

  /**
   * Standard Abstraction for a clause.
   * Given f(X+g(5)) we get f(Y1) v Y1!=X+Y2 v Y2!=g(Y3) v Y3!=5.
   * After standard abstraction, each literal of a clause is either over the
   * foreground signature or the background signature.
   */
  def stdAbstractClause(cl: ConsClause): ConsClause = {
    var defs = List.empty[Lit]

    val cl1 = cl mapLits { case Lit(sign, e) => 
      val (eAbst, eDefs) = purifyEqn(e)
      defs :::= eDefs map { Lit(false, _) }
      Lit(sign, eAbst)
    }

    cl1 append defs
  }

  /**
   * Purifying a list of literals. Assume this is called during preprocessing,
   * where all variables are general.
   * @param fs A list of Formula assumed to be literals.
   * @return Two lists; the FG literals and the BG literals, respectively.
   */
  def purifyFormulas(fs: List[Formula]): (List[Lit], List[Formula]) = {
    var fgRes = List.empty[Lit]
    var bgRes = List.empty[Formula]
    var open = fs

    while (!open.isEmpty) {
      // Take the next literal
      val f = open.head
      open = open.tail
      f match {
        case l @ (Neg(Atom(_, _)) | Atom(_, _)) ⇒ {
          // println("In purify: " + l)
          // f is a literal - decompose it
          val (sign, e) = l match {
            case Neg(a @ Atom(_, _)) ⇒ a.toSignedEqn match { case (sign, eqn) ⇒ (!sign, eqn) }
            case a @ Atom(_, _)      ⇒ a.toSignedEqn
          }
          // println("In purify: " + l + " -- " + sign + " -- " + e)

          // 	Purify it, gives the pure literal together with the definitions
          // (equations) resulting from purification
          val (ePure, eDefs) = purifyEqn(e)
          // println(ePure + " -- " + eDefs)
          val lPure = Lit(sign, ePure)
          // println(lPure)
          // eDefs are all pure equations, hence:
          val defsPure = eDefs map { Lit(false, _) }
          // These are all the new pure literals
          val newPure = lPure :: defsPure
          // Distribute them to the proper results
          //          bgRes :::= (newPure filter { _.kind == BG }) map { _.toLiteral }
          // fgRes :::= newPure filter { _.kind == FG }
          bgRes :::= (newPure filter { _.isBG }) map { _.toLiteral }
          fgRes :::= newPure filter { !_.isBG }
        }
        case f ⇒ bgRes ::= f // f is a non-literal-formula, assume it is BG
      }
    }
    (fgRes, bgRes)
  }

}
