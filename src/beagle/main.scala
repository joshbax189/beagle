package beagle

import calculus._
import datastructures._
import fol._
import term._
import parser._
import util._
import cnfconversion._
import bgtheory._
import finite._
//import proofoutput.TFFProofReporter

object main {

  val versionString = "0.9.29 (6/10/2015)"
  val programName = "beagle"

  /**
   *  Indicates program has begun proof and needs to print stats if
   *  terminated. If terminating normally set back to false before
   *  calling sys.exit.
   */
  var needStats = false

  /** True if the input clause set belongs to the BGT fragment.*/
  var haveGBTClauseSet = false

  /** The default signature for parsing */
  val fullSignature = LFA.addStandardOperators(LRA.addStandardOperators(LIA.addStandardOperators(Signature.signatureEmpty)))

  var inPreprocessing: Boolean = false // hack

  /**
   * Set if non-linear terms have been replaced- prevents incorrectly concluding counter-sat
   */
  var haveReplacedNLTerms = false

  /**
   * Print usage information.
   */
  def usage() {
    println()
    println("Usage: " + programName + " [FLAG]... FILE")
    // println("Call " + programName + " without options for help")
    flags.usage()
  }

  /** Print SZS status string.*/
  def szsStatus(status: String, inputFileName: String) {
    reporter.log("")
    println("% SZS status " + status + " for " + inputFileName)
  }

  /*
   * Print methods for proof preamble
   */
  def printInputFormulas(fs: Seq[Formula]) {
    println("Input formulas")
    println("==============")
    fs foreach { f =>
      printer.pp(f); println()
      f.annotation foreach { t => println("/* " + t + " */") }
    }

    println()
  }

  def printSignature() {
    println("Clause set signature")
    println("====================")
    Sigma.show()
    println()
  }

  def printClauseSet(clauses: List[ConsClause]) {
    println("Clause set")
    println("==========")
    // allClauses foreach { cl ⇒ println(cl + (cl.annotation match { case None ⇒ ""; case Some(t) ⇒ " /* " + t + " */" })) }
    clauses foreach { println(_) }
    println()
  }

  def printLemmas(lemmas: List[ConsClause]) {
    println("Lemmas")
    println("======")
    lemmas foreach { println(_) }
    println()
  }

  def parseInputFile(inputFileName: String): (Seq[Formula], Signature, Seq[Formula]) = {
    val doSMTLibFile =
      flags.formatFlag.value == "smt" ||
        (flags.formatFlag.value == "auto" &&
          (List(".smt", ".smt2") exists { inputFileName.stripSuffix(_) != inputFileName }))

    import Signature._

    //set the input signature based on the format flag
    val sigIn =
      if (flags.finiteBeagle.value) {
        //if finite beagle is used, force tff-int and add FinitePred to signature
        flags.formatFlag.setValue("tff-int")
        LIA.addStandardOperators(signatureEmpty) + FinitePred
      } else {
        //same as usual
        flags.formatFlag.value match {
          case "tff-real" ⇒ LFA.addStandardOperators(signatureEmpty)
          case "tff-rat" ⇒ LRA.addStandardOperators(signatureEmpty)
          case "tff-int" ⇒ LIA.addStandardOperators(signatureEmpty)
          // The whole lot, that is, combined theories. So far this is only a means to get around specifying the format
          // explicitly and let beagle do it for you.
          case "tff-ari" ⇒ fullSignature
          case "auto" if !doSMTLibFile ⇒ fullSignature
          case "tff" ⇒ signatureEmpty
          case "cnf" ⇒ signatureEmpty
          case _ if doSMTLibFile ⇒ null // irrelevant
          case format ⇒ throw new CmdlineError("Format " + format + " not implemented")
        }
      }

    val (fs, sigOut, conjectures, domElemInts) =
      if (doSMTLibFile)
        try {
          if (flags.include.value != "") 
	    throw CmdlineError("'-include' cannot be used with SMT-LIB input files.")
          SMTLibParser.parseSMTLibFile(inputFileName, flags.genvars.value)
        } catch {
          case SMTLibParser.Error(s: String) => {
            println("SMTLibParser: error: " + s)
            sys.exit(1)
          }
        }
      else {
        if (flags.include.value != "") {
          // include file given, parse that first
          val (hfs1, sigOut1, h1, _) = TPTPParser.parseTPTPFile(flags.include.value, sigIn, flags.genvars.value)
          val (hfs2, sigOut2, h2, deis) = TPTPParser.parseTPTPFile(inputFileName, sigOut1, flags.genvars.value)
          (hfs2 ::: hfs1, sigOut2, h1 ::: h2, deis)
        } else
          TPTPParser.parseTPTPFile(inputFileName, sigIn, flags.genvars.value)
      }

    //Sigma = sigOut

    (fs, sigOut, conjectures)
  }

  /**
   * I assume that the parser would expect an unmodified signature here.
   */
  def parseLemmaFile(lemmaFileName: String, sigma: Signature): List[ConsClause] = 
    if (lemmaFileName == "")
      List.empty
    else { 
      val fs = TPTPParser.parseTPTPFile(lemmaFileName, sigma, genvars = true)._1 
      formulasToClauses(fs, forceGenvars = true, haveLIA = false)
    }

/*  def parseFormulas(formulas: List[String], genvars: Boolean) = 
    formulas map {
      TPTPParser.parseFormula(_, fullSignature, genvars = genvars)
    } flatMap {
      f ⇒ toCNF(f, true)
    } flatMap {
      litList ⇒ literalListToClauses(litList, canLIAQE = false)
    }
 */

  def parseFormulas(formulas: List[String], genvars: Boolean, haveLIA: Boolean) = {
    val fs = formulas map {
      TPTPParser.parseFormula(_, fullSignature, genvars = genvars)
    }
    formulasToClauses(fs, forceGenvars = genvars, haveLIA = haveLIA)
  }


  /**
   * Set flags in order to model the HSP calculus
   */
  def setHSPFlags() {
    flags.genvars.setValue("false")
    flags.stdabst.setValue("true")
    flags.bgsimp.setValue("cautious")
    flags.split.setValue("off")
  }

  def setPrinter(printerName: String) {
    beagle.util.printer =
      if (printerName == "tff") TFFPrinter else DefaultPrinter
  }

  def setReporter(inputFileName: String) {
    if (beagle.util.flags.debug.value == "inf")
      beagle.util.reporter = DebugReporter
    else if (beagle.util.flags.debug.value == "bg")
      beagle.util.reporter = DebugBGReporter
    else if (beagle.util.flags.quietFlag.value || flags.finiteBeagle.value) //force a quiet reporter for finite beagle
      beagle.util.reporter = QuietReporter
    else if (beagle.util.flags.proof.value) {
      beagle.util.reporter = DefaultReporter
      // beagle.util.printer = TFFPrinter
      // beagle.util.reporter = new TFFProofReporter(inputFileName + "_proof")
    } else
      beagle.util.reporter = DefaultReporter
  }

  /**
   * Set the term ordering.
   * If `auto` is selected choose LPO if there is any BG sorts in
   * the argument sort set, otherwise choose KBO.
   * @param sorts A set of sorts used to decide between LPO and KBO.
   */
  def setOrdering(sorts: Set[Type]) {
    if (flags.termOrdering.value == "auto") {
      if (sorts exists { _.thyKind == BG })
        flags.termOrdering.setValue("LPO")
      else
        flags.termOrdering.setValue("KBO")
    }
    // set the global ordering object as appropriate
    flags.termOrdering.value match {
      case "LPO" => Ordering.setOrdering(LPO)
      case "KBO" => Ordering.setOrdering(KBO)
    }
  }



  /**
   * Set the BG solver
   */
  def setSolver(bgSorts: Set[Type]) {

    val LIADefaultSolver = "iQE"
    val FDDefaultSolver = "fd"

    if (flags.liasolver.value == "" && // unspecified
      flags.smtsolver.value != "")
      // override
      flags.liasolver.setValue("smt")

    val solver =
      if (bgSorts.isEmpty) "empty"
      else {
        // Although bgSorts could contain more than one sort we sample only the first one. This is currently good enough.
        // If there's more than one BG sort, then we're in the case of finite domains, and tff-fd has to be given explicitly anyway
        (flags.formatFlag.value, flags.liasolver.value, bgSorts.head) match {
          case ("tff", _, _) ⇒ throw CmdlineError("Cannot use format tff in presence of BG theories")
          case (_, "cooper", LIA.IntSort) => "cooper"
          case (_, "iQE", LIA.IntSort) => "iQE"
          case ("tff-int", "", LIA.IntSort) |
              ("tff-ari", "", LIA.IntSort) |
              ("auto", "", LIA.IntSort) |
              ("smt", "", LIA.IntSort) ⇒ LIADefaultSolver
          case ("tff-int", "smt", LIA.IntSort) |
              ("tff-ari", "smt", LIA.IntSort) |
              ("auto", "smt", LIA.IntSort) |
              ("smt", "smt", LIA.IntSort) ⇒ "smt"
          case ("tff-rat", _, LRA.RatSort) |
              ("tff-ari", _, LRA.RatSort) |
              ("auto", _, LRA.RatSort) |
              ("smt", _, LRA.RatSort) ⇒ "smt"
          case ("tff-real", _, LFA.RealSort) |
              ("tff-ari", _, LFA.RealSort) |
              ("auto", _, LFA.RealSort) |
              ("smt", _, LFA.RealSort) ⇒ "smt"
          case ("tff-fd", "", _) ⇒ FDDefaultSolver
          case (_, _, _) ⇒ throw CmdlineError("Incompatible combination of format/bgsolver/BG theories")
        }
      }

    if (solver == "empty")
      bgtheory.setSolver("empty")
    else if (solver == "smt") {
      if (flags.smtsolver.value == "")
         throw GeneralError("Need to specify SMT solver in presence of BG sort(s) " + bgSorts.toList.toMyString("", ", ",""))
      bgtheory.setSMTSolver(flags.smtsolver.value, bgSorts.head)
      reporter.debug("Background SMT solver: " + flags.smtsolver.value + " for sort " + bgSorts.head)
    } else {
      // Use a built-in LIA solver
      bgtheory.setSolver(solver)
      reporter.debug(s"Background theory solver: $solver \n")
    }
    reporter.debug("Theory solver reports UNSAT cores: " + bgtheory.solver.canUNSATCore + "\n")
  }

  /** The 'run' procedure below returns one of the following */

  sealed abstract class DerivationResult
  case class Unsat(emptyClause: ConsClause) extends DerivationResult
  case object Timeout extends DerivationResult
  case class Sat(finalState: State) extends DerivationResult
  case class Unknown(finalState: State) extends DerivationResult // running out of ideas what to do next

  /*
   * Run 'the prover' on sets of given and sos clauses until the timeoutout is reached, 
   *  exec 'settings' first (typically flag settings)
   * @param sos The set of support for this particular call.
   * @param timeout The amount of time allowed for this round of inferencing.
   */
  def run(settings: => Unit, given: List[ConsClause], sos: List[ConsClause], timeout: Double): DerivationResult = {

    settings

    // determine globally instead
    // val haveGBTClauseSet = given forall { _.isGBTClause }
    // The set of terms we have put in definitions already
    var definedBSFGTerms = List.empty[Term]
    var givenExt = given // given clauses possibly extended with Definitions
    var result: Option[DerivationResult] = None

    while (result == None) try {
      (new State(givenExt, sos)).derive(timeout) match {
	case Right(state) => {
	  if (!haveReplacedNLTerms && haveGBTClauseSet && !flags.nodefine.value) {
            // We have sufficient completeness a priori
            // This includes the case of the empty background theory
            // No point in continuing by instantiation.
            result = Some(Sat(state))
	  } else if (flags.nodefine.value) {
            // Don't have Define, little we can do
            result = Option(Unknown(state))
	  } else if (!flags.useInst.value || flags.formatFlag.value == "tff-fd") {
            // Not allowed to instantiate - give up at this stage
            // or inst flag used differently, in conjunction with tff-fd
            result = Option(Unknown(state))
	  } else {
            // Try instatiation + define
            // var inconclusive = false // whether instantiation is needed to get a conclusive result
            import scala.util.control.Breaks
            val breakInner = new Breaks
            breakInner.breakable {
              for (
		cl ← state.old.clauses;
		clUn = cl.unabstrAggressive;
		lit ← clUn.lits;
		t ← List(lit.eqn.lhs, lit.eqn.rhs);
		s ← t.subterms;
		if !s.isGround && s.isBSFG && !(definedBSFGTerms exists { _ ~ s })
              ) {
		// s is a target term for instantiation then.
		// println("xx " + s)
		// println("xx " + definedBSFGTerms)
		val gamma = Term.mkSkolemElementSubst(s.vars)
		val sgamma = gamma(s)
		val (newDefEqn, newOp) = sgamma.asInstanceOf[FunTerm].mkDefinition()
                Sigma += newOp
		val newDef = new ConsClause(List(newDefEqn.toLit), Nil, ClsInfo(Set.empty, 0, ByDefine(cl), cl.info.deltaConjecture+1))
		definedBSFGTerms ::= s
		givenExt ::= newDef.abstr
		reporter.log("Restarting with a definition " + newDef)
		stats.nrRestarts += 1
		breakInner.break()
              }
              // After the for-loop. Not having broken means we have definitions for all BS FG terms unless
              // inconclusive is true. In the latter case we could not instantiate in a new way or Define
              // could not be applied and we give up
              result = Option(Unknown(state))
            } // breakInner
	  } // else
	}
	case Left(emptyClause) => result = Option(Unsat(emptyClause)) //i.e. FAIL
      }
    } catch {
      //case FAIL ⇒ result = Some(Unsat)
      case TIMEOUT ⇒ result = Option(Timeout)
    }
    // After the while loop
    result.get
  }

  /** Divided into a preprocessing and a run phase */
  def main(args: Array[String]) {

    Timer.total.start()
    Timer.preProc.start()

    // what to do when the user presses ^c in terminal
    // now only prints stats if the proof has begun and not terminated normally.
    sys.addShutdownHook(shutdownHook)

    var inputFileName: String = null

    inPreprocessing = true

    try {

      // Get the input file name
      // parseflags returns the index of the first non-flag parameter
      val pos = flags.parseflags(args)
      // need to check -help flag before reporting the Incomplete commandline error
      if (flags.help.value) {
        usage()
        sys.exit(1)
      }
      if (flags.version.value) {
        println("%s version %s".format(programName, versionString))
        sys.exit(1)
      }

      if (pos >= args.length)
        throw CmdlineError("Missing filename")

      inputFileName = args(pos)

      // set some flags depending on other flags
      if (flags.hsp.value) setHSPFlags()
      // Setting the printer (to TFF) before proff search has some unintended
      // consequences, cf GEG025=1.p which takes more inferences then.
      // Some hidden and non-obvious use of toString somewhere...
      // For the time being do TFF printing only when it is really needed,
      // i.e. for outputting a refutation or a saturation
      // setPrinter()
      setReporter(inputFileName)

      reporter.log("This is %s, version %s\n".format(programName, versionString))

      val (fs, sigOut, conjectures) = parseInputFile(inputFileName)
      // val haveLIA = (fs.sorts filter { _.kind == BG }) == Set(LIA.IntSort) // The only BG sort is int?
      val haveLIA = (fs.sorts exists { _ == LIA.IntSort }) // hope to get rid of other BG sorts by preproc

      setOrdering(fs.sorts)

      Sigma = sigOut

      // fs foreach { f => { printer.pp(f); println() } }

      // Conversion to clauses. "haveLIA" might be a too optimistic a guess, possibly need to undo
      var clauses: List[ConsClause] = null
      bgtheory.setSolver("PreProcSolver") // Needed for simplification in CNF transformation
      try {
        clauses = formulasToClauses(fs, forceGenvars = false, haveLIA = haveLIA)
      } catch {
        case _: RuntimeException | _: IllSortedTermFail | _: bgtheory.ConversionError => {
          if (haveLIA)
            // try without LIA optimization, e.g. ARI519=1.p is solved this way
            clauses = formulasToClauses(fs, forceGenvars = false, haveLIA = false)
          else
            throw InternalError("preprocessing failed")
        }
        // case e: NonLinearTermFail =>
        //   // Try without LIA optimization, e.g. ARI119=1.p is solved this way
        //   // This should no longer occur, though, with the replacement by #nlpp
        //   clauses = formulasToClauses(fs, haveLIA = false)
      }
      
      // requires the original signature so perform sort inference after
      var lemmas = parseLemmaFile(flags.lemmas.value, Sigma)

      // Do the conjectureGenvars flag
      // That is, rename the abstraction variables in conjecture clauses into ordinary ones
      if (!util.flags.genvars.value && util.flags.conjectureGenvars.value) {
        clauses = clauses map { cl => if (cl.info.deltaConjecture == 0) cl.freshGenVars else cl }
      }

      val lemmasAndClauses = lemmas ::: clauses
      // Add axioms for multiplication if required
      if (lemmasAndClauses exists { cl => 
        ( cl.operators contains LIA.NLPPOpInt ) }) {
        haveReplacedNLTerms = true
        if (!flags.nonlpp.value) {
          val multAxiomsLemmas = List(
            "! [M: $int, N: $int] : '#nlpp'($sum(M, 1), N) = $sum(N, '#nlpp'(M, N))"
          )
          val multAxiomsClauses = List(
            "! [N: $int] : '#nlpp'(0, N) = 0"
          )
          // reporter.log("Adding integer multiplication axioms because of presence of non-linear multiplication")
          clauses = clauses ::: parseFormulas(multAxiomsClauses, genvars = true, haveLIA = true)
          lemmas = lemmas ::: parseFormulas(multAxiomsLemmas, genvars = true, haveLIA = true)
        }
      }
      else if (lemmasAndClauses exists { cl => 
        ((cl.operators contains LRA.NLPPOpRat) ||
          (cl.operators contains LRA.Quotient.op)) } ) {
        // to prevent reporting "satisfiable"
        haveReplacedNLTerms = true
        val multAxiomsLemmas = List(
          "~ (? [X: $rat] : (2/1 = '#nlpp'(X, X)))",
          "! [X: $rat, Y: $rat] : ('#nlpp'(X, Y) = '#nlpp'(Y, X))",
          "! [X: $rat, Y: $rat] : (X = '#nlpp'(Y, $quotient(X,Y)))",
          "! [X: $rat, Y: $rat, Z: $rat] : ( Z = $quotient(X,Y) <=> X = '#nlpp'(Y, Z))"
        )
        reporter.log("Adding integer multiplication axioms because of presence of non-linear multiplication")
        lemmas = lemmas ::: parseFormulas(multAxiomsLemmas, genvars = true, haveLIA = false)
      }
      else if (lemmasAndClauses exists { cl => ((cl.operators contains LFA.NLPPOpReal) ||
          (cl.operators contains LFA.Quotient.op)) } ) {
        haveReplacedNLTerms = true
        val multAxiomsLemmas = List(
          "~ (? [X: $real] : (2.0 = '#nlpp'(X, X) & $is_rat(X)))",
          "! [X: $real] : ( $greatereq(X, 0.0) => ( ? [Y: $real] : X = '#nlpp'(Y, Y) ))",
          "! [X: $real, Y: $real] : ('#nlpp'(X, Y) = '#nlpp'(Y, X))",
          "! [X: $real, Y: $real] : (X = '#nlpp'(Y, $quotient(X,Y)))",
          "! [X: $real, Y: $real, Z: $real] : ( Z = $quotient(X,Y) <=> X = '#nlpp'(Y, Z))"
        )
        reporter.log("Adding integer multiplication axioms because of presence of non-linear multiplication")
        lemmas = lemmas ::: parseFormulas(multAxiomsLemmas, genvars = true, haveLIA = false)
      }

      // check for equality
      if (clauses forall (_.lits.forall(_.isPredLit))) util.noEquality = true

      if (util.flags.inferSorts.value) {
        //note that inferSorts updates Sigma too
        clauses = Sigma.inferSorts(clauses)
      }

      // toCNF may change the signature, by skolemization, hence can analyse only now
      // also want to pick up any inferred types which might be finite.
      Sigma.analyse()

      var bgSorts = (clauses.foldLeft(Set.empty[Type])(_ ++ _.sorts)) filter { _.thyKind == BG }
      // reporter.log("Background sorts used in clause set: " + bgSorts.toList.toMyString("none", "", ", ", ""))
      // println(clauses)


      // if (bgSorts contains LFA.RealSort) {
      //   // brutally coerce all operators on reals into rationals. This allows us to use one solver only, the rational solver,
      //   // and to mix reals and rationals in one input file.
      //   clauses = clauses map { LRA.toRat(_) }
      //   bgSorts = (bgSorts - LFA.RealSort) + LRA.RatSort
      //   Sigma = Sigma.toRat
      // }

      if (flags.nodemod.value) {
        reporter.log("Setting -nodefine because -nodemod was given")
        flags.nodefine.setValue("true")
      }

      // if (bgSorts.size > 1 && (bgSorts contains LRA.RatSort))
      if (bgSorts.size > 1)
        throw GeneralError("Combination of rational/real arithmetic with other background theories not yet supported")

      // Set the background solver
      setSolver(bgSorts)

/*
 // disabled - not effective
      if (flags.delBGLemmas.value) {
        // Filter out BG lemmas. All parameter-free BG clauses are considered as lemmas,
        // irrespective of whether they are pure or not.
        def isBGLemma(cl: ConsClause) = cl.isBG && cl.symConsts.isEmpty
        if (verbose) {
          val BGLemmas = clauses filter { isBGLemma(_) }
          if (BGLemmas.nonEmpty) {
            println("Delete clauses that are considered as BG lemmas:")
            BGLemmas foreach { println(_) }
            println()
          }
        }
        clauses = clauses filterNot { isBGLemma(_) }
      }
 */
      clauses = clauses flatMap { _.abstr.simplifyCheap }
      lemmas = lemmas flatMap { _.abstr.simplifyCheap }

      haveGBTClauseSet = clauses forall { _.unabstrAggressive.isGBTClause }

      var useAllClauses = true
      // whether clauses consists of all input clauses.
      // Needed for determining if "saturation" means "satisfiable"

      if (flags.relevance.value >= 0 && conjectures.nonEmpty) {
        // Do relvancy filtering
        val conjectureFGOps = conjectures.foldLeft(Set.empty[Operator]) { (acc, f) => acc ++ f.FGOps }
        var relevantFGOps = conjectureFGOps 
        // partial transitive closure
        var (i, progress) = (0, true)
        while (i < flags.relevance.value && progress) {
          progress = false // pretend the following foreach-loop doesn't add anything new
          clauses foreach { cl =>
            if ((cl.FGOps intersects relevantFGOps) && // cl is relevant ...
                (cl.FGOps exists { op => ! ( relevantFGOps contains op ) } )) { // ... by providing at least one new FG op
              progress = true
              relevantFGOps = relevantFGOps union cl.FGOps
            }
          }
          i += 1
        }
        // Now get the relevant clauses; always include BG clauses
        val h = clauses.length
        clauses = clauses filter { cl =>  (cl.FGOps subsetOf relevantFGOps) || cl.isBG }
        useAllClauses = clauses.length == h
        if (verbose) {
          println("FG operators in conjecture: " + conjectureFGOps.toList.toMyString("{", ", ", "}"))
          println("Relevant FG operators: " + relevantFGOps.toList.toMyString("{", ", ", "}"))
          println()
        }
      }

      if (verbose) {
        printInputFormulas(fs)
        println()
        printSignature()
        printClauseSet(clauses)
        if (!lemmas.isEmpty) printLemmas(lemmas)
      }

      // if (haveGBTClauseSet)
      //   println("GBT fragment for " + inputFileName)

      if (flags.cnfOnly.value)
        sys.exit(0)

      Timer.preProc.stop()

      if (flags.iQE.value) {
        // flags.bgsolver.setValue("iQE")
        bgtheory.LIA.iQE.Solver.check(clauses)
        sys.exit(0)
      }


      /** Determine the time left for inferencing, i.e. remove the preprocessing time. */
      def realTimeout() =
        if (flags.timeout.value == 0) 7.0 * 24.0 * 60.0 * 60.0 // say, 7 days
        else (flags.timeout.value.toDouble - Timer.total.sinceStarted())
      needStats = true

      if (verbose) {
        println("Derivation")
        println("==========")
      }

      inPreprocessing = false

      val status =
	if(flags.finiteBeagle.value) 
          FiniteSearch.run(clauses, realTimeout())
        else if (!flags.auto.value) 
          run({}, clauses, lemmas, realTimeout())
        else if (clauses forall { _.unabstrAggressive.isGBTClause })
          // No need to try alternatives
          run({}, clauses, lemmas, realTimeout())
        else if (util.flags.genvars.value)
          // Cannot do the subsequent case 
          run({ }, clauses, lemmas, realTimeout())
        else {
          // auto mode, no GBT clause set, not complicated, no genvars: try first with abstraction variables, then ordinary variables
          val run1Timeout = realTimeout() / 2.0
          val runStart = Timer.now()
          val hStatus = run({
            util.flags.partinst.setValue("true")
            util.flags.weightAgeRatio.setValue("5")
            util.flags.weightGoalRatio.setValue("-1")
          }, clauses, lemmas, run1Timeout)
          val runElapsed = Timer.now() - runStart
          hStatus match {
            case Unsat(_) | Sat(_) => hStatus // Done!
            case Timeout | Unknown(_) => {
              val run2Timeout = realTimeout()
              println("***********************************************************************")
              println("After %.1f sec it's time to try something different, got %.1f sec left".format(runElapsed, run2Timeout))
              println("***********************************************************************")
              stats.nrStrategies += 1
              // Spend the rest of the time on a different strategy
              // Tacitly assume 
              run({ util.flags.genvars.setValue("true") }, clauses map { _.freshGenVars() }, lemmas, run2Timeout)
            }
          }
        }

      status match {
        case Sat(finalState) ⇒ {
          szsStatus(
            if (!useAllClauses)
              "Unknown"
            else if (conjectures.nonEmpty)
              "CounterSatisfiable"
            else "Satisfiable", inputFileName)
          if (verbose) {
            //Print signature so this can be plugged right into another solver
            if (flags.printer.value == "tff") {
              setPrinter("tff")
              println()
              println("% SZS output start Saturation for " + inputFileName)
              Sigma.show()
              println()
              println("%Saturated clause set:")
              finalState.old.clauses foreach { cl =>
                println("tff(%s, plain, %s).".format("c_"+cl.id, cl))
              }
              println("% SZS output end Saturation for " + inputFileName)
              setPrinter("default")
            } else  {
              println("\nSaturated clause set:")
              finalState.old.clauses foreach { println(_) }
            }
            println()
          }

        }
        case Unsat(emptyClause) ⇒ {
          szsStatus(if (conjectures.nonEmpty) "Theorem" else "Unsatisfiable", inputFileName)
          if (flags.proof.value) {
            val ref = Proof.mkRefutation(emptyClause)
            if (flags.printer.value == "tff") {
              setPrinter("tff")
              println()
              println("% SZS output start CNFRefutation for " + inputFileName)
              Proof.showRefutationTFF(ref)
              println("% SZS output end CNFRefutation for " + inputFileName)
              setPrinter("default")
            } else {
              println()
              println("Refutation:")
              Proof.showRefutationDefault(ref)
            }
          }
        }
        case Timeout => {
          reporter.log("\nTimeout reached")
          szsStatus("Unknown", inputFileName)
        }
        case Unknown(finalState) ⇒ {
          szsStatus("Unknown", inputFileName)
/*	  
	  //try again using finite beagle
	  reporter.log("*** Attempting Finite Search ***\n")
	  //FiniteSearch.runWithDefaultRanges(finalState.old.clauses.toList,realTimeout) match {
	  //using finalState.old leads to bugs when backtracking
	  try {
	    if ( FiniteSearch.runWithDefaultRanges(clauses.toList,realTimeout) == Unsat)
	      //only makes sense if unsat/thm
	      szsStatus(if (haveConjecture) "Theorem" else "Unsatisfiable", inputFileName)
	    else {
	      reporter.log("*** No result from Finite Search ***")
              if (verbose) {
		println("Saturated clause set:")
		//Print signature so this can be plugged right into another solver
		if (flags.printer.value == "tff") { Sigma.show(); println() }
		finalState.old.clauses foreach { println(_) }
		println()
              }
              szsStatus("Unknown", inputFileName)
	    }
	  } catch {
	    case e:OutOfMemoryError => { 
	      println("Memory error...")
	      reporter.log("*** No result from Finite Search ***")

              szsStatus("Unknown", inputFileName)
	    }
	    case e: Throwable => {
	      reporter.log("*** No result from Finite Search ***")
              szsStatus("Unknown", inputFileName)
	    }
	  }
*/
        }
      }
    } catch {
      case bgtheory.NonLinearTermFail(t) => {
        reporter.log("Cannot normalize into a linear term: " + t)
        szsStatus("Unknown", inputFileName)
        sys.exit(1)
      }
      case m: CmdlineError ⇒ {
        System.err.println(m.s)
        // too verbose to be useful:
        // usage()
        println("Try 'beagle -help'")
        sys.exit(1)
      }
      case x: UnhandledBGClause ⇒ {
        System.err.println("Clause " + x.cl + ": " + x.msg)
        sys.exit(1)
      }
      case x: java.io.FileNotFoundException ⇒ {
        System.err.println(x)
        sys.exit(1)
      }
      case m: GeneralError => {
        System.err.println("Error: " + m.s)
        sys.exit(1)
      }
      case m: SyntaxError => {
        System.err.println("Syntax error: " + m.s)
        sys.exit(1)
      }
      case m: InternalError => {
        System.err.println("Internal error: " + m.s)
        sys.exit(1)
      }
      case m: Message ⇒ {
        System.err.println(m.s)
        sys.exit(1)
      }
    }
    Timer.total.stop()
    println()
    stats.show()
    needStats = false
    sys.exit(0)
  }

  /**
   * Called whenever program is terminated.
   */
  private def shutdownHook = {
    reporter.onExit
    if (needStats) {
      println("\nExecution interrupted\n")
      Timer.total.stop()
      stats.show()
    }
  }

}
