package beagle.cnfconversion

import beagle._
import rules._
import fol._
import term._
import util._

object optimized {

  /** @param full currently ignored */
  def toCNF(f: Formula, full: Boolean): (List[List[Formula]], Set[Operator]) = {
    //note that skolemisation is only done in tseitin
    //println(f)
    val (fsTseitin, newOps) = tseitin(f.reduceOutermostFG(elimIffNot)
				       .reduceOutermostFG(elimImpl))
    //println("Mark 1")
    // Second, each obtained formula is turned into a set of clauses using
    // standard transformations.
    // Tseitin results in a formula all whose quantifiers are universal
    // and appear in positive polarity only. This is important below.
    val fs =
    fsTseitin flatMap { f1 =>
      f1.reduceOutermostFG(elimIff).
        reduceOutermostFG(elimImpl).
        // Only now quantifiers can be pulled out, as <=> has been eliminated
        //reduceInnermostFG(pulloutQuants).
        // remove double quantifiers
        //reduceInnermostFG(normalizeQuantifiers).
        //matrix.
        // The matrix now is made from Neg, And, and Or,
        // convert to CNF
        reduceOutermostFG(pushdownNeg).
        reduceInnerOnePass(elimTrivial).
        toListAnd
    }
    //println("Mark 2")
    return (standard.fastCNF(fs.map(_.toListOr), true), newOps)
  }

  /**
   * @todo: following comments no longer accurate.
   *
   * Structure-preserving elimination of nested operators that would cause
   * exponential blow-up when multipled out. Inspired by Tseitin's transformation.
   * Assume that Negation has been pushed inwards and double negation has been
   * eliminated, so that it can occur in front of atoms only.
   * Also assume that IffNot and, Implies and Implied have been eliminated.
   * @return The transformed formulas and the set of skolem operators introduced.
   */
  def tseitin(f: Formula): (List[Formula], Set[Operator]) = {

    def nnf(f: Formula) = f.reduceOutermost(pushdownNeg)//.reduceInnermost(elimNegNeg)
    def ExistsMaybe(xs: List[Var], f: Formula) = if (xs.isEmpty) f else Exists(xs, f)

    def isSimplePos(f: Formula): Boolean = {
      f.isBG ||
      (f match {
	case Atom(_, _) => true
	case Neg(Atom(_, _)) => true
	// Quantifiers OK, as long as there's a simple formula underneath
	case Forall(_, f) => isSimplePos(f)
	case _ => false
      })
    }

    def isSimpleNeg(f: Formula): Boolean = {
      f.isBG ||
      (f match {
	case Atom(_, _) => true
	case Neg(Atom(_, _)) => true
	// Quantifiers OK, as long as there's a simple formula underneath
	case Exists(_, f) => isSimplePos(f)
	case _ => false
      })
    }

    // open is a list of triples (name, body, vars) where
    // - name simple (see below), 
    // - body needs possibly Tseitinisation, and 
    // - vars is a list of variables governing both the variables
    //   in name and body.
    // hTseitin may extend open as it proceeds.
    var open = List[(Formula, Formula, List[Var])]((TrueAtom, nnf(f), List.empty))
    var done = List.empty[Formula]

    //this accumulates the results of skolemisations and naming of subformulae
    var newSig = Set.empty[Operator]

    def hTseitin(f: Formula, embeddingOp: Option[BinOp], vars: List[Var]): Formula = {
      // f is the formula whose complex proper subterms are to be extracted.
      // Assume f has been 'nnf'alized, so that negation can occur only in front
      // of atoms, unless f is a pure bg formula
      // embeddingOp can be And or Or only.
      // vars is a list of free variables that contains (at least) each free 
      // variable in f.
      //println(s"** $f x $embeddingOp xx $vars")

      /** Add to the open list a definition for body however with polarity as given by the 
       * flag positive.
       * Note that this modifies open & Sigma
       * @return the name for the body
       */
      def mkNameAndAddToOpen(positive: Boolean, body: Formula): Atom = {
        val vars = body.vars.toList
        val name = freshDef(vars)
        val op = Operator(FG, name.pred, Arity((vars map { _.sort }), Signature.OSort))
	//note sigma is modified here also!
        Sigma += op //TODO- a hack to allow Atom->PredEqn conversion
	newSig += op

        if (positive)
          open ::= (name, body, vars)
        else
          open ::= (Neg(name), nnf(Neg(body)), vars)
        name
      }

      // Body of hTseitin
      if (isSimplePos(f) && !f.isInstanceOf[QuantForm])
        f
      else f match {
        case Forall(xs, Forall(ys, f1)) => 
	  hTseitin(f1, embeddingOp, vars ++ xs ++ ys)
        case Forall(xs, f1) => {
          // Continue descending
          //hTseitin(f1, embeddingOp, vars ++ xs)

          val rho = Term.mkRenaming(xs)
	  val xs2 = rho(xs).asInstanceOf[List[Var]].toSet
	  (for (fi <- rho(f1).toListAnd) yield {
	    val relevant = xs2 intersect (fi.vars)
	    hTseitin(fi,  embeddingOp, vars ++ relevant)
	  }) reduceLeft (And(_,_))
        }
        case Exists(xs, Exists(ys, g)) => 
	  hTseitin(Exists(xs ::: ys, g), embeddingOp, vars)
        case Exists(xs, Or(f1, f2)) =>
          // Distribute exists
          hTseitin(Or(Exists(xs, f1), Exists(xs, f2)),
            embeddingOp,
            vars)
        case Exists(xs, f1) => {
          // Try to distribute over And
          f1 match {
            case And(f2l, f2r) => {
              // try to distribute Exists xs over f2l and f2r
              // This can be done for the variables that occur in f2l (resp f2r) only
              val f2lRelVars = (xs restrictTo f2l.vars) restrictToNot (f2r.vars)
              val f2rRelVars = (xs restrictTo f2r.vars) restrictToNot (f2l.vars)
              val f2SharedVars = xs restrictToNot (f2lRelVars ::: f2rRelVars).toSet
              if (f2SharedVars.length < xs.length)
                // Successfully shifted some of xs variables down
                return hTseitin(ExistsMaybe(f2SharedVars,
                  And(ExistsMaybe(f2lRelVars, f2l),
                    ExistsMaybe(f2rRelVars, f2r))),
                  embeddingOp,
                  vars)
            }
            // If the above is not successful or f is not of the form Exists-And
            // we do Skolemization
            case _ => ()
          }

	  // As an invariant of descending into the formula, vars
          // contains all free variables in f, however possible more.
          // It is clear that the skolem term constructed here needs to be
          // parametrized only in the free variables of f, which could be
          // fewer than in vars. Hence we collect these as the relevant 
          // variables first.
	  val relVars = vars restrictTo f.vars

          // The skolemization substitution, for all vars in xs
          val sigma = 
	    xs.foldLeft(Subst.empty)( (acc,x) => {
              val (binding,op) = skolemization.mkSkoSubst(relVars, x)
	      newSig += op
	      acc + binding
            })

          // Must do nnf here, because after sigma is applied possibly have a non-pure bg formula now
          // which requires nnf-isation. An example where this applies is ARI622=1.p
          hTseitin(nnf(sigma(f1)), embeddingOp, vars)
        }

        case Iff(f1, f2) => {
          // f is of the form f1 <=> f2
          // The result is (f1Pos => f2Pos) /\ (f2Neg => f1Neg)
          // where open is extended by
          // Neg(f1Pos) => Neg(f1)     (contrapositive of f1 => f1Pos)
          // f2Pos => f2
          // f1Neg => f1
          // Neg(f2Neg) => Neg(f2)     (contrapositive of f2 => f2Neg)
          // If f1 is simple then f1Neg, f1Pos don't have to be built,
          // similarly for f2.
          // todo: skolemize in place if isSimple
          val f1Pos = if (isSimpleNeg(f1)) f1 else mkNameAndAddToOpen(false, f1)
          val f2Pos = if (isSimplePos(f2)) f2 else mkNameAndAddToOpen(true, f2)
          val f1Neg = if (isSimplePos(f1)) f1 else mkNameAndAddToOpen(true, f1)
          val f2Neg = if (isSimpleNeg(f2)) f2 else mkNameAndAddToOpen(false, f2)
          And(Implies(f1Pos, f2Pos), Implies(f2Neg, f1Neg))
        }

        case BinOpForm(op, f1, f2) => {
          // op can be And or Or only
          if (embeddingOp == None || (embeddingOp == Some(op)))
            // Continue descending
            BinOpForm(op,
              hTseitin(f1, Some(op), vars),
              hTseitin(f2, Some(op), vars))
          else
            // embeddingOp is different to op, so we need to extract.
            // Context can only be a positive one, as all negation has been pushed
            // inwards and we're not inside an Iff
            mkNameAndAddToOpen(true, f)
        }
      }
    }

    // Body of Tseitin
    while (!open.isEmpty) {
      // Select the first open element 
      // Invariant: 
      val (selectedName, selectedBody, vars) = open.head
      //println("==> selected = " + selected)
      open = open.tail
      done ::= Implies(selectedName, hTseitin(selectedBody, None, vars))//.closure(Forall)
    }
    (done, newSig)
  }

  private val defCtr = new util.Counter

  private def freshDef(vars: List[Var]) = Atom("def_" + defCtr.next(), vars)

}
