package beagle.cnfconversion

import beagle._
import fol._
import term._

/**
 * Logical rules used to rewrite Formulas for CNF conversion
 * and simplification.
 * Rules as shown here are applied left to right separated by a '→'.
 */
object rules {

  /*
   * A couple of useful rules
   */

  /** f₁ !⇔ f₂ → ¬(f₁⇔f₂) */
  val elimIffNot: FormulaRewriteRules = {
    case IffNot(f1, f2) ⇒ Neg(Iff(f1, f2))
  }

  //TODO- can you do a macro type replacement of f2 by f1? or vice versa?
  /** f₁ ⇔ f₂ → (f₁ ⇒ f₂) ∧ (f₂ ⇒ f₁) */
  val elimIff: FormulaRewriteRules = {
    case Iff(f1, f2) ⇒ And(Implies(f1, f2), Implies(f2, f1))
  }

  val pushNegThruQuant: FormulaRewriteRules = {
    case Neg(QuantForm(q, xs, f)) ⇒ QuantForm(q.dual, xs, Neg(f))
  }

  /**
   * Move quantifiers to the outside of formulas.
   * Here ρ is a renaming for xs and Q is any quantifier.
   * Contains the following cases: </br>
   * 1) g ∧ (∃xs. f) → ∃(xs)ρ. (g ∧ fρ) </br>
   * 2) (∃xs. f) ∧ g → ∃(xs)ρ. (fρ ∧ g) </br>
   * 3) g ∧ (∀xs. f) → ∀(xs)ρ. (g ∧ fρ) </br>
   * 4) (∀xs. f) ∧ g → ∀(xs)ρ. (fρ ∧ g) </br>
   * 5) g ∨ (Qxs. f) → Q(xs)ρ. (g ∨ fρ) </br>
   * 6) (Qxs. f) ∨ g → Q(xs)ρ. (fρ ∨ g) </br>
   */
  val pulloutQuants: FormulaRewriteRules = {
    case Neg(QuantForm(q, xs, f)) ⇒ QuantForm(q.dual, xs, Neg(f))

    //(g & Qxs. f) => Qr(xs) (g & r(f))
    /*    case And(g, QuantForm(q, xs, f)) ⇒ {
      val rho = Term.mkRenaming(xs)
      QuantForm(q, rho(xs).asInstanceOf[List[Var]], And(g, rho(f)))
    }
    //(Qxs. f & g) => Qr(xs) (r(f) & g)
    case And(QuantForm(q, xs, f), g) ⇒ {
      val rho = Term.mkRenaming(xs)
      QuantForm(q, rho(xs).asInstanceOf[List[Var]], And(rho(f), g))
    }
*/

    // Hack instead of the two rules above, to get shorter Skolem functions for definitions using <=> 
    // and existential quantification. SHould be based on polarity, really; same for OR below
    //(g & ?xs. f) => ?r(xs)(g & r(f))
    case And(g, Exists(xs, f)) ⇒ {
      val rho = Term.mkRenaming(xs)
      Exists(rho(xs).asInstanceOf[List[Var]], And(g, rho(f)))
    }

    //(?xs. f & g) => ?r(xs)(r(f) & g)
    case And(Exists(xs, f), g) ⇒ {
      val rho = Term.mkRenaming(xs)
      Exists(rho(xs).asInstanceOf[List[Var]], And(rho(f), g))
    }

    //(g & !xs. f) => !r(xs)(g & r(f))
    case And(g, Forall(xs, f)) ⇒ {
      val rho = Term.mkRenaming(xs)
      Forall(rho(xs).asInstanceOf[List[Var]], And(g, rho(f)))
    }

    /** (∀xs. f) ∧ g → ∀(xs)ρ. (fρ ∧ g), where ρ is a renaming for xs*/
    case And(Forall(xs, f), g) ⇒ {
      val rho = Term.mkRenaming(xs)
      Forall(rho(xs).asInstanceOf[List[Var]], And(rho(f), g))
    }
    
    //(g | Qxs. f) => Qr(xs). (g | r(f))
    case Or(g, QuantForm(q, xs, f)) ⇒ {
      val rho = Term.mkRenaming(xs)
      QuantForm(q, rho(xs).asInstanceOf[List[Var]], Or(g, rho(f)))
    }

    case Or(QuantForm(q, xs, f), g) ⇒ {
      val rho = Term.mkRenaming(xs)
      QuantForm(q, rho(xs).asInstanceOf[List[Var]], Or(rho(f), g))
    }

  }

  /** f₁ ⇒ f₂ → (¬f₁) ∨ f₂, sim. for ⇐ */
  val elimImpl: FormulaRewriteRules = {
    case Implies(f1, f2) ⇒ Or(Neg(f1), f2)
    case Implied(f2, f1) ⇒ Or(Neg(f1), f2)
  }

  /** ¬(¬f₁) → f₁ */
  val elimNegNeg: FormulaRewriteRules = {
    case Neg(Neg(f1)) ⇒ f1
  }

  /**
   * Pushes all negation symbols in front of atoms.
   * Assume that ⇒ (`Implies`) and ⇐ (`Implied`) have been eliminated before.
   */
  val pushdownNeg: FormulaRewriteRules = {
    case Neg(Neg(f1))     ⇒ f1
    case Neg(Or(f1, f2))  ⇒ And(Neg(f1), Neg(f2))
    case Neg(And(f1, f2)) ⇒ Or(Neg(f1), Neg(f2))
    // Implicit negation symbol in IffNot !
    case Neg(Iff(f1, f2)) ⇒
      (f1, f2) match {
        // try to get ORs: 
        case (And(_, _), _) ⇒ Iff(Neg(f1), f2)
        case (_, And(_, _)) ⇒ Iff(f1, Neg(f2))
        // try to not disrupt the structure: 
        case (Neg(_), _)    ⇒ Iff(Neg(f1), f2)
        case (_, Neg(_))    ⇒ Iff(f1, Neg(f2))
        // Default
        case (_, _)         ⇒ Iff(f1, Neg(f2))
      }
    case Neg(QuantForm(q, xs, f)) ⇒ QuantForm(q.dual, xs, Neg(f))
  }

  val pushdownNegNoQuant: FormulaRewriteRules = {
    case Neg(Neg(f1))     ⇒ f1
    case Neg(Or(f1, f2))  ⇒ And(Neg(f1), Neg(f2))
    case Neg(And(f1, f2)) ⇒ Or(Neg(f1), Neg(f2))
    // Implicit negation symbol in IffNot !
    case Neg(Iff(f1, f2)) ⇒
      (f1, f2) match {
        // try to get ORs: 
        case (And(_, _), _) ⇒ Iff(Neg(f1), f2)
        case (_, And(_, _)) ⇒ Iff(f1, Neg(f2))
        // try to not disrupt the structure: 
        case (Neg(_), _)    ⇒ Iff(Neg(f1), f2)
        case (_, Neg(_))    ⇒ Iff(f1, Neg(f2))
        // Default
        case (_, _)         ⇒ Iff(f1, Neg(f2))
      }
  }

  val pushdownOr: FormulaRewriteRules = {
    case Or(And(f1, f2), f3) ⇒ And(Or(f1, f3), Or(f2, f3))
    case Or(f1, And(f2, f3)) ⇒ And(Or(f1, f2), Or(f1, f3))
  }

  /**
   * 1) (f₁ ∨ f₂) ∧ f₃ → (f₁ ∧ f₃) ∨ (f₂ ∧ f₃) </br>
   * 2) f₁ ∧ (f₂ ∨ f₃) → (f₁ ∧ f₂) ∨ (f₁ ∧ f₃)
   */
  val pushdownAnd: FormulaRewriteRules = {
    case And(Or(f1, f2), f3) ⇒ Or(And(f1, f3), And(f2, f3))
    case And(f1, Or(f2, f3)) ⇒ Or(And(f1, f2), And(f1, f3))
  }

  /**
   * Contains symmetric cases of: </br>
   * 1) f ∧ ⊤ → f </br>
   * 2) f ∧ ⊥ → ⊥ </br>
   * 3) f ∨ ⊤ → ⊤ </br>
   * 4) f ∨ ⊥ → f </br>
   * 5) ¬⊤ → ⊥ </br>
   * 6) ¬⊥ → ⊤ </br>
   * 7) f ∧ f → f </br>
   * 8) f ∨ f → f
   */
  val elimTrivial: FormulaRewriteRules = {
    case And(f, TrueAtom)      ⇒ f
    case And(TrueAtom, f)      ⇒ f
    case And(f, FalseAtom)     ⇒ FalseAtom
    case And(FalseAtom, f)     ⇒ FalseAtom
    case Or(f, TrueAtom)       ⇒ TrueAtom
    case Or(TrueAtom, f)       ⇒ TrueAtom
    case Or(f, FalseAtom)      ⇒ f
    case Or(FalseAtom, f)      ⇒ f
    case Neg(FalseAtom)        ⇒ TrueAtom
    case Neg(TrueAtom)         ⇒ FalseAtom
    // todo: are the following two too expensive? 
    case And(f, g) if (f == g) ⇒ f
    case Or(f, g) if (f == g)  ⇒ f
  }

  // Exploit Associativity
  val flattenAndOr: FormulaRewriteRules = {
    case And(And(f, g), h) ⇒ And(f, And(g, h))
    case Or(Or(f, g), h)   ⇒ Or(f, Or(g, h))
  }

  /*
 // todo: implement this
  // Exploit Commutativity to sort, useful as preparation to exploit idempotency
  // but not yet implemented yet either.
  val sortAndOr : FormulaRewriteRules = { 
    // Assume in right-associative order
  } 
*/

  val normalizeQuantifiers: FormulaRewriteRules = {
    case Exists(xs1, Exists(xs2, f)) ⇒ Exists(xs1 ::: xs2, f)
    case Forall(xs1, Forall(xs2, f)) ⇒ Forall(xs1 ::: xs2, f)
  }

}
