package beagle

import beagle._
//import beagle.main._ //includes DerivationResult type
import datastructures.Eqn
//import finite._
import fol._
import term._
import bgtheory.LIA._
//import calculus._
import util._

package object finite {

  /**
   * Like mkDefinition but allows non-ground terms to be defined too.
   * @return An `Eqn` representing the definition.
   * @see [[beagle.fol.term.FunTerm.mkDefinition]]
   */
  def makeExceptionPoint(t: FunTerm): Eqn = {
    assume(t.op.kind == FG && t.sort.thyKind == BG, 
    		"makeException: need a B-sorted term with an FG-operator")

    Eqn(t, DefParam(Term.defineCtr.next))
  }
  
  /**
   * @param piX a `Map` from `Var` to the sets we want to instantiate
   * over.
   * @return All instantiating substitutions over the given sets and
   * variables.
   */
  def instances(piX: Map[Var, Domain[DomElemInt]]): List[Subst] = {
    var res = List(Subst.empty)
    
    for((xi, pi) <- piX)
      res = pi.asSet.flatMap(d => res.map(s => s + (xi -> d))).toList
    
    return res
  }

  /** Make a fresh parameter */
  def makeParam(t: Term) = {   
    val paramName = "α_" + Term.defineCtr.next()
    new SymConst(Operator(t.sort.thyKind, paramName, Arity0(t.sort)))
  }

}
