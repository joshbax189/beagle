package beagle.finite

import beagle._
import finite._
import bgtheory.LIA.DomElemInt
import fol._
import term._
import util.flags

/**
 * The various find algorithms and helper methods.
 */
object Find {
  
  private var iLastVar: Int = 0
  private var lastSatSet: Domain[DomElemInt] = EmptyIntDomain

  def apply(m: FQClauseSet) = {
    if (util.flags.linStrategy.value && !lastSatSet.isEmpty) 
      findLinear(m)
    else find(m)
  }
  
  /**
   * To allow breaking from the recursive search method if an
   * exception point is found.
   */
  case class FoundExceptionPoint(d: DomElemInt) extends Exception
  
  /**
   * Special strategy for find: focus on one variable at a time and repeat
   * until no more repairs are needed.
   * Can reuse a satisfiable subset detected last round.
   */
  def findLinear(m: FQClauseSet): Option[(Var,DomElemInt)] = {
    
    FDStats.timeFind.start()

    //just try for lastVar, if fails then do original find()
    val xiPre=(m.fqVars.take(iLastVar)); //(x_0,...,x_i-1)
    val xi = m.fqVars(iLastVar);
    val xiEmpty=xiPre.map((_,EmptyIntDomain)).toMap; //[x_0 -> empty, ..., x_i-1 -> empty]
    
    if ( iLastVar == m.fqVars.size-1 || //at this point we will be checking m[empty] which we know is sat
        FiniteSearch.isSat( m(xiEmpty+(xi->EmptyIntDomain)) )
           ) {

      println("-Linear strategy")
      println("Target variable is "+xi)
      println("Satisfiable Set M["+xiPre.map("0_"+_).mkString(", ")+
    		  (if(iLastVar!=0) ", " else "") + "0_"+xi + (if(iLastVar!=m.fqVars.size-1) ", " else "")+
    		  m.fqVars.takeRight(m.fqVars.size-iLastVar-1).map("d_"+_).mkString(", ")+"] is:")
      m(xiEmpty+(xi->EmptyIntDomain)).cs foreach { println _ }
      println()
      
      //find Gamma maximal proper subset of delta_xi
      // such that B-satisfiability holds
      
      println("Searching for a set gamma...")
      
      try {
	//TODO what replaces m.delta(xi) here?
	//perhaps m.delta(xi).diff(lastSatSet)
	val gamma = genericSearch(lastSatSet,m.delta(xi).diff(lastSatSet),xi,m(xiEmpty))
	      
	println("Max. Sat. subset Gamma is:")
	println(gamma)
	      
	//if gamma is not a proper subset- cannot find d!
	if((gamma.size < m.delta(xi).size)) {
	        
	  //Then take d in del_xi as any point adjacent to Gamma.   
	  //Try the end points if delta is a range?
	        
	  val d = m.delta(xi) match {
	    case RangeExcept(a,_,_) if (!gamma(DomElemInt(a))) => DomElemInt(a)
	    case RangeExcept(_,b,_) if (!gamma(DomElemInt(b-1))) => DomElemInt(b-1)
	    case _ => m.delta(xi).diff(gamma).head
	  }
	  
	  lastSatSet=gamma+d
	  
          FDStats.timeFind.stop()
        
          return Some((xi, d))
	}else {
	  FDStats.timeFind.stop()
	  return None
	}
      } catch {
	case FoundExceptionPoint(d) => {
	  println("Found single modification point d="+d)
	  
	  //might not know gamma, best we can do is {d}
	  lastSatSet=lastSatSet+d
      
	  FDStats.timeFind.stop()
	  return Some((xi,d))
	}
      }
    }else {
      //The last update of xi has made M[0,xi,x] unsat
      //call find to iterate over other variables
      FDStats.timeFind.stop()
      return find(m)
    }	   
  }
  
  /**
   * Assume X is of the form X=(x_1,...,x_n)
   * M is a clause set with finite ranges del_X which has had the FD
   * transform applied.
   * @return a pair (x,d) such that x in X and d in delta_x\pi_x
   */
  def find(m: FQClauseSet): Option[(Var, DomElemInt)] = {
    
    FDStats.timeFind.start()
    
    println("Variable order: "+m.fqVars.mkString(", "))

    for {
      i <- 0 until m.fqVars.size
      xiPre = (m.fqVars.take(i)) //(x_0,...,x_i-1)
      xi = m.fqVars(i)
      xiEmpty = xiPre.map((_,EmptyIntDomain)).toMap //[x_0 -> empty, ..., x_i-1 -> empty]
      if ( i == m.fqVars.size-1 || //at this point we will be checking m[empty] which we know is sat
          FiniteSearch.isSat( m(xiEmpty+(xi->EmptyIntDomain)) )
        )
    } {
      
      //TODO OPTIMISATION: what happens when the new m is the same as the previous?
      // can eliminate the sat check
      // will partitions be different/relevant?
      
      println("Target variable is "+xi)
      println("Satisfiable Set M["+xiPre.map("0_"+_).mkString(", ")+
    		  (if(i!=0) ", " else "") + "0_"+xi + (if(i!=m.fqVars.size-1) ", " else "")+
    		  m.fqVars.takeRight(m.fqVars.size-i-1).map("d_"+_).mkString(", ")+"] is:")
      m(xiEmpty+(xi->EmptyIntDomain)).cs foreach { println _ }
      println()
      
      //find Gamma maximal proper subset of delta_xi
      // such that B-satisfiability holds
      
      println("Searching for a set gamma...")
      
      try {
	      //val gamma = new SetDom(search(m.delta(xi),xi,m(xiEmpty)))
	      val gamma = genericSearch(EmptyIntDomain,m.delta(xi),xi,m(xiEmpty))
	      
	      println("Max. Sat. subset Gamma is:")
	      println(gamma)
	      
	      //if gamma is not a proper subset- cannot find d!
	      if((gamma.size < m.delta(xi).size)) {
	        
	        //Then take d in del_xi as any point adjacent to Gamma.
	        
	        //old
	        //val d = m.delta(xi).diff(gamma).head
	        
	        //- can we try the end points if delta is a range?
	        
	        val d = m.delta(xi) match {
	          case RangeExcept(a,_,_) if (!gamma(DomElemInt(a))) => DomElemInt(a)
	          case RangeExcept(_,b,_) if (!gamma(DomElemInt(b-1))) => DomElemInt(b-1)
	          case _ => m.delta(xi).diff(gamma).head
	        }
	        
	        /*
		 if ( isSat( m(xiEmpty+(xi->gamma)) ) && //both should follow already by construction of gamma
		 !isSat( m(xiEmpty+(xi->(gamma+d))) )) {*/
		//println("Satisfactory d found!")
		
		lastSatSet=gamma
		iLastVar=i

		FDStats.timeFind.stop()
		
		return Some((xi, d))
	      }
	    } catch {
	      case FoundExceptionPoint(d) => {
	        println("Found single modification point d="+d)
	        
		//this should have been set by genericSearch
		lastSatSet=lastSatSet+d
		iLastVar=i

		FDStats.timeFind.stop()
	        return Some((xi,d))
	      }
	    }
        
    }
    
    //if you haven't returned by here model is not reparable
    FDStats.timeFind.stop()
    
    return None
  }
    
  /**
   * Implementation of binary search.
   * The generation of partitions is polymorphic in the implementation of 
   * domains. All return roughly equal sized partitions.
   * @param sat- the maximal satisfiable set of instances found so far
   * @param unsat- the set which remains to be searched
   * @param x the variable to substitute for in cs
   * @param cs
   * @return a maximal satisfiable domain.
   */
  @annotation.tailrec
  private def genericSearch(sat: Domain[DomElemInt], unsat: Domain[DomElemInt], x: Var, cs: FQClauseSet): Domain[DomElemInt] = {
    
    if(unsat.isEmpty) 
      return sat
    else if(unsat.size==1){
      val p = sat union unsat
      
      println("Testing single partition: "+sat+" u "+unsat)
      
      if(FiniteSearch.isSat( cs(Map(x->p)) )) 
        return p
      else {
        lastSatSet=sat
	throw new FoundExceptionPoint(unsat.head)
      }

    }else {
      val (d1,d2) = unsat.partition
      val (p1,p2) = ((sat union d1), (sat union d2))
        
      println("Testing partitions p1="+sat+" u "+d1+", p2="+sat+" u "+d2)
      
      if (FiniteSearch.isSat( cs(Map(x->p1)) ))
        genericSearch(p1,d2,x,cs)
      else {
        //both (sat u d1) and (sat u d2) unsat so try remaining combinations here:
        
        //discard d1 and partition d2:
        
        //this result will be sat u d' where d' is a subset of d2...
        if(d1.size==1) {
          //d1 cannot be partitioned and (sat u d1) is unsat so:
	  lastSatSet=sat
	  throw new FoundExceptionPoint(d1.head)
        }else
          genericSearch(sat,d1,x,cs)
      }
    } 
  }

}
