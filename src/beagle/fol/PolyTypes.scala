package beagle.fol

import beagle._
import util._
import term._

/* For Parametric Types:
 * This is the target of parsing of TFF1 files.
 * Initially the resulting formulas will be monomorphised to TFF0 using the simple transform from the TPTP spec
 * This could also be included in the TPTP parser eliminating the extra step
 * but that seems messy since we will want to do something clever with these later?
 */

/**
 * Foreground/background kinds, used to classify both sorts and operators
 * But not Expressions.
 */
sealed abstract class OpKind
case object FG extends OpKind
case object BG extends OpKind
/** TY operators are type constructors */
case object TY extends OpKind

/**
 * Type constructors behave exactly like operators and are recognised
 * by being of the TY (type) kind.
 * All of the arguments of a type constructor are types and it returns a type.
 * @example
 * tff(list, type, list: $tType > $tType).
 * //parses to:
 * Operator(Type,"list",Arity1(tType -> tType))
 */
object TypeCons {

  def apply(name: String, nrArgs: Int) = 
    Operator(TY, name, new Arity(List.fill(nrArgs)(Signature.TSort),Signature.TSort))

}

//usage tff(isEmpty, type, isEmpty: !>[A: $tType]: (list(A) > $o) ).

/** Type identifies Terms which can also serve as "Sorts".
 * It generalises the old Sort class but adds the new field tVars and changes the
 * Sort.kind to Type.thyKind to distinguish it from an Expression Kind
 * (which is always TY for type terms).
 * Type is mixed into TVars or terms which have type constructors as operators,
 * including atomic types (which are identical to the old Sort class).
 */
trait Type {

  // Abstract members
  val name: String

  def tVars: Set[TypeVar]

  /** Which theory this type comes from.
   * For now only Atomic sorts can have the BG kind.
   */
  val thyKind: OpKind = FG

  /** some element used for "$-closing" */
  var someElement: Const = null

  //Note: The methods sortsCanUnify, sameUnderlyingSort, isSubsortOf deal with an
  // obsolete version of polymorphic types- they should be removed/renamed when 
  // bgtheory/FD is removed.

  /** For parametric sorts.
   * By default either equal or non-empty intersection, this is a generalisation of
   * sort equality.
   */
  def sortsCanUnify(s: Type) = (this == s)

  /** Defines well-sortedness, i.e. whether two sets have a common super-set */
  def sameUnderlyingSort(s: Type) = (this == s)

  /** For more complex sort relations.
   * The default is that all sorts are disjoint
   */
  def isSubsortOf(s: Type) = (this == s)

  /** By default, sorts are either identical or non-intersecting.
   * @return the intersection of two sorts, or None if this is not possible.
   */
  def intersectWith(s: Type): Option[Type] = 
    if (this == s) Some(s) else None

  /** A unique symbolic constant per sort, used in main loop for generating instances */
  lazy val someSkolemElement = 
    SymConst(Operator(thyKind, printer.skolemEltPref + this.toString, Arity0(this)))

}

class TypeVar(val name: String, val index: Int) extends Var with Type {
  val kind = Expression.TY

  def freshVar(): Var = this
  // Members declared in term.Term
  val sort: Type = Signature.TSort
  
  // Members declared in Type
  def tVars = Set(this)
  
  // Members declared in term.Var
  def freshGenVar(): GenVar = ???

}

object TypeVar {
  def unapply(x: Any) = x match {
    case x: TypeVar => Some(x.toString)
    case x: Var if(x.sort==Signature.TSort) => Some(x.toString)
    case _ => None
  }
}

//like PFunTerm- a type term instance of a type constructor
case class PolyType(cons: Operator, tArgs: List[Type]) extends FunTerm with Type {
  override def toString = cons+tArgs.mkString("(",",",")")
  val name = this.toString
  val op = cons
  val args = tArgs.asInstanceOf[List[Term]]

  // Members declared in Expression
  def applySubst(sigma: Subst): term.Term = ???

  override lazy val sorts: Set[Type] = Set(Signature.TSort)
  val vars = this.tVars.asInstanceOf[Set[Var]]
  
  // Members declared in beagle.datastructures.PMI
  //TODO- indexing for type terms, is it even different?
  def replaceAt(pos: beagle.datastructures.PMI.Pos,t: term.Term): term.Term = ???
  def termIndex: beagle.datastructures.PMI.TermIndex = ???
  
  // Members declared in Type
  lazy val depth = (args map { _.depth }).max + 1
  def replace(lhs: term.FunTerm,rhs: term.Term): term.Term = this
  def subterms: Iterator[term.Term] = ???
  def tVars: Set[TypeVar] = tArgs.foldLeft(Set.empty[TypeVar])(_++_.tVars)
  lazy val varsCnt: Map[term.Var,Int] = ???
  lazy val weightForKBO: Int = ???

  def elimNonLinearMult: term.Term = this
  val minBSFGTerms: Set[term.Term] = Set()
  def replace(lhs: Atom,rhs: Formula): term.Term = this
  val symConsts: Set[term.SymConst] = Set()
  
}

//like Const or Sort
//note sort.kind = TY because, as an expression it is a type
// but sort.thyKind = FG | BG defines which theory it is in
class Sort(val name: String, override val thyKind: OpKind) extends Const with Type {
  override lazy val kind = Expression.TY

  //From Term
  lazy val op = TypeCons(name,0)

  // Members declared in beagle.datastructures.PMI
  def termIndex: beagle.datastructures.PMI.TermIndex = ???
  
  val depth: Int = 0

  def replace(lhs: Atom, rhs: Formula): Term = this
  def replace(lhs: FunTerm, rhs: Term): Term = this
  val symConsts: Set[SymConst] = Set()
  val minBSFGTerms: Set[Term] = Set()

  def tVars: Set[TypeVar] = Set()

}

case class FGSort(override val name: String) extends Sort(name, FG) {
//  val kind = FG
  someElement = FGConst("$" + name, this)
}

case class BGSort(override val name: String) extends Sort(name, BG) {
//  val kind = BG
}

//typing judgements are only necessary when parsing or when building substitutions

//when an operator is declared with polymorphic signature it needs type variables to be included in any instance
//just like regular variables-- do this when building the signature
//class PolyOp(kind: Kind, name: String, arity: PolyArity) extends Operator(kind,name,arity.monoArity)

// A single quantifier cluster can bind BOTH type variables and term variables. 
// Universal and existential quantifiers over type variables are allowed under the propositional connectives, 
// including equivalence, 
// as well as under other quantifiers over type variables, 
// but not in the scope of a quantifier over a term variable.
/*
object PolyToMono {

  //create an instance of typed as predicate
  private def typedAsPred(t: Term, tau: Any): Atom = Atom("$ty",List(t,tau))

  def convert(fs: List[Formula], s: Signature): (List[Formula],Signature) = {
    //nasty way to get around dependency injection of signatures for now...
    var sOut = s

    def convertF(f: Formula): Formula = f match {
      case Forall(xs, g) => {
	//separate type and term vars
	val (typeVars,termVars) = xs.partition(_.isInstanceOf[TypeVar])
	val condition = termVars.map( typedAsPred(_,()) ).toAnd
	Forall( typeVars.map(convertTerm(_)) ++ termVars, 
		 Implies(condition,convertF(g)) )
      }
      case And(f1,f2) => And(convertF(f1),convertF(f2))
      case Neg(g) => Neg(convertF(g))
      case Equation() => //does this capture predicates too?
      case Atom() =>
    }

    def convertTerm[T <: Term](t: T): T = t match {
      case x: TypeVar => //create new var
      case x: Var => x
      case TypeCons =>
      case f: FunTerm =>
    }

    def typingAxiom(op: Operator): Formula = ???

    return (fs.map(convert _) ++ typingAxioms ++ inhabited, s)
  }
}
*/
