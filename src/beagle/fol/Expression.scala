package beagle.fol

import beagle._
import term._

/**
 * Adds operations which are expected to be applied (and perhaps the same)
 * at many levels different levels; such as formula, term, literal, variable etc.
 *
 * The type parameter `E` represents the result type for methods which modify the
 * current expression, or the argument type for comparison type methods like variant.
 * So, for example
 * {{{ def Clause(...) extends Expression[Clause] }}}
 */
trait Expression[E] {

  import Signature._
  /*
   * Things to be implemented
   */

  /**
   * The free variables in the expression
   */
  val vars: Set[Var]

  /**
   * The application of a substitution to an expression.
   */
  def applySubst(sigma: Subst): E

  /**
   * The sorts of all sub-expressions in this expression not
   * including OSort.
   */
  val sorts: Set[Type]

  /**
   * The set of all operators in this expression.
   */
  val operators: Set[Operator]
  /** The depth of this expression */
  val depth: Int
  /** The set of B-sorted functional terms with a top-level foreground symbol */
  val minBSFGTerms: Set[Term]
  /** The set of B-sorted terms with a top-level foreground symbol,
   * which are not subterms of any other BSFG term. */
  val maxBSFGTerms: Set[Term]
  /** The symbolic constants occuring in this */
  val symConsts: Set[SymConst]
  val kind: Expression.Kind // See comment in Expression object

  def isPureBG = kind == Expression.PureBG
  def isImpureBG = kind == Expression.ImpureBG
  def isBG = isPureBG || isImpureBG

  def isFG = kind == Expression.FG

  def isType = kind == Expression.TY

  /**
   * Match `this` to `that` under a set of pre-existing substitutions `gammas`.
   * Concretely, if μ is a result of this then μ(this) = that, and if μ is the
   * i-th result then μ = σᵢ.τ for some substitution τ.
   * @return All substitutions that match `this` to `that` as an extension
   * of any of the preexisting ones as above.
   */
  def matchers(that: E, gammas: List[Subst]): List[Subst]

  /**
   * The list of all most general unifiers of this and that.
   * (For terms, the empty set or a singleton, for equations, 0, 1 or 2
   * substitutions.)
   */
  def mgus(that: E): List[Subst]

  override def toString: String

  /*
  * Derived stuff
  */

  /** All variables contained in this expression which have a background sort. */
  lazy val bgVars = vars filter { _.sort.thyKind == BG }
  /** General variables which do not have a background sort. */
  lazy val nonBGVars = vars -- bgVars
  /** Variable subterms of this expression which are general. */
  lazy val genVars =  vars filter { _.isGenVar }

  lazy val FGOps = (this.operators filter { op => op != TTOp && op.kind == FG })

  /** Build a renaming substitution for this expression and apply it.
   * @return σ(`this`) which is a variant of `this` and does not have any
   * variables in common.
   */
  def fresh() = applySubst(Term.mkRenaming(vars))
  
  /** Build a renaming substitution as usual but replace all abstraction
   * variables with new general variables.
   * @return `this` with all variables renamed and abstraction variables
   * replaced with general variables.
   */
  def freshGenVars() = applySubst(Term.mkRenamingIntoGenVars(vars))

  // The invariant is that all variables in all data structures
  // use indices that are less than or equal to variantCtr.

  /** Replace all variables with fresh constants. */
  def sko() = applySubst(Term.mkSkolemSubst(vars))

  //removed- confusing, as it does not produce all matchers with an other expression,
  // and subsumed elsewhere e.g. term.
  @deprecated(message="Use matcher(that): Option[Subst] instead", since="0.9")
  def matchers(that: E): List[Subst] = matchers(that,List(Subst.empty))

  /** A specialisation of `matchers` to again return an `Option`
   * if you only want to extend a single substitution.
   * @return a substitution extending `gamma` and matching `this` to `that`, or `None`.
   */
  def matcher(that: E, gamma: Subst): Option[Subst] = 
    matchers(that, List(gamma)) match {
      case m :: Nil => Some(m)
      case Nil => None
      case _ => throw util.InternalError("too many matchers produced")
    }

  /** @return at most one matcher for `this` to `that`. */
  def matcher(that: E): Option[Subst] = matcher(that, Subst.empty)


  /** @return True if there are no free variables. */
  def isGround = (vars.isEmpty)
/*
  /**
   * Whether this is an expression over linear integer arithmetic, etc
   */
  lazy val isLIA = isBG && (sorts subsetOf Set(IntSort)) && (operators subsetOf liaOps)
  lazy val isLRA = isBG && (sorts subsetOf Set(RatSort)) && (operators subsetOf lraOps)
  lazy val isLFA = isBG && (sorts subsetOf Set(RealSort)) && (operators subsetOf lfaOps)
  lazy val isFD = isBG &&
    bgVars.forall(_.sort.isInstanceOf[IntSubsort]) &&
    (operators forall { o ⇒ (commonIntArithOps(o)) })
  //TODO- an alternative: no equation has Int sort?
*/
  /*
   * Instantiation pre-order 
   */

  /**
   * Variantship. Two Es are variants iff "there exists" a renaming matcher.
   * Notice that we can't use `matcher` here because for clauses and equations
   * the matcher might not be the first one returned.
   */
  def ~(that: E) = {
    (this.matchers(that,List(Subst.empty))) exists { _.isRenaming }
  }

  /** Non-strict instanceship, think of it as an instance with the possibility of
   * equality.
   * @example
   * f(X) is a non-strict instance of f(Y) because of the matcher [Y → X]
   * and also, f(Y) is a non-strict instance of f(X). </br>
   * g(X,Y) is not a non-strict instance of g(X,a), but g(X,Y) >>~ g(X,a).
   */
  def >>~(that: E) = {
    !(this.matchers(that,List(Subst.empty))).isEmpty
  }

  /** Strict instanceship.
   * @example
   * f(X) is not a strict instance of f(Y) because of the matcher [X → Y]. </br>
   * g(X,a) is a strict instance of g(X,Y).
   */
  def >>(that: E) = {
    //      !(this matchers that).isEmpty &&
    //       (that matchers this).isEmpty 
    // this >~ that && (!that >~ this)
/*
    val sigmas = (this matcher that)
    (!sigmas.isEmpty) && (sigmas forall { !_.isRenaming })*/
    (this.matchers(that,List(Subst.empty))) exists { !_.isRenaming } //there is a matcher, but it is not a renaming
  }

}

object Expression {

  /**
   * Kind: The kind of expressions, in particular terms, literals, clauses:
   * One of FG, PureBG, ImpureBG.
   * Used in weak abstraction and indexing.
   * The kinds are linearly ordered as PureBG < ImpureBG < FG. This ordering is used to determine the kind of
   * a term from its subterms by taking their least upper bound (lub) For example, a term containing PureBG and FG terms
   * is itself a FG term.
   */
  sealed abstract class Kind {
    def lub(k: Kind): Kind
  }
  /** Contains some FG operator. */
  case object FG extends Kind {
    def lub(k: Kind) = FG
  }
  /** Only defined BG operators (includes DomElem and parameters) and AbstVar, no GenVar. */
  case object PureBG extends Kind {
    def lub(k: Kind) = k
  }

  /** Over BG signature but may contain GenVars. */
  case object ImpureBG extends Kind {
    def lub(k: Kind) = if (k == FG) FG else ImpureBG
  }

  /** Expressions can have Type kind too */
  case object TY extends Kind {
    def lub(k: Kind) = k
  }

  /**
   * Matching of lists of Expressions. The caller must makes sure that
   * l2 is at least as long as l1, otherwise unexpected thing may happen.
   */
  def matchers[E <: Expression[E]](l1: List[E], l2: List[E], gammas: List[Subst]): List[Subst] = {
    var hl1 = l1 // we loop over l1 and
    var hl2 = l2 // we loop over l2
    var hgammas = gammas
    // the result substitution
    while (!hl1.isEmpty) {
      hgammas = hl1.head.matchers(hl2.head, hgammas)
      hl1 = hl1.tail
      hl2 = hl2.tail
    }
    hgammas
  }

  def lub[E <: Expression[E]](l: List[E]): Kind = l.foldLeft(PureBG: Kind)(_ lub _.kind)

}



 
