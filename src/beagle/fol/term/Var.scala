package beagle.fol.term

import beagle._
import fol._
import util._
import datastructures.PMI._

/**
 * In variables we keep their sort explicitly - makes life so much easier
 */
abstract class Var extends Term with VarOrSymConst {

  // val subTermsWithPos = PMI.empty
  val termIndex = TermIndex.empty
  // val ops = Set.empty[Operator]
  def replaceAt(pos: Pos, t: Term) = if(pos.isEmpty) t else this
  def replace(lhs: FunTerm, rhs: Term) = this
  def replace(lhs: Atom, rhs: Formula) = this

  // Abstract members
  val name: String
  val index: Int

  val depth = 0
  val varsCnt = Map(this -> 1)
  val vars = Set(this)
  val weightForKBO = KBO.varWeight
  val symConsts = Set.empty[SymConst]
  val minBSFGTerms = Set.empty[Term]
  val maxBSFGTerms = Set.empty[Term]
  val sorts = Set(sort)
  val operators = Set.empty[Operator]

  val preorder = List(this)

  //  val tlKind = None

  val thisvar = this
  def subterms = new Iterator[Term] {
    // delivers just this
    var gotit = false; def next() = { gotit = true; thisvar }; def hasNext = !gotit
  }
  def get(p: Pos): Option[Term] = 
    if(p.isEmpty) Option(this)
    else None

  def freshVar(): Var
  def freshGenVar(): GenVar
  lazy val elimNonLinearMult = this

  override def fresh() = freshVar() // override fresh() in Expression - gives a variable instead of a term

  // def normalize = this

  // Mixin Expression
  def applySubst(s: Subst) =
    s.lookup(this).getOrElse(this)

  def occursIn(t: Term): Boolean =
    t match {
      case y @ Var(_, _, _)   ⇒ this == y
      case FunTerm(fun, args) ⇒ args exists { this occursIn (_) }
    }
  
  override def toString = printer.varToString(this) //toStringWithSort
  def toStringNoSort = name + (if (index > 0) "_" + index else "") + (if (isAbstVar) "ᵃ" else "")
  def toStringWithSort = toStringNoSort + (if (sort == Signature.ISort) "" else ":" + sort.name)
}

object Var {
  def unapply(t: Term) = t match {
    case AbstVar(name, index, sort) ⇒ Some((name, index, sort))
    case GenVar(name, index, sort)  ⇒ Some((name, index, sort))
    case _                          ⇒ None
  }
}

/* Subclasses of Var */

/**
 * "Abstraction-Variable"- a specialisation of Var which only allows substitution of: <\br>
 * 1) Domain Elements, e.g. 0,1,2,3,... <\br>
 * 2) Parameters (i.e. BG-constants), or <\br>
 * 3) other AbstVars <\br>
 * Then this can only ever take a BG sort as a constructor argument.
 * The definition of "pure-BG" is any BG term which has only Abstraction Vars (no General Vars).
 */
case class AbstVar(name: String, index: Int, sort: Type) extends Var {
  assume(sort.thyKind == BG, "AbstVar: sort must be BG")
  val kind = Expression.PureBG
  def freshVar() = AbstVar(name, Term.variantCtr.next(), sort)
  def freshGenVar() = GenVar(name, Term.variantCtr.next(), sort)
  // val isBG = true
  // val isPureBG = true
  // val isPureFG = false
}

object AbstVar {
  //doesn't make sense
  //def apply(name: String) = new AbstVar(name, 0, Signature.ISort)
  def apply(name: String, sort: Type) = new AbstVar(name, 0, sort)
}

/**
 * General Variable, normally use this whenever you need any non-specific variable.
 */
case class GenVar(name: String, index: Int, sort: Type) extends Var {
  def freshVar() = GenVar(name, Term.variantCtr.next(), sort)
  def freshGenVar() = freshVar()
  val kind = if (sort.thyKind == BG) Expression.ImpureBG else Expression.FG
  // val isBG = sort.kind == BG
  // val isPureBG = false
  // val isPureFG = sort.kind == FG // todo: Do we ever need this?

}
object GenVar {
  /** Default constructor produces variables with a TPTP 'individual' sort (`ISort`) */
  def apply(name: String) = new GenVar(name, 0, Signature.ISort)
  /** Default constructor for 0 index GenVars */
  def apply(name: String, sort: Type) = new GenVar(name, 0, sort)
}
