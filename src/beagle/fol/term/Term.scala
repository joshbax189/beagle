package beagle.fol.term

import beagle._
import fol._
//import Expression._
import util._
import calculus._
import datastructures._
import Signature._
import PMI._
import bgtheory.LIA._

/**
 * Generic interface for FOL terms.
 * Immediate subclasses are `Var` and `FunTerm` which include almost all FOL terms
 * except for the special `Let` and `ITE` terms.
 * However these are compiled away after parsing so most functionality of `Term`
 * does not in general apply to them.
 */
abstract class Term extends Expression[Term] with PMI[Term] {
// with Ordered[Term]

  /*
   * Mixin Expression
   */

  import unification._

  def mgus(that: Term) =
    try {
      val sigma = unify(this, that)
      List(sigma)
    } catch {
      case UnifyFail ⇒ List()
    }

  def matchers(that: Term, gammas: List[Subst]) =
    for (
      gamma ← gammas;
      sigma ← matchTo(this,that,gamma)
    ) yield sigma

  /*
   * Mixin Ordered
   */

  // def compare(that: Term) = this.toString compareTo that.toString

  /*
   * Abstract members
   */

  def iteExpanded: (Term, List[(Var, ITE)]) = Term.expandITE(this)

  /**
   * The term with any non-linear multiplication x·y replaced with
   * a dedicated uninterpreted symbol.
   */
  def elimNonLinearMult: Term

  /** The depth of the term */
  val depth: Int 
  /** The result sort of the term */
  val sort: Type 

  /** Preorder traversal of this term as a tree */
  val preorder: List[Any]

  /** The number of occurrences of a given variable in this */
  val varsCnt: Map[Var, Int]
  /** The weight of this, as used in KBO */
  val weightForKBO: Int
  /** The symbolic constants occuring in this. */
  val symConsts: Set[SymConst]

  // todo: check which of the following are needed

  /** The set of B-sorted functional terms with a top-level foreground symbol */
  val minBSFGTerms: Set[Term] 
  /**
   * The set of B-sorted terms with a top-level foreground symbol,
   * which are not subterms of any other BSFG term.
   */
  val maxBSFGTerms: Set[Term] 

  /**
   * replace binds all occurrences of f-terms in this to `lhs` via a matcher σ
   * and replaces all these occurrences by the corresponding (rhs)σ
   * @param lhs is a flat term over pairwise different variables, something like f(X,Y).
   */
  def replace(lhs: FunTerm, rhs: Term): Term
  
  /** Only used for ite's */
  def replace(lhs: Atom, rhs: Formula): Term  

  /** An iterator over all (improper) subterms of this */
  def subterms: Iterator[Term]

  /**
   * Use cases of mgus and matchers.
   * The result of mgus and matchers is always empty or a singleton
   */
  def mgu(that: Term) =
    (this mgus that) match {
      case Nil          ⇒ None
      case sigma :: Nil ⇒ Some(sigma)
      case _ => throw InternalError("mgus: unexpected result")
    }

  /** @return The head operator if this is a `FunTerm`, otherwise `None` if it is a `Var` */
  def opOption: Option[Operator] = this match {
    case FunTerm(op,_) => Some(op)
    case _ => None
  }

  //def matcher(that: Term) = (this matchers that)
    /*(this matchers that) match {
      case Nil          ⇒ None
      case sigma :: Nil ⇒ Some(sigma)
      case _ => throw InternalError("mgus: unexpected result")
    }*/

  def isFlat = depth <= 1

  // todo: check which of the following is really needed
  def isVar = isInstanceOf[Var]
  def isAbstVar = isInstanceOf[AbstVar]
  def isGenVar = isInstanceOf[GenVar]
  /** True only for instances of 'SymConst'*/
  def isSymConst = isInstanceOf[SymConst]
  def isDomElem = isInstanceOf[DomElem[_]]
  def isDomElemInt = isInstanceOf[bgtheory.LIA.DomElemInt] //|| isInstanceOf[DomElemFD]
  def isDomElemRat = isInstanceOf[bgtheory.LRA.DomElemRat]
  def isDomElemReal = isInstanceOf[bgtheory.LFA.DomElemReal]

  lazy val isBSFG = this match {
    case FunTerm(op, _) ⇒ op.isBSFGOp
    case _                ⇒ false
  }

  def isNLPPTerm = this match {
    case PFunTerm(bgtheory.LIA.NLPPOpInt, _) ⇒ true
    case PFunTerm(bgtheory.LRA.NLPPOpRat, _) ⇒ true
    case PFunTerm(bgtheory.LFA.NLPPOpReal, _) ⇒ true
    case _                ⇒ false
  }

  /**
    * isMinBSFGTerm: this is a minimal BS-sorted FG term:
    * - its toplevel operator is BS-sorted and FG
    * - non of its proper subterms is made with a BS-sorted and FG operator
    */
  def isMinBSFGTerm = this match {
    case FunTerm(op, args) ⇒
      op.isBSFGOp &&
      (args forall { arg => arg.operators forall { op => ! op.isBSFGOp } })
    case _ => false
  }

  // The weight of the term, used for selecting the next clause
  /* private def weightFun: Int = 
    if (isPureBG) 1 // Pure background terms never participate atively in inferences 
    else if (isVar) 1
    else {
      val NonDomElemFunTerm(op,args) = this
      1 + args.foldLeft(0)(_+ _.weight)
    }
    */
  def weightFun: Int = this match {
    case _: Var => 1
    // case d: bgtheory.LIA.DomElemInt => math.abs(d.value) // not good at all ...
    case d: DomElem[_] => (util.flags.domElemWeightMultiplier.value * d.toString.length) // * 5 // default for Dom Elemens
    case c: SymConst => 
      c.op.skolemCtr match {
        case None => 1
        case Some(ctr) => ctr / 10 + 1
      }
      // Penalize nlpp terms
    case PFunTerm(_:NLPPOp, args) => 1 + args.foldLeft(0)(_+ _.weight)
    case PFunTerm(_, args) => 1 + args.foldLeft(0)(_+ _.weight)
  }
  lazy val weight = weightFun
    
  // Convenience methods
  def gtr(that: Term) = Ordering.ordering.gtr(this, that)
  def geq(that: Term) = Ordering.ordering.geq(this, that)
  def compare(that: Term) = Ordering.ordering.compare(this, that)

  /** @return The term at the given position, or None if there is no such position. */
  def get(p: Pos): Option[Term]

  /**
   * Replace a particular subterm everywhere in an expression.
   * @todo subTermsWithPos does not include vars so this does not work for
   * replacing variables.
   * @todo could add this to the PMI interface.
   */
  def replaceSubterm(subterm: Term, replacement: Term): Term = {
    //this.subTermsWithPos
    Term.allSubtermsWithPos(this)
	  .foldLeft(this)((acc,p) => {
	    if(p._1==subterm) 
	      acc.replaceAt(p._2,replacement)
	    else 
	      acc 
	  })
  }
  
  import bgtheory.LIA.cooper._, bgtheory.LIA.Polynomial

  def asPolynomial = Polynomial.toPolynomial(this, true)

  /** Convert to the (improper) polynomial representation if possible
   * otherwise recurse into subterms and make those polynomial.
   */
  lazy val canonical: Term = Polynomial.toCanonical(this)
  /*  if (sort == IntSort)
      Polynomial.toPolynomial(this, allowImproper = true).toTerm //this always succeeds
    else {
      this match {
	case PFunTerm(op, args) => // Can canonicalize the arguments - this might not be int-sorted
          PFunTerm(op, args map { _.canonical })
	case _ => //Consts/vars or already in canonical form
	  this
      }
    }*/

  /** Simplify wrt Theory by applying canonical if needed, then usual background simplification.
   * Canonical rewriting only applies to Int sorted terms when aggressive simplifiction is enabled.
   * Preprocessing is a special case: solver simplification also removes `is_int` literals etc.
   */
  lazy val simplifyBG = bgtheory.solver.simplify(this)._1
    /*if (sort == IntSort && flags.bgsimp.value == "aggressive") {
      if (main.inPreprocessing)
        // we do a little more, to get rid off all these is_int, to_int etc
        bgtheory.solver.simplify(canonical)
      else
	canonical
    } else
      bgtheory.solver.simplify(this)*/

  // def allSubterms = this match {
  //   case t:Var => List(t)
  //   case t:Const => List(t)
  //   case t:PFunTerm => t :: (t.args.flatMap(_.allSubterms(prefix)))
  // }

}

/* More specific subclasses of Term */

case class Let(bindings: List[(Var, Term)], body: Term) extends Term {
    // As usual, unintended capturing of variables in the range of sigma by quantified
  // variable must be avoided. Renaming away the quantified variable before applying sigma
  // solves the problem.
  // val letVars = bindings map { _._1 }
  // val letTerms = bindings map { _._2 }
  val (letVars, letTerms) = bindings.unzip
  lazy val vars = (body.vars -- letVars) ++ letTerms.vars // todo: check: is this the Right Thing?

  lazy val preorder = body.preorder
  
  val termIndex = ???
  val ops = ???
  def replaceAt(pos: Pos, t: Term) = ???
  def get(p: Pos) = ???

  lazy val elimNonLinearMult = 
    Let(letVars zip (letTerms map { _.elimNonLinearMult }), body.elimNonLinearMult)

  // val varsCnt = body.varsCnt
  // todo not correct: need to combine with Term.varsCnt(letTerms)
  // val varsCnt = letVars.foldLeft(Term.combineCnts(body.varsCnt, Term.varsCnt(letTerms)))(_.updated(_, 0))
  // new version without Terms.varsCnt
  lazy val varsCnt = 
    //first combine body and letTerms varsCnt  
    //then set all letVars to be 0
    (body::letTerms).foldLeft(Map[Var,Int]())( 
      (varsCnt,subterm) => varsCnt ++ (subterm.varsCnt.transform((x,c) => varsCnt.getOrElse(x,0)+c))
    ) ++
    letVars.map((_->0))

  lazy val kind = body.kind
  lazy val operators = body.operators ++ letTerms.operators
  val symConsts = body.symConsts ++ 
  { var s = Set.empty[SymConst]
    letTerms foreach { t => s ++= t.symConsts }
    s
  }
  // the following is rather approximative, but we eliminate lets anyway,
  // and so this does not matter
  val depth = body.depth + 1
  val sorts = body.sorts
  def subterms = body.subterms
  val minBSFGTerms = body.minBSFGTerms
  val maxBSFGTerms = body.maxBSFGTerms
  val weightForKBO = body.weightForKBO
  val sort = body.sort
  //TODO- this fits with the above, correct?

  def applySubst(sigma: Subst) = {
    val rho = Term.mkRenaming(letVars)
    //      QuantForm(q, rho(xs), sigma(rho(f)))
    Let(bindings map { case(v, t) => 
      (rho(v).asInstanceOf[Var], sigma(t)) }, sigma(rho(body)))
  }
  def replace(lhs: FunTerm, rhs: Term) = {
    val rho = Term.mkRenaming(letVars)
    Let(bindings map { case(v, t) => 
      (rho(v).asInstanceOf[Var], t.replace(lhs, rhs)) }, rho(body).replace(lhs, rhs))
  }

  def replace(lhs: Atom, rhs: Formula)= {
    val rho = Term.mkRenaming(letVars)
    Let(bindings map { case(v, t) => 
      (rho(v).asInstanceOf[Var], t.replace(lhs, rhs)) }, rho(body).replace(lhs, rhs))
  }

  override def toString = printer.letTermToString(this)

}

case class ITE(cond: Formula, thenTerm: Term, elseTerm: Term) extends Term {
  assume(thenTerm.sort == elseTerm.sort, s"ITE result terms $thenTerm and $elseTerm do not have the same sort")

  def applySubst(sigma: Subst) = 
    if (sigma actsOn this) ITE(sigma(cond), sigma(thenTerm), sigma(elseTerm)) else this

  def replace(lhs: FunTerm, rhs: Term) = 
    ITE(cond.replace(lhs, rhs), thenTerm.replace(lhs, rhs), elseTerm.replace(lhs, rhs))
  def replace(lhs: Atom, rhs: Formula) = 
    ITE(cond.replace(lhs, rhs), thenTerm.replace(lhs, rhs), elseTerm.replace(lhs, rhs))

  lazy val elimNonLinearMult = ITE(cond.elimNonLinearMult, thenTerm.elimNonLinearMult, elseTerm.elimNonLinearMult)

  lazy val varsCnt = ??? // never used
  lazy val vars = cond.vars ++ thenTerm.vars ++ elseTerm.vars
  lazy val weightForKBO = ??? // never used 
  lazy val symConsts = cond.symConsts ++ thenTerm.symConsts ++ elseTerm.symConsts
  val kind = Expression.FG
  // This doe not work, gives an error in parsing. This is, because predicates defined in let-binders
  // appear only temporarily in the signature.
  // val operators = cond.operators ++ thenTerm.operators ++ elseTerm.operators
  val operators = Set.empty[Operator] 
  val sorts = cond.sorts ++ thenTerm.sorts ++ elseTerm.sorts
  val sort = thenTerm.sort
  lazy val preorder = ??? // never used

  lazy val minBSFGTerms = ??? // never used
  lazy val maxBSFGTerms = ??? // never used

  lazy val termIndex = ??? // never used

  def replaceAt(pos: Pos, t: Term) = ??? // never used

  def subterms = ??? // never used 
  def get(p: Pos) = ???
  lazy val depth = Math.max(thenTerm.depth,elseTerm.depth)

  override def toString = "$ite_t(%s, %s, %s)".format(cond, thenTerm, elseTerm)

}

/**
 * Contains generic methods for terms.
 */
object Term {

  import scala.collection.mutable.HashMap
  val defineCtr = new util.Counter
  val variantCtr = new util.Counter()

  var defineMap = HashMap.empty[String, Int]

  def resetDefineMap() {
    defineMap = HashMap.empty
  }

  /** Create a renaming substition from vars to a set of fresh variables, preserving kind */
  def mkRenaming(vars: Iterable[Var]) =
    new Subst((vars map { x ⇒ (x, x.freshVar()) }).toMap)

  /** Renaming substitution into ordinary variables */
  def mkRenamingIntoGenVars(vars: Iterable[Var]) =
    new Subst((vars map { x ⇒ (x, x.freshGenVar()) }).toMap)

  /**
   * Recursively expand all if-then-else terms by replacing their
   * occurrences with a fresh variable.
   * These variable/ITE bindings can be used at the formula level to
   * construct a formula that captures the meaning of the ITE term.
   * @return the expanded term, and a list of var/ite bindings created.
   */
  def expandITE(t: Term): (Term, List[(Var, ITE)]) = t match {
    case ITE(cond, thenTerm, elseTerm) => {
      val condEx = cond.iteExpanded
      val thenTermEx = expandITE(thenTerm)
      val elseTermEx = expandITE(elseTerm)
      val h = ( ITE(condEx, thenTermEx._1, elseTermEx._1), 
               thenTermEx._2 ::: elseTermEx._2 )
	// Now replace h._1 by a fresh variable and append that binding to h._2
	val x = GenVar("x", thenTerm.sort).fresh()
      (x, (x -> h._1) :: h._2)
    }
    case Let(bindings, body) => {
      //bindings has type List[(Var,Term)]) want (List(Var,Term),List(Var,ITE))
      val (newBinds,newITEBinds) = 
	bindings.foldLeft( (List[(Var,Term)](), List[(Var,ITE)]()) ){ 
	  case ((letBinds,iteBinds),(x,t)) => {
	    val (resTerm, newITEBinds) = expandITE(t)
	    ( (x->resTerm) :: letBinds, iteBinds ++ newITEBinds )
	  }
	}
      val (bExp,bBinds) = expandITE(body)
      (Let(newBinds,bExp), newITEBinds ::: bBinds)
    }
    case PFunTerm(f,args) => {
      val h: List[(Term,List[(Var,ITE)])] = args map { _.iteExpanded }
      (PFunTerm(f, h map { _._1 }), (h map { _._2 }).flatten)
    }
    case _ => (t, Nil)
  }

  /**
   * The term with any non-linear multiplication x·y replaced with
   * a dedicated uninterpreted symbol.
   */
  def elimNonLinearMult(t: Term): Term = ???

  /*
   * Skolemizing substitutions
   */

  val skoCtr = new util.Counter

  /**
   * Substitution that replaces variables by fresh constants
   * The variables will provide the sort, the kind will be that of the variable.
   * JOSH- now adds the new skolem operator to the signature
   */
  def mkSkolemSubst(vars: Iterable[Var]) =
    new Subst(
      (for(x <- vars) yield {
	val newSko = SymConst(Operator(x.sort.thyKind, "$sk_" + skoCtr.next(), Arity0(x.sort)))
	//must add the skolem operator to the global signature.
	Sigma+=newSko.op
	(x -> newSko)
      }).toMap)
    //new Subst((vars map { x ⇒ (x -> SymConst(Operator(x.sort.kind, "$sk_" + skoCtr.next(), Arity0(x.sort)))) }).toMap)

  /**
   * @return a Substitution replacing every variable with *the default* Skolem element
   * from the appropriate sort.
   */
  def mkSkolemElementSubst(vars: Iterable[Var]) =
    new Subst((vars map { x ⇒ (x -> x.sort.someSkolemElement) }).toMap)

  /**
   * Compute the varsCnt of the list of terms ts
   * This is now done locally for PFunTerms and LetTerms- otherwise get GCOverflow when making lots of terms.
   */
  // def varsCnt(ts: List[Term]): Map[Var,Int] = {
  //   var res = Map.empty[Var, Int]
  //   // Go over all ts and accumulate the result
  //   for (
  //     t ← ts;
  //     (x, m) ← t.varsCnt //for each tuple (x,m) in varsCnt of t (is varsCnt lazy for t?)
  //   ) {
  //     res.get(x) match {
  //       case None    ⇒ res = res + (x -> m) //if x not already in res, add (x->m)
  //       case Some(n) ⇒ res = res.updated(x, m + n) //otherwise add count to value and store
  //     }
  //   }
  //   res
  // }

  // def combineCnts(m1: Map[Var, Int], m2: Map[Var, Int]) = {
  //   var res = Map.empty[Var, Int]
  //   for (
  //     (x, m) ← m1
  //   ) {
  //     res.get(x) match {
  //       case None    ⇒ res = res + (x -> m)
  //       case Some(n) ⇒ res = res.updated(x, m + n)
  //     }
  //   }
  //   res
  // }

  /** Extend `vars` with a given list of sorted vars `vs`*/
  def add(vars: List[Var], vs: Iterable[Var]) = {
    var res = vars
    for (v ← vs) {
      if (res exists { x ⇒ (x.name == v.name) && (x.index == v.index) && (x.sort != v.sort) })
        throw SyntaxError("double declaration of variable " + v)
      else
        res ::= v
    }
    res
  }

  /** Gather all symbolic constants in a list of terms */
  def symConsts(ts: List[Term]) = ts.foldLeft(Set.empty[SymConst]) { _ ++ _.symConsts }

  def minBSFGTerms(ts: List[Term]) = ts.foldLeft(Set.empty[Term]) { _ ++ _.minBSFGTerms }

  // def pair(s: Term, t: Term) = PFunTerm("$pair", List(s, t))

  /** Decompose a term into a list of (sub)term and position pairs.
   * Note that this includes variables but `subTermsWithPos` defined using `PMI`, does not.
   * @todo possibly just modify the definition of subTermsWithPos?
   */
  def allSubtermsWithPos(t: Term, acc: Pos = Nil): List[(Term,Pos)] = t match {
    case t:FunTerm => (t,acc) :: (t.args.zipWithIndex flatMap { si => allSubtermsWithPos(si._1,acc :+ si._2)}).toList
    case v:Var => List((v,acc))
    case _ => throw new InternalError(s"Term $t is not supported")
  }

}

/**
 * As the name implies this trait applies to both `Var` and `SymConst` types
 * which is useful for procedures which do not distinguish them, mainly found in Cooper.
 */
trait VarOrSymConst {

  def toTerm: Term = this match {
    case x: Var      ⇒ x
    case c: SymConst ⇒ c
    case _           ⇒ throw InternalError("toTerm: unexpected object " + this)
  }
  def isVar: Boolean
  def asVar = this.asInstanceOf[Var]
  def isSymConst: Boolean
  def asSymConst = this.asInstanceOf[SymConst]
  
  def vOrSoccursIn(f: Formula) = this match {
    case x: Var      ⇒ f.vars contains x
    case c: SymConst ⇒ f.symConsts contains c
    case _           ⇒ throw InternalError("vOrSoccursIn: unexpected object " + this)
  }

  /** The identifier of this */
  def ident = this match {
    case x: Var => x.name
    case c: SymConst => c.op.name
  }

  /** Total ordering based on names. Used for iQE.
    * We cannot use Term.gtr because that ordering is not total
    */
  def >(that: VarOrSymConst) =
    if (this.isSymConst && that.isSymConst)
      // use usual ordering, so that can use precedence
      this.asSymConst gtr that.asSymConst
    else 
      this.ident > that.ident

  def >=(that: VarOrSymConst) = (this == that) || (this > that) 

}


