package beagle.fol

import beagle._
import util._
import bgtheory._
import term._

/**
 * A Signature consists of a set of sorts and a set of operators over these sorts.
 * Optionally, precs is a list of precedences over (the names of) operators.
 */
class Signature(val sorts: Set[Type] = Set.empty,
  val operators: Set[Operator] = Set.empty,
  val precs: List[(String, String)] = List.empty) {

  // Some derived notions
  val bgSorts = sorts filter { _.thyKind == BG }
  val fgSorts = sorts filter { _.thyKind == FG }

  //unused
  //val properSorts = sorts - Signature.TSort

  val bgOperators = operators filter { _.kind == BG }
  /** All foreground operators, both user defined and theory */
  val fgOperators = operators filter { _.kind == FG }

  /** TFF1 type constructors */
  val typeCons = operators filter { _.kind == TY }

  /** Return a set of templates for the given sort.
    *  A template is simply an operator with sort s which only contains variables.
    * E.g. f(X,Y) or a are both templates. */
  def templates(s: Type): Set[FunTerm] = {
    // The FG operators with the result sort s
    val ops = fgOperators filter { _.arity.resSort == s }
    ops map { op => FGFunTerm(op,
      // Make a list of fresh variables of the proper sorts
      op.arity.argsSorts map { as =>
        as.thyKind match {
          case FG => GenVar("XT", Term.variantCtr.next(), as)
          case BG => if (util.flags.genvars.value) GenVar("XT", Term.variantCtr.next(), as) else AbstVar("XT", Term.variantCtr.next(), as)
        }
      })
    }
  }

  private var memoEqOps = sorts map { s => 
    (s -> Operator(FG, "$equal", Arity2((s, s) -> Signature.OSort)))
  } toMap

  /** Reusable equality operators. */
  def eqOperators(s: Type): Operator = {
    if(!memoEqOps.isDefinedAt(s))
      memoEqOps += (s -> Operator(FG, "$equal", Arity2((s, s) -> Signature.OSort)))

    memoEqOps(s)
  }
  
  /** Convert all foreground operators from Real to Rat*/
  def toRat = new Signature(sorts, bgOperators ++ (fgOperators map { _.toRat }), precs)

  /** Compute the transitive closure of a given relation. */
  private def tc(rel: List[(String, String)]) = {
    var res = List.empty[(String, String)]
    // Invariant: res is transitively closed.
    // We add the elements of rel one after the other.
    // I think this is Dijkstra's algorithm (long time ago ...)
    for ((s, t) <- rel) {
      var sPred = List[String]()
      var tSucc = List[String]()
      res foreach { case (x, y) => 
        if (x == t) tSucc ::= y
        if (y == s) sPred ::= x
      }
      res ::= (s, t)
      for (sp <- sPred) res ::= (sp, t)
      for (ts <- tSucc) res ::= (s, ts)
      for (sp <- sPred; ts <- tSucc) res ::= (sp, ts)
    }
    res
  }

  private val precstc = tc(precs)

  // Sanity check, for cycles
  (precstc find { case (f, g) => f == g }) match {
    case Some((f, _)) =>
      throw new GeneralError("Cycle in declared precedences, involving e.g.: " + f)
    case _ => ()
  }

  /**fgOperatorsPrecMap: a linear ordering among all foreground operators,
   * using the lexicographic ordering over (a) the user given precedences and
   * (b) precedence over non-constants over constants.
   *  Moreover, $true > $false and $false is least.
   * fgOperatorsPrecMap is in fact a map from fgOperators to a set of all those fgOperators
   * that it precedes. Hence, checking whether an operator has precedence over another one should be really fast.
   * JOSH- filtered any FG skolem operators and added just before TRUE > FALSE in the
   * precedence.*/
  private lazy val fgOperatorsPrecMap: Map[Operator, Set[Operator]] = {

    var res = Map.empty[Operator, Set[Operator]]

    // The operators declared in the precedences:
    val precsDeclOps = (precs flatMap { case (f, g) => List(f, g) }).toSet
    val (skoOpsList, fgOpsList) = fgOperators.toList partition { op => 
      op.isSkolem && !precsDeclOps.contains(op.shortName)
    }

    //linearize the defined precedences first:
    var newTC = precstc
    precsDeclOps foreach { a =>
      precsDeclOps foreach { b =>
        if (a != b && !newTC.contains(a,b) && !newTC.contains(b,a)) {
          //newTC is transitive closure of (a,b) :: newTC
          //but newTC is already transitively closed
          //this is just one iteration of the inner loop of tc() applied to (a,b)
          //this is much faster
          var sPred = List[String]()
          var tSucc = List[String]()
          newTC foreach { case (x, y) =>
            if (x == b) tSucc ::= y
            if (y == a) sPred ::= x
          }
          newTC ::= (a,b)
          for (sp <- sPred) newTC ::= (sp, b)
          for (ts <- tSucc) newTC ::= (a, ts)
          for (sp <- sPred; ts <- tSucc) newTC ::= (sp, ts)
        }
      }
    }

    val linearized =
      // The non-zero arity operators for which we do not have precedences:
      (fgOpsList filter {
        op => op.arity.nrArgs > 0 && !(precsDeclOps contains op.shortName)
      } sortWith { (op1, op2) => (op1 compareTo op2) > 0 
      }) :::
      // The operators for which we do have precedences
      (fgOpsList filter {
        precsDeclOps contains _.shortName
      } sortWith { (op1, op2) =>
        newTC contains (op1.shortName, op2.shortName)
        //precstc contains (op1.shortName, op2.shortName)
      }) :::
      // The zero arity operators for which we do not have precedences:
      (fgOpsList filter {
        op => op.arity.nrArgs == 0 && !(precsDeclOps contains op.shortName) &&
        op != Signature.falseOp && op != Signature.trueOp
      } sortWith { (op1, op2) => (op1.shortName compareTo op2.shortName) > 0 
      }) :::
      skoOpsList :::
      List(Signature.trueOp, Signature.falseOp)

    for (
      fs <- linearized.tails;
      if !fs.isEmpty;
      f = fs.head
    ) res += (f -> fs.tail.toSet)
    res
  }

  /** @return whether op1 has higher precedence than op2 */
  def gtrPrecSimple(op1: Operator, op2: Operator) = fgOperatorsPrecMap(op1) contains op2

  /** The (foreground) sorts with finite domains
    * Each key in this map is mapped to all constants of this finite domain*/
  var domain: Map[Type, List[Term]] = null

  lazy val fdSorts = domain.keys.toList

  def findSort(s: String) = sorts find { _.name == s }

  /** Find an operator by name and sorts of its arguments */
  def findOperator(s: String, argsSorts: List[Type]): Option[Operator] = {
    operators find {
      op ⇒
        (op.name == s && (op match {
          case op: PolyOp ⇒ true
          case _ ⇒ {
	    (argsSorts.length==op.arity.nrArgs) &&
            ((op.arity.argsSorts zip argsSorts) forall { x ⇒ x._2 sameUnderlyingSort x._1 })
          }
        }))
    }
  }

  /** Use this to retrieve an operator by its name, if you are sure it is not overloaded */
  def findOperator(s: String) = operators find { _.name == s }

  /** @returns an existing operator with the same name and arity, or a new operator
   * over individuals and the updated signature.
   * @todo- this might not be ideal in the FOF case.
   */
  def findOrInsertOp(s: String, argsSorts: List[Type]): (Operator,Signature) = {
    findOperator(s,argsSorts) match {
      case Some(op) => (op,this)
      case None => {
	val defOp = Operator(FG, s, Arity((argsSorts map { _ ⇒ Signature.ISort }), Signature.ISort))
	findOperator(s) map { op => 
	  println(s"WARNING: Implicitly defined operator $defOp: ${defOp.arity} overloads $op: ${op.arity}") 
	}
	(defOp, this+defOp)
      }
    }
  }

  def findFormatFn(s: String, a: Arity) =
    // Silently ignore result sort, assume operators are unique per name and args sorts
    findOperator(s, a.argsSorts) match {
      case None ⇒ None
      case Some(op) ⇒ Some(op.formatFn)
    }

  def existsOp(s: String) = operators exists { _.name == s }

  /**
   * Add a sort. </br>
   * Note that duplicate sorts usually can't be detected, because equality of `Sort`
   * is defined (by case class) as depending on name.
   */
  def +(sort: Type): Signature = {
    /*if (sorts exists { t ⇒ (t.name == sort.name && t != sort) })
      throw SyntaxError("double declaration of sort " + sort)*/

    if (sorts(sort)) this
    else new Signature(sorts + sort, operators, precs)
  }

  /**
   * Add a rank. </br>
   * Note that it does not make sense to allow operators which differ only by kind,
   * e.g. adding +:(Int,Int)->Int with FG kind should not be possible.
   * @throws SyntaxError if operator with same name and arity exists in the signature.
   */
  def +(op: Operator): Signature = {
    if(operators(op)) this
    else if (operators exists { o ⇒ (o.name == op.name && o.arity == op.arity) })
      throw SyntaxError("double declaration of operator" + op)
    else new Signature(sorts, operators + op, precs)
  }

  /** Form the union of two signatures if possible.
   * @todo how are precedences computed?
   */
  def merge(that: Signature): Option[Signature] = {
    //union of sorts:
    //the only conflict is if there is a sort that is both BG & FG
    for(s1 <- this.sorts;
	if(!that.sorts(s1));
	s2 <- that.sorts;
	if(s2.name == s1.name))
      return None

    val newSorts = this.sorts union that.sorts

    //union of operators:
    //there is only a conflict if we have same name and arg sorts but different result
    for(op <- this.operators;
	if(!that.operators(op));
	op2 <- that.operators;
	if(op.name == op2.name && op.arity.argsSorts==op2.arity.argsSorts))
      return None

    val newOps = this.operators union that.operators

    //todo: is this union of precedences correct?
    return (try {
      Some(new Signature(newSorts,newOps,this.precs++that.precs))
    } catch {
      case e: GeneralError => //bad precedence combination
	None
    })
  }

  def analyse() {
    domain = Map.empty
    for (
      s ← fgSorts;
      if s != Signature.OSort && s != Signature.TSort;
      sOps = fgOperators filter { _.sort() == s };
      if (sOps forall { _.arity.nrArgs <= 1 }) // could also do <= 0
    ) // turn all operators into symbolic constants and set the domain to these
    {
      var domainElems = List.empty[Term]
      for (op ← sOps)
        domainElems ::=
          (if (op.arity.nrArgs == 0)
            SymConst(op)
          else // arity is 1
            PFunTerm(op, List(GenVar("X", op.arity.argsSorts.head))))
      if (!(sOps exists { _.arity.nrArgs == 0 }))
        // have no constant - add one
        domainElems ::= s.someElement
      domain += s -> domainElems
    }
  }

  /** Collect all (FG) sorts for which there are no operators that have that sort as
   * a result sort.
   */
  lazy val freeSorts: Set[Type] = {
    operators.foldLeft(sorts.map((_,0)).toMap)({
      case (acc,Operator(_,_,arity)) if (arity.nrArgs > 0) => 
	acc + (arity.resSort -> (acc(arity.resSort)+1))
      case (acc,_) => acc
    }).collect({
      case (sort,count) if (sort.name(0)!='$' && count==0) => sort
    }).toSet
  }

  def show() {
    println(printer.signatureToString(this))
  }

  /** Whether this signature is in the Effectively PRopositional fragment.
   * This is the case if there are no function symbols in this signature.
   * @param ignoreArith If true do not count any arithmetic operators either.
   */
  def isEPR(ignoreArith: Boolean): Boolean = {
    operators.forall(op => (op.sort()==Signature.OSort || 
			    op.arity.nrArgs==0) &
			    (!ignoreArith ||
			    op.kind==BG || 
			    op.name=="#nlpp")) 
  }

  /**
   * checkedXX: creates an XX, type-checked against sig and varTypes
   */
  def checkedEquation(s: Term, t: Term) = {
    if(s.sort != Signature.OSort && (s.sort sameUnderlyingSort t.sort))
      Equation(s, t) // For simplicity, no need to discover BG Equations in the parser
    else if(s.sort.thyKind == BG && t.sort.thyKind==BG) {
      //may be castable if one is a DomElem
      //note that BG types are always constant 'sorts'
      val newSort = bgtheory.lubSort(s.sort,t.sort)
      Equation(bgtheory.castTo(s,newSort), bgtheory.castTo(t,newSort))
    }else
	throw new SyntaxError("ill-sorted (dis)equation: between " + s + " and " + t)
  }

  /*  def checkedAtom(pred: String, args: List[Term]) =
    // Assume that pred has been entered into sig already
    findOperator(pred) match {
      case Some(op: VaryOp) if (op.sortCheck(args)) ⇒ Atom(pred, args)
      case Some(Operator(_, _, Arity(argsSorts, Signature.OSort))) ⇒
        if (argsSorts.zip(args map { _.sort }).forall(x ⇒ x._2 sameUnderlyingSort x._1))
          Atom(pred, args)
        else
          throw new SyntaxError("ill-sorted atom: " + Atom(pred, args))
      case _ ⇒ throw new SyntaxError("internal error: checkAtom called on non-declared predicate symbol " + pred)
    }
*/

  def checkedAtom(pred: String, args: List[Term]) =
    // Assume that pred has been entered into sig already
    findOperator(pred, args.sortsOf) match {
      case Some(op: PolyOp) if (op.sortCheck(args)) ⇒ Atom(pred, args)
      case Some(_) ⇒ Atom(pred, args)
      case None ⇒ {
        throw new SyntaxError("ill-sorted atom: " + Atom(pred, args))
      }
    }

  def checkedFunTerm(fun: String, args: List[Term]): FunTerm = {
    // Assume that fun has been entered into sig already
    findOperator(fun, args.sortsOf) match {
      case Some(op: PolyOp) => 
        if (args.isEmpty) 
          SymConst(op) 
        else if (op.sortCheck(args))
          PFunTerm(op, args)
        else
          throw SyntaxError("ill-sorted term: " + PFunTerm(op, args))
      case Some(op: Operator) => FunTerm(op,args)
        //if (args.isEmpty) SymConst(op) else PFunTerm(op, args)
      case None => 
        throw new SyntaxError("Undefined function symbol: " + fun + "^"+args.length)
    }
  }

  def checkedTypeTerm(fun: String, args:List[Type]): Type = {
    // Assume that fun has been entered into sig already
    findOperator(fun, args.map(_ => Signature.TSort)) match {
      case Some(op: Operator) =>
        if (args.isEmpty) new Sort(op.name,op.kind) else PolyType(op, args)
      case None => 
        throw new SyntaxError("Undefined function symbol: " + fun + "^"+args.length)
    }
  }

  def typeExistsChecked(s: Type) = s match {
    case sort: Sort =>
      if (this.sorts contains sort)
	sort
      else
	throw new SyntaxError("Error: type has not been declared: " + sort)
     case _ => s //complex types are checked in checkedTypeTerm, possibly these could be combined
    }

  def setPrecs(newPrecs: List[(String, String)]) = 
    new Signature(sorts, operators, newPrecs)

  
  //-----------------___EXPERIMENTAL-------------------------------------------
  import datastructures._
  
  case class TempSort(n: String) extends Sort(n,FG) {
    //val kind = FG
    //val params = None
    override def sameUnderlyingSort(s: Type) = s match {
      case _:TempSort => true
      case _ => false
    }
  }
  
  /**
   * Apply union find to $i sorts to hopefully reduce trivial inferences.
   * Invoked with -infer flag.
   */
  def inferSorts(cs: List[ConsClause]): List[ConsClause] = {
      var sortCounter = -1
      var tempSorts = List[Type]()
      
      def newSort = {sortCounter+=1; val ns = TempSort("S"+sortCounter); tempSorts::=ns; ns}
      
      //rebuild terms in cs with new operators given in ops
      def rebuildTerm(t: Term, ops: Map[String,Operator]): Term = {
        t match {
          case FGConst(op) if(ops.isDefinedAt(op.name)) => 
	    FGConst(ops(op.name))
          case PFunTerm(op,args) if(ops.isDefinedAt(op.name)) => 
	    PFunTerm(ops(op.name),args.map(rebuildTerm(_,ops)))
          case x => x
        }
      }
      
      def rebuild(clauseSet: List[ConsClause], ops: Map[String, Operator]): List[ConsClause] =
        for(c <- clauseSet) yield {
	  c.modified(lits = c.lits.map(_.map(rebuildTerm(_,ops))))
        }
      
      //if you have an individual sort replace with a new sort
      val newOps = 
	      for(op <- this.operators) yield {
	        Operator(op.kind,
	        		op.name,
	        Arity(op.arity.argsSorts map {case Signature.ISort => newSort; case c => c},
	        		{if (op.arity.resSort==Signature.ISort) newSort else op.arity.resSort}))
	      }
      
      //for each variable occurrence in cs, create a new sort
      var updatedCS =
	      for(c <- cs) yield {
	        var subs = Subst()
	        c.vars.foreach(x => 
	          if (x.sort==Signature.ISort) 
	            subs+=(x->new GenVar(x.name,x.index,newSort))
	        )
	        subs(c)
	      }
      
      //replace all old operators with ones having the new temporary sorts
      //BUG- when rebuilding Eqn it tries to sort using the old precedences, which fails
      Sigma = new Signature(this.sorts++tempSorts,newOps.toSet,this.precs)
      updatedCS = rebuild(updatedCS,newOps.map(op => (op.name,op)).toMap)
      
      //build equivalence classes using a map to a representative
      //initially each sort maps to itself
      var sortEq = tempSorts.zip(tempSorts).toMap
      
      //makes it easier to use sortEq
      def sortRep(s: Type) = sortEq.getOrElse(s,s)
      
      //sort representatives should be updated too
      def updateSortEq(t: Map[Type,Type]) {
        sortEq++=t
        sortEq=sortEq.mapValues(s=>t.getOrElse(s,s))
      }
      //propagate sort assignments towards the leaves of terms
      def propagate(t: Term) {
        t match {
          case f: FunTerm => {
            f.op.arity.argsSorts.zip(f.args.map(_.sort)) foreach {
              case (s1,s2) if sortRep(s1)!=sortRep(s2) => updateSortEq(Map(sortRep(s2)->sortRep(s1)))
              case _ => ()
              }
            f.args.foreach(propagate _)
          }
          case x: Var => ()
        }
      }
      
      for(c <- updatedCS;
    	  l <- c.lits;
    	  eq = l.eqn){
    	  //l.eqn- both terms same sort
          if(sortRep(eq.lhs.sort)!=sortRep(eq.rhs.sort))
            updateSortEq(Map(sortRep(eq.lhs.sort)->sortRep(eq.rhs.sort)))

          propagate(eq.lhs)
          propagate(eq.rhs)
      }
      
      //these are all of the remaining sort representatives
      val properSorts = tempSorts.filter(s => sortRep(s)==s)

      val resultOps =
	(for(op <- newOps) yield {
	  (op.name -> 
	   Operator(op.kind,
	            op.name,
		    Arity(op.arity.argsSorts.map(s => sortRep(s)),
	        	  sortRep(op.arity.resSort))))
	}).toMap
      
      //apply argument sort of functions to immediate variable subterms
      def applyVarSort(t: Term, s: Type): Term = {
        t match {
          case Var(x,i,_) => GenVar(x,i,s)
          //case _:Const => t
          case PFunTerm(op, args) => PFunTerm(op,args.zip(op.arity.argsSorts).map(a => applyVarSort(a._1,a._2)))
          case _ => t
        }
      }
      
      //replace each sort with its representative (variables too)
      Sigma = new Signature(this.sorts++properSorts,resultOps.values.toSet,this.precs)
      updatedCS = rebuild(updatedCS,resultOps)
      
      updatedCS =
	for(c <- updatedCS) yield {
	  c.modified(lits = for(l <- c.lits;
		        	eqn=l.eqn) yield {
			      Lit(l.isPositive, 
		        	  Eqn(applyVarSort(eqn.lhs,sortRep(eqn.rhs.sort)),
		        	      applyVarSort(eqn.rhs,sortRep(eqn.lhs.sort))))
			    })
	}
      
      return updatedCS
    }
}

/**
 * Predefined sorts and signatures
 */
object Signature {

  // Predefined sorts
  /** Individuals (of the Herbrand Universe)
   * @note cannot call the object iSort because if so Scala pattern matching assumes iSort is a *variable*
   */
  object ISort extends FGSort("$i")

  /** Truth values */
  object OSort extends FGSort("$o") // () => FGConst("$$_o", OSort)

  /** The sort of sorts (kinds), not really a FG sort but no problems treating it as one */
  object TSort extends FGSort("$tType") 

  // object AnyBGSort extends BGSort("$__anyBGSort__", null)
  // object AnyFGSort extends FGSort("$__anyFGSort__") // used as the "supersort" of all sorts, used for equations

  // Predefined operators
  val trueOp = new DefinedOp(FG, "$true", Arity0(OSort))
  val falseOp = new DefinedOp(FG, "$false", Arity0(OSort))

  /** Signature, over empty background signature */
  val signatureEmpty = new Signature( Set(ISort, OSort, TSort),
				      Set(trueOp, falseOp))


}

