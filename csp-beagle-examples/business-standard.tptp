%business model with lists and type system redone in accord with TFF
%- Employees are modeled as records with three fields:
% - an identifier (a logical constant, say)
% - a performance field with domain { bad, ok, good }, i.e. an enumeration
%   type
% - a boolean field: gets_gratification
%- An organisation is a list of employees.

%Now we model organisations that
%- have at least one employee for each performance class. That is, the lists
% will have length at least three. This is for testing purposes, say.
%  And,
%- identifiers are unique, that is, there are no two employees with the same
% ident, and
%- for each employee, if performance is good then gets_gratification is true.

%The conjecture to be (dis)proven from the model is "gets_gratification implies
%performance is good".

%%%%%%%%%%%%%% Sorts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% - boolean and performance: enumeration data types
%% - record: a record type 

%% Standard translation

%% Boolean as an enum type
tff(bool_type,type, bool: $tType ).
tff(bool_type,type, tt: bool ).
tff(bool_type,type, ff: bool ).

tff(boolean, axiom, (
	(! [X: bool] : ( X = tt | X = ff ))
	& (tt != ff))
).

% Performance- enum type
%tff(perf_type, type, perf: $tType ).
%tff(perf_type, type, bad: perf ).
%tff(perf_type, type, ok: perf ).
%tff(perf_type, type, good: perf ).
%
%tff(performance1, axiom, (
%	( ![X: perf] : (X = bad | X = ok | X = good))
%	& (good != bad)
%	& (good != ok)
%	& (bad != ok))
%).

%%% Record
tff(record_type, type, record: $tType ).

%%id field
tff(id_select_type, type, rselect_id: record > $int ).
tff(id_store_type, type, rstore_id: (record * $int) > record ).

tff(record1, axiom, 
	![R: record,V: $int]: ( rselect_id(rstore_id(R, V)) = V )
).

%%perf field
%tff(perf_select_type, type, rselect_perf: record > perf ).
%tff(perf_store_type, type, rstore_perf: (record * perf) > record ).
%
%tff(record2, axiom, 
%	![R: record,V: perf]: ( rselect_perf(rstore_perf(R, V)) = V )
%).

%%grat field
tff(grat_select_type, type, rselect_grat: record > bool ).
tff(grat_store_type, type, rstore_grat: ( record * bool ) > record ).

tff(record3, axiom, 
	![R: record,V: bool]: ( rselect_grat(rstore_grat(R, V)) = V )
).

%%recursive select definitions
%tff(record4, axiom, 
%	![R: record,V: perf]: ( rselect_id(rstore_perf(R, V)) = rselect_id(R) )
%).
tff(record5, axiom, 
	![R: record,V: bool]: ( rselect_id(rstore_grat(R, V)) = rselect_id(R) )
).
%tff(record6, axiom, 
%	![R: record,V: $int]: ( rselect_perf(rstore_id(R, V)) = rselect_perf(R) )
%).
%tff(record7, axiom, 
%	![R: record,V: bool]: ( rselect_perf(rstore_grat(R, V)) = rselect_perf(R) )
%).
tff(record8, axiom, 
	![R: record,V: $int]: ( rselect_grat(rstore_id(R, V)) = rselect_grat(R) )
).
%tff(record9, axiom, 
%	![R: record,V: perf]: ( rselect_grat(rstore_perf(R, V)) = rselect_grat(R) )
%).

% Records are inhabited
%tff(record10, axiom, ?[R] : record(R)).

%Extensionality (is it needed?)

% tff(record8, axiom, (
%    ! [R1: record, R2: record] : (
%      ( rselect_id(R1) = rselect_id(R2)
%        & rselect_perf(R1) = rselect_perf(R2)
%        & rselect_grat(R1) = rselect_grat(R2) )
%     => R1 = R2 ))
%  ).
%
% tff(record8, axiom, (
%    ! [R1: record, R2: record] : (
%      ( rselect_id(R1) = rselect_id(R2)
%        & rselect_perf(R1) = rselect_perf(R2)
%        & rselect_grat(R1) = rselect_grat(R2) )
%     => R1 = R2 ))
%  ).

tff(idem, axiom, 
	![R: record, V: $int ]: rstore_id(rstore_id(R, V),V) = rstore_id(R, V)
).

%tff(idem2, axiom, 
%	![R: record,V: perf]: ( rstore_perf(rstore_perf(R, V),V) = rstore_perf(R, V) )
%).

tff(idem3, axiom, 
	![R: record ,V: bool]: ( rstore_grat(rstore_grat(R, V),V) = rstore_grat(R, V) )
).

%overwriting
tff(test,conjecture, ![R:record]: rselect_id(rstore_id(rstore_id(R,0),1)) != 0 ).

%%%%%%%%%%%%%% Test cases %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%% Simple test cases

%% Provable:
%no rule to deduce rselect_grat(R0) = G
%tff(test, conjecture, (
%    ![G, R0, R1] :
%       ( ( record(R0) 
%         & record(R1) 
% 	  		 & boolean(G)
%         & R0 = rec(peter, ok, G) 
% 	       & G != tt
%         & R1 = rstore_perf(R0, bad) )
%      => rselect_grat(R1) = ff ))).

%%Corrected
%tff(test, conjecture, (
%    ![G: bool, R0: record, R1: record] :
%       (( G != tt
%         & R1 = rstore_perf(rstore_grat(R0,G), bad) )
%      => rselect_grat(R1) = ff ))
%).

%% Not provable
% tff(test, conjecture, (
%     ! [G: bool, R0: record, R1: record ] :
%       ( (R0 = rec(peter, ok, G) 
%         & R1 = rstore_perf(R0, bad) )
%      => rselect_grat(R1) = ff ))).

%tff(test, conjecture,
%	![R0, R1, G]: ((record(R0)
%								& record(R1) 
%								& boolean(G)
%								& R1 = rstore_perf(rstore_grat(R0,G),bad))
%						=> rselect_grat(R1) = ff)
%).

%tff(test, conjecture,
%	![R0,G]: ( (record(R0) & boolean(G)) => (rselect_grat(rstore_perf(rstore_grat(R0,G),bad)) = G & G = ff))
%).
%tff(test, conjecture,
%	![G]: (boolean(G) => G = ff)
%).
%tff(record9, conjecture, 
%	![R]: ( (record(R)) => rselect_grat(rstore_perf(R, bad)) = rselect_grat(R) )
%).

%EDarwin proves this
%tff(test, conjecture,
%	![R0]: ( (record(r) & R0 = rstore_grat(r,tt)) => (rselect_grat(R0) = tt ))
%).

%%%%%%%%%%% The test case from the top of this file

%- for each employee, if performance is good then gets_gratification is true.
%tff(impl, axiom, (
%   ![R] : 
%     ( ( record(R) 
%       & rselect_perf(R) = good )
%    => rselect_grat(R) = tt ))).

%tff(emp_type, type, employee: $o).
%
%tff(e2, axiom, ?[R: record]: employee(rstore_grat(rstore_perf(R,ok),tt)) ).
%
%tff(emp2, axiom, (
%   ![E: record] : 
%     (	( employee(E) & rselect_perf(E) = good )
%    => rselect_grat(E) = tt ))
%).
%
%%This now is counter-sat?
%% Test 1: The axiom above the other way round, not provable
%tff(impl, conjecture, (
%   ![R: record] : 
%     ( (rselect_grat(R) = tt & employee(R))
%    => rselect_perf(R) = good ))
%).

% Test2: The axiom above in a contrapositive-like fashion, exploiting enumeration datatype 
% properties, provable
%% tff(test, conjecture, (
%%    ! [R] : 
%%      ( ( record(R) 
%%        & rselect_grat(R) = ff )
%%     => ( rselect_perf(R) = ok 
%%        | rselect_perf(R) = bad ) ))).
 
