

%% A state is of the form state(i, val) where n and val are integers.
%% The value i is a natural number that defines the i-th state in a run.
%% The component val is, for simplicity,
%% a "value" modified by state transitions. In a more realistic setting, val would 
%% assign values to more than one variable, or be an assignment to one "big" database. 
tff(state, type, (state: ($int * $int) > $o)).

tff(inc_state, type, (inc_state: ($int * $int) > $o)).
tff(dec_state, type, (dec_state: ($int * $int) > $o)).

%% trans(i, val, i+1, val') is used to get from a state(i, val) to a state (i+1, val')
%% tff(trans, type, (trans: ($int * $int * $int * $int) > $o)).

%% The state transitions, in three different versions.
%% Increment by one ("acquire leave")		      
%% Decrement by some nondeterministic value ("take leave")
%% Notice we get a disjuntion between positive literals, corresponding to the
%% non-deterministic choices of transitions

%% Version 1, always take 3 days leave

%% tff(a, axiom, ! [I: $fd(-10,10), Val: $fd(0,10) ] ).


tff(trans1, axiom, (
   ! [I: $fd(0,10), Val: $fd(0,10)] :
     ( state(I, Val)
    => ( inc_state(I, Val) 
       | dec_state(I, Val) )))).

tff(trans2, axiom, (
   ! [I: $fd(0,10), Val: $fd(0,10)] :
     ( state(I, Val)
    => ~ ( inc_state(I, Val) 
         & dec_state(I, Val) )))).

tff(inc, axiom, (
   ! [I: $fd(0,10), Val: $fd(0,10)] :
     ( inc_state(I, Val) 
    => state($sum(I, 1), $sum(Val, 1)) ))).

tff(inc, axiom, (
   ! [I: $fd(0,10), Val: $fd(0,10)] :
     ( dec_state(I, Val) 
    => ( $lesseq(3, Val) 
       & state($sum(I, 1), $difference(Val, 3)) )))).


%% tff(state, axiom, ! [I: $fd(0,10), Val1: $fd(0,10), Val2: $fd(0,10)] :
%%      ( ( state(I, Val1)
%%        & state(I, Val2) )
%%     => Val1 = Val2)).


%% tff(state, axiom, ! [I: $fd(0,10), Val: $fd(0,10)] :
%%      ( state(I, Val) 
%%     => $greatereq(I, 0) )).


%% tff(inc_trans, axiom, (
%%    ! [I: $fd(0,10), Val: $fd(0,10), IP: $fd(0,10), ValP: $fd(0,10)] :
%%      ( state(I, Val)
%%     => ( state($sum(I,1), $sum(Val, 1)))))).

%% Version 2, some unspecified *uniform* value for taking leave, gives rise to a parameter
%% tff(decval, type, decval: $int).
%% tff(inc_trans, axiom, (
%%    ! [Val: $int, ValP: $int] :
%%      ( trans(Val, ValP)
%%    <=> ( ValP = $sum(Val, 1)
%%        | ( $lesseq(decval, Val) 
%%          & ValP = $difference(Val, decval)))))).

%% Version 3, some unspecified "taking leave", which may differ from state to state, 
%% gives rise to a B-sorted foreground function symbols
%% tff(leave, type, leave: $int > $fd(0,2)).
%% tff(inc_trans, axiom, (
%%    ! [Val: $fd(0,2), ValP: $fd(0,2)] :
%%      ( trans(Val, ValP)
%%    <=>  ( ValP = $sum(Val, 1)
%%        | ( $greater(Val, 0)
%%          & $greater(leave(Val), 0)
%%          & $lesseq(leave(Val), Val) 
%%          & ValP = $difference(Val, leave(Val))))))).

%% tff(inc_trans, axiom, (
%%    ! [Val: $fd(0,2), ValP: $fd(0,2)] :
%%      ( trans(Val, ValP)
%%     => ( ValP = $sum(Val, 1)
%%        | ( ? [K: $fd(0,2)] :
%%              ( $greater(K, 0)
%%              & $lesseq(K, Val) 
%%              & ValP = $difference(Val, K))))))).

%% Reachability of states (could also use <=> instead of <= if required, see below)
%% tff(reach1, axiom, ( 
%%   ! [ Val: $fd(0,2)] :
%%     ( reach(Val) 
%%    <=> ( init(Val)
%%       | ( ? [ValPrev: $fd(0,2)] : 
%%             ( reach(ValPrev) 
%% 	    & trans(ValPrev, Val) )))))).

%% Proof obligation 1: 
%% safety property: Assuming "init(val0) <=> val0 > 0", 
%%   is "reach(Val) => Val > 0" implied?
%% This is disprovable, as reach(0) is possible.

%% The premise (choose):
%% tff(val0, type, (val0: $fd(0,2))).
%% tff(inita, axiom, (init(val0) & $greater(val0, 0))).
%% tff(inita, axiom, (init(1))).
%% The conclusion - non-provable (not by beagle, not by SPASS+T - requires (finite) counter model finding)
%% Notice that for this to make sense (to exclude spurious counterexamples) the <=> version of the definition 
%% of reach must be used

%% initial state
tff(init, axiom, state(0, 0)).

tff(conc, conjecture, ? [I: $fd(0,10)] :
   state(I, 4)).

%% tff(conc, conjecture, (
%%       ! [Val: $fd(0,2)] :
%%             ( reach(Val)
%%             => $greater(Val, 0)))).
%% However, the negation of the above implication is provable with init(1)
%% tff(conc, conjecture, (
%%        ? [Val: $fd(0,2)] :
%%              ( reach(Val) 
%%              & ~ $greater(Val, 0)))).

%% Proof obligation 2: prove that certain states are reachable
%% tff(init, conjecture, (init(0) => reach(2))).
%% tff(init, conjecture, ! [ Val: $fd(0,2) ] : (init(Val) => reach($sum(Val,2)))).

%% Proof obligation 2: prove that val >= 0 is invariant

%% tff(c, conjecture, (
%%    ! [ Val: $fd(0,2), ValP: $fd(0,2)] : 
%%      ( ( $greatereq(Val, 0)
%%        & trans(Val, ValP) )
%%     => $greatereq(ValP, 0) ))).

%% Proof obligation 2a: prove that val > 0 is not invariant
%% (SPASS+T does not terminate, beagle -negsel -fifoSel does)
%% tff(c, conjecture, (
%%    ! [ Val: $fd(0,2), ValP: $fd(0,2)] : 
%%      ( ( $greater(Val, 0)
%%        & trans(Val, ValP) )
%%     => $greater(ValP, 0) ))).




