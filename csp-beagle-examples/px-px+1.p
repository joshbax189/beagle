tff(a, type, (a: $fd(5,7))).
tff(p, type, (p: $int > $o)).

tff(p, axiom, ![X:$fd(0,20)]: (p(X) => p($sum(X,1)))).
%% tff(p, axiom, p(100)).
 tff(p, axiom, p(1)).
%%tff(p, axiom, p(a)).

tff(c, conjecture, ? [X: $fd(10,12)] : p(X)).

