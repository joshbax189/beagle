%% The well-known send-more-money problem,
%% See http://en.wikibooks.org/wiki/Prolog/Constraint_Logic_Programming
%% sendmoremoney(Vars) :-
%%     Vars = [S,E,N,D,M,O,R,Y],
%%     Vars ins 0..9,
%%     S #\= 0,
%%     M #\= 0,
%%     all_different(Vars),
%%                  1000*S + 100*E + 10*N + D
%%     +            1000*M + 100*O + 10*R + E
%%     #= 10000*M + 1000*O + 100*N + 10*E + Y.

%% It is in fact a combination of finite domain constraints (alldifferent) and linear integer arithmetic over variables 
%% over these constraints.


tff(smm_type,type,( smm: ($int * $int * $int * $int * $int * $int * $int * $int) > $o )).
tff(dom_type,type,( dom: $int > $o )).

tff(dom, axiom, ( 
  ! [X: $int] : 
    ( dom(X) <=> (X=0 | X=1 | X=2 | X=3 | X=4 | X=5 | X=6 | X=7 | X=8 | X=9)))).
%% This one would be nicer, but is not "sufficiently complete"
%% tff(a, axiom, ( ! [I: $int] : ( dom(I) => ($lesseq(0,I) & $lesseq(I,9))))).

tff(s, axiom, ( 
  ! [S:$int, E:$int, N:$int, D:$int, M:$int ,O:$int ,R:$int ,Y:$int] :
    ( smm(S,E,N,D,M,O,R,Y) 
  <=> ( dom(S) & dom(E) & dom(N) & dom(D) & dom(M) & dom(O) & dom(R) & dom(Y) 
      & S != 0
      & M != 0
      & (S != E & S != N & S != D & S != M & S != O & S != R & S != Y )
      & (E != N & E != D & E != M & E != O & E != R & E != Y )
      & (N != D & N != M & N != O & N != R & N != Y )
      & (D != M & D != O & D != R & D != Y )
      & (M != O & M != R & M != Y )
      & (O != R & O != Y )
      & (R != Y )
      & (                  $sum( $sum($product(1000, S), $sum($product(100, E), $sum($product(10, N), D))),
	                         $sum($product(1000, M), $sum($product(100, O), $sum($product(10, R), E))) ) =
        $sum($product(10000, M), $sum($product(1000, O), $sum($product(100, N), $sum($product(10, E), Y))))
      ))))).

%% Consistency-based formulations

tff(v_type,type, s: $int).
tff(v_type,type, e: $int).
tff(v_type,type, n: $int).
tff(v_type,type, d: $int).
tff(v_type,type, m: $int).
tff(v_type,type, o: $int).
tff(v_type,type, r: $int).
tff(v_type,type, y: $int).

%% Verify a given solution
%% tff(a, axiom, smm(9,5,6,7,1,0,8,2) ).

%% Partial solution given (doable)
%% tff(a, axiom, smm(9,5,6,7,m,0,8,2) ).

%% Partial solution given (too difficult)
tff(a, axiom, smm(9,5,n,d,m,0,8,2) ).

%% Full problem
%% tff(a, axiom, smm(s,e,n,d,m,o,r,e) ).

%% Refutational formulations

%% Verify a given solution
%% (Easy)
%% tff(c, conjecture, smm(9,5,6,7,1,0,8,2) ).

%% tff(c, conjecture, (
%%     ? [N:$int, D:$int, M:$int] :
%%      ( smm(9,5,N,D,M,0,8,2) 
%%      ))).

%% Partial solution given
%% tff(c, conjecture, (
%%     ? [S:$int, E:$int, N:$int, D:$int, M:$int] :
%%      ( smm(S,E,N,D,M,0,8,2) 
%%      ))).

%% Full problem
%% tff(c, conjecture, (
%%    ? [S:$int, E:$int, N:$int, D:$int, M:$int ,O:$int ,R:$int ,Y:$int] :
%%     ( smm(S,E,N,D,M,O,R,Y) 
%%     ))).




