%--------------------------------------------------------------------------
% File     : SYN073-1 : TPTP v5.3.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : Pelletier Problem 50
% Version  : Especial.
% English  :

% Refs     : [Pel86] Pelletier (1986), Seventy-five Problems for Testing Au
% Source   : [Pel86]
% Names    : Pelletier 50 [Pel86]

% Status   : Unsatisfiable
% Rating   : 0.00 v2.0.0
% Syntax   : Number of clauses     :    2 (   1 non-Horn;   1 unit;   1 RR)
%            Number of atoms       :    3 (   0 equality)
%            Maximal clause size   :    2 (   2 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    2 (   1 constant; 0-1 arity)
%            Number of variables   :    3 (   1 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_NEQ_NHN

% Comments :
%--------------------------------------------------------------------------
cnf(clause_1,negated_conjecture,
    ( big_f(a,X)
    | big_f(X,Y) )).

cnf(prove_this,negated_conjecture,
    ( ~ big_f(X,f(X)) )).

%--------------------------------------------------------------------------
