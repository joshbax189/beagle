tff(t,type, t: $tType).
tff(a, type, a: t).
tff(b, type, b: t).
tff(c, type, c: t).
tff(p, type, p: t > $o).

tff(a,axiom, p(a) & p(b) & p(c) ).
tff(a, axiom, ![X:t]: (X=a | X=b | X=c) ).
tff(c, conjecture, ![X:t]: p(X)).