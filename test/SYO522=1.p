%------------------------------------------------------------------------------
% File     : SYO522=1 : TPTP v5.4.0. Bugfixed v5.2.0.
% Domain   : Syntactic
% Problem  : Functions are either odd or even
% Version  : Especial.
% English  : 

% Refs     : [Wal06] Waldmann (2006), Email to Geoff Sutcliffe
% Source   : [Wal06]
% Names    : 

% Status   : Theorem
% Rating   : 0.89 v5.4.0, 0.88 v5.3.0, 1.00 v5.2.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type)
%            Number of atoms       :    6 (   2 equality)
%            Maximal formula depth :    5 (   5 average)
%            Number of connectives :    1 (   0   ~;   1   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    3 (   1   >;   2   *;   0   +;   0  <<)
%            Number of predicates  :    3 (   2 propositional; 0-2 arity)
%            Number of functors    :    5 (   2 constant; 0-3 arity)
%            Number of variables   :    6 (   0 sgn;   0   !;   6   ?)
%            Maximal term depth    :    3 (   2 average)
%            Arithmetic symbols    :    3 (   1 pred;    0 func;    2 numbers)
% SPC      : TFF_THM_EQU_ARI

% Comments : 
% Bugfixes : v5.2.0 - Changed $plus to $sum, and $times to $product.
%------------------------------------------------------------------------------
tff(f_type,type,(
    f: ( $int * $int * $int ) > $int )).

tff(fxxx_is_either_even_or_odd,conjecture,
    ( ? [X: $int,Y: $int,Z: $int] : f(X,X,Y) = $product(2,Z)
    | ? [X: $int,Y: $int,Z: $int] : f(X,Y,Y) = $sum($product(2,Z),1) )).

%------------------------------------------------------------------------------
