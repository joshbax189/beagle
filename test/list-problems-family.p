% Declare some precedences:
%$ :prec max > swap > inRange > sorted > tail > append > in > length > head > cons > nil

tff(list_type, type, ( list: $tType )).
tff(nil_type,  type, ( nil: list )).
tff(cons_type, type, ( cons: ($int * list) > list )).
tff(head_type, type, ( head: list > $int )).
tff(tail_type, type, ( tail: list > list )).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% List axioms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Selectors:
tff(l1, axiom, ( ! [ K: $int, L: list ] : head(cons(K, L)) = K )).
tff(l2, axiom, ( ! [ K: $int, L: list ] : tail(cons(K, L)) = L )).
%% %% %% Constructors:
tff(l3, axiom, ( ! [ L: list ] : 
   ( L = nil
   | L = cons(head(L), tail(L)) ))).
tff(l4, axiom, ( ! [ K: $int, L: list ] : cons(K, L) != nil)).

%% %% Sufficient completeness:
%% tff(l1, axiom, ? [E: $int] : head(nil) = E).
%% tff(l1, axiom, tail(nil) = nil).


%%%%%%%%%%%%%%%%%%%%%%%
%% Sorted
%%%%%%%%%%%%%%%%%%%%%%%
tff(sorted, type, sorted: list > $o).
tff(sorted, axiom, (
   ! [ L: list ] :
     ( sorted(L) 
   <=> ( L = nil 
       | ( ? [ K: $int ] : 
	   ( L = cons(K, nil) ) )
       | ( ? [ K: $int, K1: $int, T: list ] : 
	 ( L = cons(K, cons(K1, T))
         & $lesseq(K, K1) 
	 & sorted(cons(K1, T)))))))).

%% tff(sorted, type, sorted: list > $o).
%% tff(sorted, axiom, (
%%    ! [ L: list ] :
%%      ( sorted(L) 
%%    <=> ( L = nil 
%%        | tail(L) = nil 
%%        | ( $lesseq(head(L), head(tail(L)))
%% 	 & sorted(tail(L)) ))))).

%% Test
%% tff(c, conjecture, sorted(cons(1, cons(2, cons(3, nil))))).
%% tff(c, conjecture, sorted(cons(2, cons(1, cons(3, nil))))).
%% tff(c, conjecture, ~sorted(cons(2, cons(1, cons(3, nil))))).


%%%%%%%%%%%%%%%%%%%%%%%
%% Length
%%%%%%%%%%%%%%%%%%%%%%%
tff(t, type, length: (list > $int)).
tff(l, axiom, length(nil) = 0).
tff(l, axiom, ! [H: $int, T: list] : length(cons(H, T)) = $sum(1, length(T))).


%%%%%%%%%%%%%%%%%%%%%%%
%% Problems
%%%%%%%%%%%%%%%%%%%%%%%
%% The problems below are parametric in n:
tff(n, type, n: $int).
tff(n, axiom, n=40).

%% Problem 1
tff(c, conjecture, (
  ? [L: list, A: $int, B: $int, C: $int] : 
    ( length(L) = n 
    & sorted(L)))).

%% Beagle times in seconds
%% n=5: 0.7
%% n=10: 1.3
%% n=15: 1.8
%% n=20: 2.7
%% n=25: 3.3
%% n=30: 4.0


%% Problem 2
%% tff(t, type, append: ((list * list) > list)).
%% tff(l, axiom, ! [L: list] : append(nil, L) = L).
%% tff(l, axiom, ! [I: $int, K: list, L: list] : append(cons(I, K), L) = cons(I, append(K, L))).
%% tff(c, conjecture, 
%%    ~ ( ! [K: list, L: list] : 
%%          ( ( $greater(length(K), n)
%%            & $greater(length(L), n) )
%%         => $greater(length(append(K, L)), $sum(n,$sum(n,2)))))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%% Interesting other problem, beagle can't solve it for lack of sufficient completeness
%% tff(c, conjecture, (
%%   ? [L: list, A: $int, B: $int, C: $int] : 
%%     ( length(L) = n 
%%     & ~sorted(L)))).

%% What we'd need is enumerating lists of increasing size over finite domains
%% so that everything becomes concrete. Beagle can prove this:
%% tff(c, conjecture, (
%%   ? [L: list, A: $int, B: $int, C: $int] : 
%%     ( L = cons(A, cons(B, cons(C, nil)))
%%     & (A = 1 | A = 2)
%%     & (B = 1 | B = 2)
%%     & (C = 1 | C = 2)
%%     & ~sorted(L)))).





