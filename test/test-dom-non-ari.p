tff(p_type,type,(
    dom: $tType )).

tff(a_type,type,(
    a: dom )).
tff(a_type,type,(
    b: dom )).
tff(a_type,type,(
    c: dom )).
tff(a_type,type,(
    d: dom )).
tff(a_type,type,(
    e: dom )).
tff(a_type,type,(
    f: dom )).
tff(a_type,type,(
    g: dom )).

tff(test, axiom,
    ( ! [X: dom] :
        ( ( X = a
	  | X = b
	  | X = c 
	  | X = d 
	  | X = e  
	  | X = f )))).


tff(test, axiom, a != f).
tff(test, axiom, b != f).
tff(test, axiom, c != f).
tff(test, axiom, d != f).
tff(test, axiom, e != f).

tff(test, axiom, a != e).
tff(test, axiom, b != e).
tff(test, axiom, c != e).
tff(test, axiom, d != e).

tff(test, axiom, a != d).
tff(test, axiom, b != d).
tff(test, axiom, c != d).

tff(test, axiom, a != c).
tff(test, axiom, b != c).

tff(test, axiom, a != b).

tff(test, axiom, g != a).
tff(test, axiom, g != b).
tff(test, axiom, g != c).
tff(test, axiom, g != d).
tff(test, axiom, g != e).
tff(test, axiom, g != f).



%------------------------------------------------------------------------------
