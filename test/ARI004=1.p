%------------------------------------------------------------------------------
% File     : ARI004=1 : TPTP v5.3.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Integer: Something less than 13
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.38 v5.3.0, 0.43 v5.2.0, 0.60 v5.1.0, 0.50 v5.0.0
% Syntax   : Number of formulae    :    1 (   1 unit;   0 type)
%            Number of atoms       :    1 (   0 equality)
%            Maximal formula depth :    2 (   2 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    1 (   1 constant; 0-0 arity)
%            Number of variables   :    1 (   0 sgn;   0   !;   1   ?)
%            Maximal term depth    :    1 (   1 average)
%            Arithmetic symbols    :    3 (   2 pred;    0 func;    1 numbers)
% SPC      : TFF_THM_NEQ_ARI

% Comments :
%------------------------------------------------------------------------------
tff(something_less_13,conjecture,(
    ? [X: $int] : $less(X,13) )).
%------------------------------------------------------------------------------
