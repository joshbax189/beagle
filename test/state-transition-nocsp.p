%% Run this as beagle -nogenvars -negsel state-transition-nocsp.p

%% A state is of the form state(i, val) where n and val are integers.
%% The value i is a natural number that defines the i-th state in a run.
%% The component val is, for simplicity,
%% a "value" modified by state transitions. In a more realistic setting, val would 
%% assign values to more than one variable, or be an assignment to one "big" database. 
tff(state, type, (state: ($int * $int) > $o)).

%% Bounded model checking: runs up to this length
tff(bound, type, bound: $int).

%% In each state we increment or decrement, described by the following "actions"
tff(inc_state, type, (inc_state: ($int * $int) > $o)).
tff(dec_state, type, (dec_state: ($int * $int) > $o)).

%% State transitions: increment or decrement
%% Notice the "or", which means branching out in the search tree according to state transitions
 
tff(trans1, axiom, (
   ! [I: $int, Val: $int] :
     ( ( state(I, Val)
       & $lesseq(I, bound) )
    => ( inc_state(I, Val) 
       | dec_state(I, Val) )))).

%% But not both
tff(trans2, axiom, (
   ! [I: $int, Val: $int] :
     ~ ( inc_state(I, Val) 
       & dec_state(I, Val) ))).

%% Increment action ("increase leave val by one")
tff(inc, axiom, (
   ! [I: $int, Val: $int] :
     ( inc_state(I, Val) 
    => state($sum(I, 1), $sum(Val, 1)) ))).

%% Decrement action, in three versions

%% Version 1: decrease val by three
%% Notice if Val is not sufficiently large the branch is closed,
%% i.e. the decrement action is not applicable. That's how we do guards.
%% tff(inc, axiom, (
%%    ! [I: $int, Val: $int] :
%%      ( dec_state(I, Val) 
%%     => ( $lesseq(5, Val) 
%%        & state($sum(I, 1), $difference(Val, 5)) )))).

%% Version 2: decrease val by a symbolic number, d
%% tff(d, type, d: $int).
%% %% d must be positive
%% tff(d, axiom, $greater(d, 0)).
%% tff(inc, axiom, (
%%    ! [I: $int, Val: $int] :
%%      ( dec_state(I, Val) 
%%     => ( $lesseq(d, Val) 
%%        & state($sum(I, 1), $difference(Val, d)) )))).

%% Version 3: decrease val by a symbolic number, dependent on I
tff(d, type, d: $int > $int).
%% d(I) must be positive
tff(d, axiom, ! [I: $int] : $greater(d(I), 0)).
tff(inc, axiom, (
   ! [I: $int, Val: $int] :
     ( dec_state(I, Val) 
    => ( $lesseq(d(I), Val) 
       & state($sum(I, 1), $difference(Val, d(I))) )))).

%% States are uniquely identified by their I component.
tff(state, axiom, ! [I: $int, Val1: $int, Val2: $int] :
     ( ( state(I, Val1)
       & state(I, Val2) )
    => Val1 = Val2)).

%% Initial state given convretely
tff(init, axiom, state(0, 0)).
%% Initial value for val given symbolically
%% tff(initval, type, val0: $int).
%% tff(initval, axiom, $greatereq(val0, 0)).
%% tff(init, axiom, state(0, val0)).

%% Set the depth bound
tff(bound, axiom, bound=5).

%% Safety property: at any time Val is greater or equal 0.
%% Can easily be made unprovable by setting the guard of the decrement action
%% to e.g. $lesseq(2, Val) 
tff(conc, conjecture, ! [I: $int, Val: $int] :
    ( ( $lesseq(0, I)  
      & $lesseq(I, bound)
      & state(I, Val) )
   => $greater(Val, 0))).




