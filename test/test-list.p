
%$ :prec inRange > append > head > tail > cons

tff(list_type, type, ( list: $tType )).
tff(nil_type,  type, ( nil: list )).
tff(cons_type, type, ( cons: ($int * list) > list )).
tff(head_type, type, ( head: list > $int )).
tff(tail_type, type, ( tail: list > list )).

tff(k, type, ( k: $int )).
tff(l, type, ( l: list )).

tff(inRange, type, ( inRange: ( list * $int ) > $o )).
tff(contains, type, ( contains: ( list * $int ) > $o )).
tff(append, type, ( append: ( list * list ) > list )).

%% list axioms:
%% Selectors:
tff(l1, axiom, ( ! [ K: $int, L: list ] : head(cons(K, L)) = K )).
tff(l2, axiom, ( ! [ K: $int, L: list ] : tail(cons(K, L)) = L )).
%% Constructors:
tff(l3, axiom, ( ! [ L: list ] : 
   ( L = nil
    | ( ? [ K: $int, R: list ] : L = cons(K, R) )))).
tff(l4, axiom, ( ! [ K: $int, L: list ] : cons(K, L) != nil)).
%% Constructors are injective
tff(l5, axiom, ( ! [ K1: $int, L1: list, K2: $int, L2: list ] : 
                   ( cons(K1, L1) = cons(K2, L2)
                  => ( K1 = K2
                     & L1 = L2 )))).


%% Sufficient completeness
tff(l1, axiom, (? [E: $int] : head(nil) = E)).
tff(l1, axiom, tail(nil) = nil).

%% Contains
tff(a, axiom, ( 
   ! [L: list, N: $int] : 
     ( contains(L, N) 
   <=> ( L != nil 
       & ( head(L) = N 
         | contains(tail(L), N)))))).

%% Append
tff(a, axiom, ( 
   ! [L1: list, L2: list, L3: list] : 
     ( append(L1, L2) = L3 
   <=> ( ( L1 = nil & L2 = L3 )
       | ( L1 != nil & L3 = append(tail(L1), cons(head(L1), L2))))))).

%% inRange axioms

tff(inRange1, axiom, ( ! [N: $int ] : inRange(nil, N) )).
tff(inRange2, axiom, ( ! [N: $int, K: $int, L: list ] : 
   ( ( $lesseq(0, K) 
     & $less(K, N) 
     & inRange(L, N) ) 
 <=> inRange(cons(K, L), N) ))).

%% Provable
%%tff(c, conjecture, contains(cons(1, cons(2, cons(3, nil))), 2)).
%%tff(c, conjecture, ~contains(cons(1, cons(2, cons(3, nil))), 4)).
%% tff(c, conjecture, contains(append(cons(1, cons(2, nil)), cons(3, cons(4, nil))), 3)).
%% Provable, with -genvars

%tff(c, conjecture, inRange(cons(1, cons(3, cons(0, nil))), 5)).
%tff(c, conjecture, ~ inRange(cons(1, cons(3, cons(0, nil))), 2)).
tff(c, conjecture, ( ! [ L: list, N: $int ] : 
   ( L != nil
  => ( inRange(L, N) 
    => inRange(tail(L), N))))).

%% Non-provable:

%% tff(c, conjecture, ~ inRange(cons(1, cons(3, cons(0, nil))), 5)).
%% tff(c, conjecture, inRange(cons(1, cons(3, cons(0, nil))), 2)).



%------------------------------------------------------------------------------
