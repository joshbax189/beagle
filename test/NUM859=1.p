%------------------------------------------------------------------------------
% File     : NUM859=1 : TPTP v5.4.0. Released v4.1.0.
% Domain   : Number Theory
% Problem  : Basic upper bound replace maximum with less-or-equal
% Version  : Especial.
% English  : This is an abstraction of a problem to show equivalence of two 
%            given constraint models. More precisely, the task is to prove that
%            the minimal solutions of a certain constraint model are preserved
%            if the applications of the "maximum" function in it are replaced
%            by "upper bounds" only.

% Refs     : [Bau10] Baumgartner (2010), Email to G. Sutcliffe
%          : [BS09]  Baumgartner & Slaney (2009), Constraint Modelling: A C
% Source   : [Bau10]
% Names    :

% Status   : Theorem
% Rating   : 0.89 v5.4.0, 1.00 v5.3.0, 0.80 v5.2.0, 0.83 v5.1.0, 0.80 v5.0.0
% Syntax   : Number of formulae    :   16 (   0 unit;   7 type)
%            Number of atoms       :   49 (   2 equality)
%            Maximal formula depth :    8 (   5 average)
%            Number of connectives :   17 (   2   ~;   2   |;   4   &)
%                                         (   7 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   18 (   7   >;  11   *;   0   +;   0  <<)
%            Number of predicates  :   16 (   9 propositional; 0-3 arity)
%            Number of functors    :    2 (   0 constant; 1-2 arity)
%            Number of variables   :   27 (   0 sgn;  26   !;   1   ?)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :    2 (   2 pred;    0 func;    0 numbers)
% SPC      : TFF_THM_EQU_ARI

% Comments : 
%------------------------------------------------------------------------------
tff(summation_type,type,(
    summation: $int > $int )).

tff(ub_type,type,(
    ub: ( $int * $int * $int ) > $o )).

tff(model_max_type,type,(
    model_max: ( $int * $int * $int ) > $o )).

tff(model_ub_type,type,(
    model_ub: ( $int * $int * $int ) > $o )).

tff(minsol_model_max_type,type,(
    minsol_model_max: ( $int * $int * $int ) > $o )).

tff(minsol_model_ub_type,type,(
    minsol_model_ub: ( $int * $int * $int ) > $o )).

tff(max_type,type,(
    max: ( $int * $int ) > $int )).

%----summation(X) is meant as an abstraction of a certain summation term in
%----the original constraint problem
tff(summation_monotone,axiom,(
    ! [X: $int,Y: $int] :
      ( $lesseq(X,Y)
    <=> $lesseq(summation(X),summation(Y)) ) )).

%----Maximum function
tff(max_1,axiom,(
    ! [X: $int,Y: $int] :
      ( max(X,Y) = X
      | ~ $lesseq(Y,X) ) )).

tff(max_2,axiom,(
    ! [X: $int,Y: $int] :
      ( max(X,Y) = Y
      | ~ $lesseq(X,Y) ) )).

%----Z is an upper bound of Y and Z
tff(ub,axiom,(
    ! [X: $int,Y: $int,Z: $int] :
      ( ub(X,Y,Z)
    <=> ( $lesseq(X,Z)
        & $lesseq(Y,Z) ) ) )).

%----The model - version with max
tff(model_max_2,axiom,(
    ! [X: $int,Y: $int,N: $int] :
      ( model_max(X,Y,N)
    <=> $lesseq(max(X,Y),N) ) )).

%----The model - version with ub
tff(model_ub_2,axiom,(
    ! [X: $int,Y: $int,N: $int] :
      ( model_ub(X,Y,N)
    <=> ? [Z: $int] :
          ( ub(X,Y,Z)
          & $lesseq(Z,N) ) ) )).

%----minimal solution, model_max
tff(minsol_model_max,axiom,(
    ! [X: $int,Y: $int,N: $int] :
      ( minsol_model_max(X,Y,N)
    <=> ( model_max(X,Y,N)
        & ! [Z: $int] :
            ( model_max(X,Y,Z)
           => $lesseq(N,Z) ) ) ) )).

%----minimal solution, model_ub
tff(minsol_model_ub,axiom,(
    ! [X: $int,Y: $int,N: $int] :
      ( minsol_model_ub(X,Y,N)
    <=> ( model_ub(X,Y,N)
        & ! [Z: $int] :
            ( model_ub(X,Y,Z)
           => $lesseq(N,Z) ) ) ) )).

%----Conjecture: minimal solutions of model_max and model_ub are the same:
tff(max_is_ub_1,conjecture,(
    ! [X: $int,Y: $int,Z: $int] :
      ( minsol_model_ub(X,Y,Z)
    <=> minsol_model_max(X,Y,Z) ) )).

%------------------------------------------------------------------------------
