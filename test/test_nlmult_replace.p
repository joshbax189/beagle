% When is non-linear mult eliminated/introduced.
% Status   : Unsatisfiable

%NLPP should not be introduced for these examples:
% tff(a1, axiom, $product(5,7) = 35).
% tff(a2, axiom, ![X: $int, Y: $int]: $product($product(3,5),X) = $product(15,Y)).
% tff(a2, axiom, ![X: $int, Y: $int]: $product($sum(3,5),X) = $product(4,Y)).
% tff(a2, axiom, ![X: $int, Y: $int]: $product(4,X) = $product($difference(8,4),Y)).

%these should be eliminated- beagle should be able to prove unsat quickly!
tff(a2, axiom, ![X: $int, Y: $int]: $product(Y,X) = $product(4,Y)).

tff(a,type, a: $int).
tff(a2, axiom, ![X: $int, Y: $int]: $product(a,X) = $product(4,Y)).
tff(a2, axiom, ?[X: $int]: ![Y: $int]: $product(Y,X) = $product(4,Y)).
tff(a2, axiom, ![X: $int, Y: $int]: $product($sum(3,$product(3,a)),X) = $product(4,Y)).




