tff(array_type,type,(
    array: $tType )).

tff(read_type,type,(
    read: ( array * $int ) > $int )).

tff(write_type,type,(
    write: ( array * $int * $int ) > array )).

tff(sorted_type,type,(
    sorted: ( array * $int ) > $o )).

tff(ax1,axiom,(
     ! [A: array,I: $int,V: $int] : read(write(A,I,V),I) = V )).

tff(ax2,axiom,(
    ! [A: array,I: $int,J: $int,V: $int] :
      ( I = J
      | read(write(A,I,V),J) = read(A,J) ) )).


%% tff(a, axiom, (
%%     ! [A: array] :
%%       ( sorted(A)
%%     <=> ( ! [I: $int, J: $int] :
%%             ( $less(I, J)
%%            => $lesseq(read(A, I), read(A, J))))))).

tff(a, axiom, (
    ! [A: array, N: $int] :
      ( sorted(A, N)
    <=> ( ! [I: $int, J: $int] :
          ( ( $lesseq(0, I) 
            & $less(I, N) 
	    & $less(I, J)
	    & $less(J, N) )
        => $lesseq(read(A, I), read(A, J)) ))))).

tff(a, type, a0: array).
tff(a, type, a1: array).
tff(a, type, a2: array).
tff(a, type, a3: array).

tff(a, axiom, a1 = write(a0, 0, 2)).
tff(a, axiom, a2 = write(a1, 1, 4)).
tff(a, axiom, a3 = write(a2, 2, 3)).

tff(c, conjecture, (
   sorted(a3, 3)
)).



%------------------------------------------------------------------------------
