% A path exists of a given weight.
% Status   : Theorem

tff(node_type, type, node: $tType).

% Weighted edges
tff(edge_type, type, edge: ( node * node * $int) > $o).

tff(reach_type, type, reach: ( node * node * $int ) > $o).

tff(n1, type, n1: node).
tff(n2, type, n2: node).
tff(n3, type, n3: node).
tff(n4, type, n4: node).
tff(n5, type, n5: node).

tff(nodes_distinct, axiom, n1!=n2 & n1!=n3 & n1!=n4 & n1!=n5 &
                           n2!=n3 & n2!=n4 & n2!=n5 &
                           n3!=n4 & n3!=n5 &
                           n4!=n5 ).

tff(nodes, axiom, ![X: node]: ( X=n1 | X=n2 | X=n3 | X=n4 | X=n5 ) ).

% G has the shape:
 % n1->n2->n3
 % |       |
 % v       v
 % n4  ->  n5
tff(g, axiom, edge(n1, n2, 3)).
tff(g, axiom, edge(n2, n3, 2)).
tff(g, axiom, edge(n1, n4, 5)).
tff(g, axiom, edge(n4, n5, 7)).
tff(g, axiom, edge(n3, n5, 10)).

tff(reachable_base_case, axiom, ( 
  ! [N1: node, N2: node, C: $int] : 
    ( edge(N1, N2, C) => reach(N1, N2, C) ))).

tff(reachable_transitive, axiom, ( 
  ! [N1: node, N2: node, N3: node, C1: $int, C2: $int] : 
    ( ( reach(N1, N2, C1)
      & reach(N2, N3, C2) )
  => reach(N1, N3, $sum(C1,C2)) ))).

tff(reach_n5_from_n1, conjecture, ?[X: $int]: ( reach(n1,n5,X) & $less(X,15)) ).

%------------------------------------------------------------------------------
