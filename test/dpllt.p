tff(p_type,type,(
    p: $int > $o )).

tff(a_type,type,(
    a: $int )).
tff(a_type,type,(
    b: $int )).
tff(c_type,type,(
    c: $int )).


tff(a, axiom, $lesseq(2,a)).
tff(a, axiom, $lesseq(a,3)).
tff(p, axiom, p(a)).
%% tff(p, axiom, ~p(2)).
%% tff(p, axiom, ~p(3)).
%% Purified
tff(p, axiom, ~p(b)).
tff(p, axiom, ~p(c)).
tff(b, axiom, b=2).
tff(b, axiom, c=3).

%% DPLL(T) needs to decide on the interface literals a=b, a=c, b=c (many more if more constants are present).
%% It seems better to instead enumerate the possible assignments for a, i.e. a=2, a=3 
%% However, if the domain is larger than the number of interface constants the situation might reverse.
%% Anyway, hierarchic superposition does not need either!

