%Uses FD notation

%% The well-known send-more-money problem,
%% See http://en.wikibooks.org/wiki/Prolog/Constraint_Logic_Programming
%% sendmoremoney(Vars) :-
%%     Vars = [S,E,N,D,M,O,R,Y],
%%     Vars ins 0..9,
%%     S #\= 0,
%%     M #\= 0,
%%     all_different(Vars),
%%                  1000*S + 100*E + 10*N + D
%%     +            1000*M + 100*O + 10*R + E
%%     #= 10000*M + 1000*O + 100*N + 10*E + Y.

%% It is in fact a combination of finite domain constraints (alldifferent) and linear integer arithmetic over variables 
%% over these constraints.


tff(smm_type,type,( smm: ($int * $int * $int * $int * $int * $int * $int * $int) > $o )).
tff(s, axiom, ( 
  ! [S:$fd(0,9), E:$fd(0,9), N:$fd(0,9), D:$fd(0,9), M:$fd(0,9) ,O:$fd(0,9) ,R:$fd(0,9) ,Y:$fd(0,9)] :
    ( smm(S,E,N,D,M,O,R,Y) 
  <=> ( S != 0
       & M != 0
       & $$alldiff(S,E,N,D,M,O,R,Y)
       & (                  $sum( $sum($product(1000, S), $sum($product(100, E), $sum($product(10, N), D))),
	                         $sum($product(1000, M), $sum($product(100, O), $sum($product(10, R), E))) ) =
        $sum($product(10000, M), $sum($product(1000, O), $sum($product(100, N), $sum($product(10, E), Y))))
      ))))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Consistency-based formulations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% tff(v_type,type, s: $fd(0,9)).
%% tff(v_type,type, e: $fd(0,9)).
%% tff(v_type,type, n: $fd(0,9)).
%% tff(v_type,type, d: $fd(0,9)).
%% tff(v_type,type, m: $fd(0,9)).
%% tff(v_type,type, o: $fd(0,9)).
%% tff(v_type,type, r: $fd(0,9)).
%% tff(v_type,type, y: $fd(0,9)).

%% Verify a given solution
%tff(a, axiom, smm(9,5,6,7,1,0,8,2) ).

%% Partial solution given (doable)
%% tff(a, axiom, smm(9,5,6,7,m,0,8,2) ).

%% Partial solution given (too difficult)
%tff(a, axiom, smm(9,5,n,d,m,0,8,2) ).

%% Full problem
%% tff(a, axiom, smm(s,e,n,d,m,o,r,y) ).

%% Uniqueness of solutions
tff(c, conjecture, (
   ! [S1:$fd(0,9), E1:$fd(0,9), N1:$fd(0,9), D1:$fd(0,9), M1:$fd(0,9) ,O1:$fd(0,9) ,R1:$fd(0,9) ,Y1:$fd(0,9), S2:$fd(0,9), E2:$fd(0,9), N2:$fd(0,9), D2:$fd(0,9), M2:$fd(0,9) ,O2:$fd(0,9) ,R2:$fd(0,9) ,Y2:$fd(0,9)] :
    ( ( smm(S1,E1,N1,D1,M1,O1,R1,Y1) 
      & smm(S2,E2,N2,D2,M2,O2,R2,Y2) )
   => ( S1=S2
      & E1=E2
      & N1=N2
      & D1=D2
      & M1=M2
      & O1=O2
      & R1=R2
      & Y1=Y2 )
    ))).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Refutational formulations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Verify a given solution
%% (Easy)
%% tff(c, conjecture, smm(9,5,6,7,1,0,8,2) ).

%% Partial non-solution given
%% tff(c, conjecture, (
%%      ? [N:$fd(0,9), D:$fd(0,9), M:$fd(0,9)] :
%%       ( smm(9,5,N,D,M,0,8,3) 
%%       ))).

%% Partial solution given
%% tff(c, conjecture, (
%%     ? [S:$fd(0,9), E:$fd(0,9), N:$fd(0,9), D:$fd(0,9), M:$fd(0,9)] :
%%      ( smm(S,E,N,D,M,0,8,2) 
%%      ))).

%% Full problem
%% tff(c, conjecture, (
%%    ? [S:$fd(0,9), E:$fd(0,9), N:$fd(0,9), D:$fd(0,9), M:$fd(0,9) ,O:$fd(0,9) ,R:$fd(0,9) ,Y:$fd(0,9)] :
%%     ( smm(S,E,N,D,M,O,R,Y) 
%%     ))).


%% Problem analysis: are solutions unique?
%% tff(c, conjecture, (
%%    ? [S1:$fd(0,9), E1:$fd(0,9), N1:$fd(0,9), D1:$fd(0,9), M1:$fd(0,9) ,O1:$fd(0,9) ,R1:$fd(0,9) ,Y1:$fd(0,9), S2:$fd(0,9), E2:$fd(0,9), N2:$fd(0,9), D2:$fd(0,9), M2:$fd(0,9) ,O2:$fd(0,9) ,R2:$fd(0,9) ,Y2:$fd(0,9)] :
%%     ( smm(S1,E1,N1,D1,M1,O1,R1,Y1) 
%%     & smm(S2,E2,N2,D2,M2,O2,R2,Y2)
%%     & ( S1 !=S2
%%       | E1 !=E2
%%       | N1 !=N2
%%       | D1 !=D2
%%       | M1 !=M2
%%       | O1 !=O2
%%       | R1 !=R2
%%       | Y1 !=Y2 ) ))).



