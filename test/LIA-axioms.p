
%% -(-X) = X
tff(a1, axiom, ( ! [X:$int] : $uminus($uminus(X)) = X)).

%% (X+(-Y))+Y = X
tff(a1, axiom, ( ! [X:$int, Y:$int] : $sum($sum(X,$uminus(Y)),Y)  = X)).

%% X+0 = X
tff(a1, axiom, ( ! [X:$int] : $sum(X,0) = X)).

%% 0+X = X
tff(a1, axiom, ( ! [X:$int] : $sum(0,X) = X)).

%% 0*X = 0
tff(a1, axiom, ( ! [X:$int] : $product(0,X) = 0)).

%% X*0 = 0
tff(a1, axiom, ( ! [X:$int] : $product(X,0) = 0)).

%% X*1 = X
tff(a1, axiom, ( ! [X:$int] : $product(X,1) = X)).

%% 1*X = X
tff(a1, axiom, ( ! [X:$int] : $product(1,X) = X)).

%% not X < X
tff(a1, axiom, ( ! [X:$int] : ~$less(X,X))).

%% X < X+1
tff(a1, axiom, ( ! [X:$int] : $less(X,$sum(X,1)))).

%% X < Y or Y < X or X = Y
tff(a1, axiom, ( ! [X:$int, Y:$int] : ($less(X,Y) | $less(Y,X) | X=Y))).

%% X+(-X) = 0
tff(a1, axiom, ( ! [X:$int] : $sum(X,$uminus(X)) = 0)).

%% (-X)+X = 0
tff(a1, axiom, ( ! [X:$int] : $sum($uminus(X),X) = 0)).

%% not X < Y or not Y < Z or X < Z
%% tff(a1, axiom, ( ! [X:$int, Y:$int, Z:$int] : (~$less(X,Y) | ~$less(Y,Z) | $less(X,Z)))).



