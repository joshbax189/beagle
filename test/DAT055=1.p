%------------------------------------------------------------------------------
% File     : DAT055=1 : TPTP v5.4.0. Released v5.0.0.
% Domain   : Data Structures
% Problem  : Boyer-Moore min-max problem
% Version  : Especial.
% English  : 

% Refs     : [BM88]  Boyer & Moore J. (1988), Integrating Decision Procedur
%          : [Wal06] Waldmann (2006), Email to Geoff Sutcliffe
% Source   : [Wal06]
% Names    : 

% Status   : Theorem
% Rating   : 0.50 v5.4.0, 0.62 v5.3.0, 0.57 v5.2.0, 0.60 v5.1.0, 1.00 v5.0.0
% Syntax   : Number of formulae    :    7 (   4 unit;   6 type)
%            Number of atoms       :   12 (   0 equality)
%            Maximal formula depth :    4 (   3 average)
%            Number of connectives :    3 (   0   ~;   0   |;   2   &)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :   10 (   8 propositional; 0-2 arity)
%            Number of functors    :    7 (   4 constant; 0-2 arity)
%            Number of variables   :    1 (   0 sgn;   1   !;   0   ?)
%            Maximal term depth    :    3 (   2 average)
%            Arithmetic symbols    :    5 (   3 pred;    1 func;    1 numbers)
% SPC      : TFF_THM_NEQ_ARI

% Comments : 
%------------------------------------------------------------------------------
tff(list_type,type,(
    list: $tType )).

tff(a_type,type,(
    a: list )).

tff(l_type,type,(
    l: $int )).

tff(k_type,type,(
    k: $int )).

tff(min_type,type,(
    min: list > $int )).

tff(max_type,type,(
    max: list > $int )).

tff(boyer_moore_max_min,conjecture,
    ( ( ! [X: list] : $lesseq(min(X),max(X))
      & $lesseq(l,min(a))
      & $less(0,k) )
   => $less(l,$sum(max(a),k)) )).

%------------------------------------------------------------------------------
