%------------------------------------------------------------------------------
% File     : DAT003=1 : TPTP v5.4.0. Released v5.0.0.
% Domain   : Data Structures
% Problem  : Element 3 is 33
% Version  : [PW06] axioms.
% English  :

% Refs     : [PW06]  Prevosto & Waldmann (2006), SPASS+T
%          : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    : (27) [PW06]

% Status   : Theorem
% Rating   : 0.33 v5.4.0, 0.50 v5.1.0, 0.00 v5.0.0
% Syntax   : Number of formulae    :    6 (   2 unit;   3 type)
%            Number of atoms       :   13 (   5 equality)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :    2 (   0   ~;   1   |;   0   &)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    5 (   2   >;   3   *;   0   +;   0  <<)
%            Number of predicates  :    6 (   5 propositional; 0-2 arity)
%            Number of functors    :    9 (   7 constant; 0-3 arity)
%            Number of variables   :    9 (   0 sgn;   9   !;   0   ?)
%            Maximal term depth    :    5 (   2 average)
%            Arithmetic symbols    :    8 (   1 pred;    0 func;    7 numbers)
% SPC      : TFF_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
%----Includes axioms for arrays
include('Axioms/DAT001=0.ax').
%------------------------------------------------------------------------------
tff(co1,conjecture,(
    ! [V: array] :
      ( read(write(write(write(V,3,33),4,444),5,55),3) = 33))).
%------------------------------------------------------------------------------
