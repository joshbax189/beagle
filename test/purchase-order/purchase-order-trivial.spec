// Provable:
// modelcheck-sym --eagerFOChecks purchase-order-trivial.spec "E X inRange(db.order, db.nrStockItems)" 
// Requires inconsistency check, which fails:
// modelcheck-sym --eagerFOChecks purchase-order-trivial.spec "A X inRange(db.order, db.nrStockItems)" 
// A perhaps better example that demonstrates the need for detecting satisfiability is k-induction,
// which will produce some uprovable obligations
TYPES

DB = { 
  order: List[Integer],
  nrStockItems: Integer
}

SIGNATURE

inRange: [List[Integer], Integer] -> Bool

DEFINITIONS

inRange: ∀ l: List[Integer] . ∀ n: Integer . 
    ( inRange(l, n) 
    ⇔ ( l= [| |] 
       ∨ (0 <= head(l) ∧ head(l) < n ∧ inRange(tail(l), n))))

CONSTRAINTS

initDB: (db.nrStockItems >= 0 ∧ ¬(db.order = [| |]) ∧ inRange(db.order, db.nrStockItems))

DIGRAPH

"start" [ init = "true" ]
"stop" 

"start" -> "stop" [ script = "db.order = tail(db.order)" ]
// "start" -> "stop" [ script = "db.order = tail(db.nrStockItems :: tail(db.order))" ]
