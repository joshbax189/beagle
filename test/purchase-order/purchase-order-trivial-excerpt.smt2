(set-option :pull-nested-quantifiers true)
(set-option :mbqi true)
(set-option :macro-finder true)
; (declare-datatypes (T) ((List (nil) (cons (head T) (tail (List T))))))

(declare-fun inRange ((List Int) Int) Bool)
; declaring recursive inRange as constraint
(assert (forall ((l0 (List Int)))
	  (forall ((n0 Int)) 
	    (= (inRange l0 n0) 
	       (or (= l0 (as nil (List Int))) 
		   (and (and (<= 0 (head l0))
		             (< (head l0) n0)) 
			(inRange (tail l0) n0)))))))

;;query 1 (sat)
;;(assert (exists ((l (List Int)) (n Int)) (inRange l n)))

;;query 2 (unsat)
;; (assert (exists ((l (List Int)) (n Int))
;; 	(and 
;; 	  (not (= l (as nil (List Int))))
;; 	  (inRange l n)
;; 	  (not (inRange (tail l) n)))))

;;query 3 (unsat)
;; (assert (exists ((l (List Int)) (n Int))
;; 	(and 
;; 	  (not (= l (as nil (List Int))))
;; 	  (inRange l n)
;; 	  (not (inRange (insert (- (- n 1) (head l)) (tail l)) n)))))

;; query 4 (unsat)
;; (assert (exists ((l (List Int)) (n Int))
;; 	(and 
;; 	  (not (= l (as nil (List Int))))
;; 	  (inRange l n)
;; 	  (not (>= (- (- n 1) (head l)) 0)))))

;;query 5 (sat)

(declare-fun e1 () Int)
(declare-fun e2 () Int)
(declare-fun e3 () Int)
(define-fun  zl () (List Int) (insert e1 (insert e2 nil)))

(assert (exists ((l (List Int)) (n Int))
	(and 
	  (not (= l (as nil (List Int))))
	  (= l zl)
	  (inRange l n)
	  (< (- n (head l)) 2))))

;;query 6 (sat)
;; (assert (exists ((l (List Int)) (n Int))
;; 	(and 
;; 	  (inRange l n)
;; 	  (not (inRange (tail l) n)))))


(check-sat)
