tff(p_type,type,(
    p: $int > $o )).

tff(f_type,type,(
    f: $int > $int )).
tff(f_type,type,(
    g: $int > $int )).

%% tff(g_type,type,(
%%     g: ($int * $int) > $int )).

tff(a_type,type,(
    a: $int )).

tff(a_type,type,(
    b: $int )).


%% tff(a, axiom, $less(a, 7)).
%% tff(a, axiom, p(g(a))).
%% %% tff(a, axiom, ! [X: $int] : ( f(X) = $sum(X, 1))).
%% tff(a, axiom, ! [X: $int] : ( g(X) = $sum(X, 1) <= $less(X, 10))).
%% tff(a, axiom, ~ p($sum(a,1))).

%% This should be provable:
tff(a, axiom, ($less(2, a) & $less(a, 7))).
tff(a, axiom, p(g(f(a)))).
tff(a, axiom, ! [X: $int] : ( f(X) = $sum(X, 1) <= $greater(X, 0))).
tff(a, axiom, ! [X: $int] : ( g(X) = $sum(X, -1) <= $less(X, 10))).
tff(a, axiom, ~ p(f(g(a)))).

%% tff(f, axiom, ( ? [X: $int, Y: $int] : f(X) = Y )).
%% tff(f, conjecture, ( ! [X: $int] : ( ? [Y: $int] : f(X) = Y ))).

%% tff(x, conjecture, ( ! [X: $int] : $lesseq(f(X), f(X)))).
%% tff(x, conjecture, ( ? [X: $int, Y: $int] : $lesseq(f(X), f(Y)))).

%% satisfiable 
%% tff(x, axiom, ~ ( ? [X: $int] : $less(f(X), f(X)))).

%% tff(a, axiom, p($sum(g($sum(a,1), $sum(f(a),2)), 3))).

%% tff(a, axiom, ! [X: $int] : $greater(f(X), X)).
%% tff(c, conjecture, ? [X: $int] : $greatereq(f(X), X)).




%------------------------------------------------------------------------------
