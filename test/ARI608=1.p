%------------------------------------------------------------------------------
% File     : ARI608=1 : TPTP v5.4.0. Released v5.1.0.
% Domain   : Arithmetic
% Problem  : Combining monotonicity and transitivity
% Version  : Especial.
% English  :

% Refs     : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    :

% Status   : Theorem
% Rating   : 0.38 v5.4.0, 0.62 v5.3.0, 0.57 v5.2.0, 0.80 v5.1.0
% Syntax   : Number of formulae    :    5 (   3 unit;   4 type)
%            Number of atoms       :   10 (   0 equality)
%            Maximal formula depth :    6 (   3 average)
%            Number of connectives :    4 (   0   ~;   0   |;   2   &)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    1 (   1   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    7 (   5 propositional; 0-2 arity)
%            Number of functors    :    4 (   3 constant; 0-1 arity)
%            Number of variables   :    2 (   0 sgn;   2   !;   0   ?)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :    3 (   3 pred;    0 func;    0 numbers)
% SPC      : TFF_THM_NEQ_ARI

% Comments :
%------------------------------------------------------------------------------
tff(f_type,type,(
    f: $int > $int )).

tff(a_type,type,(
    a: $int )).

tff(b_type,type,(
    b: $int )).

tff(c_type,type,(
    c: $int )).

tff(f_mon_implies_trans,conjecture,
    ( ( ! [X: $int,Y: $int] :
          ( $lesseq(X,Y)
         => $lesseq(f(X),f(Y)) )
      & $lesseq(a,b)
      & $less(b,c) )
   => $lesseq(f(a),f(c)) )).

%------------------------------------------------------------------------------
