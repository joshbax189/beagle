% Declare some precedences:
%$ max > in > swap > inRange > tail > head > cons > nil

tff(list_type, type, ( list: $tType )).
tff(nil_type,  type, ( nil: list )).
tff(cons_type, type, ( cons: ($int * list) > list )).
tff(head_type, type, ( head: list > $int )).
tff(tail_type, type, ( tail: list > list )).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% List axioms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Selectors:
tff(l1, axiom, ( ! [ K: $int, L: list ] : head(cons(K, L)) = K )).
tff(l2, axiom, ( ! [ K: $int, L: list ] : tail(cons(K, L)) = L )).
%% %% %% Constructors:
tff(l3, axiom, ( ! [ L: list ] : 
   ( L = nil
   | L = cons(head(L), tail(L)) ))).
tff(l4, axiom, ( ! [ K: $int, L: list ] : cons(K, L) != nil)).
%% %% Constructors are injective
%% Not needed - this is a consequence
%% tff(l5, axiom, ( ! [ K1: $int, L1: list, K2: $int, L2: list ] : 
%%                    ( cons(K1, L1) = cons(K2, L2)
%%                   => ( K1 = K2
%%                      & L1 = L2 )))).

%% %% Sufficient completeness:
%% tff(l1, axiom, ? [E: $int] : head(nil) = E).
%% tff(l1, axiom, tail(nil) = nil).


%%%%%%%%%%%%%%%%%%%%%%%
%% List membership
%%%%%%%%%%%%%%%%%%%%%%%


%% tff(in, type, in: ($int * list) > $o).
%% tff(in_conv, axiom, 
%%    ! [X: $int, L: list] : 
%%      ( in(X, L) 
%%    <=> ( ( ? [H: $int, T: list] : 
%%              ( L = cons(H, T)
%%              & X = H ) )
%%        | ( ? [H: $int, T: list] : 
%% 	     ( L = cons(H, T)
%%              & in(X, T)))))).

%% Provable:
%% tff(c, conjecture, in(2, cons(1, cons(2, cons(3, nil))))).
%% tff(c, conjecture, ~in(4, cons(1, cons(2, cons(3, nil))))).
%% tff(c, conjecture, ! [A: $int] : ~in(A, nil)).

%% Non-provable, prove negation instead
%% tff(c, conjecture, in(4, cons(1, cons(2, cons(3, nil))))).
%% tff(c, conjecture, in(4, nil)).


%%%%%%%%%%%%%%%%%%%%%%%
%% General
%%%%%%%%%%%%%%%%%%%%%%%
%% Suppose we want to disprove
%% tff(c, conjecture, 
%%    ! [L: list, L1: list] :
%%      ( ( L != nil
%%        & L1 = tail(L) )
%%     => L1 != nil )).

%% We do this by disproving the negation:
%% 
%% tff(c, conjecture, 
%%    ~ ( ! [L: list, L1: list] :
%%        ( ( L != nil
%%          & L1 = tail(L) )
%%       => L1 != nil ))).

%%%%%%%%%%%%%%%%%%%%%%%
%% inRange
%%%%%%%%%%%%%%%%%%%%%%%
tff(inRange, type, inRange: ($int * list) > $o).
tff(inRange, axiom, (
   ! [ N: $int, L: list ] :
     ( inRange(N, L) 
  <=>  ( L = nil 
       | ? [ K: $int, T: list ] : 
	 ( L = cons(K, T)
         & $lesseq(0, K) 
	 & $less(K, N) 
	 & inRange(N, T) ))))).

%% Provable
%% tff(c, conjecture, inRange(4, cons(1, cons(3, cons(2, nil))))).
%% tff(c, conjecture, 
%%    ! [N: $int] : 
%%      ( $greatereq(N, 4) => inRange(N, cons(1, cons(3, cons(2, nil)))))).

%% Unprovable (conjectures below are already negated)

%% Problem 1: beagle -indexing -genvars -bgsimp cautious: 6.2 sec ; spass+t: 0.3 sec
%% Problem 1: beagle -indexing -bgsimp cautious: 10 sec
%% tff(c, conjecture, 
%%    ~ ( inRange(4, cons(1, cons(5, cons(2, nil)))))).

%% Problem 2: beagle -indexing -genvars -bgsimp cautious: 7.2 sec ; spass+t: 0.3 sec
%% Problem 2: beagle -indexing -bgsimp cautious: >60 sec
%% tff(c, conjecture, 
%%    ~ ( ! [N: $int] : 
%%          ( $greatereq(N, 4) => inRange(N, cons(1, cons(5, cons(2, nil))))))).

%% Problem 3: beagle -indexing -genvars -bgsimp cautious: 3.9 sec ; spass+t: 0.3 sec
%% Problem 3: beagle -indexing -bgsimp cautious: >60 sec
%% tff(c, conjecture, (
%%         ~ ( ! [L: list, N: $int] : 
%%             ( inRange(N, tail(L))
%%            => inRange(N, L))))).

%% Problem 4: beagle -indexing -genvars -bgsimp cautious: 2.7 sec ; spass+t: 0.3 sec
%% Problem 4: beagle -indexing -bgsimp cautious: 2.7 sec
%% tff(c, conjecture, (
%%         ~ ( ? [L: list, N: $int] : 
%%             ( L != nil
%%             & inRange(N, L)
%%             & $less($difference(N, head(L)), 1))))).

%% Problem 5: beagle -indexing -genvars -bgsimp cautious: 8.2 sec ; spass+t: 0.3 sec
%% Problem 5: beagle -indexing -bgsimp cautious: >60 sec
%% tff(c, conjecture,
%%    ~ ( ! [N: $int, L: list ] :
%%          ( inRange(N, L) => inRange($difference(N, 1), L) ))).

%% Problem 6: beagle -indexing -genvars -bgsimp cautious: 2.8 sec ; spass+t: 0.3 sec
%% Problem 6: beagle -indexing -bgsimp cautious: 2.6 sec
%% tff(c, conjecture, (
%%         ~ ( ! [L: list, N: $int] : 
%%             ( ( L != nil
%%                 & inRange(N, L) ) 
%%              => $greatereq($difference(N, head(L)), 2))))).

%% Problem 7: beagle -indexing -genvars -bgsimp cautious: 4.5 sec ; spass+t: 5.2 sec
%% Problem 7: beagle -indexing -bgsimp cautious: >60 sec
tff(c, conjecture, 
    ~ ( ! [N: $int, L0: list, L1: list ] :
        ( ( $greater(N, 0) 
          & inRange(N, L0) 
          & L1 = cons($difference(N,2), L0) )
       => inRange(N, L1) ))).

%% tff(t, type, length: (list > $int)).
%% tff(l, axiom, length(nil) = 0).
%% tff(l, axiom, ! [H: $int, T: list] : length(cons(H, T)) = $sum(1, length(T))).

%% tff(a, conjecture, ( ? [L: list, N: $int] : 
%%  	            ( $greatereq(N, 3) 
%%  		    & $greatereq(length(L), 20)
%%  	            & inRange(N, L)))).



%% Various concrete lists over symbolic constants
%% tff(e1, type, e1: $int).
%% tff(e2, type, e2: $int).
%% tff(e3, type, e3: $int).
%% tff(e3, type, zl: list).
%% The "z" in the name makes these constants rather big, bigger than nil in particular.
%% This is good for efficiency.
%% Chose one:
%tff(l0, axiom, zl = nil).
%tff(l1, axiom, zl = cons(e1, nil)).
%tff(l2, axiom, zl = cons(e1, cons(e2, nil))).
%tff(l3, axiom, zl = cons(e1, cons(e2, cons(e3, nil)))).



%%%%%%%%%% Function symbols %%%%%%%%%%%%%%%%%%%%

%% tff(foo, type, foo: list > $int).
%% Unsatisfiable:
%% tff(a, axiom, 
%%    ! [L: list, K: $int] : 
%%      ( foo(L) = K 
%%    <=> ( ( L = nil & K = 1)))).

%% Notice the "corresponding" relational definition *is* satisfiable:
%% tff(bar, type, bar: (list * $int) > $o).
%% tff(a, axiom, 
%%     ! [L: list, K: $int] : 
%%       ( bar(L, K) 
%%     <=> ( ( L = nil & K = 1)))).

%% But it seems "iff" is hardly every needed.
%% But with "if"-direction alone can get inconsistencies:
%% Unsatisfiable, as by the second case foo(cons(1, nil)) = 2 and by the third case foo(cons(1, nil)) = 3
%% tff(a, axiom, 
%%    ! [L: list, K: $int] : 
%%      ( foo(L) = K 
%%     <= ( ( L = nil 
%%          & K = 0 )
%%        | ( ? [H: $int, T: list] :
%%              ( L = cons(H, T)
%% 	     & K = 1 ))
%%        | ( ? [H: $int, T: list] :
%%              ( L = cons(H, T)
%% 	     & K = 3 
%%              & foo(T) = 0))))).

%% Still, get inconsistencies rather easily:
%% tff(a, axiom, 
%%    ! [L: list, K: $int] : 
%%      ( foo(L) = K 
%%     <= L = nil )).



%%%%%%%%%%%%%%%%%%%%%%%
%% Length
%%%%%%%%%%%%%%%%%%%%%%%
%% tff(t, type, length: (list > $int)).
%% tff(l, axiom, length(nil) = 0).
%% tff(l, axiom, ! [H: $int, T: list] : length(cons(H, T)) = $sum(1, length(T))).


%% Provable
%% tff(c, conjecture, length(cons(1, cons(2, nil))) = 2).
%% tff(c, conjecture, length(cons(1, cons(2, nil))) != 3).

%% Unprovable:
%% tff(c, conjecture, ? [L: list] : length(L) = length(cons(1, L))).
%% The negation is provable:
%% tff(c, conjecture, ~ (? [L: list] : length(L) = length(cons(1, L)))).

%% Unprovable:
%% tff(c, conjecture, 
%%    ! [L1: list, L2: list] :
%%      ( length(L1) = length(L2) 
%%   => L1 = L2 )).
%% Negation, provable:
%% tff(c, conjecture, 
%%    ~ ( ! [L1: list, L2: list] :
%%        ( length(L1) = length(L2) 
%%     => L1 = L2 ))).

%% Combination of above theories 
%% tff(a, axiom, ~ ( ? [L: list, N: $int] : 
%% 	            ( $greatereq(N, 3) 
%% 		    & $greatereq(length(L), 4)
%% 	            & inRange(L, N)))).

%%%%%%%%%%%%%%%%%%%%%%%
%% Count
%%%%%%%%%%%%%%%%%%%%%%%
%% tff(t, type, count: (($int * list) > $int)).
%% tff(a, axiom, ! [K: $int] : count(K, nil) = 0).
%% tff(a, axiom, ! [K: $int, H: $int, T: list, N: $int] : 
%%    ( count(K, cons(H, T)) = count(K, T) <= K != H )).
%% tff(a, axiom, ! [K: $int, H: $int, T: list, N: $int] : 
%%    ( count(K, cons(H, T)) = $sum(count(K, T), 1) <= K = H )).

%% %% Provable. 
%% tff(c, conjecture, count(1, cons(1, cons(2, cons(3, cons(1, nil))))) = 2).

%% Valid, but too difficult, requires monotonicity of count:
%% tff(c, conjecture, 
%%    ! [X: $int, L: list] : 
%%      ( in(X, L) => $greatereq(count(X,L), 1) )).
%% Same here:
%% tff(c, conjecture, 
%%    ! [X: $int, L: list] : 
%%      ( $greatereq(count(X,L), 1) => in(X, L) )).

%% Unprovable:
%% tff(c, conjecture, 
%%      ( ! [X: $int, L: list] : 
%%          ( in(X, L) => count(X,L) = 1 ))).
%% The negation is provable:
%% tff(c, conjecture, 
%%    ~ ( ! [X: $int, L: list] : 
%%          ( in(X, L) => count(X,L) = 1 ))).


%% STOP here

%% Append:
%%tff(t, type, zappend: ((list * list) > list)).
%%tff(l, axiom, ! [L: list] : zappend(nil, L) = L).
%%tff(l, axiom, ! [I: $int, K: list, L: list] : zappend(cons(I, K), L) = cons(I, zappend(K, L))).

%%tff(c, conjecture, ? [L: list] : ! [K: list] : length(zappend(K, L)) = length(K)).
%%tff(c, conjecture, ? [L: list] : length(zappend(cons(1, nil), L)) = length(cons(1, nil))).

%%%%%%%%%%%%%%%%%%%%%%%
%% List maximum
%%%%%%%%%%%%%%%%%%%%%%%

%% tff(max_type, type, max: list > $int).
%% tff(max1, axiom, max(nil)=0 ).
%% tff(max2, axiom, 
%%    ! [ L: list, X: $int ] : 
%%      ( ( $lesseq(max(L),X) 
%%       => max(cons(X,L)) = X ) 
%%      & ( $greatereq(max(L),X) 
%%       => max(cons(X,L))=max(L)))).




%%tff(swap, type, ( swap: list > list )).

%% swap axioms
%%tff(swap1, axiom, ( ! [ L: list, N1: $int, N2: $int ] : swap(cons(N1, cons(N2, L))) = cons(N2, cons(N1, L)))).
%% %% Need some more, to get sufficient completeness, e.g. head(swap(nil)) would not be defined
%%tff(swap2, axiom, ( ! [ N1: $int ] : swap(cons(N1, nil)) = cons(N1, nil))).
%%tff(swap3, axiom, ( swap(nil) = nil)).


%% tff(c, conjecture, (! [K1: $int, K2: $int] : swap(cons(K1, cons(K2, nil))) = cons(K1, cons(K2, nil)))).
%%tff(c, conjecture, (? [K1: $int, K2: $int] : swap(cons(K1, cons(K2, nil))) != cons(K1, cons(K2, nil)))).





