
// Just a test for "tail":
// fitzroy relations.spec "tail(1 :: 5 :: 2 :: [| |]) = 5 :: 2 :: [| |]"

// Below are the examples from the "proving infinite satisfiability" paper

// fitzroy relations.spec "¬ inRange(4, 1 :: 5 :: 2 :: [| |])"

// fitzroy relations.spec "¬ (∀ n:Integer . (n >= 4 ⇒ inRange(n, 1 :: 5 :: 2 :: [| |])))"

// Z3 does not prove this (in short time)
// fitzroy relations.spec "¬ (∀ l:List[Integer] . ∀ n:Integer . inRange(n, tail(l)) ==> inRange(n, l))"

// Z3 does not prove this (in short time)
// fitzroy relations.spec "¬ (∃ l:List[Integer] . ∃ n:Integer . ((¬ (l=[| |]) ∧ inRange(n, l)) ==> n - head(l) < 1))"

// Z3 does not prove this (in short time)
// fitzroy relations.spec "¬ (∀ l:List[Integer] . ∀ n:Integer . inRange(n, l) ==> inRange(n-1, l))"

// Z3 does not prove this (in short time)
// fitzroy relations.spec "¬ (∀ l:List[Integer] . ∀ n:Integer . ((¬ (l=[| |]) ∧ inRange(n, l)) ==> n - head(l) >= 2))"

// Z3 does not prove this (in short time)
// fitzroy relations.spec "¬ (∀ l0:List[Integer] . ∀ l1:List[Integer] . ∀ n:Integer . n>0 ∧ inRange(n, l0) ∧ l1 = (n-2) :: l0 ==> inRange(n, l1))"


TYPES

DB = { 
}

SIGNATURE

 inRange: [Integer, List[Integer]] -> Bool

DEFINITIONS

CONSTRAINTS
inRange:
  ∀ n:Integer . ∀ l: List[Integer] . 
    ( inRange(n, l)   
    ⇔ ( l = [| |] 
      ∨ ( ∃ k:Integer . ∃ t:List[Integer] . 
          ( l = k :: t 
          ∧ 0 <= k 
          ∧ k < n 
          ∧ inRange(n, t)))))


DIGRAPH

"init" [ init = "true" ]


