(set-option :pull-nested-quantifiers true)
(set-option :mbqi true)
(set-option :macro-finder true)

;Just the definitions and conjectures necessary for tests in LPAR paper
;Z3 version

; Extensional array theory is built in-
; keywords select: A I, store A I E

;init is built in too- use
; (assert (= all1 ((as const (Array Int Int)) 1)))
;to declare all1 as a constant array.

;abbreviation of array sort
(define-sort IArray () (Array Int Int))

;Max definition (note max is a built-in)
;; (declare-fun maxA (IArray Int) Int)
;; (assert (forall ((a IArray)(n Int)(w Int))
;; 		(=> (and (forall ((i Int))
;; 				      (=> (and (> n i) (>= i 0)) 
;; 					       (<= (select a i) w) ))
;; 			      (exists ((i Int))
;; 				      (and (> n i)
;; 					   (>= i 0) 
;; 					   (= (select a i) w))))
;; 			 (= (maxA a n) w))
;; ))

;test
;; (declare-const a IArray)
;; (declare-const n Int)
;; (assert (< (maxA a 10) (maxA a 9)))

;; (declare-const a0 IArray)
;; (assert (= a0 ((as const IArray) 0)))
;; (assert (!= (maxA a0 10) 0))

;; % %%Proof of functionality of prefix max function
;; % % BEAGLE - Theorem
;; % % SPASS+T - Theorem
;; % % tff(c, conjecture, 
;; % %    ! [A: array, N: $int, W1: $int, W2: $int] : 
;; % %      ( ( ( $greatereq(N,0) & 
;; % %        	     ( ! [I: $int] : 
;; % % 	       	 (( $greater(N,I) &
;; % % 		    $greatereq(I,0) )
;; % %                  => $lesseq(read(A, I), W1) )) &
;; % %              ( ? [I: $int] : 
;; % % 	       	 ( $greater(N,I) &
;; % % 		   $greatereq(I,0) &
;; % %                    read(A, I) = W1 ))
;; % % 	     )
;; % %        & ( $greatereq(N,0) & 
;; % %        	     ( ! [I: $int] : 
;; % % 	       	 (( $greater(N,I) &
;; % % 		    $greatereq(I,0) )
;; % %                  => $lesseq(read(A, I), W2) )) &
;; % %              ( ? [I: $int] : 
;; % % 	       	 ( $greater(N,I) &
;; % % 		   $greatereq(I,0) &
;; % %                    read(A, I) = W2 ))
;; % % 	     ) )
;; % %     => W1 = W2 )).

;Sorted definition
;; (declare-fun sorted (IArray Int) Bool )
;; (assert (forall ((a IArray)(n Int))
;; 		(iff (forall ((i Int)(j Int))
;; 			     (=> (and (> n j) 
;; 					   (> j i)
;; 					   (> i 0)) 
;; 				      (>= (select a j) (select a i))))
;; 		     (sorted a n))
;; ))

;alternative using define-fun
(define-fun sorted ((a IArray)(n Int)) Bool
  (forall ((i Int)(j Int))
	  (=> (and (> n j) 
			(> j i)
			(> i 0)) 
		   (>= (select a j) (select a i))))
)

;inRange definition
;; (declare-fun inRange (IArray Int Int) Bool)
;; (assert (forall ((a IArray)(r Int)(n Int))
;; 		(iff (forall ((i Int))
;; 			     (=> (and (> n i) 
;; 					   (>= i 0))
;; 				      (and (>= r (select a i))
;; 					   (>= (select a i) 0))))
;; 		     (inRange a r n))
;; ))

;distinct definition
;; (declare-fun distinctA (IArray Int) Bool)
;; (assert (forall ((a IArray)(n Int))
;; 		(iff (forall ((i Int)(j Int))
;; 			     (=> (=> (and (> n i)
;; 						    (> n j) 
;; 					            (> j 0) 
;; 						    (> i 0))
;; 					       (= (select a i) (select a j)))
;; 				      (= i j)))
;; 		     (distinctA a n))
;; ))

(define-fun distinctA ((a IArray)(n Int)) Bool
  (forall ((i Int)(j Int))
	  (=> (=> (and (> n i)
				 (> n j) 
				 (> j 0) 
				 (> i 0))
			    (= (select a i) (select a j)))
		   (= i j)))	  
)

;check (DOESNT TERMINATE T/O 2m)
;; (declare-const ident IArray)
;; (assert (forall ((i Int)) (= (select ident i) i) ))
;; (assert (distinct ident 10))

;reverse definition
;; (declare-fun rev (IArray Int) IArray)

;; (assert (forall ((a IArray)(b IArray)(n Int))
;; 		(=> (forall ((i Int))
;; 				 (or (and (> n i)
;; 					  (> i 0)
;; 					  (= (select b i) (select a (- n (+ i 1)))))
;; 				     (and (or (> 0 i) 
;; 					      (>= i n)) 
;; 					  (= (select b i) (select a i)))))
;; 			 (= (rev a n) b))
;; ))

;; (define-fun rev ((a IArray)(b IArray)(n Int)) IArray
;;   (forall ((i Int))
;; 	  (ite (and (> n i)
;; 		    (> i 0))
;; 	       (= (select b i) (select a (- n (+ i 1))))
;; 	       a)
;; ))

;; (assert (forall ((i Int)) (= (rev ((as const IArray) 0) i) ((as const IArray) 0))))

;; %Functionality proof
;; % Beagle - ?
;; % SPASS+T - Theorem (doesn't finish when sorted definition is included, all others fine)
;; % tff(c, conjecture, ![A: array, B1: array, B2: array, N: $int]: (
;; %        		 (( ![I: $int]: ((
;; % 		    ( $greatereq(I,0) & 
;; % 		      $greater(N,I)) & 
;; % 		      read(B1,I)=read(A,$difference(N,$sum(I,1)))) |
;; % 		    ( ($greater(0,I) | 
;; % 		       $greatereq(I,N) )
;; % 		       & read(B1,I)=read(A,I) ))
;; % 	    	    ) &
;; % 		    ( ![I: $int]: ((
;; % 			    ( $greatereq(I,0) & 
;; % 			      $greater(N,I)) & 
;; % 			      read(B2,I)=read(A,$difference(N,$sum(I,1)))) |
;; % 			    ( ($greater(0,I) | 
;; % 			       $greatereq(I,N) )
;; % 			       & read(B2,I)=read(A,I) ))
;; % 	    	   ) )
;; % 		   => B1 = B2 )
;; % ).

;%Small examples using sorted only------------------------------------------
;%tff(c, conjecture, ~sorted(write(write(write(init(0),1,1),2,4),3,4),5)).

; This works!
;; (declare-const a0 IArray)
;; (assert (= a0 ((as const IArray) 0)))
;; (assert (not (sorted (store (store (store a0 1 1) 2 4) 3 4) 3)))

;%tff(c, conjecture, ~ (?[M: $int, N: $int]: ~sorted(init(M),N) )).
;; (declare-const m Int)
;; (declare-const n Int)
;; (assert (sorted ((as const IArray) 0) n))
; Works!
;; (assert (not (sorted a0 n)))
;---------------------------------------------------------------------------

;; %%EXAMPLES:

;; PROBLEM 1  
;; (~![A: array, N: $int]: ( $greatereq(N,0) => inRange(A,max(A,N),N) ))
;; %	SPASS+T		Beagle:
;; % real	0m0.156s	0m1.396s

; Z3: UNKNOWN
 
;; (assert (forall ((a IArray)(n Int))
;; 		     (=> (>= n 0) 
;; 			      (inRange a (maxA a n) n))))

;; PROBLEM 2   
;; ~( ![N: $int, I: $int]: distinct(init(N),I) )
;; % 	SPASS+T		Beagle:
;; % real	0m0.146s	0m0.975s

; Z3: SAT )

;; (assert (not (forall ((n Int)(i Int))
;; 		     (distinctA ((as const IArray) n) i) )))


;; PROBLEM 3  
;; ~(![A: array, N: $int]: read(rev(A,$sum(N,1)),0)=read(A,N))
;; % SPASS+T		Beagle
;; % real	0m0.272s	T/O using -genvars only

; Z3 UNKNOWN

;; (assert (forall ((a IArray)(n Int))
;; 	     (= (select a (+ n 1)) (select a n))))

; IGNORE -----------------------------------------------------------------------------
;; ;; %4- not working
;; ;; %tff(c4, conjecture, ~ ![A: array, N: $int]: ( (distinct(A,N) & $greater(N,0) )=> distinct(rev(A,N),N)) ).
;; ;; %tff(c, conjecture, ~ ?[A: array]: (![N: $int]: (sorted(A,N) & distinct(A,N)))).
;-------------------------------------------------------------------------------------

;; PROBLEM 4
;; ~![A: array, N: $int]: (sorted(A,N) => ~sorted(rev(A,N),N))
;; % SPASS+T		Beagle
;; % real	0m0.113s	T/O with -genvars and -indexing

;Z3 UNKNOWN

;; (assert (forall ((a IArray)(n Int))
;; 		     (=> (sorted a n)
;; 			      (not (sorted (rev a n) n))))
;; )

;; PROBLEM 5
;; ~![M: $int]: (?[N: $int]: ~sorted(rev(init(N),M),M))
;; %	SPASS+T		Beagle:
;; % real	0m0.156s	T/O using -genvars and -indexing

;; (assert (forall ((m Int))
;; 		     (exists ((n Int))
;; 			     (not (sorted (rev ((as const IArray) n) m) m)))
;; 		     ))

;; PROBLEM 6
;; ~![A: array, N: $int]: ( (sorted(A,N) & $greater(N,0)) => distinct(A,N))
;; % SPASS+T		Beagle:
;; % real	0m0.166s	0m2.372s

;Z3 UNKNOWN

;; (assert (forall ((a IArray)(n Int))
;; 		     (=> (and (sorted a n) (> n 0))
;; 			      (distinctA a n))))

(check-sat)
