% Declare some precedences:
%$ max > swap > inRange > tail > append > in > length > head > cons > nil

tff(list_type, type, ( list: $tType )).
tff(nil_type,  type, ( nil: list )).
tff(cons_type, type, ( cons: ($int * list) > list )).
tff(head_type, type, ( head: list > $int )).
tff(tail_type, type, ( tail: list > list )).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% List axioms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Selectors:
tff(l1, axiom, ( ! [ K: $int, L: list ] : head(cons(K, L)) = K )).
tff(l2, axiom, ( ! [ K: $int, L: list ] : tail(cons(K, L)) = L )).
%% %% %% Constructors:
tff(l3, axiom, ( ! [ L: list ] : 
   ( L = nil
   | L = cons(head(L), tail(L)) ))).
tff(l4, axiom, ( ! [ K: $int, L: list ] : cons(K, L) != nil)).
%% %% Constructors are injective
%% Not needed - this is a consequence
%% tff(l5, axiom, ( ! [ K1: $int, L1: list, K2: $int, L2: list ] : 
%%                    ( cons(K1, L1) = cons(K2, L2)
%%                   => ( K1 = K2
%%                      & L1 = L2 )))).

%% %% Sufficient completeness:
%% tff(l1, axiom, ? [E: $int] : head(nil) = E).
%% tff(l1, axiom, tail(nil) = nil).


%%%%%%%%%%%%%%%%%%%%%%%
%% List membership
%%%%%%%%%%%%%%%%%%%%%%%


%% tff(in, type, in: ($int * list) > $o).
%% tff(in_conv, axiom, 
%%    ! [X: $int, L: list] : 
%%      ( in(X, L) 
%%    <=> ( ( ? [H: $int, T: list] : 
%%              ( L = cons(H, T)
%%              & X = H ) )
%%        | ( ? [H: $int, T: list] : 
%% 	     ( L = cons(H, T)
%%              & in(X, T)))))).

%% Provable:
%% tff(c, conjecture, in(2, cons(1, cons(2, cons(3, nil))))).
%% tff(c, conjecture, ~in(4, cons(1, cons(2, cons(3, nil))))).
%% tff(c, conjecture, ! [A: $int] : ~in(A, nil)).

%% Non-provable, prove negation instead
%% tff(c, conjecture, in(4, cons(1, cons(2, cons(3, nil))))).
%% tff(c, conjecture, in(4, nil)).


%%%%%%%%%%%%%%%%%%%%%%%
%% inRange
%%%%%%%%%%%%%%%%%%%%%%%
tff(inRange, type, inRange: ($int * list) > $o).
tff(inRange, axiom, (
   ! [ N: $int, L: list ] :
     ( inRange(N, L) 
   <=> ( L = nil 
       | ? [ K: $int, T: list ] : 
	 ( L = cons(K, T)
         & $lesseq(0, K) 
	 & $less(K, N) 
	 & inRange(N, T) ))))).

%%%%%%%%%%%%%%%%%%%%%%%
%% Length
%%%%%%%%%%%%%%%%%%%%%%%
tff(t, type, length: (list > $int)).
tff(l, axiom, length(nil) = 0).
tff(l, axiom, ! [H: $int, T: list] : length(cons(H, T)) = $sum(1, length(T))).

%% Provable
%% tff(c, conjecture, length(cons(1, cons(2, nil))) = 2).
%% tff(c, conjecture, length(cons(1, cons(2, nil))) != 3).

%% Unprovable (negation sign already added):
%% tff(c, conjecture, ~ (length(cons(1, cons(2, nil))) = 3)).
%% tff(c, conjecture, ~ (? [L: list] : length(L) = length(cons(1, L)))).

%% Problem 1: beagle -indexing -genvars -bgsimp cautious: 4.3 sec ; spass+t: 9.0 sec
%% Problem 1: beagle -indexing -bgsimp cautious: 6.2 sec ; 
%% tff(c, conjecture, 
%%    ~ ( ! [L1: list, L2: list] :
%%        ( length(L1) = length(L2) 
%%     => L1 = L2 ))).

%% Combination with above theories 
%% Problem 2: beagle -indexing -genvars -bgsimp cautious: 5.4 sec ; spass+t: 1.11
%% Problem 2: beagle -indexing -bgsimp cautious: ??? sec
%% tff(a, conjecture, 
%%    ~ ( ! [L: list, N: $int] : 
%%          ( ( $greatereq(N, 3) 
%% 	   & $greatereq(length(L), 4) )
%%         => inRange(N, L)))).


%%%%%%%%%%%%%%%%%%%%%%%
%% Count
%%%%%%%%%%%%%%%%%%%%%%%
tff(t, type, count: (($int * list) > $int)).
tff(a, axiom, ! [K: $int] : count(K, nil) = 0).
tff(a, axiom, ! [K: $int, H: $int, T: list, N: $int] : 
   ( count(K, cons(H, T)) = count(K, T) <= K != H )).
tff(a, axiom, ! [K: $int, H: $int, T: list, N: $int] : 
   ( count(K, cons(H, T)) = $sum(count(K, T), 1) <= K = H )).

%% Unprovable:
%% Problem 3: beagle -indexing -genvars -bgsimp cautious: 2.5 sec ; spass+t: 0.3 sec
%% Problem 3: beagle -indexing -bgsimp cautious: 2.6 sec
%% tff(c, conjecture,
%%    ~ ( ! [N: $int, L: list] :
%%        ( count(N, L) = count(N, cons(1, L))))).

%% tff(c, conjecture,
%%    ~ ( ! [N: $int, L: list] :
%%        ( L != nil 
%%       => count(N, L) = count(N, tail(L))))).

%% Problem 4: beagle -indexing -genvars -bgsimp cautious: 2.7 sec ; spass+t: 0.3 sec
%% Problem 4: beagle -indexing -bgsimp cautious: 2.4 sec
%% tff(c, conjecture, 
%%        ~ ( ! [N: $int, L: list ] :
%%              $greatereq(count(N, L), length(L) ))).

%% Too hard:
%% tff(c, conjecture, 
%%        ~ ( ? [N: $int, L: list ] :
%%              $greater(count(N, L), length(L) ))).

%% Problem 5: beagle -indexing -genvars -bgsimp cautious: 2.4 sec ; spass+t: 0.8
%% Problem 5: beagle -indexing -bgsimp cautious: 2.2 sec ; spass+t: 
%% tff(c, conjecture, 
%%         ~ ( ! [L1: list, L2: list ] :
%%  	     ( L1 != L2 
%%  	    => ( ! [N: $int] : count(N, L1) != count(N, L2))))).


%% Too hard:
%% tff(c, conjecture, 
%%        ~ ( ? [N: $int, L: list ] :
%%              $greater(count(N, L), length(L) ))).


%% Append:
tff(t, type, append: ((list * list) > list)).
tff(l, axiom, ! [L: list] : append(nil, L) = L).
tff(l, axiom, ! [I: $int, K: list, L: list] : append(cons(I, K), L) = cons(I, append(K, L))).


%% Unprovable:
%% Problem 6: beagle -indexing -genvars -bgsimp cautious: 2.1 sec ; spass+t: 0.3 sec
%% Problem 6: beagle -indexing -bgsimp cautious: 2.1 sec
%% tff(c, conjecture, 
%%    ~ ( ! [K: list, L: list] : length(append(K, L)) = length(K))).

%% tff(c, conjecture, 
%%    ~ ( ! [K: list, L: list] : length(append(K, L)) = $sum($sum(length(K), length(L)), 1))).

%% Problem 7: beagle -indexing -genvars -bgsimp cautious: 37 sec ; spass+t: >60 sec
%% Problem 7: beagle -indexing -bgsimp cautious: 6.9 sec
%% tff(c, conjecture, 
%%    ~ ( ! [K: list, L: list] : 
%%          ( ( $greater(length(K), 1)
%%            & $greater(length(L), 1) )
%%         => $greater(length(append(K, L)), 4)))).

tff(in, type, in: ($int * list) > $o).
tff(a, axiom, ! [N: $int, L: list] : (in(N, L) <=> ($greater(count(N, L), 0)))).

%% tff(c, conjecture, 
%%     ~ ( ! [M: $int, N: $int, L: list, L1: list ] :
%%           ( ( in(N, L) 
%%             & L1 = cons(M, L) )
%%          => count(N, L1) = count(N, L)))).


%% Problem 8: beagle -indexing -genvars -bgsimp cautious: >60 sec ; spass+t: 9.1 sec
%% Problem 8: beagle -indexing -bgsimp cautious: 6.2 sec ; spass+t: 
%% tff(c, conjecture, 
%%     ~ ( ! [M: $int, N: $int, K: list, L: list, L1: list ] :
%%           ( ( in(N, L) 
%%             & ~ in(M, K) 
%%             & L1 = append(L, cons(M, K)) )
%%          => count(N, L1) = count(N, L)))).



%% STOP here

%%%%%%%%%%%%%%%%%%%%%%%
%% List maximum
%%%%%%%%%%%%%%%%%%%%%%%

%% tff(max_type, type, max: list > $int).
%% tff(max1, axiom, max(nil)=0 ).
%% tff(max2, axiom, 
%%    ! [ L: list, X: $int ] : 
%%      ( ( $lesseq(max(L),X) 
%%       => max(cons(X,L)) = X ) 
%%      & ( $greatereq(max(L),X) 
%%       => max(cons(X,L))=max(L)))).




%%tff(swap, type, ( swap: list > list )).

%% swap axioms
%%tff(swap1, axiom, ( ! [ L: list, N1: $int, N2: $int ] : swap(cons(N1, cons(N2, L))) = cons(N2, cons(N1, L)))).
%% %% Need some more, to get sufficient completeness, e.g. head(swap(nil)) would not be defined
%%tff(swap2, axiom, ( ! [ N1: $int ] : swap(cons(N1, nil)) = cons(N1, nil))).
%%tff(swap3, axiom, ( swap(nil) = nil)).


%% tff(c, conjecture, (! [K1: $int, K2: $int] : swap(cons(K1, cons(K2, nil))) = cons(K1, cons(K2, nil)))).
%%tff(c, conjecture, (? [K1: $int, K2: $int] : swap(cons(K1, cons(K2, nil))) != cons(K1, cons(K2, nil)))).





