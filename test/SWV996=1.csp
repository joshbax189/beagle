%------------------------------------------------------------------------------
% File     : SWV996=1 : TPTP v5.4.0. Released v5.0.0.
% Domain   : Software Verification
% Problem  : Backward simplification: node deletion 2
% Version  : Especial.
% English  : A problem extracted from model checking a safety problem (no
%            violation of mutual exclusion) for a parameterized system (a
%            variant of the protocol due to Szymanski).

% Refs     : [MP90]  Manna & Pnueli (1990), Tools and Rules for the Practic
%          : [Ran10] Ranise (2010), Email to Geoff Sutcliffe
% Source   : [Ran10]
% Names    : sz2_fixpoint_2 [Ran10]

% Status   : CounterSatisfiable
% Rating   : 1.00 v5.4.0, 0.67 v5.2.0, 1.00 v5.0.0
% Syntax   : Number of formulae    :    6 (   3 unit;   5 type)
%            Number of atoms       :   22 (   8 equality)
%            Maximal formula depth :   13 (   4 average)
%            Number of connectives :   20 (   6   ~;   0   |;  13   &)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :   10 (   7 propositional; 0-2 arity)
%            Number of functors    :   11 (   9 constant; 0-1 arity)
%            Number of variables   :    4 (   0 sgn;   4   !;   0   ?)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :    9 (   3 pred;    0 func;    6 numbers)
% SPC      : TFF_CSA_EQU_ARI

% Comments : Considered a relatively simple benchmark for infinite state model
%            checkers.
%          : In the SMT AUFLIA logic. Generated using the MCMT system -
%            http://homes.dsi.unimi.it/~ghilardi/mcmt/
%------------------------------------------------------------------------------
tff(z1_type,type,(
    z1: $fd(1,10) )).

tff(z2_type,type,(
    z2: $fd(1,10) )).

tff(z3_type,type,(
    z3: $fd(1,10) )).

%tff(a_type,type,(
%    a: $int > $int )).
%
%tff(b_type,type,(
%    b: $int > $int )).

tff(a_type,type, ( a: $int > $fd(1,12) )).
tff(b_type,type,( b: $int > $fd(1,5) )).

%tff(0,conjecture,
%    ( ( ! [Z1: $int] :
%          ( $lesseq(1,a(Z1))
%          & $lesseq(a(Z1),12) )
%      & ! [Z1: $int] :
%          ( $lesseq(1,b(Z1))
%          & $lesseq(b(Z1),5) )
%      & $true
%      & z1 != z2
%      & z1 != z3
%      & z2 != z3
%      & ! [Z1: $int,Z2: $int] :
%          ~ ( Z1 != Z2
%            & a(Z1) = 10
%            & a(Z2) = 10 ) )
%   => ~ ( a(z1) = 9
%        & a(z2) = 10
%        & $less(b(z2),3)
%        & $less(z2,z1) ) )).

tff(1,conjecture,
    ( ( $$alldiff(z1,z2,z3)
      & ! [Z1: $fd(0,10),Z2: $fd(0,10)] :
          ~ ( Z1 != Z2
            & a(Z1) = 10
            & a(Z2) = 10 ) )
   => ~ ( a(z1) = 9
        & a(z2) = 10
        & $less(b(z2),3)
        & $less(z2,z1) ) )).

%------------------------------------------------------------------------------
