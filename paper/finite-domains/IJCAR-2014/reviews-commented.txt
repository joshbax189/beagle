| ----------------------- REVIEW 1 ---------------------
| PAPER: 41
| TITLE: Finite Quantification in Hierarchic Theorem Proving
| AUTHORS: Peter Baumgartner, Joshua Bax and Uwe Waldmann
| 
| OVERALL EVALUATION: 2 (accept)
| REVIEWER'S CONFIDENCE: 3 (medium)
| 
| ----------- REVIEW -----------
| The paper continues the first and third authors' work on hierarchic
| superposition.  In hierarchic superposition, a (decidable) background
| theory is extended with free function symbols that range into a sort
| of the background theory.   In the present paper, the authors consider
| formulas where variables occurring below such free function symbols are
| quantified over a finite subset of their domains.
| 
| The method developed in the paper is refutationally sound w.r.t. the
| standard semantics, i.e., if the formula is determined to be
| unsatisfiable w.r.t. the finite domain, then it is also unsatisfiable
| w.r.t. the infinite domain.
| 
| This work has practical applications in, e.g., software verification
| or bug-finding.
| 
| The paper is generally well-written, although the notation is
| sometimes overwhelming.  The introduction provides a nice, informal
| overview of the developed method.
| 
| I think that the presentation of the experimental results in section 5
| could be improved by also putting the runtimes of Z3 into Table 1.
| This would make it easier to compare the performance of the tools at a
| glance and would also highlight the superiority of Beagle on the
| selected benchmarks.  Also, is it possible to give an experimental
| comparison with other tools such as CVC4 or Mace4?  

Not done.

| Furthermore, could
| you please state which version of Z3 was used?
| 

Done (version 4.1)

| Regarding footnote 5, could you provide an example showing why this
| would be unsound?
| 

Added example, although this makes the footnote quite long.

| Minor comments:
| ---------------
| 
| p2, l9:
| "," after "apply"
|
| p2, l26:
| "," after "refinement"
| 
| p2, l34:
| "," after "following"
| 
| p3, l1:
| "," after "superposition"
| 
| p3, l27:
| "," after "example"
| 
| p5, l30:
| "a FG term" --> "an FG term"
| remove "," after "FG term"
| 
| p5, l32:
| "a FG operator" --> "an FG operator"
| 
| p7, l17:
| "We have formally" --> "We formally have"
| 
| p7, l19:
| "," after "(x_1, ..., x_n)"
| 
| p8, l29:
| "that" between "assume" and "the"
| 
| p9, l1:
| "-" after "top-level" should be "--"
| 
| p14, l20:
| "enumerating" --> "to enumerate"
| 
| p14, l27:
| "," around "under certain conditions"
| 

All done

| 
| ----------------------- REVIEW 2 ---------------------
| PAPER: 41
| TITLE: Finite Quantification in Hierarchic Theorem Proving
| AUTHORS: Peter Baumgartner, Joshua Bax and Uwe Waldmann
| 
| OVERALL EVALUATION: 1 (weak accept)
| REVIEWER'S CONFIDENCE: 3 (medium)
| 
| ----------- REVIEW -----------
| This paper is about checking satisfiability of formulas 
| in which all quantifiers range over finite domains. 
| The authors aim to develop a method more efficient than the 
| naive method which, as the authors stated, would be to use exhaustive 
| instantiation and invoke a suitable SMT solver afterwards. 
| The method in the paper, instead, is built upon
| the version of hierarchic superposition presented in [8]. 
| 
| 
| The main procedure checkSAT accepts finitely quantified clauses that 
| are defined as clauses in which all variables occurring 
| under free BG-sorted operators are quantified over finite
| domains. The main idea is to map every 
| free BG-sorted operator to a constant function through 
| a default interpretation and make refinements by 
| finding exceptions in a conflict-driven way. Necessary machinery 
| for this is “finite domain transformation” introduced in Section 3. 
| The procedure checkSAT combines B-satisfiability checks 
| and finite domain transformations as described in the pseudocode in 
| Section 4. The efficiency of the method follows from the 
| fact that B-satisfiability tests and finite domain 
| transformations (in the sub-procedure called find) 
| can be done efficiently for foreground and background theories 
| considered in this paper. 
| 
| 
| Regarding the presentation, the paper is somewhat difficult 
| to read. This is mainly due to compact background information 
| on hierarchic theorem proving in Section 2. For further details, 
| it seems that the reader is implicitly 
| referred to previous work on hierarchic superposition, in 
| particular to authors’ previous work [8]. 
| It would be better to expand Section 2 by borrowing 
| some more definitions from [8], such as formal definition of 
| B-satisfiability. Similarly in Section 3, definition 3.3
| for finite domain transformation is hard to follow. 
| 
| 
| In conclusion, the paper targets an important research problem, 
| describes a new method based on hierarchic theorem proving and 
| shows that the method works for the class of theories considered, 
| along with experimental results. The results are correct, 
| although some clarifications are necessary.
| The subject is within the scope of IJCAR and results are strong 
| enough to deserve publication. However, since there must be 
| some improvements in the presentation, the paper must receive a weak accept. 
| 
| 
| Additional comments: 
| 
| page 1: both Abstract and Introduction start with the same sentences as in [8]:
| it would be better to rephrase them
| 

Why? Not acted upon.

| - It would be better to have one last paragraph that outlines
| the rest of the paper. 
| 

Such paragraphs are typically a waste of time and space. I have instead put
pointers to the main sections into the discussion of the example in Section
1. 

| page 2: 
| 
| - In line 4, for the word “counter-satisfiability”, I suggest to include its definition.  
| 

Replaced by "non-validity".

| 
| page 5 : 
| 
| - In paragraph 3, the term “true_p” is not defined. Isn’t it enough to use
| just “true”? 

I've removed the whole paragraph by putting it into a commented. It did not
seem essential to me for this paper. (Uwe?)


| - In the footnote 4, x \neq t should be x = t. 
| 

Of course not.

| 
| page 6: 
| 
| - Definition of B-satisfiability should be given more formally, as in [8], 
| because all results use it. In addition, it must be checked if 
| it is necessary to update the proofs of the lemmas after giving formal definition.
|  

For now only moved the informal definition from page 6 forward to the earliest
possible spot in Section 2.

| 
| page 8: 
| 
| - Paragraph before Prop 3.5: to increase readability, 
| generalization of finite domain transformation 
| to clause sets and other related definitions in this paragraph 
| could be given in definition form in a similar way to definition 3.3. 
| 

Could do, but I cannot see the gain. It would take some space, hence not done.

| 
| page 11: 
| 
| - Lemma 4.11: the statement should be refined. The first sentence may be omitted and 
| the lemma can start with an assumption that the method find is called from checkSAT 
| on line 11.  
| 

Done.

| - Lemma 4.11: it must be clarified in the proof what role the binary search 
| mentioned in the pseudocode plays. It seems that binary search is related to 
| the way “i” is chosen, at least for theories with properties mentioned at 
| the beginning of the page. However, the proof does not say anything about 
| this issue at all.
| 

Good point.

I have rewritten the procedure and the surrounding comments, this way made
things more precise and faithful with the implementation.

| 
| 
| Typos: 
| 
| page 2: 
| 
| - In line 3, ressources ==> resources.
| 
| 
| page 15: 
| 
| - Reference [3]: hierachic ==> hierarchic. 
| - Reference [9]: mace ==> MACE or Mace.
| 
| 

Done.

| ----------------------- REVIEW 3 ---------------------
| PAPER: 41
| TITLE: Finite Quantification in Hierarchic Theorem Proving
| AUTHORS: Peter Baumgartner, Joshua Bax and Uwe Waldmann
| 
| OVERALL EVALUATION: 1 (weak accept)
| REVIEWER'S CONFIDENCE: 4 (high)
| 
| ----------- REVIEW -----------
| Dealing with quantifiers in the presence of background theories is a challenging problem and 
|     restrictions are needed to obtain a "reasonably complete" decision procedures. 
|     In this paper the authors investigate this problem in the context of hierarchic superposition. 
|     The authors propose to restrict variables (below foreground symbols) to range 
|     over finite subsets of the domain which is  a natural restriction. Then, a naive approach can solve 
|     satisfiability by exhaustive instantiation (provided we have a reasoner
for EA fragment of the background theo| ry). The authors propose more
sophisticated approach by approximating functi| ons over domains 
|     by constant functions and refine this approximation by adding exceptions and instantiations.  
|     The proposed approach is interesting and seems promising at least for
problems with a small number of variable| s.  The paper is written well with a
helpful running example, but there ar| e some points that I think should be clarified. 
|     
|     Detailed comments: 
| 
|     p3 l4 explain why there is nothing to do for axioms (1) and (2). 
| 

Difficult to explain with limited space. I tried it, please check.

|     p3 l -4 replacing 1000^j by 0^j needs explanation 
| 

Indeed, this is nonsense: 1000^j will not be replace by 0^j .

|     p5 l -5 "abstract out certain BG terms" -- make "certain" more precise. 
| 

Have now: "After abstracting out BG terms other than BG domain elements and variables
that occur as subterms of FG terms, ..."

|     p6 "or at least every ..." be more precise is it a sufficient condition?
| 

Todo: Uwe, can you please say something hereq?


|     p7 The finite domain transformation results in a doubly exponential (?) number of clauses 
|        wrt. the number of variables. I think this issue should be discussed in the paper. 
|     

I think it's not that bad. Please check what I wrote in the second paragraph
after Definition 3.2.

|     p10 Algorithm find: I do not see how in the general case it is possible to use 
|         binary search to find a maximal subset \Gamma such that ... is B-satisfiable. 
|         The same question relates to the bound O(m\log(n)) on page 4.


That should be addressed now.

| 
|
