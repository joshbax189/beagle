%Just the definitions and conjectures necessary for tests in LPAR paper
%$ read > write > init

%% Theory of arrays with integer indices and integer elements
tff(array_type,type,(
    array: $tType )).

tff(read_type,type,(
    read: ( array * $int ) > $int )).

tff(write_type,type,(
    write: ( array * $int * $int ) > array )).

tff(ax1,axiom,(
     ! [A: array,I: $int, V: $int] : read(write(A,I,V),I) = V )).

tff(ax2,axiom,(
    ! [A: array,I: $int,J: $int,V: $int] :
      ( I = J
      | read(write(A,I,V),J) = read(A,J) ) )).

tff(ext, axiom, (
	 ![A: array, B: array]: 
	      ( (![I: $int]: read(A,I)=read(B,I) ) 
	      	=> A=B )
)).

tff(init_type, type, init: $int > array).

%% Initialized arrays: init(V) is the array that has the value V everywhere

tff(ax3, axiom, 
   ! [V: $int] : 
     ( ! [I: $int] : read(init(V), I) = V )).

%% end of array axioms

%%%%%%%%%%%%
tff(n, type, n: $int).
tff(n, axiom, n=1000).

%% Domain predicate
tff(d, type, indom: ($int * $int * $int) > $o).
tff(d, axiom, ! [I: $int, L: $int, H: $int] : ( ( $lesseq(L, I) & $lesseq(I, H) ) => indom(I, L, H))).

%% (0) %%%%%%%%%%%%%%%%
tff(a, type, a: array).
%% Sortedness axiom
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, n)
%%        & indom(J, 1, n)
%%        & $less(I, J) )
%%     => $lesseq(read(a, I), read(a, J))))).

%% Add a hypothesis:
tff(m, type, m: $int).
tff(m, axiom, ( $lesseq(1, m) & $less(m, 1000))).
tff(m, axiom, $less(read(a, m), read(a, $sum(m, 1)))).

%% (1) %%%%%%%%%%%%%%%%
%% Satisfiable:
%% tff(eI, type, eI: $int).
%% tff(eJ, type, eJ: $int).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, 999)
%%        & indom(J, 1, 0)
%%        & $less(I, J) )
%%     => $lesseq(eI, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [I: $int] : ( indom(I, 1, 999) => read(a, I) = eI))).
%% tff(sa, axiom, ( 
%%    ! [J: $int] : ( indom(J, 1, 0) => read(a, J) = eJ))).

%% (2) %%%%%%%%%%%%%%%%
%% FD(I=1000)
%% Satisfiable
%% tff(eI, type, eI50: $int).
%% tff(eI, type, eI: $int).
%% tff(eJ, type, eJ: $int).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, 1000) & I != 1000
%%        & indom(J, 1, 999)
%%        & $less(I, J) )
%%     => $lesseq(eI, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(J, 1, 999)
%%        & $less(1000, J) )
%%     => $lesseq(eI50, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [I: $int] : ( ( indom(I, 1, 1000) & I != 1000 ) => read(a, I) = eI))).
%% tff(sa, axiom, ( 
%%    ! [I: $int] : read(a, 1000) = eI50)).
%% tff(sa, axiom, ( 
%%    ! [J: $int] : ( indom(J, 1, 999) => read(a, J) = eJ))).




%% (3) %%%%%%%%%%%%%%%%
%% tff(eI, type, eI50: $int).
%% tff(eI, type, eI: $int).
%% tff(eJ, type, eJ: $int).
%% tff(eJ, type, hJ: $int).
%% tff(hj, axiom, hJ=49). %% Satisfiable
%% %% tff(hj, axiom, hJ=50). %% Unsatisfiable
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, 1000) & I != 50
%%        & indom(J, 1, hJ)
%%        & $less(I, J) )
%%     => $lesseq(eI, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(J, 1, hJ)
%%        & $less(50, J) )
%%     => $lesseq(eI50, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [I: $int] : ( ( indom(I, 1, 1000) & I != 50 ) => read(a, I) = eI))).
%% tff(sa, axiom, ( 
%%    ! [I: $int] : read(a, 50) = eI50)).
%% tff(sa, axiom, ( 
%%    ! [J: $int] : ( indom(J, 1, hJ) => read(a, J) = eJ))).

%% (4) %%%%%%%%%%%%%%%%
%% FD(I=1000, J=1000)
tff(eI, type, eI50: $int).
tff(eI, type, eJ50: $int).
tff(eI, type, eI: $int).
tff(eJ, type, eJ: $int).
tff(sa, axiom, ( 
   ! [I: $int, J: $int] : 
     ( ( indom(I, 1, 1000) & I != 1000
       & indom(J, 1, 1000) & J != 1000
       & $less(I, J) )
    => $lesseq(eI, eJ)))).
tff(sa, axiom, ( 
   ! [I: $int, J: $int] : 
     ( ( indom(J, 1, 1000)
       & $less(1000, J) )
    => $lesseq(eI50, eJ)))).
tff(sa, axiom, ( 
   ! [I: $int, J: $int] : 
     ( ( indom(I, 1, 1000)
       & $less(I, 1000) )
    => $lesseq(eI, eJ50)))).
tff(sa, axiom, ( 
   ! [I: $int, J: $int] : 
     ( ( $less(1000, 1000) )
    => $lesseq(eI50, eJ50)))).

tff(sa, axiom, ( 
   ! [I: $int] : ( ( indom(I, 1, 1000) & I != 1000 ) => read(a, I) = eI))).
tff(sa, axiom, ( 
   ! [J: $int] : ( ( indom(J, 1, 1000) & J != 1000 ) => read(a, J) = eJ))).
tff(sa, axiom, ( 
   ! [I: $int] : read(a, 1000) = eI50)).
tff(sa, axiom, ( 
   ! [I: $int] : read(a, 1000) = eJ50)).
