%Just the definitions and conjectures necessary for tests in LPAR paper
%$ read > write > init

%% Theory of arrays with integer indices and integer elements
tff(array_type,type,(
    array: $tType )).

tff(read_type,type,(
    read: ( array * $int ) > $int )).

tff(write_type,type,(
    write: ( array * $int * $int ) > array )).

tff(ax1,axiom,(
     ! [A: array,I: $int, V: $int] : read(write(A,I,V),I) = V )).

tff(ax2,axiom,(
    ! [A: array,I: $int,J: $int,V: $int] :
      ( I = J
      | read(write(A,I,V),J) = read(A,J) ) )).

tff(ext, axiom, (
	 ![A: array, B: array]: 
	      ( (![I: $int]: read(A,I)=read(B,I) ) 
	      	=> A=B )
)).

tff(init_type, type, init: $int > array).

%% Initialized arrays: init(V) is the array that has the value V everywhere

tff(ax3, axiom, 
   ! [V: $int] : 
     ( ! [I: $int] : read(init(V), I) = V )).

%% end of array axioms

%%%%%%%%%%%%
tff(n, type, n: $int).
tff(n, axiom, n=1000).

%% Domain predicate
tff(d, type, indom: ($int * $int * $int) > $o).
tff(d, axiom, ! [I: $int, L: $int, H: $int] : ( ( $lesseq(L, I) & $lesseq(I, H) ) => indom(I, L, H))).

%% 0 %%%%%%%%%%%%%%%%
tff(a, type, a: array).
%% Sortedness axiom
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, n)
%%        & indom(J, 1, n)
%%        & $less(I, J) )
%%     => $lesseq(read(a, I), read(a, J))))).

%% Add a hypothesis:
tff(m, type, m: $int).
tff(m, axiom, ( $lesseq(1, m) & $lesseq(m, $difference(n,1)) )).
tff(m, axiom, $less(read(a, m), read(a, $sum(m, 1)))).

%% 1 %%%%%%%%%%%%%%%%
%% FD(sa, {})
%% tff(eI, type, eI: $int).
%% tff(eJ, type, eJ: $int).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, 0)
%%        & indom(J, 1, 1)
%%        & $less(I, J) )
%%     => $lesseq(eI, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [I: $int] : ( indom(I, 1, 0) => read(a, I) = eI))).
%% tff(sa, axiom, ( 
%%    ! [J: $int] : ( indom(J, 1, 1) => read(a, J) = eJ))).


%% Makes FD(sa, ({}, {})) unsatisfiable. But wrt indom(I, 1, 1000) and indom(j, 1, 999) have satisfiability
%% Hence FD(sa, {(1000, {})})


%% (2) Get unsatisfiability for I = 1..2, J = 1..1000
%% tff(eI, type, eI: $int).
%% tff(eI, type, eJ: $int).
%% tff(eJ, type, eJ1: $int).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, 2) 
%%        & indom(J, 1, 1000) & J != 1
%%        & $less(I, J) )
%%     => $lesseq(eI, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [I: $int] : ( indom(I, 1, 2) => read(a, I) = eI))).
%% tff(sa, axiom, ( 
%%    ! [J: $int] : ( ( indom(J, 1, 1000) & J != 1 ) => read(a, J) = eJ))).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, 2) 
%%        & $less(I, 1) )
%%     => $lesseq(eI, eJ1)))).
%% tff(sa, axiom, read(a, 1) = eJ1).


%% (3) FD(I=2, J=1)
tff(eI, type, eI: $int).
tff(eI, type, eI2: $int).
tff(eI, type, eJ: $int).
tff(eJ, type, eJ1: $int).
tff(sa, axiom, ( 
   ! [I: $int, J: $int] : 
     ( ( indom(I, 1, 1000) & I != 2
       & indom(J, 1, 1000) & J != 1
       & $less(I, J) )
    => $lesseq(eI, eJ)))).
%% J=1
tff(sa, axiom, ( 
   ! [I: $int, J: $int] : 
     ( ( indom(I, 1, 1000) & I != 2
       & $less(I, 1) )
    => $lesseq(eI, eJ1)))).
%% I=2
tff(sa, axiom, ( 
   ! [I: $int, J: $int] : 
     ( ( indom(J, 1, 1000) & J != 1
       & $less(2, J) )
    => $lesseq(eI2, eJ)))).
%% I=2, J=1
tff(sa, axiom, ( 
   ! [I: $int, J: $int] : 
     ( ( $less(2, 1) )
    => $lesseq(eI2, eJ1)))).

tff(sa, axiom, ( 
   ! [I: $int] : ( ( indom(I, 1, 1000 ) & I != 2 ) => read(a, I) = eI))).
tff(sa, axiom, ( 
   ! [J: $int] : ( ( indom(J, 1, 1000) & J != 1 ) => read(a, J) = eJ))).
tff(sa, axiom, read(a, 1) = eJ1).
tff(sa, axiom, read(a, 2) = eI2).




%% 2 %%%%%%%%%%%%%%%%

%% tff(eI, type, eI: $int).
%% tff(eI, type, eI0: $int).
%% tff(eJ, type, eJ: $int).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, 1000) & I != 1000
%%        & indom(J, 1, 1000)
%%        & $less(I, J) )
%%     => $lesseq(eI, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [J: $int] : 
%%      ( ( indom(J, 1, 1000)
%%        & $less(1000, J) )
%%     => $lesseq(eI0, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [I: $int] : 
%%      ( ( indom(I, 1000, 0) 
%%        & I != 1000 )
%%     => read(a, I) = eI))).
%% tff(sa, axiom, ( read(a, 1000) = eI0)).
%% tff(sa, axiom, ( ! [J: $int] : ( indom(J, 1, 1000) => read(a, J) = eJ))). 
%% Change J's range to [0..999] to get satisfiability

%% Get again unsatisfiability even for all I in 1..0, J = 1000, but satisfiability for J in 1..999. 
%% Hence FD(sa, {(1000, 1000)})

%% 3 %%%%%%%%%%%%%%%%

%% tff(eI, type, eI: $int).
%% tff(eI, type, eI0: $int).
%% tff(eJ, type, eJ: $int).
%% tff(eJ, type, eJ0: $int).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, n) & I != 1000
%%        & indom(J, 1, n) & J != 1000
%%        & $less(I, J) )
%%     => $lesseq(eI, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [J: $int] : 
%%      ( ( indom(J, 1, 1000)
%%        & $less(1000, J) )
%%     => $lesseq(eI0, eJ)))).
%% tff(sa, axiom, ( 
%%    ! [I: $int, J: $int] : 
%%      ( ( indom(I, 1, n) & I != 1000
%%        & $less(I, 1000) )
%%     => $lesseq(eI, eJ0)))).
%% tff(sa, axiom, ( $less(1000, 1000) => $lesseq(eI0, eJ0))).

%% tff(sa, axiom, ( 
%%    ! [I: $int] : 
%%      ( ( indom(I, 1, n) 
%%        & I != 1000 )
%%     => read(a, I) = eI))).
%% tff(sa, axiom, ( 
%%    ! [J: $int] : 
%%      ( ( indom(J, 1, n) 
%%        & J != 1000 )
%%     => read(a, J) = eJ))).
%% tff(sa, axiom, ( read(a, 1000) = eI0)).
%% tff(sa, axiom, ( read(a, 1000) = eJ0)).

     
