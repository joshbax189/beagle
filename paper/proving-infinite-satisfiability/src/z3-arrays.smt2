(set-option :pull-nested-quantifiers true)
(set-option :mbqi true)
(set-option :macro-finder true)

;The definitions and conjectures necessary for tests in LPAR paper
;Z3 version

; Extensional array theory is built in-
; keywords select: A I, store A I E

;init is built in too- use
; (assert (= all1 ((as const (Array Int Int)) 1)))
;to declare all1 as a constant array.

;abbreviation of array sort
(define-sort IArray () (Array Int Int))

;Max definition (note max is a built-in)
(declare-fun maxA (IArray Int) Int)
(assert (forall ((a IArray)(n Int)(w Int))
		(implies (and (forall ((i Int))
				      (implies (and (> n i) (>= i 0)) 
					       (<= (select a i) w) ))
			      (exists ((i Int))
				      (and (> n i)
					   (>= i 0) 
					   (= (select a i) w))))
			 (= (maxA a n) w))
))
  
;max functional test
(echo "maxA is functional:")
(push)
(assert (not (forall ((a IArray)(n Int)(w1 Int)(w2 Int))
		(implies (and (and (forall ((i Int))
					   (implies (and (> n i) (>= i 0)) 
						    (<= (select a i) w1) ))
				   (exists ((i Int))
					   (and (> n i)
						(>= i 0) 
						(= (select a i) w1))))
			      (and (forall ((i Int))
					   (implies (and (> n i) (>= i 0)) 
						    (<= (select a i) w2) ))
				   (exists ((i Int))
					   (and (> n i)
						(>= i 0) 
						(= (select a i) w2)))))
			 (= w1 w2))
)))
(check-sat)
(pop)

;Sorted definition
(define-fun sorted ((a IArray)(n Int)) Bool
  (forall ((i Int)(j Int))
	  (implies (and (> n j) 
			(> j i)
			(> i 0)) 
		   (>= (select a j) (select a i))))
)

;inRange definition
(define-fun inRange ((a IArray)(r Int)(n Int)) Bool
  (forall ((i Int))
	  (implies (and (> n i) 
			(>= i 0))
		   (and (>= r (select a i))
			(>= (select a i) 0))))
)

;distinct definition
(define-fun distinctA ((a IArray)(n Int)) Bool
  (forall ((i Int)(j Int))
	  (implies (implies (and (> n i)
				 (> n j) 
				 (> j 0) 
				 (> i 0))
			    (= (select a i) (select a j)))
		   (= i j)))	  
)

;reverse definition
(declare-fun rev (IArray Int) IArray)

(assert (forall ((a IArray)(b IArray)(n Int))
		(implies (forall ((i Int))
				 (or (and (> n i)
					  (> i 0)
					  (= (select b i) (select a (- n (+ i 1)))))
				     (and (or (> 0 i) 
					      (>= i n)) 
					  (= (select b i) (select a i)))))
			 (= (rev a n) b))
))

;rev functional test
(echo "rev is functional:")
(push)

(assert (not (forall ((a IArray)(n Int)(b1 IArray)(b2 IArray))
		(implies (and (forall ((i Int))
				 (or (and (> n i)
					  (> i 0)
					  (= (select b1 i) (select a (- n (+ i 1)))))
				     (and (or (> 0 i) 
					      (>= i n)) 
					  (= (select b1 i) (select a i)))))
			      (forall ((i Int))
				 (or (and (> n i)
					  (> i 0)
					  (= (select b2 i) (select a (- n (+ i 1)))))
				     (and (or (> 0 i) 
					      (>= i n)) 
					  (= (select b2 i) (select a i))))))
			 (= b1 b2))
)))

(check-sat)
(pop)

;; %%EXAMPLES:

;; PROBLEM 1  
;; (~![A: array, N: $int]: ( $greatereq(N,0) => inRange(A,max(A,N),N) ))

(push)
(echo "Problem 1----------------------------------")
 
(assert (forall ((a IArray)(n Int))
		     (implies (>= n 0) 
			   (inRange a (maxA a n) n))
))

(check-sat)
(pop)

;; PROBLEM 2   
;; ~( ![N: $int, I: $int]: distinct(init(N),I) )

(push)
(echo "Problem 2----------------------------------")

(assert (forall ((n Int)(i Int))
		     (distinctA ((as const IArray) n) i) ))
(check-sat)
(pop)

;; PROBLEM 3  
;; ~(![A: array, N: $int]: read(rev(A,$sum(N,1)),0)=read(A,N))

(push)
(echo "Problem 3----------------------------------")

(assert (forall ((a IArray)(n Int))
	     (= (select (rev a (+ n 1)) 0) (select a n))))

(check-sat)
(pop)

;; PROBLEM 4
;; ~![A: array, N: $int]: (sorted(A,N) => ~sorted(rev(A,N),N))

(push)
(echo "Problem 4----------------------------------")

(assert (forall ((a IArray)(n Int))
		     (implies (sorted a n)
			      (not (sorted (rev a n) n))))
)

(check-sat)
(pop)

;; PROBLEM 5
;; ~![M: $int]: (?[N: $int]: ~sorted(rev(init(N),M),M))

(push)
(echo "Problem 5----------------------------------")

(assert (forall ((m Int))
		     (exists ((n Int))
			     (not (sorted (rev ((as const IArray) n) m) m)))
		     ))

(check-sat)
(pop)

;; PROBLEM 6
;; ~![A: array, N: $int]: ( (sorted(A,N) & $greater(N,0)) => distinct(A,N))

(push)
(echo "Problem 6----------------------------------")

(assert (forall ((a IArray)(n Int))
		     (implies (and (sorted a n) (> n 0))
			      (distinctA a n))))

(check-sat)