import os,re

#produces a list of file names

def filterFunc(s):
    return ( re.match('^[A-Z]+-summary$',s)!=None )

summaryFiles = filter(filterFunc,os.listdir('.'))

#f is a file, assumed to be at the start
def skipComment(f):
    #first is ----
    f.readline()
    #then readline until ----- again
    while re.match('^-+$',f.readline())==None:
        continue
    #next is headers
    f.readline()

results=[]

#for each *-summary file
for name in summaryFiles:
    print 'Reading ',name
    f = open(name,'r')

    #process into tuples (name,rating,expect,result,usedStrat)
    skipComment(f)
    #add the remaining as tuples
    for li in f:
        #columns are: name, rating, expect, result, ... strat is 4th from end...
        #unless error or memory...
        fields=re.match('^([^\s]+)\s+([0-9\.]+)\s+([_a-z]+)\s+([_a-z]+)\s+.*\s([0-9])\s+[0-9]+\s+[0-9]+\s+[0-9]+$',li)
        if fields==None:
            fields=re.match('^([^\s]+)\s+([0-9\.]+)\s+([_a-z]+)\s+([_a-z]+)\s+.*',li)
            results.append((fields.group(1),fields.group(2),fields.group(3),fields.group(4),False))
            if fields==None:
                print "Error- this line did not match"
                print li
        else:
            results.append((fields.group(1),fields.group(2),fields.group(3),fields.group(4),fields.group(5)!='1'))

    f.close()

#count the number of correct solutions
def countCorrect(records):
    i=0
    for r in records:
        if r[2]==r[3]:
            i+=1
    return i

print ''
print '# Problems:',len(results)
print '# Correct Solutions', countCorrect(results)

#print the memberships of each solution category:
def printSolnTypes():
    #put into a map
    #result is 3
    catMap={}
    for r in results:
        if not(catMap.has_key(r[3])):
            catMap[r[3]]=[]
        catMap[r[3]].append(r)

    for k in catMap.keys():
        print k, "-",len(catMap[k])

    print '\nThe following had ERROR status'
    for r in catMap['error']:
        print r

print '\nSolutions by type:\n-------------'
printSolnTypes()

#print # solved per problem category
def printSolvedByCat():
    catMap={}
    for r in results:
        cat=r[0][0:3]
        if not(catMap.has_key(cat)):
            catMap[cat]=[]
        catMap[cat].append(r)

    for k in catMap.keys():
        total=len(catMap[k])
        correct=countCorrect(catMap[k])
        print k,':',correct,'/',total

    print 'The following DAT problems were not solved'
    for k in catMap['DAT']:
        if k[2]!=k[3]:
            print k
    print 'The following ARI problems were not solved'    
    for k in catMap['ARI']:
        if k[2]!=k[3]:
            print k

print '\nSolutions by TPTP category:\n-------------'
printSolvedByCat()

#print the # correct by difficulty in 0.1 buckets
def printSolvedByDiff():
    catMap={}
    for r in results:
        rating=int(float(r[1])*10)
        if not(catMap.has_key(rating)):
            catMap[rating]=[]
        catMap[rating].append(r)

    for k in catMap.keys():
        total=len(catMap[k])
        correct=countCorrect(catMap[k])
        print float(k)/10,':',correct,'/',total

    print 'The following <0.1 rated problems were missed'
    for p in catMap[0]:
        if p[2]!=p[3]: 
            print p


print '\nSolutions by difficulty rating (range [n,n+0.09]):\n-------------'
printSolvedByDiff()
        
#index by name to correlate with INT/RAT/REAL
def nameIdx():
    nameIdx={}
    for r in results:
        name=r[0].split('/')[1] #strip dir name
        if not(nameIdx.has_key(name)):
            nameIdx[name]=[]
        nameIdx[name].append(r)
    #read lines of int_probs.list
    #read lines of rat_probs.list
    #read lines of real_probs.list
    return nameIdx

print '\nSolutions invoking multiple strategies:'
usedMult=0
successStrat=0
for r in results:
    if (r[4]):
        usedMult+=1
        if r[2]==r[3]:
            successStrat+=1

print 'Total:',usedMult,'\nSuccessful:',successStrat
