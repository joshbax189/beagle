\documentclass[10pt, notitlepage]{article}
\usepackage{amsmath, amsfonts,amsthm}
\usepackage{listings}
\lstset{ escapeinside={\%*}{*)} }

\newcommand{\nats}{\mathbb{N}}
\newcommand{\ints}{\mathbb{Z}}

\newcommand{\define}[1]{\textit{#1}}

\newtheorem{defn}{Definition}[section]
\newtheorem{lemma}{Lemma}[section]

\newcommand{\algK}{\mathcal{K}}

%% Inference rules
\newcommand{\IR}[1]{\text{\normalfont\sffamily\small#1}} %% "I"nference "R"ule
\def\void{}
\newcommand{\infrule}[3][\void]{%
  {\renewcommand\arraystretch{1.25}
    \ifx\void#1\else#1\hspace{0.5em}\fi
    \begin{array}[c]{@{\hspace*{1em}}c@{\hspace*{1em}}}#2\\\hline #3
    \end{array}}}
\newcommand{\abstr}{\mathrm{abstr}}
\newcommand{\sort}{\mathrm{sort}}
\newcommand{\BAlgs}{\mathcal{B}}
\newcommand{\back}{\mathrm{B}}
\newcommand{\eq}{\approx}

\title{HSP for Bounded Domains}
\begin{document}
\maketitle
\section{Abstract/Motivation}
%Add broad points of motivation
One angle is to re-use the results for an existing calculus to derive a new decision procedure- in the manner of [NRRBS] and in so doing investigate different approaches to combining theories hierarchic superposition instead of Nelson-Oppen.

The approach will augment the superposition calculus with a simplification procedure based on constraints. The simplification procedures are stronger than those allowed by previous approaches to constrained superposition, mainly because of the specialisation to finite domains.

\section{Basic Definitions}
The key assumption here is the existence of a finite number of not necessarily distinct constants which name domain members. I will call these
\define{domain constants} to distinguish them from `elements' and the implication that they are distinct. 
%Alternatively we could use `BG constants' as these constants are the only allowed BG terms in terms of an HSP style development.

Here I will assume a single sorted signature for simplicity and a BG theory axiomatized simply by 
\begin{equation}\label{ccc}
\forall x. ~x \approx d_0 \vee\ldots \vee x \approx d_n
\end{equation}
In this case $d_i$ are the domain constants. In general we refer to domain constants either by $a, b, c, \ldots$ with the implicit precedence $a > b > c > ...$ or, where more accurate numberings are needed: $d_0 > d_ 1 > \ldots$ so that there are $j-i-1$ constants between $d_i$ and $d_j$ in the precedence order, where $j > i$.

No restriction is made to enforce a Bernays-Sch\"onfinkel signature; functions and non-domain constants are allowed.
Similarly to the approach in the HSP paper, the cardinality constraint clause (\ref{ccc}) is replaced by clauses
\begin{equation}
f(x) \approx d_0 \vee \ldots \vee f(x) \approx d_n
\end{equation}
for each operator in the signature in order to guarantee sufficient completeness.

%Instantiating substitution
An \define{instantiating substitution} (called a numbering substitution in HW) is a substitution whose range is a subset of domain constants, i.e. no variable renamings are permitted.
Such a substitution $\tau$ is \emph{more general} than another instantiating substitution $\tau'$ if the range of $\tau$ is a proper subset of the range of $\tau'$. Hence a most general instantiating substitution is often called a \define{minimal instantiating substitution} (i.e. referring to the set intersection of all such instantiating substitutions).
The range of a \define{simple} substitution only includes variables or domain constants.

\section{Constraints}
The function of constraints in this application is to guard against performing unnecessary first-order inferences. This is in contrast with the typical application of constraints in first-order calculi, which is to add theory reasoning (equivalently, to abstract T-unification). The difference is that we will usually test the satisfiability of constraints immediately after an inference rather than collecting the constraint until an empty clause is obtained. So we will focus on efficiently finding normal forms and require the expansion and simplification of constraint formulas when generated in order to simplify this satisfiability test.\\

A \define{constraint} $K$ is a conjunction of literals of the form $(s < t)$ or $(s=t)$ where $s$ and $t$ may be either domain constants or variables. Parentheses distinguish constraint literals from clause literals.
Since constraints are only over domain constants, $>$ is just the precedence relation on domain constant symbols.
$K$ is identified with the set of instantiating substitutions $\tau$ all whose domains are $var(K)$ and for which $K \tau = \top$.
%TODO- really?
Then a clause with its constraint $C:K$ represents the set of (not necessarily ground) instances $\{ C\tau : \tau \in K\}$.

\subsection{Algorithm $\algK$}
Define an algorithm $\algK_\prec(s,t)$ which given terms $s,t$ returns a constraint $K$ such that for all $\sigma \in K$ we have $s\sigma \prec t\sigma$. Effectively this just applies the definition of the LPO on terms. Similarly, for $K=\algK_\approx(s,t)$ all $\sigma \in K$ satisfy $s\sigma \approx t\sigma$.
[Note that this is effectively an instantiating unifier of the two terms. Since equality constraints are immediately simplified by applying an equivalent substitution it might be simpler to just compute a unifier here and apply that].
%TODO- this ^^
The algorithm $\algK$ returns a constraint formula built from constraint literals combined with $\land$, $\lor$ and these formulas are assumed to be in DNF. 
Each conjunctive clause over constraint literals becomes a separate constraint for a fresh copy of the appropriate clause and this may be simplified by using all equalities in the constraint as substitutions.

\subsection{Constraint semantics}
%TODO- the idea behind representing constraints as sets of substitutions is to give some way of finding minimal substitutions. If these are not needed then the above definition K\sigma=T etc. is sufficient.
First we define the semantics of the constraint literals in the $\algK$ algorithms.
Assume the set of domain constants is $D$, $x,y$ are variables and $c,d$ are domain constants.
\begin{align*}
\top &:= \{\epsilon\} \text{ where $\epsilon$ is the identity substitution}\\
\bot &:= \emptyset \\
(x \prec c) &:= \{[x \mapsto d] : d \in D \land d < c\}\\
(c \prec x) &:= \{[x \mapsto d] : d \in D \land c < d\}\\
(x \prec y) &:= \{[x \mapsto c, y \mapsto d]: c,d \in D \land c < d\}\\
(x \approx c) &:= \{[x \mapsto c]\}\\
(x \approx y) &:= \{[x \mapsto c, y \mapsto c] : c \in D\}
\end{align*}

\begin{defn}[Satisfaction]
Given an instantiating substitution $\sigma$, then $\sigma$ satisfies:
\begin{enumerate}
\item a constraint literal $L$ iff $\sigma = \tau \mu$ for $\tau \in L$.
\item a constraint clause $K = L_0 \land \ldots \land L_n$ ($L_i$ are literals)  if there are $\tau_i \in L_i$ for each $i$ such that $\sigma = \tau_0\ldots \tau_n \mu$ for some instantiating $\mu$.
\item a constraint $K = C_0 \lor \ldots \lor C_n$  if $\sigma$ satisfies at least one $C_i$.
\end{enumerate}
\end{defn}

If $\sigma$ satisfies $K$ then we write $\sigma \in K^*$. By this definition, for all instantiating $\sigma$, $\sigma \in \top^*$ but for any constraint $K$, $\epsilon \notin K^*$ unless $K = \top$ (where $\epsilon$ is the identity substitution). 

\begin{defn}[Constraint membership]
For an instantiating substitution $\tau$ and constraint $K$, we say that $\tau \in K$ iff
\begin{enumerate}
\item $\tau \in K^*$  and,
\item for all variables $x$, $x\tau=x$ iff no $[x \mapsto c] \in K^*$.
\end{enumerate}
\end{defn}
As a consequence of the above definition it is useful to refer to the \define{domain} of a constraint as the domain of any one of the substitutions present in it, i.e. $dom(K) = dom(\sigma)$ for any $\sigma \in K$.

The distinction between satisfying a constraint and being a member is that member instantiations are usually minimal (most general) with respect to some property. Equally, every satisfying instantiation extends some minimal instantiation (member) of $K$.

Note that for constraint literals constraint membership corresponds to set membership. Based on this we will define the semantics for the constraint combinators.


\section{Calculus}

\subsection{Termination of the ground calculus}

I assume a standard superposition calculus (the one presented in the paramodulation chapter of HAR-01) and aim to prove that under the assumption of a finite model (even allowing an infinite Herbrand universe) that the ground superposition proof is terminating.
~\\
The calculus $\mathcal{G}$ will consist of the rules:

\begin{equation*}
\infrule{l \approx r \lor C \qquad s[l]\not\approx t \lor D}{s[r]\not\approx t \lor C \lor D}{ \IR{Neg-Sup}}
\end{equation*}
where
\begin{itemize}
\item $l \succ r$ and $s \succ t$,
\item $(l \approx r)$ is strictly greatest in $C$, 
\item $(s\not\approx t)\sigma$ is selected.
\end{itemize}

\begin{equation*}
\infrule{l \approx r \lor C \qquad s[l]\approx t \lor D}{s[r]\approx t \lor C \lor D}{\IR{Pos-Sup}}
\end{equation*}
as for $\IR{Neg-Sup}$ except that $s \approx t$ must be strictly maximal.

\begin{equation*}
\infrule{C \vee t \not\approx t}{C}{\IR{Eq-Res}}
\end{equation*}

where:
\begin{itemize}
\item $t \not\approx t$ is selected
\end{itemize}

\begin{equation*}
\infrule{C \vee s \approx t \vee s \approx r}{ C \lor t\not \approx r \lor s \approx r}{\IR{Fact}}
\end{equation*}
where:
\begin{itemize}
\item $s \succ t$
\item $s \approx t$ is greatest in $(C \lor l \approx r)$
\end{itemize}


For a clause set $S$ and clause $C$, let $S^{\prec C} = \{D \in S: D \prec C\}$; a clause $C$ is \define{redundant} wrt. $S$ iff $S^{\prec C} \models C$ and an inference is redundant if its result is redundant wrt. clauses smaller than the main premise of the inference (or a premise is redundant).

\begin{defn}
A derivation is a sequence of clause sets $S_0, S_1, \ldots $ such that each clause set $S_{i+1}$ is either $S_i \cup \{C\}$ where $S_i \models C$ and the inference generating $C$ is not redundant; or $S_i -\{ C \}$ where $C$ is redundant with respect to $S_i$.
\end{defn}


The idea behind the proof is that we can always construct a finite rewrite system for a finite (equational) model and hence an infinite derivation must eventually be redundant.

\begin{lemma}
Every fair derivation starting from a sufficiently complete set of ground clauses which encode a finite domain assumption is finite.
\end{lemma}

\begin{proof}
Assume that we have an infinite derivation $S_0, S_1 \ldots$ that satisfies the above conditions. 
The \define{persisting clauses} $S_{\infty} = \cup_i \cap_{k > i} S_k$ of the derivation must number infinitely many otherwise the derivation contains redundant inferences a priori.
By completeness, if $\square \notin S_\infty$ then we can build a model for the clause set. 
Since we know the model domain has a fixed upper bound to its size we can give a rewrite system which also models $S_\infty$ but has only finitely many rules (it simply assigns a domain constant for each operator $f$ and argument tuple of domain constants of appropriate arity). 
Denote by $\mu$ the reduced rewrite system formed by inter-reducing the rules of this system.

Now $R_S$ is the rewrite system generated from $S_\infty$ by applying the model generation procedure. We claim that $\mu \subseteq R_S$. 
Take $l \to r \in \mu$. 
By soundness and convergence of $R_S$ it follows that $l \to r \in R_S^*$. 
If $l$ can be rewritten by some rule $s \to t \in R_S$ then $l$ can also be rewritten by $\mu^*$ since this must contain $s \to t$. This contradicts that $\mu$ is reduced and so the rule $l \to r$ is in $R_S$.
Since $\mu$ is already a model for $S_\infty$, we have that $R_S = \mu$ by the conditions of the model generation procedure.

Then, only finitely many clauses of $S_\infty$ produce rewrite rules and so there are persisting redundant clauses. This contradicts the fairness of the derivation, as some simplifications are postponed infinitely. Therefore there are no infinite derivations.
\end{proof}

\subsection{First Order Calculus}
The problem in lifting to the first-order case is that there are simplifications which may never be discovered, e.g. 
\begin{equation*}
\infrule{\underline{p(x,z)} \lor \neg p(x,y) \lor \neg p(y,z) \qquad \underline{\neg p(u,v)} \lor p(u,w) \lor \neg p(v,w)}{p(u,w) \lor \neg p(v,w) \lor \neg p(u,y) \lor \neg p(y, v)}{}
\end{equation*}
This is a valid instance of negative superposition under the usual ordering restrictions. However, in the presence of an axiom $\forall x.~x \approx d_0 \lor x \approx d_1$, all ground inferences from the premises above have at least one trivial premise. This is not detected by the superposition calculus.

We add the full set of ordering restrictions from the ground calculus as constraints so that an inference is only non-redundant if its conclusion has a satisfiable constraint. Then the problem of simplification is reduced to finding unsatisfiable/trivial constraints.


\begin{equation*}
\infrule{l \approx r \lor C : K_1 \qquad s[l']\not\approx t \lor D : K_2}{(s[r]\not\approx t \lor C \lor D)\sigma : K'}{ \IR{Neg-Sup}}
\end{equation*}
where
\begin{itemize}
\item $l' \not\in Var$,
\item $\sigma$ is the simple mgu of $l$ and $l'$,
\item $(s\not\approx t)$ is selected.
\end{itemize}
$K' =$
\begin{itemize}
\item $(K_1 \land K_2)\sigma$
\item $\algK_\succ(l\sigma, r\sigma)\land \algK_\succ(s\sigma, t\sigma)$
\item $(l \approx r)\sigma$ is strictly maximal in $C\sigma$, 
\end{itemize}

\begin{equation*}
\infrule{l \approx r \lor C : K_1 \qquad s[l']\approx t \lor D : K_2}{(s[r]\approx t \lor C \lor D)\sigma : K'}{\IR{Pos-Sup}}
\end{equation*}
as for $\IR{Neg-Sup}$ except that $(s \approx t)\sigma$ must be strictly maximal.

\begin{equation*}
\infrule{C \vee t \not\approx t' : K}{C\sigma : K\sigma}{\IR{Eq-Res}}
\end{equation*}

where:
\begin{itemize}
\item $\sigma$ is the simple mgu of $t$ and $t'$
\item $t \not\approx t'$ is selected
\end{itemize}

\begin{equation*}
\infrule{C \vee s \approx t \vee l \approx r : K}{ (C \lor t\not \approx r \lor l \approx r)\sigma : K'}{\IR{Fact}}
\end{equation*}
where:
\begin{itemize}
\item $\sigma$ is the simple mgu of $s$ and $l$.
\end{itemize}
$K' =$
\begin{itemize}
\item $K\sigma$
\item $\algK_\succ(s\sigma, t\sigma)$
\item $(s \approx t)\sigma$ is greatest in $(C \lor l \approx r)\sigma$
\end{itemize}

\begin{equation*}
\infrule{c \approx d \lor C: K}{ (c \approx d)\tau : K\tau \qquad \{C\tau,(c\not\approx d)\tau\}}{\IR{BG-Split}}
\end{equation*}
where:
\begin{itemize}
\item $c,d$ are domain constants or variables.
\item $\tau \in K^*$ such that $(c\approx d)\tau$ and $C\tau$ are variable disjoint.
\item $(c \approx d)\tau$ is strictly maximal in $C\tau$.
\end{itemize}
%TODO add maximality constraints here ^^ ?

\subsection{Completeness considerations}
Including constraints in the first-order calculus affects completeness since the usual lifting arguments assume all instances of the clause exists. Specifically, in superposition inferences the constraint on the main premise may exclude ground instances which can be obtained by ground superposition on ground instances which satisfy the constraint. This superposition occurs at positions below variables in the first-order clause. A concrete example:
\begin{equation*}
\infrule{c \approx d \qquad s[x]\not\approx t \vee C: (x > d)}{(s[d]\not\approx t \vee C)\sigma}{}
\end{equation*}
Where we assume $c > d$ and $\sigma = [x \mapsto c]$. Clearly the result is derivable in the ground calculus but, due to the constraint, it is not among the simple ground instances of the main premise. Because of fairness we cannot simply ignore such an inference- it may never be carried out, even using ground unit demodulation since the clause $s[c]\not\approx t \vee C$ may not be derived either.
%TODO- formalise
Due to the restriction to simple substitutions this case can only happen when the selected literal in the left premise is an equation between domain constants or variables. The remainder of left premise must also consist of just variables or domain constants since the selected literal must be maximal.
Such clauses can be simplified by a split rule, instantiating where necessary to remove shared variables. Then we can assume that only positive unit clauses can participate as the left premise in inferences like the one above.

To avoid having to instantiate below variables we can gather all generated positive unit clauses (or their ground instances if needed) and first rewrite the current clause set (and their constraints) with these equations to eliminate any occurrences of the left hand term. Then all future constraints in the current branch must be evaluated wrt. to the rewrite system defined by these rules. For example, if we have derived $b \approx d$ then the constraint $(a < x) \land (x<c)$ should become unsatisfiable. This can be achieved by evaluating inequality constrains using the normal forms of terms under the current domain rewriting system.

\section{Constraint Simplification}

There are two aspects of constraint simplification. One is to discover constraints which are unsatisfiable and therefore eliminate the associated clause and the other is to simplify the syntactic representation of the constraint to make it easier to test for unsatisfiability in future.

\begin{lemma}
If no cycles and no chains which are longer than the distance between endpoints then the constraint is satisfiable.
\end{lemma}
\begin{proof}
Assume a contradiction. Take $x$ to be an unassigned variable which has no unassigned variables smaller than it. It is possible to find such a variable since there are no cycles. Consider the least upper bound of the domain constants directly smaller than $x$.
\end{proof}


\section{Applications}
\section{Related Work}
\end{document}