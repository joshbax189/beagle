BEAGLE - a theorem prover for hierarchic superposition
======================================================

Beagle is an automated theorem prover for first-order logic with equality over
linear integer/rational/real arithmetic. It accepts formulas in the [FOF, TFF, and
TFF-INT formats of the TPTP syntax](http://www.cs.miami.edu/~tptp/TPTP/TR/TPTPTR.shtml). 
It also accepts [SMT-LIB](http://www.smtlib.org/) version 2 input files. See the 
[SMTtoTPTP tool](https://bitbucket.org/peba123/smttotptp) for more details on this.

This is an experimental fork of the main Beagle distribution which is available at
https://bitbucket.org/peba123/beagle .

Installation
============

Beagle can be built with sbt, the Scala Build Tool.
The command `sbt compile` compiles the sources into Scala class files.
Assuming a Scala 2.11 runtime environment is installed, the shell script `beagle` can be used: Run

    /BEAGLE_ROOT/beagle -help

to get some basic usage information.

Background reasoning via SMT solvers
----------------------------------------

Beagle depends on an external SMT solver for the background theories of (linear)
real and rational arithmetics. For linear integer arithmetic no external SMT
solver is required, but one can be used instead of the built-in solver.
The SMT solver must be installed separately, it is not included in this
distribution.

The SMT solver must comply with the [SMT-LIB](http://www.smtlib.org/)
standard, version 2. Its invocation is specified by the Beagle flag
`-smtsolver` flag, e.g.,

	beagle -smtsolver /usr/local/bin/cvc4 FILE

Beagle calls the SMT solver via an SMT-LIB file and receives its result from
standard output. The SMT solver must accept its input file name as its last
argument. Beagle tests if the SMT solver supports the computation of
unsatisfiable cores and, if so, makes use of them.

Suitable SMT solvers are, e.g., [CVC4](http://cvc4.cs.nyu.edu/web/) and
[Z3](https://github.com/Z3Prover/z3). 

Notice that when specified, the SMT solver is also used for linear integer
arithmetic. This behaviour can be overridden by the `-liasolver` flag and
specifying either `iQE` or `cooper`. (I recommend `iQE` over `cooper`.)

Example:

	beagle -smtsolver /usr/local/bin/cvc4 -liasolver iQE FILE


Running out of memory
---------------------

Occasionally it is helpful to enable higher ressource limits.
If you are encountering, e.g., heap problems, try adding the option

	-Xms512M -Xmx4G -Xss10M -XX:MaxPermSize=384M

to the java call.

Differences to Main Distribution
================================
The major difference to the main distribution is the implementation of the
algorithm described in the paper 'Finite Quantification in Hierarchic Theorem Proving'.
This is enabled by the flag `-fin`.
The algorithm requires input clauses to be finitely quantified, this is done using the predicate
`$fin(X,0,10)` which translates to the formula `0 <= X & X < 10`.

A later version of this algorithm is available in the /gmf branch and is invoked using the `-gmf`
flag.
The input syntax for describing FQ-clauses is the same as above.

In addition there is a version of Cooper's algorithm for Quantifier Elimination which stores bindings
to parameters between calls.
This function is in the /dev branch only.

Documentation
=============
Documentation beyond `beagle -help` is not yet available.

Precedences
-----------
Beagle, a superposition prover, works internally with term orderings (LPO if theories are present, KBO otherwise). 
The precedences among operators for LPO can be specified in the input TPTP file as follows:

    %$ :prec f > g > h
    %$ :prec f > k > l

Beagle will linearize the stated precedences into a total ordering on operator symbols.

The corresponding syntax in SMT-LIB input files is as follows:

    (set-option :prec ("f > g > h" "f > k > l"))

Publications
============

- [Peter Baumgartner, Joshua Bax, Uwe Waldmann. Beagle - A Hierarchic Superposition Theorem Prover](http://www.nicta.com.au/pub?id=8726).
This is the main paper on Beagle

- [Peter Baumgartner, Uwe Waldmann. Hierarchic superposition with weak
  abstraction](http://www.nicta.com.au/pub?id=6667)

- [Peter Baumgartner, Joshua Bax, Uwe Waldmann. Finite Quantification in
  Hierarchic Theorem Proving](http://www.nicta.com.au/pub?id=7842)

- [Peter Baumgartner, Joshua Bax. Proving Infinite
  Satisfiability](http://www.nicta.com.au/pub?id=7341)

- [Geoff Sutcliffe, Stephan Schulz, Koen Claessen, Peter Baumgartner. The TPTP
  Typed First-order Form and Arithmetic](http://www.nicta.com.au/pub?id=4724)

Contact
=======

Peter Baumgartner

http://users.cecs.anu.edu.au/~baumgart/

Email: Peter.Baumgartner@nicta.com.au

Usage reports, comments and suggestions are gratefully received.
